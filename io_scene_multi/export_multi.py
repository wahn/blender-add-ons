"""
This script is an exporter to several renderer's scene description files.
Currently:
ASS (Arnold Scene Source)
IGS (Indigo Scene Format - see http://indigorenderer.github.io)
LXS (Luxrender's scene description - similar to RIB)
MI  (mental ray's scene description)
MXS (Maxwell's scene description)
RAD (Radiance's scene description)
RIB (RenderMan compliant - for now PRMan and 3Delight)
"""

import os
import platform
# Blender
import bpy
import mathutils
# renderers
from .renderers.ass import AssExporter
from .renderers.igs import IgsExporter
from .renderers.lxs import LxsExporter
from .renderers.mxs import MxsExporter
from .renderers.rib import RibExporter
from .renderers.rad import RadExporter

###############################################################################

def get_global_worldspace(vec, obj):
    return obj.matrix_world * vec

class Options:
    def __init__(self,
                 lighting = 'no_lights',
                 textures = False,
                 cycles = False,
                 aov_N = False,
                 aov_P = False,
                 aov_Z = False,
                 aov_emission = False,
                 aov_direct_diffuse = False,
                 aov_direct_specular = False,
                 aov_reflection = False,
                 aov_refraction = False,
                 aov_indirect_diffuse = False,
                 aov_indirect_specular = False):
        self.lighting = lighting
        self.textures = textures
        self.cycles = cycles
        self.aov_N = aov_N
        self.aov_P = aov_P
        self.aov_Z = aov_Z
        self.aov_emission = aov_emission
        self.aov_direct_diffuse = aov_direct_diffuse
        self.aov_direct_specular = aov_direct_specular
        self.aov_reflection = aov_reflection
        self.aov_refraction = aov_refraction
        self.aov_indirect_diffuse = aov_indirect_diffuse
        self.aov_indirect_specular = aov_indirect_specular

###############################################################################

class MultiExporter:
    def __init__(self, options):
        # options
        self.options = options
        # known exporters
        if self.options.lighting == 'no_lights':
            self.exporters = [AssExporter(options),
                              # MiExporter(options),
                              RibExporter(options)
            ]
        elif self.options.lighting == 'direct_lighting':
            self.exporters = [AssExporter(options),
                              LxsExporter(options),
                              # MiExporter(options),
                              RibExporter(options)
            ]
        elif self.options.lighting == 'global_illumination':
            self.exporters = [AssExporter(options),
                              IgsExporter(options),
                              LxsExporter(options),
                              # MiExporter(options),
                              MxsExporter(options),
                              RadExporter(options),
                              RibExporter(options)
            ]
            if platform.system() == "Linux":
                self.exporters = [AssExporter(options),
                                  LxsExporter(options),
                                  # MiExporter(options),
                                  RadExporter(options),
                                  RibExporter(options)
                ]
        else:
            print("WARNING: unknown option for lighting: '%s'" %
                  self.options.lighting)
            # use 'no_lights' exporters
            self.exporters = [AssExporter(options),
                              # MiExporter(options),
                              RibExporter(options)
            ]
        # filename related
        self.mainDir = None
        self.mainName = None
        # scene related
        self.scene = None
        # camera related
        self.camObj = None
        self.camData = None
        # motion blur related
        self.mBlur = []

    def calculateObjectBoundingBox(self, obj):
        # first vertex
        vec = mathutils.Vector((obj.bound_box[0][0],
                                obj.bound_box[0][1],
                                obj.bound_box[0][2]))
        # in world space
        vec = get_global_worldspace(vec, obj)
        minX = maxX = vec[0]
        minY = maxY = vec[1]
        minZ = maxZ = vec[2]
        bb = obj.bound_box
        for i in list(range(8)):
            corner = bb[i]
            vec = mathutils.Vector((corner[0], corner[1], corner[2]))
            corner = get_global_worldspace(vec, obj)
            if corner[0] < minX:
                minX = corner[0]
            if corner[1] < minY:
                minY = corner[1]
            if corner[2] < minZ:
                minZ = corner[2]
            if corner[0] > maxX:
                maxX = corner[0]
            if corner[1] > maxY:
                maxY = corner[1]
            if corner[2] > maxZ:
                maxZ = corner[2]
        return (minX, maxX, minY, maxY, minZ, maxZ)

    def calculateSceneBoundingBox(self):
        initialized = False
        if not (len(self.scene.objects) > 0):
            return (0.0, 0.0, 0.0, 0.0, 0.0, 0.0)
        for objIndex in range(len(self.scene.objects)):
            if not initialized:
                # intialize with first object's bounding box
                obj = self.scene.objects[objIndex]
                if obj.type == 'CAMERA':
                    continue # skip
                else:
                    (minX,
                     maxX,
                     minY,
                     maxY,
                     minZ,
                     maxZ) = self.calculateObjectBoundingBox(obj)
                    initialized = True
            else:
                obj = self.scene.objects[objIndex]
                if obj.type == 'CAMERA':
                    continue # skip
                else:
                    (minX2,
                     maxX2,
                     minY2,
                     maxY2,
                     minZ2,
                     maxZ2) = self.calculateObjectBoundingBox(obj)
                if minX2 < minX:
                    minX = minX2
                if maxX2 > maxX:
                    maxX = maxX2
                if minY2 < minY:
                    minY = minY2
                if maxY2 > maxY:
                    maxY = maxY2
                if minZ2 < minZ:
                    minZ = minZ2
                if maxZ2 > maxZ:
                    maxZ = maxZ2
        return (minX, maxX, minY, maxY, minZ, maxZ)

    def createNewMaterial(self, obj):
        # create new material
        bpy.ops.material.new()
        mat = bpy.data.materials.new(obj.name + "_mat")
        print("WARNING: create new material %s" % (obj.name + "_mat"))
        # select object
        bpy.ops.object.select_all(action='DESELECT')
        selected = bpy.data.objects[obj.name]
        selected.select = True
        bpy.context.scene.objects.active = selected
        # create a slot
        bpy.ops.object.material_slot_add()
        # and add the material to the object (not the data)
        selected.material_slots[0].link = 'OBJECT'
        selected.material_slots[0].material = mat
        # use Lambert for diffuse and Phong for specular
        mat.diffuse_shader = 'LAMBERT'
        mat.specular_shader = 'PHONG'
        return mat

    def isAnimated(self, obj):
        if obj.animation_data != None:
            return True
        elif obj.parent != None:
            return self.isAnimated(obj.parent)
        return False

    def isPrimitive(self, obj):
        isPrimitive = False
        primType = None
        if (self.nameStartsWith(obj.data.name, "Cone")):
            isPrimitive = True
            primType = "Cone"
        elif (self.nameStartsWith(obj.data.name, "Cylinder")):
            isPrimitive = True
            primType = "Cylinder"
        elif (self.nameStartsWith(obj.data.name, "Disk")):
            isPrimitive = True
            primType = "Disk"
        elif (self.nameStartsWith(obj.data.name, "Ring")):
            isPrimitive = True
            primType = "Ring"
        elif (self.nameStartsWith(obj.data.name, "Sphere")):
            isPrimitive = True
            primType = "Sphere"
        return isPrimitive, primType

    def isVisible(self, obj):
        visible = False
        # layers
        for i in range(20):
            if obj.layers[i]:
                if self.scene.layers[i]:
                    visible = True
        # check hide_render flag
        if obj.hide_render:
            visible = False
        return visible

    def getInfoLamp(self, obj):
        info = []
        data = obj.data
        # color
        if data.use_nodes:
            # assume there is an emission node with a color value
            if data.node_tree.nodes['Emission'].inputs['Color'].is_linked:
                print("WARNING: nodes['Emission'].inputs['Color'].is_linked")
                info.append([data.color[0], data.color[1], data.color[2]])
            else:
                # not linked to another node
                color = data.node_tree.nodes['Emission'].inputs['Color']
                r = color.default_value[0]
                g = color.default_value[1]
                b = color.default_value[2]
                a = color.default_value[3]
                info.append([r, g, b])
        else:
            info.append([data.color[0], data.color[1], data.color[2]])
        # energy
        if data.use_nodes:
            # assume there is an emission node with a strength value
            if data.node_tree.nodes['Emission'].inputs['Strength'].is_linked:
                print("WARNING: nodes['Emission'].inputs['Strength'].is_linked")
                info.append(data.energy)
            else:
                # not linked to another node
                strength = data.node_tree.nodes['Emission'].inputs['Strength']
                info.append(strength.default_value)
        else:
            info.append(data.energy)
        # falloff
        falloff = []
        try:
            falloff.append(data.falloff_type)
        except AttributeError:
            pass
        else:
            falloff.append(data.distance)
            if (data.falloff_type == 'CONSTANT' or
                data.falloff_type == 'INVERSE_LINEAR' or
                data.falloff_type == 'INVERSE_SQUARE'):
                # no additional info
                pass
            elif data.falloff_type == 'CUSTOM_CURVE':
                print("WARNING: falloff_type '%s' is not supported" %
                      data.falloff_type)
            elif data.falloff_type == 'LINEAR_QUADRATIC_WEIGHTED':
                falloff.append(data.linear_attenuation)
                falloff.append(data.quadratic_attenuation)
        info.append(falloff)
        # use flags
        use = []
        # use_diffuse
        use.append(data.use_diffuse)
        # use_specular
        use.append(data.use_specular)
        # use flags
        info.append(use)
        if data.type == 'SPOT':
            # spot parameters
            spot = []
            # spot_size
            spot_size = data.spot_size
            spot.append(spot_size)
            # spot_blend
            spot_blend = data.spot_blend
            spot.append(spot)
            # spot parameters
            info.append(spot)
        return info

    def getInfoMesh(self, obj, mesh):
        info = []
        if mesh == None:
            data = obj.data
        else:
            # might be called from a exporter to convert on the fly
            data = mesh
        # gather info here
        materials = []
        vertices = []
        vertexNormals = []
        polygons = []
        polygonNormals = []
        smoothPolygons = []
        polygonMaterialIndices = []
        uvs = []
        # materials
        for i1 in range(len(data.materials)): # data
            material = data.materials[i1]
            if material != None:
                materials.append(material.name)
        if materials == []:
            for i1 in range(len(obj.material_slots)):
                material = obj.material_slots[i1]
                if material != None:
                    materials.append(material.name)
        info.append(materials)
        # vertices
        for i1 in range(len(data.vertices)):
            vertex = data.vertices[i1]
            vertices.append([vertex.co[0], vertex.co[1], vertex.co[2]])
            vertexNormals.append([vertex.normal[0],
                                  vertex.normal[1],
                                  vertex.normal[2]])
        info.append(vertices)
        info.append(vertexNormals)
        # trigger the tesselation
        data.update(calc_tessface = True)
        # polygons
        for i1 in range(len(data.tessfaces)):
            polygon = data.tessfaces[i1]
            vertexIndices = []
            for i2 in range(len(polygon.vertices)):
                vertexIndex = polygon.vertices[i2]
                vertexIndices.append(vertexIndex)
            polygons.append(vertexIndices)
            polygonNormals.append(polygon.normal)
            smoothPolygons.append(polygon.use_smooth)
            polygonMaterialIndices.append(polygon.material_index)
        info.append(polygons)
        info.append(polygonNormals)
        info.append(smoothPolygons)
        info.append(polygonMaterialIndices)
        # uv-coordinates
        hasUVs = False
        if (data.uv_textures.active and data.uv_textures.active.data):
            uv_layer = data.uv_layers.active.data
            if uv_layer:
                hasUVs = True
        if len(data.tessface_uv_textures) >= 1: # TODO: > 1
            uv_values = data.tessface_uv_textures[0].data.values()
            for i1 in range(len(uv_values)):
                uv_value = uv_values[i1]
                if len(uv_value.uv) == 3:
                    uv = []
                    uv.append([uv_value.uv1[0], uv_value.uv1[1]])
                    uv.append([uv_value.uv2[0], uv_value.uv2[1]])
                    uv.append([uv_value.uv3[0], uv_value.uv3[1]])
                    uvs.append(uv)
                elif len(uv_value.uv) == 4:
                    uv = []
                    uv.append([uv_value.uv1[0], uv_value.uv1[1]])
                    uv.append([uv_value.uv2[0], uv_value.uv2[1]])
                    uv.append([uv_value.uv3[0], uv_value.uv3[1]])
                    uv.append([uv_value.uv4[0], uv_value.uv4[1]])
                    uvs.append(uv)
                else:
                    print("WARNING: len(uv_value.uv) = %s" % len(uv_value.uv))
        info.append([hasUVs, uvs])
        return info

    def getInfoMeshOld(self, obj):
        info = []
        data = obj.data
        # gather info here
        materials = []
        vertices = []
        vertexNormals = []
        polygons = []
        polygonNormals = []
        smoothPolygons = []
        polygonMaterialIndices = []
        uvs = []
        # materials
        for i1 in range(len(data.materials)):
            material = data.materials[i1]
            if material != None:
                materials.append(material.name)
        info.append(materials)
        # vertices
        for i1 in range(len(data.vertices)):
            vertex = data.vertices[i1]
            vertices.append([vertex.co[0], vertex.co[1], vertex.co[2]])
            vertexNormals.append([vertex.normal[0],
                                  vertex.normal[1],
                                  vertex.normal[2]])
        info.append(vertices)
        info.append(vertexNormals)
        # polygons
        for i1 in range(len(data.polygons)):
            polygon = data.polygons[i1]
            vertexIndices = []
            for i2 in range(len(polygon.vertices)):
                vertexIndex = polygon.vertices[i2]
                vertexIndices.append(vertexIndex)
            polygons.append(vertexIndices)
            polygonNormals.append(polygon.normal)
            smoothPolygons.append(polygon.use_smooth)
            polygonMaterialIndices.append(polygon.material_index)
        info.append(polygons)
        info.append(polygonNormals)
        info.append(smoothPolygons)
        info.append(polygonMaterialIndices)
        # uv-coordinates
        if len(data.uv_layers) >= 1:
            uv_values = data.uv_layer_clone.data.values()
            for i1 in range(len(uv_values)):
                uv = uv_values[i1].uv
                uvs.append([uv[0], uv[1]])
        info.append(uvs)
        return info

    def getInfoSurface(self, obj):
        info = []
        data = obj.data
        # gather info here
        materials = []
        splines = []
        # materials
        for i1 in range(len(data.materials)): # data
            material = data.materials[i1]
            if material != None:
                materials.append(material.name)
        if materials == []:
            for i1 in range(len(obj.material_slots)):
                material = obj.material_slots[i1]
                if material != None:
                    materials.append(material.name)
        info.append(materials)
        # splines
        for i1 in range(len(data.splines)):
            spline = data.splines[i1]
            points = []
            for i2 in range(len(spline.points)):
                point = spline.points[i2]
                points.append([point.co[0],
                               point.co[1],
                               point.co[2],
                               point.co[3]])
            splines.append([[spline.order_u, spline.order_v], points])
        info.append(splines)
        return info

    def nameStartsWith(self, name, pattern):
        retBool = False
        if len(name) >= len(pattern):
            words = name.split(pattern)
            if words[0] == "":
                retBool = True
        return retBool

    def export(self, filename):
        # PRMan
        RMANTREE = "/mill3d/server/apps/PIXAR/linux-x86-64/RenderManProServer-20.0"
        head, tail = os.path.split(filename)
        self.mainDir = head
        root, ext = os.path.splitext(tail)
        self.mainName = root
        # scene
        self.scene = bpy.context.scene
        percent = self.scene.render.resolution_percentage / 100.0
        xresolution = int(self.scene.render.resolution_x * percent)
        yresolution = int(self.scene.render.resolution_y * percent)
        border = []
        if self.scene.render.use_border:
            border.append(self.scene.render.border_min_x)
            border.append(self.scene.render.border_max_x)
            border.append(self.scene.render.border_min_y)
            border.append(self.scene.render.border_max_y)
        AA_samples = 1
        if self.scene.render.use_antialiasing:
            if self.scene.render.antialiasing_samples == '5':
                AA_samples = 5
            elif self.scene.render.antialiasing_samples == '8':
                AA_samples = 8
            elif self.scene.render.antialiasing_samples == '11':
                AA_samples = 11
            elif self.scene.render.antialiasing_samples == '16':
                AA_samples = 16
        tile_x = tile_y = 64
        tile_x = self.scene.render.tile_x
        tile_y = self.scene.render.tile_y
        # scene bounding box
        self.bbox = self.calculateSceneBoundingBox()
        # motion blur
        use_motion_blur = self.scene.render.use_motion_blur
        animatedObjects = []
        animatedObjectNames = []
        motionMatrices = {}
        if use_motion_blur:
            shutter_start = 0.0
            shutter_end = self.scene.render.motion_blur_shutter
            motion_blur_samples = self.scene.render.motion_blur_samples
            # loop through all objects
            for obj in self.scene.objects:
                if self.isVisible(obj) and self.isAnimated(obj):
                    animatedObjects.append(obj)
                    animatedObjectNames.append(obj.name)
        else:
            shutter_start = 0.0
            shutter_end = 0.0
            motion_blur_samples = 0
        self.mBlur = [use_motion_blur,
                      shutter_start,
                      shutter_end,
                      motion_blur_samples]
        # render camera
        self.camObj = self.scene.camera
        if self.camObj == None:
            print("WARNING: no camera, let me create one for you ;)")
            bpy.ops.object.camera_add()
            self.scene.camera = bpy.data.objects[bpy.data.cameras[0].name]
            return
        self.camData = self.camObj.data
        # check for OSL shaders
        for text in bpy.data.texts:
            filepath = text.filepath
            paths = bpy.utils.blend_paths()
            if filepath in paths:
                index = paths.index(filepath)
                abs_paths = bpy.utils.blend_paths(absolute = True)
                abs_path = abs_paths[index]
                if abs_path != '/':
                    # check for .osl extension
                    basename = os.path.basename(abs_path)
                    root, ext = os.path.splitext(basename)
                    if ext == ".osl":
                        # copy
                        command = "cp %s %s" % (abs_path, self.mainDir)
                        print(command)
                        os.system(command)
                        # oslc
                        basename = os.path.basename(abs_path)
                        root, ext = os.path.splitext(basename)
                        iFilename = os.path.join(self.mainDir, basename)
                        oFilename = os.path.join(self.mainDir,
                                                 root + ".oso")
                        # use oslc to create a mipmapped texture
                        command = "oslc -o %s %s" % (oFilename, iFilename)
                        print(command)
                        os.system(command)
        # copy textures to export directory
        if self.options.textures:
            for image in bpy.data.images:
                if image.source == 'FILE':
                    filepath = image.filepath
                    paths = bpy.utils.blend_paths()
                    if filepath in paths:
                        index = paths.index(filepath)
                        abs_paths = bpy.utils.blend_paths(absolute = True)
                        abs_path = abs_paths[index]
                        if abs_path != '/':
                            # copy
                            command = "cp %s %s" % (abs_path, self.mainDir)
                            print(command)
                            os.system(command)
                            # tdlmake
                            basename = os.path.basename(abs_path)
                            root, ext = os.path.splitext(basename)
                            iFilename = os.path.join(self.mainDir, basename)
                            oFilename = os.path.join(self.mainDir,
                                                     root + "_tdlmake" + ext)
                            # use tdlmake to create a mipmapped texture
                            command = "tdlmake %s %s" % (iFilename, oFilename)
                            print(command)
                            os.system(command)
                            # txmake
                            basename = os.path.basename(abs_path)
                            root, ext = os.path.splitext(basename)
                            iFilename = os.path.join(self.mainDir, basename)
                            oFilename = os.path.join(self.mainDir,
                                                     root + "_txmake" + ext)
                            # use txmake to create a mipmapped texture
                            command = ("%s/txmake %s %s" %
                                       ((RMANTREE + "/bin"),
                                        iFilename, oFilename))
                            print(command)
                            os.system(command)
        # motion blur
        if use_motion_blur:
            # collect matrices for all animated objects
            frame_current = self.scene.frame_current
            frame_step = 1.0 / float(motion_blur_samples - 1)
            for i in range(motion_blur_samples):
                subframe = shutter_end * frame_step * i
                self.scene.frame_set(frame_current, subframe)
                for objIdx in range(len(animatedObjects)):
                    obj = animatedObjects[objIdx]
                    # collect matrices (per object)
                    if i == 0:
                        motionMatrices[obj.name] = []
                    m = obj.matrix_world.copy()
                    inv = obj.matrix_world.copy()
                    inv.invert()
                    motionMatrices[obj.name].append([m, inv])
            # reset frame
            self.scene.frame_set(frame_current, 0.0)
        # add animatedObjects and motionMatrices to self.mBlur for
        # usage below (in prepareExport(...))
        self.mBlur.append(animatedObjectNames)
        self.mBlur.append(motionMatrices)
        # count light emitters (and sync Cycles nodes with materials)
        light_counter = 0
        for matIndex in range(len(bpy.data.materials)):
            mat = bpy.data.materials[matIndex]
            # do not just looks for named nodes, follow link from
            # 'Material Output' instead
            if self.options.cycles and mat.use_nodes:
                output = mat.node_tree.nodes['Material Output']
                surface = output.inputs['Surface']
                # copy Cycles node settings back into material
                if self.nameStartsWith(surface.links[0].from_node.name, 'Emission'):
                    emission = mat.node_tree.nodes[surface.links[0].from_node.name]
                    color = emission.inputs['Color']
                    mat.diffuse_color[0] = color.default_value[0]
                    mat.diffuse_color[1] = color.default_value[1]
                    mat.diffuse_color[2] = color.default_value[2]
                    mat.emit = emission.inputs['Strength'].default_value
                elif self.nameStartsWith(surface.links[0].from_node.name, 'Diffuse BSDF'):
                    diffuse = mat.node_tree.nodes[surface.links[0].from_node.name]
                    color = diffuse.inputs['Color']
                    mat.diffuse_color[0] = color.default_value[0]
                    mat.diffuse_color[1] = color.default_value[1]
                    mat.diffuse_color[2] = color.default_value[2]
                    # TODO: diffuse.inputs['Normal']
                    mat.roughness = diffuse.inputs['Roughness'].default_value
                    mat.diffuse_intensity = 1.0
                    mat.specular_intensity = 0.0
                elif self.nameStartsWith(surface.links[0].from_node.name, 'Glossy BSDF'):
                    glossy = mat.node_tree.nodes[surface.links[0].from_node.name]
                    mat.raytrace_mirror.use = True
                    mat.raytrace_mirror.reflect_factor = 1.0
                    color = glossy.inputs['Color']
                    mat.mirror_color[0] = color.default_value[0]
                    mat.mirror_color[1] = color.default_value[1]
                    mat.mirror_color[2] = color.default_value[2]
                    # TODO: glossy.inputs['Normal']
                    mat.roughness = glossy.inputs['Roughness'].default_value
                    mat.diffuse_intensity = 0.0
                    mat.specular_intensity = 0.0
                elif self.nameStartsWith(surface.links[0].from_node.name, 'Mix Shader'):
                    mix = mat.node_tree.nodes[surface.links[0].from_node.name]
                    fac = mix.inputs['Fac'].default_value
                    surface1 = mix.inputs[1]
                    surface2 = mix.inputs[2]
                    color = surface1.links[0].from_node.inputs['Color']
                    mat.diffuse_color[0] = color.default_value[0]
                    mat.diffuse_color[1] = color.default_value[1]
                    mat.diffuse_color[2] = color.default_value[2]
                    try:
                        mat.roughness = surface1.links[0].from_node.inputs['Roughness'].default_value
                    except KeyError:
                        # defaults to zero
                        mat.roughness = 0.0
                    color = surface2.links[0].from_node.inputs['Color']
                    mat.mirror_color[0] = color.default_value[0]
                    mat.mirror_color[1] = color.default_value[1]
                    mat.mirror_color[2] = color.default_value[2]
                    try:
                        mat.roughness = surface2.links[0].from_node.inputs['Roughness'].default_value
                    except KeyError:
                        # probably set above (or defaults to zero)
                        pass
                    if surface2.links[0].from_node.type == 'BSDF_GLOSSY':
                        mat.raytrace_mirror.reflect_factor = fac
                        mat.diffuse_intensity = (1.0 -
                                                 mat.raytrace_mirror.reflect_factor)
                        mat.specular_intensity = fac
                    elif surface2.links[0].from_node.type == 'BSDF_TRANSPARENT':
                        mat.raytrace_mirror.reflect_factor = 0.0
                        mat.specular_intensity = 0.0
                        mat.use_transparency = True
                        mat.transparency_method = 'RAYTRACE'
                        mat.alpha = fac
                        mat.diffuse_intensity = (1.0 -
                                                 mat.alpha)
                    else:
                        print("WARNING: mix.surface2.type == '%s'" % surface2.links[0].from_node.type)
                elif self.nameStartsWith(surface.links[0].from_node.name, 'Glass BSDF'):
                    glass = mat.node_tree.nodes['Glass BSDF']
                    mat.use_transparency = True
                    mat.alpha = 0.08
                    mat.transparency_method = 'RAYTRACE'
                    color = glass.inputs['Color']
                    mat.diffuse_color[0] = color.default_value[0]
                    mat.diffuse_color[1] = color.default_value[1]
                    mat.diffuse_color[2] = color.default_value[2]
                    ior = glass.inputs['IOR']
                    mat.raytrace_transparency.ior = ior.default_value
                    # TODO: glass.inputs['Normal']
                    mat.roughness = glass.inputs['Roughness'].default_value
            if mat.emit != 0.0:
                light_counter += 1
        # check for HDR lighting
        hdrLighting = False
        if self.scene.world:
            world = self.scene.world
            texture = world.texture_slots[0] # try first texture found
            if texture != None and texture.texture.type == 'IMAGE':
                blendImage = texture.texture.image
                if blendImage.source == 'FILE':
                    hdrFilepath = blendImage.filepath
                    paths = bpy.utils.blend_paths()
                    if hdrFilepath in paths:
                        index = paths.index(hdrFilepath)
                        abs_paths = bpy.utils.blend_paths(absolute = True)
                        abs_path = abs_paths[index]
                        hdrLighting = True
                        hdrImagePath = abs_path
        # detect sun
        useSun = False
        useSky = False
        try:
            sun_data = bpy.data.lamps['sun']
        except KeyError:
            try:
                sun_data = bpy.data.lamps['Sun']
            except KeyError:
                useSun = False
                useSky = False
            else:
                useSun = True
                useSky = sun_data.sky.use_sky
        else:
            useSun = True
            useSky = sun_data.sky.use_sky
        sun = None
        if useSun:
            try:
                # assumes same name for object and data!!!
                sun = bpy.data.objects[sun_data.name]
            except KeyError:
                sun = None
            else:
                if sun.data.type != 'SUN':
                    sun = None
                # last check is if the sun is visible
                if (not self.isVisible(sun)):
                    sun = None
        # prepare export
        for exporter in self.exporters:
            exporter.prepareExport(self.scene,
                                   self.bbox,
                                   self.mainDir,
                                   self.mainName,
                                   self.mBlur,
                                   light_counter,
                                   sun)
        # export camera(s)
        if self.camData.type == "PERSP":
            for obj in self.scene.objects:
                data = obj.data
                if obj.type == 'CAMERA':
                    for exporter in self.exporters:
                        transform = obj.matrix_world.copy()
                        exporter.writeCamera(data.name,
                                             data.lens,
                                             data.angle,
                                             [xresolution, yresolution],
                                             border,
                                             AA_samples, [tile_x, tile_y],
                                             transform,
                                             obj == self.scene.camera,
                                             sun)
        else:
            print('TODO: camera "%s" has type "%s"' %
                  (self.camObj.name, self.camData.type))
        # export lights
        if self.options.lighting != 'no_lights':
            # HDR lighting first
            if hdrLighting:
                for exporter in self.exporters:
                    exporter.writeHdrLighting(hdrImagePath)
            # then loop through all lights
            for obj in self.scene.objects:
                if (self.isVisible(obj)):
                    data = obj.data
                    if obj.type == 'LAMP':
                        info = self.getInfoLamp(obj)
                        color = info[0]
                        energy = info[1]
                        falloff = info[2]
                        use = info[3]
                        if data.type == 'AREA':
                            for exporter in self.exporters:
                                transform = obj.matrix_world.copy()
                                exporter.writeAreaLight(obj.name, transform,
                                                         info)
                        elif data.type == 'HEMI':
                            print("TODO: %s(%s)" % (obj.type, data.type))
                        elif data.type == 'POINT':
                            for exporter in self.exporters:
                                transform = obj.matrix_world.copy()
                                exporter.writePointLight(obj.name, transform,
                                                         info)
                        elif data.type == 'SPOT':
                            for exporter in self.exporters:
                                transform = obj.matrix_world.copy()
                                exporter.writeSpotLight(obj.name, transform,
                                                        info)
                        elif data.type == 'SUN':
                            for exporter in self.exporters:
                                transform = obj.matrix_world.copy()
                                exporter.writeSunLight(obj.name, transform,
                                                       info)
                        else:
                            print("WARNING: unknown LAMP type %s(%s)",
                                  (obj.type, data.type))
        # apply all modifiers (only for meshes)
        bpy.ops.object.select_by_type(type = 'MESH')
        if len(bpy.context.selected_objects):
            objName = bpy.context.selected_objects[0].name
            bpy.context.scene.objects.active = bpy.data.objects[objName]
            bpy.ops.object.convert()
        # export geometry
        numObjs = len(self.scene.objects)
        bpy.context.window_manager.progress_begin(0, numObjs)
        bpy.context.window_manager.progress_update(0)
        for index in range(numObjs):
            obj = self.scene.objects[index]
            self.exportObject(obj)
            bpy.context.window_manager.progress_update(index+1)
        bpy.context.window_manager.progress_end()
        # finish export
        for exporter in self.exporters:
            exporter.finishExport(sun)

    def exportObject(self, obj, group_matrix = None):
        if (self.isVisible(obj) or group_matrix != None):
            if group_matrix != None:
                # check hide_render flag (again)
                if obj.hide_render:
                    print("INFO: %s is hidden" % obj.name)
                    return
            if obj.type == 'CAMERA':
                pass
            elif obj.type == 'EMPTY':
                transform = obj.matrix_world.copy()
                if group_matrix != None:
                    transform = group_matrix * transform
                if obj.dupli_type == 'GROUP':
                    for child in obj.dupli_group.objects:
                        self.exportObject(child, transform)
            elif obj.type == 'LAMP':
                pass
            elif obj.type == 'MESH':
                transform = obj.matrix_world.copy()
                user_matrix = None
                if group_matrix != None:
                    if obj.layers[19]:
                        # keep user matrix (e.g. for Radiance patterns)
                        user_matrix = obj.matrix_world.copy()
                    transform = group_matrix * transform
                # assume a material is in the first slot
                if len(obj.material_slots) >= 1:
                    mats = obj.material_slots.items()
                    matName = mats[0][0]
                    if matName != "":
                        mat = bpy.data.materials[matName]
                    else:
                        mat = self.createNewMaterial(obj)
                else:
                    mat = self.createNewMaterial(obj)
                info = self.getInfoMesh(obj, None)
                for exporter in self.exporters:
                    exporter.writeMesh(obj.name, transform.copy(), info,
                                       user_matrix)
            elif obj.type == 'SURFACE':
                transform = obj.matrix_world.copy()
                if group_matrix != None:
                    transform = group_matrix * transform
                isPrimitive, primType = self.isPrimitive(obj)
                if isPrimitive:
                    # create temporary mesh
                    mesh = obj.to_mesh(self.scene, True, 'RENDER')
                    info = self.getInfoMesh(obj, mesh)
                    if primType == "Cone":
                        for exporter in self.exporters:
                            # assume a material is in the first slot
                            if len(obj.material_slots) >= 1:
                                mats = obj.material_slots.items()
                                matName = mats[0][0]
                                if matName != "":
                                    mat = bpy.data.materials[matName]
                                else:
                                    mat = self.createNewMaterial(obj)
                            else:
                                mat = self.createNewMaterial(obj)
                            exporter.writeCone(obj.name, transform.copy(),
                                               info, mat)
                    elif primType == "Cylinder":
                        for exporter in self.exporters:
                            # assume a material is in the first slot
                            if len(obj.material_slots) >= 1:
                                mats = obj.material_slots.items()
                                matName = mats[0][0]
                                if matName != "":
                                    mat = bpy.data.materials[matName]
                                else:
                                    mat = self.createNewMaterial(obj)
                            else:
                                mat = self.createNewMaterial(obj)
                            exporter.writeCylinder(obj.name, transform.copy(),
                                                   info, mat)
                    elif primType == "Ring":
                        for exporter in self.exporters:
                            # assume a material is in the first slot
                            if len(obj.material_slots) >= 1:
                                mats = obj.material_slots.items()
                                matName = mats[0][0]
                                if matName != "":
                                    mat = bpy.data.materials[matName]
                                else:
                                    mat = self.createNewMaterial(obj)
                            else:
                                mat = self.createNewMaterial(obj)
                            exporter.writeRing(obj.name, transform.copy(),
                                               info, mat)
                    elif primType == "Sphere":
                        for exporter in self.exporters:
                            # assume a material is in the first slot
                            if len(obj.material_slots) >= 1:
                                mats = obj.material_slots.items()
                                matName = mats[0][0]
                                if matName != "":
                                    mat = bpy.data.materials[matName]
                                else:
                                    mat = self.createNewMaterial(obj)
                            else:
                                mat = self.createNewMaterial(obj)
                            exporter.writeSphere(obj.name, transform.copy(),
                                                 info, mat)
                    else:
                        print("TODO: %s(%s)" % (obj.type, primType))
                    # remove temporary mesh
                    bpy.data.meshes.remove(mesh)
                else:
                    # assume a material is in the first slot
                    if len(obj.material_slots) >= 1:
                        mats = obj.material_slots.items()
                        matName = mats[0][0]
                        if matName != "":
                            mat = bpy.data.materials[matName]
                        else:
                            mat = self.createNewMaterial(obj)
                    else:
                        mat = self.createNewMaterial(obj)
                    info = self.getInfoSurface(obj)
                    materials = info[0]
                    splines = info[1]
                    for exporter in self.exporters:
                        exporter.writeNurbsSurface(obj.name, transform.copy(),
                                                   info)
            else:
                print("TODO: %s" % obj.type)

###############################################################################

def save(operator, context, filepath = "",
         # user interface
         opt_lighting = 'no_lights',
         opt_textures = False,
         opt_cycles = False,
         opt_aov_N = False,
         opt_aov_P = False,
         opt_aov_Z = False,
         opt_aov_emission = False,
         opt_aov_direct_diffuse = False,
         opt_aov_direct_specular = False,
         opt_aov_reflection = False,
         opt_aov_refraction = False,
         opt_aov_indirect_diffuse = False,
         opt_aov_indirect_specular = False):
    if bpy.ops.object.mode_set.poll():
        bpy.ops.object.mode_set(mode = 'OBJECT')
    # print a usage message
    print("=" * 79)
    print("bpy.ops.export_scene.ailmr(filepath = \"%s\", " %
          "\\\\".join(filepath.split('\\')) +
          "opt_lighting = '%s', " % opt_lighting +
          "opt_textures = %s, " % opt_textures +
          "opt_cycles = %s, " % opt_cycles +
          "opt_aov_N = %s, " % opt_aov_N +
          "opt_aov_P = %s, " % opt_aov_P +
          "opt_aov_Z = %s, " % opt_aov_Z +
          "opt_aov_emission = %s, " % opt_aov_emission +
          "opt_aov_direct_diffuse = %s, " % opt_aov_direct_diffuse +
          "opt_aov_direct_specular = %s, " % opt_aov_direct_specular +
          "opt_aov_reflection = %s," % opt_aov_reflection +
          "opt_aov_refraction = %s," % opt_aov_refraction +
          "opt_aov_indirect_diffuse = %s, " % opt_aov_indirect_diffuse +
          "opt_aov_indirect_specular = %s)" % opt_aov_indirect_specular)
    # delegate work to classes
    exporter = MultiExporter(Options(lighting = opt_lighting,
                                     textures = opt_textures,
                                     cycles = opt_cycles,
                                     aov_N = opt_aov_N,
                                     aov_P = opt_aov_P,
                                     aov_Z = opt_aov_Z,
                                     aov_emission =
                                     opt_aov_emission,
                                     aov_direct_diffuse =
                                     opt_aov_direct_diffuse,
                                     aov_direct_specular =
                                     opt_aov_direct_specular,
                                     aov_reflection =
                                     opt_aov_reflection,
                                     aov_refraction =
                                     opt_aov_refraction,
                                     aov_indirect_diffuse =
                                     opt_aov_indirect_diffuse,
                                     aov_indirect_specular =
                                     opt_aov_indirect_specular))
    exporter.export(filepath)
    return {'FINISHED'}
