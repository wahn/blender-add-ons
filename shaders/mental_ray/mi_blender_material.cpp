#include "shader.h"
extern "C" {
#include "miaux.h"
}

#define WHITE(C) ((C).r==1 && (C).g==1 && (C).b==1)

typedef struct {
  miColor Color;
  miColor Opacity;
  miScalar Ka;
  miScalar Kd;
  miScalar Ks;
  miColor Ks_color;
  miScalar specular_hardness;
  miScalar Kr;
  miColor Kr_color;
  miScalar Kt;
  miScalar IOR;
  miColor texture;
} mi_blender_material_t;

extern "C" DLLEXPORT
int
mi_blender_material_version(void) {
  return 1;
}

extern "C" DLLEXPORT
miBoolean
mi_blender_material_init(miState* state,
                         void* params,
                         miBoolean* instance_init_required) {
  // see named_framebuffer_put.cpp
  size_t count = 0;

  mi::shader::Access_fb framebuffers(state->camera->buffertag);
  framebuffers->get_buffercount(count);
  for (unsigned int i = 0; i < count; i++) {
    const char *name = NULL;
    if (framebuffers->get_buffername(i, name)) {
      size_t index;
      if (framebuffers->get_index(name, index)) {
        mi_info("  framebuffer '%s' has index %i", name, (int) index);
      }
      else  {
        mi_info("  framebuffer '%s' has no index", name);
      }
    }
  }

  return miTRUE;
}

extern "C" DLLEXPORT
miBoolean
mi_blender_material(miColor* result,
                    miState* state,
                    mi_blender_material_t* param) {
  char normals_fb[] = "aov_N";
  char points_fb[] = "aov_P";
  char direct_diffuse_fb[] = "aov_direct_diffuse";
  char direct_specular_fb[] = "aov_direct_specular";
  char reflection_fb[] = "aov_reflection";
  char refraction_fb[] = "aov_refraction";
  mi::shader::Access_fb framebuffers(state->camera->buffertag);
  size_t buffer_index;
  miVector normal;
  miVector point;

  // normals
  if (framebuffers->get_index(normals_fb, buffer_index)) {
    if (state->type == miRAY_EYE) {
      mi_normal_to_world(state, &normal, &state->normal);
      mi_vector_normalize(&normal);
      // compensate for the way the .mi files rotate the scene
      // (in rotated_root_grp_inst)
      miScalar tmp = normal.z;
      normal.z = normal.y;
      normal.y = -tmp;
      mi_fb_put(state, buffer_index, &normal);
    }
  }
  // points
  if (framebuffers->get_index(points_fb, buffer_index)) {
    if (state->type == miRAY_EYE) {
      mi_point_to_world(state, &point, &state->point);
      // compensate for the way the .mi files rotate the scene
      // (in rotated_root_grp_inst)
      miScalar tmp = point.z;
      point.z = point.y;
      point.y = -tmp;
      mi_fb_put(state, buffer_index, &point);
    }
  }
  // check for illegal calls (see baselambert.cpp)
  if (state->type == miRAY_SHADOW || state->type == miRAY_DISPLACE) {
    return miFALSE;
  }
  miTag* light = NULL; // tag of light instance
  int n_l = 0; // number of light sources
  int samples;
  miColor light_contribution;
  miColor sum_d;
  miColor sum_s;
  miVector dir;
  miScalar dot_nl;
  // like mode=2 (see baselambert.cpp)
  mi_exclusive_lightlist(&n_l, &light, state);
  result->r = result->g = result->b = result->a = 0.0f;
  miColor Color = *mi_eval_color(&param->Color);
  miScalar specular_hardness = *mi_eval_scalar(&param->specular_hardness);
  miColor diff;
  miColor spec;
  miColor refl;
  miColor refr;
  miScalar s = 0.0f;
  diff.r = diff.g = diff.b = 0.0f;
  spec.r = spec.g = spec.b = 0.0f;
  for (mi::shader::LightIterator iter(state, light, n_l);
       !iter.at_end();
       ++iter) {
    sum_d.r = sum_d.g = sum_d.b = 0.0f;
    sum_s.r = sum_s.g = sum_s.b = 0.0f;
    while (iter->sample()) {
      // Lambert
      dot_nl = iter->get_dot_nl();
      iter->get_contribution(&light_contribution);
      sum_d.r += dot_nl * light_contribution.r;
      sum_d.g += dot_nl * light_contribution.g;
      sum_d.b += dot_nl * light_contribution.b;
      // Phong
      dir = iter->get_direction();
      s = mi_phong_specular(specular_hardness, state, &dir);
      if (s > 0.0) {
        sum_s.r += s * light_contribution.r;
        sum_s.g += s * light_contribution.g;
        sum_s.b += s * light_contribution.b;
      }
    }
    samples = iter->get_number_of_samples();
    if (samples) {
      // diffuse
      diff.r += sum_d.r / samples;
      diff.g += sum_d.g / samples;
      diff.b += sum_d.b / samples;
      // specular
      spec.r += sum_s.r / samples;
      spec.g += sum_s.g / samples;
      spec.b += sum_s.b / samples;
    }
  }
  miScalar Kd = *mi_eval_scalar(&param->Kd);
  miColor texture = *mi_eval_color(&param->texture);
  // direct_diffuse
  if (framebuffers->get_index(direct_diffuse_fb, buffer_index)) {
    if (state->type == miRAY_EYE) {
      // direct means visible directly from the camera/eye
      miColor direct_diffuse;
      direct_diffuse.r = Color.r * texture.r * Kd * diff.r;
      direct_diffuse.g = Color.g * texture.g * Kd * diff.g;
      direct_diffuse.b = Color.b * texture.b * Kd * diff.b;
      mi_fb_put(state, buffer_index, &direct_diffuse);
    }
  }
  miScalar Ks = *mi_eval_scalar(&param->Ks);
  miColor Ks_color = *mi_eval_color(&param->Ks_color);
  // direct_specular
  if (framebuffers->get_index(direct_specular_fb, buffer_index)) {
    if (state->type == miRAY_EYE) {
      // direct means visible directly from the camera/eye
      miColor direct_specular;
      direct_specular.r = Ks_color.r * Ks * spec.r;
      direct_specular.g = Ks_color.g * Ks * spec.g;
      direct_specular.b = Ks_color.b * Ks * spec.b;
      mi_fb_put(state, buffer_index, &direct_specular);
    }
  }
  // shoot reflection ray(s)
  miScalar Kr = *mi_eval_scalar(&param->Kr);
  miColor Kr_color = *mi_eval_color(&param->Kr_color);
  refl.r = refl.g = refl.b = 0.0f;
  if (Kr > 0.0f && Kr_color.r > 0.0f && Kr_color.g > 0.0f && Kr_color.b > 0.0f) {
    miVector R;
    mi_reflection_dir(&R, state);
    if (!mi_trace_reflection(&refl, state, &R)) {
      mi_trace_environment(&refl, state, &R);
    }
  }
  // shoot refraction ray(s)
  miScalar Kt = *mi_eval_scalar(&param->Kt);
  miScalar IOR = *mi_eval_scalar(&param->IOR);
  refr.r = refr.g = refr.b = 0.0f;
  if (Kt > 0.0f) {
    miaux_set_state_refraction_indices(state, IOR);
    miVector R;
    if (mi_refraction_dir(&R, state, state->ior_in, state->ior)) {
      mi_trace_refraction(&refr, state, &R);
    } else {
      mi_reflection_dir(&R, state);
      if (!mi_trace_reflection(&refr, state, &R)) {
        mi_trace_environment(&refr, state, &R);
      }
    }
  }
  // reflection
  if (framebuffers->get_index(reflection_fb, buffer_index)) {
    miColor reflection;
    reflection.r = Kr_color.r * Kr * refl.r;
    reflection.g = Kr_color.g * Kr * refl.g;
    reflection.b = Kr_color.b * Kr * refl.b;
    mi_fb_put(state, buffer_index, &reflection);
  }
  // refraction
  if (framebuffers->get_index(refraction_fb, buffer_index)) {
    miColor refraction;
    refraction.r = Kt * refr.r;
    refraction.g = Kt * refr.g;
    refraction.b = Kt * refr.b;
    mi_fb_put(state, buffer_index, &refraction);
  }
  miColor indirect_contribution;
  mi_compute_irradiance(&indirect_contribution, state);
  result->r = ((Kd * diff.r + indirect_contribution.r) * Color.r * texture.r +
               Ks_color.r * Ks * spec.r + Kr_color.r * Kr * refl.r +
               Kt * refr.r);
  result->g = ((Kd * diff.g + indirect_contribution.g) * Color.g * texture.g +
               Ks_color.g * Ks * spec.g + Kr_color.g * Kr * refl.g +
               Kt * refr.g);
  result->b = ((Kd * diff.b + indirect_contribution.b) * Color.b * texture.b +
               Ks_color.b * Ks * spec.b + Kr_color.b * Kr * refl.b +
               Kt * refr.b);
  result->a  = 1.0f;
  return miTRUE;
}
