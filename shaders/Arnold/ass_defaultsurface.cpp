#include <ai.h>
#include <cstring> // strcpy
// #include <stdio.h> // printf

AI_SHADER_NODE_EXPORT_METHODS(AssDefaultsurfaceMethods)

enum AssDefaultsurfaceParams
{
  p_Color,
  p_Opacity,
  p_Ka,
  p_Kd,
  p_texture
};

node_parameters
{
  AiParameterRGB("Color", 1.0f, 1.0f, 1.0f);
  AiParameterRGB("Opacity", 1.0f, 1.0f, 1.0f);
  AiParameterFLT("Ka", 0.2f);
  AiParameterFLT("Kd", 0.8f);
  AiParameterRGB("texture", 1.0f, 1.0f, 1.0f);
}

node_initialize
{
}

node_update
{
}

node_finish
{
}

shader_evaluate
{
  AtColor Color = AiShaderEvalParamRGB(p_Color);
  AtColor Opacity = AiShaderEvalParamRGB(p_Opacity);
  float Ka = AiShaderEvalParamFlt(p_Ka);
  float Kd = AiShaderEvalParamFlt(p_Kd);
  AtColor texture = AiShaderEvalParamRGB(p_texture);

  AtVector I;
  AiV3Sub(I, sg->P, sg->Ro);
  float diffuse = AiV3Dot(I, sg->N);

  diffuse = (diffuse * diffuse) / (AiV3Dot(I, I) * AiV3Dot(sg->N, sg->N));

  sg->out.RGB = (Ka + Kd * diffuse) * Color * texture;
  sg->out_opacity = Opacity;
  // In RSL we would need to do this: sg->out.RGB *= sg->out_opacity;
}

node_loader
{
  if (i > 0) return false;

  node->methods = AssDefaultsurfaceMethods;
  node->output_type = AI_TYPE_RGB;
  node->name = "ass_defaultsurface";
  node->node_type = AI_NODE_SHADER;
  strcpy(node->version, AI_VERSION);
  return true;
}
