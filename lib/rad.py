# (c) Jan Walter 2008
# 
# $Author: jan $
# $Date: 2008-10-19 10:26:36 -0700 (Sun, 19 Oct 2008) $
# $Revision: 149 $

import os

############
# Surfaces #
############

class RadSphere:
    def __init__(self, mod, id, 
                 xcent = 0.0, ycent = 0.0, zcent = 0.0, radius = 1.0):
        self.mod = mod
        self.id = id
        self.xcent = xcent
        self.ycent = ycent
        self.zcent = zcent
        self.radius = radius

    def __repr__(self):
        str = ('RadSphere("%s", "%s", %s, %s, %s, %s)' % 
               (self.mod, self.id, self.xcent, self.ycent, self.zcent, 
                self.radius))
        return str

    def setParams(self, xcent, ycent, zcent, radius):
        self.xcent = xcent
        self.ycent = ycent
        self.zcent = zcent
        self.radius = radius

class RadCone:
    def __init__(self, mod, id, 
                 x0 = 0.0, y0 = 0.0, z0 = -1.0, x1 = 0.0, y1 = 0.0, z1 = 1.0, 
                 r0 = 1.0, r1 = 1.0):
        self.mod = mod
        self.id = id
        self.x0 = x0
        self.y0 = y0
        self.z0 = z0
        self.x1 = x1
        self.y1 = y1
        self.z1 = z1
        self.r0 = r0
        self.r1 = r1

    def __repr__(self):
        str = ('RadCone("%s", "%s", %s, %s, %s, %s, %s, %s, %s, %s)' % 
               (self.mod, self.id, 
                self.x0, self.y0, self.z0, self.x1, self.y1, self.z1,
                self.r0, self.r1))
        return str

    def setParams(self, x0, y0, z0, x1, y1, z1, r0, r1):
        self.x0 = x0
        self.y0 = y0
        self.z0 = z0
        self.x1 = x1
        self.y1 = y1
        self.z1 = z1
        self.r0 = r0
        self.r1 = r1

class RadCup:
    def __init__(self, mod, id, 
                 x0 = 0.0, y0 = 0.0, z0 = -1.0, x1 = 0.0, y1 = 0.0, z1 = 1.0, 
                 r0 = 1.0, r1 = 1.0):
        self.mod = mod
        self.id = id
        self.x0 = x0
        self.y0 = y0
        self.z0 = z0
        self.x1 = x1
        self.y1 = y1
        self.z1 = z1
        self.r0 = r0
        self.r1 = r1

    def __repr__(self):
        str = ('RadCup("%s", "%s", %s, %s, %s, %s, %s, %s, %s, %s)' % 
               (self.mod, self.id, 
                self.x0, self.y0, self.z0, self.x1, self.y1, self.z1,
                self.r0, self.r1))
        return str

    def setParams(self, x0, y0, z0, x1, y1, z1, r0, r1):
        self.x0 = x0
        self.y0 = y0
        self.z0 = z0
        self.x1 = x1
        self.y1 = y1
        self.z1 = z1
        self.r0 = r0
        self.r1 = r1

class RadRing:
    def __init__(self, mod, id, 
                 xcent = 0.0, ycent = 0.0, zcent = 0.0,
                 xdir = 0.0, ydir = 0.0, zdir = 1.0,
                 r0 = 0.0, r1 = 1.0):
        self.mod = mod
        self.id = id
        self.xcent = xcent
        self.ycent = ycent
        self.zcent = zcent
        self.xdir = xdir
        self.ydir = ydir
        self.zdir = zdir
        self.r0 = r0
        self.r1 = r1

    def __repr__(self):
        str = ('RadRing("%s", "%s", %s, %s, %s, %s, %s, %s, %s, %s)' % 
               (self.mod, self.id, 
                self.xcent, self.ycent, self.zcent, self.xdir, self.ydir, self.zdir,
                self.r0, self.r1))
        return str

    def setParams(self, xcent, ycent, zcent, xdir, ydir, zdir, r0, r1):
        self.xcent = xcent
        self.ycent = ycent
        self.zcent = zcent
        self.xdir = xdir
        self.ydir = ydir
        self.zdir = zdir
        self.r0 = r0
        self.r1 = r1

class RadCylinder:
    def __init__(self, mod, id, 
                 x0 = 0.0, y0 = 0.0, z0 = -1.0, x1 = 0.0, y1 = 0.0, z1 = 1.0, 
                 rad = 1.0):
        self.mod = mod
        self.id = id
        self.x0 = x0
        self.y0 = y0
        self.z0 = z0
        self.x1 = x1
        self.y1 = y1
        self.z1 = z1
        self.rad = rad

    def __repr__(self):
        str = ('RadCylinder("%s", "%s", %s, %s, %s, %s, %s, %s, %s)' % 
               (self.mod, self.id, 
                self.x0, self.y0, self.z0, self.x1, self.y1, self.z1,
                self.rad))
        return str

    def setParams(self, x0, y0, z0, x1, y1, z1, rad):
        self.x0 = x0
        self.y0 = y0
        self.z0 = z0
        self.x1 = x1
        self.y1 = y1
        self.z1 = z1
        self.rad = rad

class RadTube:
    def __init__(self, mod, id, 
                 x0 = 0.0, y0 = 0.0, z0 = -1.0, x1 = 0.0, y1 = 0.0, z1 = 1.0, 
                 rad = 1.0):
        self.mod = mod
        self.id = id
        self.x0 = x0
        self.y0 = y0
        self.z0 = z0
        self.x1 = x1
        self.y1 = y1
        self.z1 = z1
        self.rad = rad

    def __repr__(self):
        str = ('RadTube("%s", "%s", %s, %s, %s, %s, %s, %s, %s)' % 
               (self.mod, self.id, 
                self.x0, self.y0, self.z0, self.x1, self.y1, self.z1,
                self.rad))
        return str

    def setParams(self, x0, y0, z0, x1, y1, z1, rad):
        self.x0 = x0
        self.y0 = y0
        self.z0 = z0
        self.x1 = x1
        self.y1 = y1
        self.z1 = z1
        self.rad = rad

class RadPolygon:
    def __init__(self, mod, id, n = 0, vertices = []):
        self.mod = mod
        self.id = id
        self.n = n
        self.vertices = vertices

    def __repr__(self):
        str = ('RadPolygon("%s", "%s", %s, %s)' % 
               (self.mod, self.id, self.n, self.vertices))
        return str

    def setParams(self, n, vertices):
        self.n = n
        self.vertices = vertices

#############
# Materials #
#############

class RadLight:
    def __init__(self, mod, id, 
                 red = 0.0, green = 0.0, blue = 0.0):
        self.mod = mod
        self.id = id
        self.red = red
        self.green = green
        self.blue = blue

    def __repr__(self):
        str = ('RadLight("%s", "%s", %s, %s, %s)' % 
               (self.mod, self.id, self.red, self.green, self.blue))
        return str

    def setParams(self, red, green, blue):
        self.red = red
        self.green = green
        self.blue = blue

class RadMirror:
    def __init__(self, mod, id, 
                 red = 0.0, green = 0.0, blue = 0.0):
        self.mod = mod
        self.id = id
        self.red = red
        self.green = green
        self.blue = blue

    def __repr__(self):
        str = ('RadMirror("%s", "%s", %s, %s, %s)' % 
               (self.mod, self.id, self.red, self.green, self.blue))
        return str

    def setParams(self, red, green, blue):
        self.red = red
        self.green = green
        self.blue = blue

class RadDielectric:
    def __init__(self, mod, id, 
                 rtn = 0.0, gtn = 0.0, btn = 0.0, n = 0.0, hc = 0.0):
        self.mod = mod
        self.id = id
        self.rtn = rtn
        self.gtn = gtn
        self.btn = btn
        self.n = n
        self.hc = hc

    def __repr__(self):
        str = ('RadDielectric("%s", "%s", %s, %s, %s, %s, %s)' % 
               (self.mod, self.id, self.rtn, self.gtn, self.btn,
                self.n, self.hc))
        return str

    def setParams(self, rtn, gtn, btn, n, hc):
        self.rtn = rtn
        self.gtn = gtn
        self.btn = btn
        self.n = n
        self.hc = hc

class RadGlass:
    def __init__(self, mod, id, 
                 rtn = 0.0, gtn = 0.0, btn = 0.0):
        self.mod = mod
        self.id = id
        self.rtn = rtn
        self.gtn = gtn
        self.btn = btn

    def __repr__(self):
        str = ('RadGlass("%s", "%s", %s, %s, %s)' % 
               (self.mod, self.id, self.rtn, self.gtn, self.btn))
        return str

    def setParams(self, rtn, gtn, btn):
        self.rtn = rtn
        self.gtn = gtn
        self.btn = btn

class RadGlow:
    def __init__(self, mod, id, 
                 red = 0.0, green = 0.0, blue = 0.0, maxrad = 0.0):
        self.mod = mod
        self.id = id
        self.red = red
        self.green = green
        self.blue = blue
        self.maxrad = maxrad

    def __repr__(self):
        str = ('RadGlow("%s", "%s", %s, %s, %s, %s)' % 
               (self.mod, self.id, self.red, self.green, self.blue,
                self.maxrad))
        return str

    def setParams(self, red, green, blue, maxrad):
        self.red = red
        self.green = green
        self.blue = blue
        self.maxrad = maxrad

class RadIllum:
    def __init__(self, mod, id, 
                 red = 0.0, green = 0.0, blue = 0.0):
        self.mod = mod
        self.id = id
        self.red = red
        self.green = green
        self.blue = blue

    def __repr__(self):
        str = ('RadIllum("%s", "%s", %s, %s, %s)' % 
               (self.mod, self.id, self.red, self.green, self.blue))
        return str

    def setParams(self, red, green, blue):
        self.red = red
        self.green = green
        self.blue = blue

class RadMetal:
    def __init__(self, mod, id, 
                 red = 0.0, green = 0.0, blue = 0.0, spec = 0.0, rough = 0.0):
        self.mod = mod
        self.id = id
        self.red = red
        self.green = green
        self.blue = blue
        self.spec = spec
        self.rough = rough

    def __repr__(self):
        str = ('RadMetal("%s", "%s", %s, %s, %s, %s, %s)' % 
               (self.mod, self.id, self.red, self.green, self.blue,
                self.spec, self.rough))
        return str

    def setParams(self, red, green, blue, spec, rough):
        self.red = red
        self.green = green
        self.blue = blue
        self.spec = spec
        self.rough = rough

class RadMetal2:
    def __init__(self, mod, id, 
                 red = 0.0, green = 0.0, blue = 0.0, spec = 0.0,
                 urough = 0.0, vrough = 0.0):
        self.mod = mod
        self.id = id
        self.red = red
        self.green = green
        self.blue = blue
        self.spec = spec
        self.urough = urough
        self.vrough = vrough

    def __repr__(self):
        str = ('RadMetal2("%s", "%s", %s, %s, %s, %s, %s, %s)' % 
               (self.mod, self.id, self.red, self.green, self.blue,
                self.spec, self.urough, self.vrough))
        return str

    def setParams(self, red, green, blue, spec, urough, vrough):
        self.red = red
        self.green = green
        self.blue = blue
        self.spec = spec
        self.urough = urough
        self.vrough = vrough

class RadAntimatter:
    def __init__(self, mod, id, mods = []):
        self.mod = mod
        self.id = id
        self.mods = mods

    def __repr__(self):
        str = ('RadAntimatter("%s", "%s", %s)' % 
               (self.mod, self.id, self.mods))
        return str

    def setParams(self, mods):
        self.mods = mods

class RadPlastic:
    def __init__(self, mod, id, 
                 red = 0.0, green = 0.0, blue = 0.0, spec = 0.0, rough = 0.0):
        self.mod = mod
        self.id = id
        self.red = red
        self.green = green
        self.blue = blue
        self.spec = spec
        self.rough = rough

    def __repr__(self):
        str = ('RadPlastic("%s", "%s", %s, %s, %s, %s, %s)' % 
               (self.mod, self.id, self.red, self.green, self.blue,
                self.spec, self.rough))
        return str

    def setParams(self, red, green, blue, spec, rough):
        self.red = red
        self.green = green
        self.blue = blue
        self.spec = spec
        self.rough = rough

class RadPlastic2:
    def __init__(self, mod, id, 
                 red = 0.0, green = 0.0, blue = 0.0, spec = 0.0,
                 urough = 0.0, vrough = 0.0):
        self.mod = mod
        self.id = id
        self.red = red
        self.green = green
        self.blue = blue
        self.spec = spec
        self.urough = urough
        self.vrough = vrough

    def __repr__(self):
        str = ('RadPlastic2("%s", "%s", %s, %s, %s, %s, %s, %s)' % 
               (self.mod, self.id, self.red, self.green, self.blue,
                self.spec, self.urough, self.vrough))
        return str

    def setParams(self, red, green, blue, spec, urough, vrough):
        self.red = red
        self.green = green
        self.blue = blue
        self.spec = spec
        self.urough = urough
        self.vrough = vrough

class RadSource:
    def __init__(self, mod, id, 
                 xdir = 0.0, ydir = 0.0, zdir = 0.0, angle = 0.0):
        self.mod = mod
        self.id = id
        self.xdir = xdir
        self.ydir = ydir
        self.zdir = zdir
        self.angle = angle

    def __repr__(self):
        str = ('RadSource("%s", "%s", %s, %s, %s, %s)' % 
               (self.mod, self.id, self.xdir, self.ydir, self.zdir,
                self.angle))
        return str

    def setParams(self, xdir, ydir, zdir, angle):
        self.xdir = xdir
        self.ydir = ydir
        self.zdir = zdir
        self.angle = angle

class RadSpotlight:
    def __init__(self, mod, id, 
                 red = 0.0, green = 0.0, blue = 0.0, angle = 0.0, 
                 xdir = 0.0, ydir = 0.0, zdir = 0.0):
        self.mod = mod
        self.id = id
        self.red = red
        self.green = green
        self.blue = blue
        self.angle = angle
        self.xdir = xdir
        self.ydir = ydir
        self.zdir = zdir

    def __repr__(self):
        str = ('RadSpotlight("%s", "%s", %s, %s, %s, %s, %s, %s, %s)' % 
               (self.mod, self.id, self.red, self.green, self.blue, 
                self.angle, self.xdir, self.ydir, self.zdir))
        return str

    def setParams(self, red, green, blue, angle, xdir, ydir, zdir):
        self.red = red
        self.green = green
        self.blue = blue
        self.angle = angle
        self.xdir = xdir
        self.ydir = ydir
        self.zdir = zdir

class RadTrans:
    def __init__(self, mod, id, 
                 red = 0.0, green = 0.0, blue = 0.0, spec = 0.0, rough = 0.0,
                 trans = 0.0, tspec = 0.0):
        self.mod = mod
        self.id = id
        self.red = red
        self.green = green
        self.blue = blue
        self.spec = spec
        self.rough = rough
        self.trans = trans
        self.tspec = tspec

    def __repr__(self):
        str = ('RadTrans("%s", "%s", %s, %s, %s, %s, %s, %s, %s)' % 
               (self.mod, self.id, self.red, self.green, self.blue,
                self.spec, self.rough, self.trans, self.tspec))
        return str

    def setParams(self, red, green, blue, spec, rough, trans, tspec):
        self.red = red
        self.green = green
        self.blue = blue
        self.spec = spec
        self.rough = rough
        self.trans = trans
        self.tspec = tspec

############
# Commands #
############

class RadCommand:
    def __init__(self, mod, id, commandLine):
        self.mod = mod
        self.id = id
        self.commandLine = commandLine

    def __repr__(self):
        str = 'RadCommand("%s", "%s", "%s")' % (self.mod,
                                                self.id,
                                                self.commandLine)
        return str

###########
# Cameras #
###########

class RadCamera:
    def __init__(self, id, vp = [0.0, 0.0, 0.0], vd = [0.0, 0.0, -1.0],
                 vh = 45.0, vv = 45.0):
        self.id = id
        self.vp = vp
        self.vd = vd
        self.vh = vh
        self.vv = vv

    def __repr__(self):
        str = 'RadCamera("%s", %s, %s, %s, %s)' % (self.id,
                                                   self.vp,
                                                   self.vd,
                                                   self.vh,
                                                   self.vv)
        return str
