bl_info = {
    "name": "Export to multiple renderers",
    "author": "Jan Walter",
    "blender": (2, 66, 0),
    "location": "File > Import-Export",
    "description": "Exports a scene in multiple file formats",
    "warning": "",
    "wiki_url": "https://bitbucket.org/wahn/blender-add-ons/wiki/io_scene_multi",
    "tracker_url": "",
    "category": "Import-Export"}

if "bpy" in locals():
    import imp
    if "export_multi" in locals():
        imp.reload(export_arnold_ass)

import bpy
# TIME_HACK: import time
from bpy.props import BoolProperty, StringProperty, EnumProperty
from bpy_extras.io_utils import ExportHelper

# TIME_HACK: def header_info(self, context):
# TIME_HACK:     t = time.localtime()
# TIME_HACK:     self.layout.label("%02d:%02d:%02d" % (t.tm_hour, t.tm_min, t.tm_sec))

# TIME_HACK: @bpy.app.handlers.persistent
# TIME_HACK: def clock_hack(context):
# TIME_HACK:     # hack to update the UI
# TIME_HACK:     for area in byp.context.screen.areas:
# TIME_HACK:         if area.type == 'INFO':
# TIME_HACK:             area.tag_redraw()

class ExportMulti(bpy.types.Operator, ExportHelper):
    bl_idname = "export_scene.ailmr"
    bl_label = "Export ASS IGS LXS MI/MXS RAD/RIB"
    bl_options = {'PRESET'}
    filename_ext = ".ailmr"
    filter_glob = StringProperty(default = "*.ailmr", options = {'HIDDEN'})

    # operator properties
    opt_lighting = EnumProperty(items = [('no_lights',
                                          'no_lights',
                                          'no_lights'),
                                         ('direct_lighting',
                                          'direct_lighting',
                                          'direct_lighting'),
                                         ('global_illumination',
                                          'global_illumination',
                                          'global_illumination')],
                                name = "Lighting",
                                description = ("Choose render algorithm " +
                                               "(and shaders)"),
                                default = 'global_illumination')
    # textures
    opt_textures = BoolProperty(name = "textures",
                                description = "copy textures, use txmake",
                                default = False)
    # Cycles materials
    opt_cycles = BoolProperty(name = "Cycles materials",
                              description = "use Cycles materials",
                              default = True)
    # AOVs
    opt_aov_N = BoolProperty(name = "Aov N",
                             description = "Normal pass",
                             default = False)
    opt_aov_P = BoolProperty(name = "Aov P",
                             description = "Position pass",
                             default = False)
    opt_aov_Z = BoolProperty(name = "Aov Z",
                             description = "Create depth pass",
                             default = False)
    opt_aov_emission = BoolProperty(name = "Aov Emission",
                                          description = "Create emission pass",
                                          default = True)
    opt_aov_direct_diffuse = BoolProperty(name = "Aov Direct Diffuse",
                                          description = "Create diffuse pass",
                                          default = True)
    opt_aov_direct_specular = BoolProperty(name = "Aov Direct Specular",
                                           description = "Create specular pass",
                                           default = True)
    opt_aov_reflection = BoolProperty(name = "Aov Reflection",
                                      description = "Create reflection",
                                      default = True)
    opt_aov_refraction = BoolProperty(name = "Aov Refraction",
                                      description = "Create refraction",
                                      default = True)
    opt_aov_indirect_diffuse = BoolProperty(name = "Aov Indirect Diffuse",
                                            description = "Create diffuse pass",
                                            default = True)
    opt_aov_indirect_specular = BoolProperty(name = "Aov Indirect Specular",
                                             description = "Create specular pass",
                                             default = True)

    def execute(self, context):
        keywords = self.as_keywords(ignore = (
                "check_existing",
                "filter_glob"))
        from . import export_multi
        return export_multi.save(self, context, **keywords)

def menu_func(self, context):
    self.layout.operator(ExportMulti.bl_idname, 
                         text = "Multiple Renderers (.ailmr)")

def register():
    # TIME_HACK: bpy.app.handlers.scene_update_pre.append(clock_hack)
    # TIME_HACK: bpy.types.INFO_HT_header.append(header_info)
    bpy.utils.register_module(__name__)
    bpy.types.INFO_MT_file_export.append(menu_func)

def unregister():
    # TIME_HACK: bpy.app.handlers.scene_update_pre.remove(clock_hack)
    # TIME_HACK: bpy.types.INFO_HT_header.remove(header_info)
    bpy.utils.unregister_module(__name__)
    bpy.types.INFO_MT_file_export.remove(menu_func)

if __name__ == "__main__":
    register()

