import os
import math
# common exporter interface
from ..common.interface import CommonExporterInterface
# Blender
import bpy
import mathutils

class LxsExporter(CommonExporterInterface):
    def __init__(self, options):
        CommonExporterInterface.__init__(self, "LxsExporter", options)
        self.lxsFile = None
        self.pbrtFile = None

    def nameStartsWith(self, name, pattern):
        retBool = False
        if len(name) >= len(pattern):
            words = name.split(pattern)
            if words[0] == "":
                retBool = True
        return retBool

    def prepareExport(self, scene, bbox, directory, name, mblur, light_counter,
                      sun = None):
        # scene related
        self.scene = scene
        self.bbox = bbox
        if scene.unit_settings.system != 'METRIC':
            print("WARNING: unit_settings == '%s'" % scene.unit_settings.system)
        self.scale_length = scene.unit_settings.scale_length
        # light related
        self.light_counter = light_counter
        # open
        filename = os.path.join(directory, name + ".lxs")
        print('INFO: %s' % filename)
        self.lxsFile = open(filename, "w")
        filename = os.path.join(directory, name + ".pbrt")
        print('INFO: %s' % filename)
        self.pbrtFile = open(filename, "w")

    def writeBlenderMaterial(self, mat):
        # check for textures
        textures_found = 0
        textures = {}
        for index in list(range(len(mat.texture_slots))):
            slot = mat.texture_slots[index]
            if slot:
                texture = slot.texture
                if slot.texture_coords == 'UV':
                    if texture.type == 'IMAGE':
                        image = texture.image
                        src = image.filepath
                        if slot.use_map_color_diffuse:
                            # diffuse texture
                            textures["diffuse"] = src
                            textures_found += 1
                            # Texture
                            paths = bpy.utils.blend_paths()
                            if src in paths:
                                index = paths.index(src)
                                abs_paths = bpy.utils.blend_paths(absolute =
                                                                  True)
                                abs_path = abs_paths[index]
                                self.writeTexture(mat.name,
                                                  "Kd", "color",
                                                  abs_path)
        self.lxsFile.write('  MakeNamedMaterial "%s"\n' % mat.name)
        self.pbrtFile.write('  MakeNamedMaterial "%s"\n' % mat.name)
        if mat.use_transparency:
            # glass
            self.lxsFile.write('    "string type" [ "%s" ]\n' % "glass")
            self.pbrtFile.write('    "string type" [ "%s" ]\n' % "glass")
            index = mat.raytrace_transparency.ior
            self.lxsFile.write('    "float index" [ %s ]\n' % index)
            self.pbrtFile.write('    "float index" [ %s ]\n' % index)
            if mat.raytrace_mirror.use:
                Kr = mat.mirror_color
            else:
                Kr = [1.0, 1.0, 1.0]
            self.lxsFile.write('    "color Kr" [ %s %s %s ]\n' %
                               (Kr[0], Kr[1], Kr[2]))
            self.pbrtFile.write('    "color Kr" [ %s %s %s ]\n' %
                               (Kr[0], Kr[1], Kr[2]))
            Kd = mat.diffuse_color
            maxColor = Kd[0]
            if Kd[1] > maxColor:
                maxColor = Kd[1]
            if Kd[2] > maxColor:
                maxColor = Kd[2]
            w = maxColor
            if maxColor > 0.7:
                Kt = [Kd[0], Kd[1], Kd[2]]
            else:
                # works with tapestry in gallery scene
                Kt = [0.96*Kd[0]/w, 0.96*Kd[1]/w, 0.96*Kd[2]/w]
            self.lxsFile.write('    "color Kt" [ %s %s %s ]\n' %
                               (Kt[0], Kt[1], Kt[2]))
            self.pbrtFile.write('    "color Kt" [ %s %s %s ]\n' %
                               (Kt[0], Kt[1], Kt[2]))
        elif mat.raytrace_mirror.use:
            # mirror
            self.lxsFile.write('    "string type" ' +
                               '[ "%s" ]\n' % "mirror")
            self.pbrtFile.write('    "string type" ' +
                               '[ "%s" ]\n' % "mirror")
            Kr = mat.mirror_color
            try:
                diffuse_tex = textures["diffuse"]
            except KeyError:
                self.lxsFile.write('    "color Kr" [ %s %s %s ]\n' %
                                   (Kr[0], Kr[1], Kr[2]))
                self.pbrtFile.write('    "color Kr" [ %s %s %s ]\n' %
                                   (Kr[0], Kr[1], Kr[2]))
            else:
                self.lxsFile.write('    "texture Kr" [ "%s::Kd" ]\n' %
                                   mat.name)
                self.pbrtFile.write('    "texture Kr" [ "%s::Kd" ]\n' %
                                   mat.name)
        elif mat.specular_intensity > 0.0:
            # glossy
            self.lxsFile.write('    "string type" ' +
                               '[ "%s" ]\n' % "glossy")
            self.pbrtFile.write('    "string type" ' +
                               '[ "%s" ]\n' % "plastic")
            Kd = mat.diffuse_color
            try:
                diffuse_tex = textures["diffuse"]
            except KeyError:
                self.lxsFile.write('    "color Kd" [ %s %s %s ]\n' %
                                   (Kd[0], Kd[1], Kd[2]))
                self.pbrtFile.write('    "color Kd" [ %s %s %s ]\n' %
                                   (Kd[0], Kd[1], Kd[2]))
            else:
                self.lxsFile.write('    "texture Kd" [ "%s::Kd" ]\n' %
                                   mat.name)
                self.pbrtFile.write('    "texture Kd" [ "%s::Kd" ]\n' %
                                   mat.name)
            Ks = mat.specular_intensity
            Ks_color = mat.specular_color
            self.lxsFile.write('    "color Ks" [ %s %s %s ]\n' %
                               (Ks * Ks_color[0],
                                Ks * Ks_color[1],
                                Ks * Ks_color[2]))
            self.pbrtFile.write('    "color Ks" [ %s %s %s ]\n' %
                               (Ks * Ks_color[0],
                                Ks * Ks_color[1],
                                Ks * Ks_color[2]))
            roughness = mat.roughness
            # lxs
            self.lxsFile.write('    "float uroughness" [ %s ]\n' %
                               roughness)
            self.lxsFile.write('    "float vroughness" [ %s ]\n' %
                               roughness)
            # pbrt
            self.pbrtFile.write('    "float roughness" [ %s ]\n' %
                               roughness)
        else:
            # matte
            self.lxsFile.write('    "string type" [ "%s" ]\n' % "matte")
            self.pbrtFile.write('    "string type" [ "%s" ]\n' % "matte")
            Kd = mat.diffuse_color
            try:
                diffuse_tex = textures["diffuse"]
            except KeyError:
                self.lxsFile.write('    "color Kd" [ %s %s %s ]\n' %
                                   (Kd[0], Kd[1], Kd[2]))
                self.pbrtFile.write('    "color Kd" [ %s %s %s ]\n' %
                                   (Kd[0], Kd[1], Kd[2]))
            else:
                self.lxsFile.write('    "texture Kd" [ "%s::Kd" ]\n' %
                                   mat.name)
                self.pbrtFile.write('    "texture Kd" [ "%s::Kd" ]\n' %
                                   mat.name)

    def writeCyclesMaterial(self, mat):
        surface = mat.node_tree.nodes['Material Output'].inputs['Surface']
        if len(surface.links) >= 1:
            try:
                color = surface.links[0].from_node.inputs['Color']
            except KeyError:
                # handle mix of diffuse and glossy/transparent
                fac = surface.links[0].from_node.inputs['Fac']
                surface1 = surface.links[0].from_node.inputs[1]
                surface2 = surface.links[0].from_node.inputs[2]
                if surface1.links[0].from_node.type != 'BSDF_DIFFUSE':
                    print("WARNING: BSDF_DIFFUSE expected, %s found" %
                          surface1.links[0].from_node.type)
                if surface2.links[0].from_node.type != 'BSDF_GLOSSY':
                    if surface2.links[0].from_node.type != 'BSDF_TRANSPARENT':
                        print("WARNING: BSDF_GLOSSY|TRANSPARENT expected, " +
                              "%s found" %
                              surface2.links[0].from_node.type)
                diff = surface1.links[0].from_node.inputs['Color']
                if surface2.links[0].from_node.type != 'BSDF_GLOSSY':
                    spec = [1, 1, 1, 1]
                else:
                    spec = surface2.links[0].from_node.inputs['Color']
                diff_rough = surface1.links[0].from_node.inputs['Roughness']
                diffuse_roughness = diff_rough.default_value
                if surface2.links[0].from_node.type != 'BSDF_GLOSSY':
                    spec_rough = None
                    specular_roughness = 0.0
                else:
                    spec_rough = surface2.links[0].from_node.inputs['Roughness']
                    specular_roughness = spec_rough.default_value
                Kd = 1.0 - fac.default_value
                r = diff.default_value[0]
                g = diff.default_value[1]
                b = diff.default_value[2]
                a = diff.default_value[3]
                Kd_color = [r, g, b]
                Ks = fac.default_value
                Kr = fac.default_value
                if surface2.links[0].from_node.type != 'BSDF_GLOSSY':
                    r = 1.0
                    g = 1.0
                    b = 1.0
                    a = 1.0
                    Ks_color = [r, g, b]
                    Kr_color = [r, g, b]
                else:
                    r = spec.default_value[0]
                    g = spec.default_value[1]
                    b = spec.default_value[2]
                    a = spec.default_value[3]
                    Ks_color = [r, g, b]
                    Kr_color = [r, g, b]
                # diffuse texture
                if diff.is_linked:
                    # image
                    image = diff.links[0].from_node.image
                    src = image.filepath
                    paths = bpy.utils.blend_paths()
                    if src in paths:
                        index = paths.index(src)
                        abs_paths = bpy.utils.blend_paths(absolute = True)
                        abs_path = abs_paths[index]
                        self.writeTexture(mat.name,
                                          "Kd", "color",
                                          abs_path)
                # glossy texture
                if (surface2.links[0].from_node.type == 'BSDF_GLOSSY' and
                    spec_rough.is_linked):
                    # image
                    image = spec_rough.links[0].from_node.image
                    src = image.filepath
                    paths = bpy.utils.blend_paths()
                    if src in paths:
                        index = paths.index(src)
                        abs_paths = bpy.utils.blend_paths(absolute = True)
                        abs_path = abs_paths[index]
                        self.writeTexture(mat.name,
                                          "Kr", "color",
                                          abs_path)
                # mix texture
                if fac.is_linked:
                    # image
                    image = fac.links[0].from_node.image
                    src = image.filepath
                    paths = bpy.utils.blend_paths()
                    if src in paths:
                        index = paths.index(src)
                        abs_paths = bpy.utils.blend_paths(absolute = True)
                        abs_path = abs_paths[index]
                        self.writeTexture(mat.name,
                                          "fac", "float",
                                          abs_path)
                # matte
                self.lxsFile.write('  MakeNamedMaterial "%s%s"\n' %
                                   (mat.name, "_diffuse"))
                self.pbrtFile.write('  MakeNamedMaterial "%s%s"\n' %
                                   (mat.name, "_diffuse"))
                self.lxsFile.write('    "string type" [ "%s" ]\n' % "matte")
                self.pbrtFile.write('    "string type" [ "%s" ]\n' % "matte")
                Kd = mat.diffuse_color
                # diffuse texture
                if diff.is_linked:
                    self.lxsFile.write('    "texture Kd" [ "%s::Kd" ]\n' %
                                       mat.name)
                    self.pbrtFile.write('    "texture Kd" [ "%s::Kd" ]\n' %
                                       mat.name)
                else:
                    self.lxsFile.write('    "color Kd" [ %s %s %s ]\n' %
                                       (Kd[0], Kd[1], Kd[2]))
                    self.pbrtFile.write('    "color Kd" [ %s %s %s ]\n' %
                                       (Kd[0], Kd[1], Kd[2]))
                # glossy/transparent
                if surface2.links[0].from_node.type != 'BSDF_GLOSSY':
                    # glass
                    self.lxsFile.write('  MakeNamedMaterial "%s%s"\n' %
                                       (mat.name, "_trans"))
                    self.pbrtFile.write('  MakeNamedMaterial "%s%s"\n' %
                                       (mat.name, "_trans"))
                    self.lxsFile.write('    "string type" [ "%s" ]\n' % "glass")
                    self.pbrtFile.write('    "string type" [ "%s" ]\n' % "glass")
                    index = 1.0
                    self.lxsFile.write('    "float index" [ %s ]\n' % index)
                    self.pbrtFile.write('    "float index" [ %s ]\n' % index)
                    Kr = [1.0, 1.0, 1.0]
                    self.lxsFile.write('    "color Kr" [ %s %s %s ]\n' %
                                       (Kr[0], Kr[1], Kr[2]))
                    self.pbrtFile.write('    "color Kr" [ %s %s %s ]\n' %
                                       (Kr[0], Kr[1], Kr[2]))
                    Kt = [1.0, 1.0, 1.0]
                    self.lxsFile.write('    "color Kt" [ %s %s %s ]\n' %
                                       (Kt[0], Kt[1], Kt[2]))
                    self.pbrtFile.write('    "color Kt" [ %s %s %s ]\n' %
                                       (Kt[0], Kt[1], Kt[2]))
                else:
                    # mirror
                    self.lxsFile.write('  MakeNamedMaterial "%s%s"\n' %
                                       (mat.name, "_glossy"))
                    self.pbrtFile.write('  MakeNamedMaterial "%s%s"\n' %
                                       (mat.name, "_glossy"))
                    self.lxsFile.write('    "string type" ' +
                                       '[ "%s" ]\n' % "mirror")
                    self.pbrtFile.write('    "string type" ' +
                                       '[ "%s" ]\n' % "plastic")
                    Kr = mat.mirror_color
                    if spec_rough.is_linked:
                        self.lxsFile.write('    "texture Kr" [ "%s::Kr" ]\n' %
                                           mat.name)
                        self.pbrtFile.write('    "texture Kd" [ "%s::Kd" ]\n' %
                                           mat.name)
                        self.pbrtFile.write('    "texture Ks" [ "%s::Ks" ]\n' %
                                           mat.name)
                    else:
                        self.lxsFile.write('    "color Kr" [ %s %s %s ]\n' %
                                           (Kr[0], Kr[1], Kr[2]))
                        self.pbrtFile.write('    "color Kd" [ %s %s %s ]\n' %
                                           (Kr[0], Kr[1], Kr[2]))
                        self.pbrtFile.write('    "color Ks" [ %s %s %s ]\n' %
                                           (Kr[0], Kr[1], Kr[2]))
                    self.pbrtFile.write('    "float roughness" [ %s ]\n' % specular_roughness)
                # mix
                # lxs
                self.lxsFile.write('  MakeNamedMaterial "%s%s"\n' %
                                   (mat.name, "_mix"))
                self.lxsFile.write('    "string type" ' +
                                   '[ "%s" ]\n' % "mix")
                self.lxsFile.write('    "string namedmaterial1" ')
                self.lxsFile.write('[ "%s_diffuse" ]\n' % mat.name)
                self.lxsFile.write('    "string namedmaterial2" ')
                # pbrt
                self.pbrtFile.write('  MakeNamedMaterial "%s%s"\n' %
                                   (mat.name, "_mix"))
                self.pbrtFile.write('    "string type" ' +
                                   '[ "%s" ]\n' % "mix")
                self.pbrtFile.write('    "string namedmaterial1" ')
                self.pbrtFile.write('[ "%s_diffuse" ]\n' % mat.name)
                self.pbrtFile.write('    "string namedmaterial2" ')
                if surface2.links[0].from_node.type != 'BSDF_GLOSSY':
                    self.lxsFile.write('[ "%s_trans" ]\n' % mat.name)
                    self.pbrtFile.write('[ "%s_trans" ]\n' % mat.name)
                else:
                    self.lxsFile.write('[ "%s_glossy" ]\n' % mat.name)
                    self.pbrtFile.write('[ "%s_glossy" ]\n' % mat.name)
                if fac.is_linked:
                    self.lxsFile.write('    "texture amount" [ "%s::fac" ]\n' %
                                       mat.name)
                    self.pbrtFile.write('    "texture amount" [ "%s::fac" ]\n' %
                                       mat.name)
                else:
                    self.lxsFile.write('    "float amount" [ %s ]\n' %
                                       fac.default_value)
                    self.pbrtFile.write('    "rgb amount" [ %s %s %s ]\n' %
                                        (1.0 - fac.default_value,
                                         1.0 - fac.default_value,
                                         1.0 - fac.default_value))
            else:
                if surface.links[0].from_node.type == 'BSDF_DIFFUSE':
                    # diffuse BRDF
                    diff = color
                    r = diff.default_value[0]
                    g = diff.default_value[1]
                    b = diff.default_value[2]
                    a = diff.default_value[3]
                    Kd = [r, g, b]
                    # diffuse texture
                    if diff.is_linked:
                        # image
                        image = diff.links[0].from_node.image
                        src = image.filepath
                        paths = bpy.utils.blend_paths()
                        if src in paths:
                            index = paths.index(src)
                            abs_paths = bpy.utils.blend_paths(absolute = True)
                            abs_path = abs_paths[index]
                            self.writeTexture(mat.name,
                                              "Kd", "color",
                                              abs_path)
                    # matte
                    self.lxsFile.write('  MakeNamedMaterial "%s"\n' %
                                       mat.name)
                    self.pbrtFile.write('  MakeNamedMaterial "%s"\n' %
                                       mat.name)
                    self.lxsFile.write('    "string type" [ "%s" ]\n' % "matte")
                    self.pbrtFile.write('    "string type" [ "%s" ]\n' % "matte")
                    if diff.is_linked:
                        self.lxsFile.write('    "texture Kd" [ "%s::Kd" ]\n' %
                                           mat.name)
                        self.pbrtFile.write('    "texture Kd" [ "%s::Kd" ]\n' %
                                           mat.name)
                    else:
                        self.lxsFile.write('    "color Kd" [ %s %s %s ]\n' %
                                           (Kd[0], Kd[1], Kd[2]))
                        self.pbrtFile.write('    "color Kd" [ %s %s %s ]\n' %
                                           (Kd[0], Kd[1], Kd[2]))
                # TODO? elif surface.links[0].from_node.type == 'EMISSION':
                elif surface.links[0].from_node.type == 'BSDF_GLOSSY':
                    diff = surface.links[0].from_node.inputs['Color']
                    r = diff.default_value[0]
                    g = diff.default_value[1]
                    b = diff.default_value[2]
                    rgb = [r, g, b]
                    roughness = surface.links[0].from_node.inputs['Roughness'].default_value
                    self.lxsFile.write('  MakeNamedMaterial "%s"\n' %
                                       mat.name)
                    self.pbrtFile.write('  MakeNamedMaterial "%s"\n' %
                                       mat.name)
                    if roughness == 0.0:
                        # mirror
                        self.lxsFile.write('    "string type" ' +
                                           '[ "%s" ]\n' % "mirror")
                        self.pbrtFile.write('    "string type" ' +
                                           '[ "%s" ]\n' % "mirror")
                        self.lxsFile.write('    "color Kr" [ %s %s %s ]\n' %
                                           (rgb[0], rgb[1], rgb[2]))
                        self.pbrtFile.write('    "color Kr" [ %s %s %s ]\n' %
                                           (rgb[0], rgb[1], rgb[2]))
                    else:
                        # metal2
                        self.lxsFile.write('    "string type" ' +
                                           '[ "%s" ]\n' % "metal2")
                        self.pbrtFile.write('    "string type" ' +
                                           '[ "%s" ]\n' % "mirror")
                        self.lxsFile.write('    ##"color Kr" [ %s %s %s ]\n' %
                                           (rgb[0], rgb[1], rgb[2]))
                        self.pbrtFile.write('    "color Kr" [ %s %s %s ]\n' %
                                           (0.5 * rgb[0], 0.5 * rgb[1], 0.5 * rgb[2]))
                        self.lxsFile.write('    "float uroughness" ' +
                                           '[ %s ]\n' % roughness)
                        # self.pbrtFile.write('    "float uroughness" ' +
                        #                    '[ %s ]\n' % roughness)
                        self.lxsFile.write('    "float vroughness" ' +
                                           '[ %s ]\n' % roughness)
                        # self.pbrtFile.write('    "float vroughness" ' +
                        #                    '[ %s ]\n' % roughness)

    def writeCamera(self, name, lens, angle, resolution, border, AA_samples,
                    tile_size, transform, isRenderCamera, sun = None):
        if not isRenderCamera:
            # name
            self.lxsFile.write("# %s\n" % name)
            self.pbrtFile.write("# %s\n" % name)
            # LookAt (see luxblend/properties/camera.py)
            m = transform
            matrix = m.transposed()
            pos = matrix[3]
            forwards = -matrix[2]
            target = (pos + forwards)
            up = matrix[1]
            # lxs
            self.lxsFile.write("# LookAt %s %s %s # position\n" %
                               (self.scale_length * pos[0],
                                self.scale_length * pos[1],
                                self.scale_length * pos[2]))
            self.lxsFile.write("#        %s %s %s # target\n" %
                               (self.scale_length * target[0],
                                self.scale_length * target[1],
                                self.scale_length * target[2]))
            self.lxsFile.write("#        %s %s %s # up\n" %
                               (up[0], up[1], up[2]))
            # pbrt
            self.pbrtFile.write("# LookAt %s %s %s # position\n" %
                               (self.scale_length * pos[0],
                                self.scale_length * pos[1],
                                self.scale_length * pos[2]))
            self.pbrtFile.write("#        %s %s %s # target\n" %
                               (self.scale_length * target[0],
                                self.scale_length * target[1],
                                self.scale_length * target[2]))
            self.pbrtFile.write("#        %s %s %s # up\n" %
                               (up[0], up[1], up[2]))
            # Camera
            self.lxsFile.write('# Camera "perspective"\n')
            self.pbrtFile.write('# Camera "perspective"\n')
            aspect = resolution[0] / float(resolution[1])
            if aspect >= 1.0:
                fov = 2 * math.degrees(math.atan(16.0 / (aspect * lens)))
            else:
                fov = 2 * math.degrees(math.atan((aspect * 16.0) / lens))
            self.lxsFile.write('#   "float fov" [ %s ]\n' % fov)
            self.pbrtFile.write('#   "float fov" [ %s ]\n' % fov)
        else:
            self.pbrtFile.write("Scale -1 1 1 # swap x-axis direction\n")
            # name
            self.lxsFile.write("# %s\n" % name)
            self.pbrtFile.write("# %s\n" % name)
            # LookAt (see luxblend/properties/camera.py)
            m = transform
            matrix = m.transposed()
            pos = matrix[3]
            forwards = -matrix[2]
            target = (pos + forwards)
            up = matrix[1]
            # lxs
            self.lxsFile.write("LookAt %s %s %s # position\n" %
                               (self.scale_length * pos[0],
                                self.scale_length * pos[1],
                                self.scale_length * pos[2]))
            self.lxsFile.write("       %s %s %s # target\n" %
                               (self.scale_length * target[0],
                                self.scale_length * target[1],
                                self.scale_length * target[2]))
            self.lxsFile.write("       %s %s %s # up\n" %
                               (up[0], up[1], up[2]))
            # pbrt
            self.pbrtFile.write("LookAt %s %s %s # position\n" %
                               (self.scale_length * pos[0],
                                self.scale_length * pos[1],
                                self.scale_length * pos[2]))
            self.pbrtFile.write("       %s %s %s # target\n" %
                               (self.scale_length * target[0],
                                self.scale_length * target[1],
                                self.scale_length * target[2]))
            self.pbrtFile.write("       %s %s %s # up\n" %
                               (up[0], up[1], up[2]))
            # Camera
            self.lxsFile.write('Camera "perspective"\n')
            self.pbrtFile.write('Camera "perspective"\n')
            aspect = resolution[0] / float(resolution[1])
            if aspect >= 1.0:
                fov = 2 * math.degrees(math.atan(16.0 / (aspect * lens)))
            else:
                fov = 2 * math.degrees(math.atan((aspect * 16.0) / lens))
            self.lxsFile.write('  "float fov" [ %s ]\n' % fov)
            self.pbrtFile.write('  "float fov" [ %s ]\n' % fov)
            # DOF
            obj = bpy.data.objects[name]
            data = obj.data
            self.lxsFile.write('#   "float focaldistance" [ %s ]\n' %
                               data.dof_distance)
            self.pbrtFile.write('#   "float focaldistance" [ %s ]\n' %
                               data.dof_distance)
            self.lxsFile.write('#   "float lensradius" [ %s ]\n' %
                               data.cycles.aperture_size)
            self.pbrtFile.write('#   "float lensradius" [ %s ]\n' %
                               data.cycles.aperture_size)
            # Film
            # lxs
            self.lxsFile.write('Film "fleximage"\n')
            self.lxsFile.write('  "integer xresolution" [ %s ]\n' %
                               resolution[0])
            self.lxsFile.write('  "integer yresolution" [ %s ]\n' %
                               resolution[1])
            # pbrt
            self.pbrtFile.write('Film "image"\n')
            self.pbrtFile.write('  "integer xresolution" [ %s ]\n' %
                               resolution[0])
            self.pbrtFile.write('  "integer yresolution" [ %s ]\n' %
                               resolution[1])
            # border?
            if border:
                xmin = border[0]
                xmax = border[1]
                ymin = 1.0 - border[3]
                ymax = 1.0 - border[2]
                self.pbrtFile.write('  "float cropwindow" [ %s %s %s %s ]\n' %
                                    (xmin, xmax, ymin, ymax))
            # fireflies 0 = off [3-10]
            self.lxsFile.write('  "integer outlierrejection_k" [ %s ]\n' % 10)
            # Sampler
            self.lxsFile.write('#Sampler "metropolis"\n')
            self.pbrtFile.write('#Sampler "metropolis"\n')
            self.lxsFile.write('Sampler "sobol"\n') # based on Cycles
            self.pbrtFile.write('Sampler "lowdiscrepancy" "integer pixelsamples" [8]\n')
            # PixelFilter
            self.lxsFile.write('PixelFilter "blackmanharris"\n')
            self.pbrtFile.write('PixelFilter "gaussian" "float xwidth" [2.000000 ] "float ywidth" [2.000000 ]\n')
            # SurfaceIntegrator
            #
            # bidirectional, path, exphotonmap, directlighting, igi,
            # distributedpath, sppm
            if self.options.lighting == 'direct_lighting':
                self.lxsFile.write('SurfaceIntegrator "directlighting"\n')
                self.pbrtFile.write('Integrator "directlighting" "integer maxdepth" [10]\n')
                self.pbrtFile.write('##Integrator "path"\n')
            else:
                self.lxsFile.write('SurfaceIntegrator "bidirectional"\n')
                self.pbrtFile.write('Integrator "path"\n')
                self.pbrtFile.write('##Integrator "directlighting" "integer maxdepth" [10]\n')
            # WorldBegin
            self.lxsFile.write("WorldBegin\n")
            self.pbrtFile.write("WorldBegin\n")
            # export all materials
            self.writeMaterials()

    def writeCommonPrimitive(self, name, lxs_transform, pbrt_transform, mat, lxs_only = False):
        self.lxsFile.write("  # %s\n" % name)
        if not lxs_only:
            self.pbrtFile.write("  # %s\n" % name)
        # AttributeBegin
        self.lxsFile.write("  AttributeBegin\n")
        if not lxs_only:
            self.pbrtFile.write("  AttributeBegin\n")
        # Transform
        m = lxs_transform
        # lxs
        self.lxsFile.write('    Transform [\n')
        self.lxsFile.write('      %s %s %s %s\n' %
                           (m[0][0],
                            m[1][0],
                            m[2][0],
                            m[3][0]))
        self.lxsFile.write('      %s %s %s %s\n' %
                           (m[0][1],
                            m[1][1],
                            m[2][1],
                            m[3][1]))
        self.lxsFile.write('      %s %s %s %s\n' %
                               (m[0][2],
                                m[1][2],
                                m[2][2],
                                m[3][2]))
        self.lxsFile.write('      %s %s %s %s\n' %
                           (self.scale_length * m[0][3],
                            self.scale_length * m[1][3],
                            self.scale_length * m[2][3],
                            m[3][3]))
        self.lxsFile.write('    ]\n')
        # Transform
        m = pbrt_transform
        # pbrt
        if not lxs_only:
            self.pbrtFile.write('    Transform [\n')
            self.pbrtFile.write('      %s %s %s %s\n' %
                                (m[0][0],
                                 m[1][0],
                                 m[2][0],
                                 m[3][0]))
            self.pbrtFile.write('      %s %s %s %s\n' %
                                (m[0][1],
                                 m[1][1],
                            m[2][1],
                                 m[3][1]))
            self.pbrtFile.write('      %s %s %s %s\n' %
                                (m[0][2],
                                 m[1][2],
                                 m[2][2],
                                 m[3][2]))
            self.pbrtFile.write('      %s %s %s %s\n' %
                                (self.scale_length * m[0][3],
                                 self.scale_length * m[1][3],
                                 self.scale_length * m[2][3],
                                 m[3][3]))
            self.pbrtFile.write('    ]\n')

    def writeCone(self, name, transform, info, mat):
        # pbrt
        self.writeMesh(name, transform, info, pbrt_only = True)
        self.writeCommonPrimitive(name, transform, transform, mat, lxs_only = True)
        # NamedMaterial
        matName = mat.name
        self.lxsFile.write('    NamedMaterial "%s"\n' % matName)
        if mat.emit != 0.0:
            # LightGroup
            self.lxsFile.write('    LightGroup "%s"\n' % matName)
            # AreaLightSource
            self.lxsFile.write('    AreaLightSource "%s"\n' % "area")
            power = mat.emit
            self.lxsFile.write('      "float power" [ %s ]\n' % power)
            L = mat.diffuse_color
            self.lxsFile.write('      "color L" [ %s %s %s ]\n' %
                               (L[0], L[1], L[2]))
        # Cone
        obj = bpy.data.objects[name]
        p1 = obj.data.splines[0].points[0].co
        p2 = obj.data.splines[0].points[8].co
        radius1 = math.fabs(p1.y)
        radius2 = math.fabs(p2.y)
        if radius1 < radius2:
            height = math.fabs(p2.z - p1.z)
            # lxs
            self.lxsFile.write('    Translate 0 0 %s\n' %
                               (self.scale_length * height))
            self.lxsFile.write('    Rotate 180 1 0 0\n')
            self.lxsFile.write('    Shape "cone"\n')
            self.lxsFile.write('      "float radius" [ %s ]\n' %
                               (self.scale_length * radius2))
            self.lxsFile.write('      "float radius2" [ %s ]\n' %
                               (self.scale_length * radius1))
        else:
            # lxs
            self.lxsFile.write('    Shape "cone"\n')
            self.lxsFile.write('      "float radius" [ %s ]\n' %
                               (self.scale_length * radius1))
            self.lxsFile.write('      "float radius2" [ %s ]\n' %
                               (self.scale_length * radius2))
        # lxs
        self.lxsFile.write('      "float height" [ %s ]\n' %
                           (self.scale_length * math.fabs(p2.z - p1.z)))
        self.lxsFile.write('      "float phimax" [ 360 ]\n')
        # AttributeEnd
        self.lxsFile.write("  AttributeEnd\n")

    def writeCylinder(self, name, transform, info, mat):
        self.writeCommonPrimitive(name, transform, transform, mat)
        # NamedMaterial
        matName = mat.name
        self.lxsFile.write('    NamedMaterial "%s"\n' % matName)
        self.pbrtFile.write('    NamedMaterial "%s"\n' % matName)
        if mat.emit != 0.0:
            # LightGroup
            self.lxsFile.write('    LightGroup "%s"\n' % matName)
            # AreaLightSource
            self.lxsFile.write('    AreaLightSource "%s"\n' % "area")
            self.pbrtFile.write('    AreaLightSource "%s"\n' % "area")
            power = mat.emit
            self.lxsFile.write('      "float power" [ %s ]\n' % power)
            L = mat.diffuse_color
            self.lxsFile.write('      "color L" [ %s %s %s ]\n' %
                               (L[0], L[1], L[2]))
            self.pbrtFile.write('      "color L" [ %s %s %s ]\n' %
                               (L[0] * power, L[1] * power, L[2] * power))
        # Cylinder
        # lxs
        self.lxsFile.write('    Shape "cylinder"\n')
        self.lxsFile.write('      "float radius" [ %s ]\n' % self.scale_length)
        self.lxsFile.write('      "float zmin" [ 0 ]\n')
        self.lxsFile.write('      "float zmax" [ %s ]\n' % self.scale_length)
        self.lxsFile.write('      "float phimax" [ 360 ]\n')
        # pbrt
        self.pbrtFile.write('    Shape "cylinder"\n')
        self.pbrtFile.write('      "float radius" [ %s ]\n' % self.scale_length)
        self.pbrtFile.write('      "float zmin" [ 0 ]\n')
        self.pbrtFile.write('      "float zmax" [ %s ]\n' % self.scale_length)
        self.pbrtFile.write('      "float phimax" [ 360 ]\n')
        # AttributeEnd
        self.lxsFile.write("  AttributeEnd\n")
        self.pbrtFile.write("  AttributeEnd\n")

    def writeHdrLighting(self, hdrImagePath):
        # lxs
        self.lxsFile.write('  # HDR lighting\n')
        self.lxsFile.write('  LightSource "infinite"\n')
        self.lxsFile.write('    "float gain" [1.0]\n')
        self.lxsFile.write('    "float importance" [1.0]\n')
        self.lxsFile.write('    "string mapname" ["%s"]\n' % hdrImagePath)
        self.lxsFile.write('    "string mapping" ["latlong"]\n')
        self.lxsFile.write('    "float gamma" [1.0]\n')
        self.lxsFile.write('    "integer nsamples" [1]\n')
        # pbrt
        self.pbrtFile.write('  # HDR lighting\n')
        self.pbrtFile.write('  LightSource "infinite"\n')
        self.pbrtFile.write('    "float gain" [1.0]\n')
        self.pbrtFile.write('    "float importance" [1.0]\n')
        self.pbrtFile.write('    "string mapname" ["%s"]\n' % hdrImagePath)
        self.pbrtFile.write('    "string mapping" ["latlong"]\n')
        self.pbrtFile.write('    "float gamma" [1.0]\n')
        self.pbrtFile.write('    "integer nsamples" [1]\n')

    def writeMaterials(self):
        for mat in bpy.data.materials:
            self.lxsFile.write("  # %s\n" % mat.name)
            self.pbrtFile.write("  # %s\n" % mat.name)
            if self.options.cycles and mat.use_nodes:
                # use Cycles nodes
                self.writeCyclesMaterial(mat)
            else:
                # use Blender materials (old raytracer, not Cycles)
                self.writeBlenderMaterial(mat)

    def writeMesh(self, name, transform, info, user_matrix = None, pbrt_only = False):
        # info
        materials = info[0]
        vertices = info[1]
        vertexNormals = info[2]
        polygons = info[3]
        polygonNormals = info[4]
        smoothPolygons = info[5]
        polygonMaterialIndices = info[6]
        hasUVs, uvs = info[7]
        # name
        if not pbrt_only:
            self.lxsFile.write("  # %s\n" % name)
        self.pbrtFile.write("  # %s\n" % name)
        # AttributeBegin
        if not pbrt_only:
            self.lxsFile.write("  AttributeBegin\n")
        self.pbrtFile.write("  AttributeBegin\n")
        # Transform
        m = transform
        # lxs
        if not pbrt_only:
            self.lxsFile.write('    Transform [\n')
            self.lxsFile.write('      %s %s %s %s\n' %
                               (m[0][0],
                                m[1][0],
                                m[2][0],
                                m[3][0]))
            self.lxsFile.write('      %s %s %s %s\n' %
                               (m[0][1],
                                m[1][1],
                                m[2][1],
                                m[3][1]))
            self.lxsFile.write('      %s %s %s %s\n' %
                               (m[0][2],
                                m[1][2],
                                m[2][2],
                                m[3][2]))
            self.lxsFile.write('      %s %s %s %s\n' %
                               (self.scale_length * m[0][3],
                                self.scale_length * m[1][3],
                                self.scale_length * m[2][3],
                                m[3][3]))
            self.lxsFile.write('    ]\n')
        # pbrt
        self.pbrtFile.write('    Transform [\n')
        self.pbrtFile.write('      %s %s %s %s\n' %
                           (m[0][0],
                            m[1][0],
                            m[2][0],
                            m[3][0]))
        self.pbrtFile.write('      %s %s %s %s\n' %
                           (m[0][1],
                            m[1][1],
                            m[2][1],
                            m[3][1]))
        self.pbrtFile.write('      %s %s %s %s\n' %
                           (m[0][2],
                            m[1][2],
                            m[2][2],
                            m[3][2]))
        self.pbrtFile.write('      %s %s %s %s\n' %
                           (self.scale_length * m[0][3],
                            self.scale_length * m[1][3],
                            self.scale_length * m[2][3],
                            m[3][3]))
        self.pbrtFile.write('    ]\n')
        # sort by materials
        infoMaterial = {}
        for matIndex in range(len(materials)):
            matName = materials[matIndex]
            # store for each material a list with len(vertices) 0 entries
            matIndixes = [0] * len(vertices)
            infoMaterial[matIndex] = matIndixes
        for pi in range(len(polygonMaterialIndices)):
            pmi = polygonMaterialIndices[pi]
            polygon = polygons[pi]
            for vi in polygon:
                # count how often a vertex is used per material
                infoMaterial[pmi][vi] += 1
        for matIndex in range(len(materials)):
            counter = 0
            for index in range(len(infoMaterial[matIndex])):
                vi = infoMaterial[matIndex][index]
                # replace the counter by ...
                if vi == 0:
                    # ... -1 if the material doesn't use this vertex
                    infoMaterial[matIndex][index] = -1
                else:
                    # ... or by an increasing index
                    infoMaterial[matIndex][index] = counter
                    counter += 1
        # for each material ...
        for matIndex in range(len(materials)):
            matName = materials[matIndex]
            # NamedMaterial
            mat = bpy.data.materials[matName]
            if self.options.cycles and mat.use_nodes:
                # use Cycles nodes
                surface = mat.node_tree.nodes['Material Output'].inputs['Surface']
                if len(surface.links) >= 1 and surface.links[0].from_node.type != 'EMISSION':
                    try:
                        color = surface.links[0].from_node.inputs['Color']
                    except KeyError:
                        # handle mix of diffuse and glossy
                        if not pbrt_only:
                            self.lxsFile.write('    NamedMaterial "%s%s"\n' %
                                               (matName, "_mix"))
                        self.pbrtFile.write('    NamedMaterial "%s%s"\n' %
                                           (matName, "_mix"))
                    else:
                        # assume diffuse BRDF
                        if not pbrt_only:
                            self.lxsFile.write('    NamedMaterial "%s"\n' %
                                               matName)
                        self.pbrtFile.write('    NamedMaterial "%s"\n' %
                                           matName)
            else:
                # use Blender materials (old raytracer, not Cycles)
                if not pbrt_only:
                    self.lxsFile.write('    NamedMaterial "%s"\n' % matName)
                self.pbrtFile.write('    NamedMaterial "%s"\n' % matName)
            if mat.emit != 0.0:
                # LightGroup
                if not pbrt_only:
                    self.lxsFile.write('    LightGroup "%s"\n' % matName)
                # AreaLightSource
                if not pbrt_only:
                    self.lxsFile.write('    AreaLightSource "%s"\n' % "area")
                self.pbrtFile.write('    AreaLightSource "%s"\n' % "area")
                power = mat.emit
                if not pbrt_only:
                    self.lxsFile.write('      "float power" [ %s ]\n' % power)
                L = mat.diffuse_color
                if not pbrt_only:
                    self.lxsFile.write('      "color L" [ %s %s %s ]\n' %
                                       (L[0], L[1], L[2]))
                self.pbrtFile.write('      "color L" [ %s %s %s ]\n' %
                                    (L[0] * power, L[1] * power, L[2] * power))
            # if there's a single smooth polygon, we export normals
            useVertexNormals = False
            for pi in range(len(polygonMaterialIndices)):
                pmi = polygonMaterialIndices[pi]
                if pmi == matIndex:
                    smoothPolygon = smoothPolygons[pi]
                    if smoothPolygon:
                        useVertexNormals = True
                        break
            # PbrtSphere?
            if self.nameStartsWith(name, "PbrtSphere"):
                # try to get custom properties
                obj = bpy.data.objects[name]
                try:
                    radius = obj["radius"]
                except KeyError:
                    radius = 1.0
                try:
                    zmin = obj["zmin"]
                except KeyError:
                    zmin = 1.0
                try:
                    zmax = obj["zmax"]
                except KeyError:
                    zmax = 1.0
                try:
                    phimax = obj["phimax"]
                except KeyError:
                    phimax = 1.0
                self.pbrtFile.write('    Shape "sphere"\n')
                self.pbrtFile.write('      "float radius" [ %s ]\n' % radius)
                self.pbrtFile.write('      "float zmin" [ %s ]\n' % zmin)
                self.pbrtFile.write('      "float zmax" [ %s ]\n' % zmax)
                self.pbrtFile.write('      "float phimax" [ %s ]\n' % phimax)
                # AttributeEnd
                self.pbrtFile.write("  AttributeEnd\n")
                return
            # mesh
            if not pbrt_only:
                self.lxsFile.write('    Shape "mesh"\n')
            self.pbrtFile.write('    Shape "trianglemesh"\n')
            if not pbrt_only:
                self.lxsFile.write('      "point P" [\n')
            self.pbrtFile.write('      "point P" [\n')
            for pi in range(len(polygonMaterialIndices)):
                pmi = polygonMaterialIndices[pi]
                if pmi == matIndex:
                    polygon = polygons[pi]
                    for vi in polygon:
                        v = vertices[vi] # use original index
                        if not pbrt_only:
                            self.lxsFile.write('        %s %s %s\n' %
                                               (self.scale_length * v[0],
                                                self.scale_length * v[1],
                                                self.scale_length * v[2]))
                        self.pbrtFile.write('        %s %s %s\n' %
                                           (self.scale_length * v[0],
                                            self.scale_length * v[1],
                                            self.scale_length * v[2]))
            if not pbrt_only:
                self.lxsFile.write('      ]\n')
            self.pbrtFile.write('      ]\n')
            # normals (based on smoothPolygons)
            if useVertexNormals:
                # normals
                if not pbrt_only:
                    self.lxsFile.write('      "normal N" [\n')
                self.pbrtFile.write('      "normal N" [\n')
                for pi in range(len(polygonMaterialIndices)):
                    pmi = polygonMaterialIndices[pi]
                    smoothPolygon = smoothPolygons[pi]
                    if pmi == matIndex:
                        if not smoothPolygon:
                            # use face normal
                            normal = polygonNormals[pi]
                        polygon = polygons[pi]
                        for vi in polygon:
                            if smoothPolygon:
                                # use vertex normal
                                n = vertexNormals[vi]
                            else:
                                n = normal # face normal
                            if not pbrt_only:
                                self.lxsFile.write('        %s %s %s\n' %
                                                   (n[0], n[1], n[2]))
                            self.pbrtFile.write('        %s %s %s\n' %
                                               (n[0], n[1], n[2]))
                if not pbrt_only:
                    self.lxsFile.write('      ]\n')
                self.pbrtFile.write('      ]\n')
            # quads or triangles?
            triangles = []
            quads = []
            for pi in range(len(polygonMaterialIndices)):
                pmi = polygonMaterialIndices[pi]
                if pmi == matIndex:
                    polygon = polygons[pi]
                    if (len(polygon) == 3):
                        triangles.append(polygon)
                    elif (len(polygon) == 4):
                        quads.append(polygon)
            self.pbrtFile.write('      "integer indices" [\n')
            # quads
            if len(quads):
                counter = 0
                if not pbrt_only:
                    self.lxsFile.write('      "integer quadindices" [\n')
                for pi in range(len(polygonMaterialIndices)):
                    pmi = polygonMaterialIndices[pi]
                    if pmi == matIndex:
                        polygon = polygons[pi]
                        if (len(polygon) == 3):
                            counter += 3
                        elif (len(polygon) == 4):
                            if not pbrt_only:
                                self.lxsFile.write('        ')
                            self.pbrtFile.write('        %s %s %s\n' % (counter, counter + 1, counter + 2))
                            self.pbrtFile.write('        %s %s %s\n' % (counter, counter + 2, counter + 3))
                            for dummy in range(4):
                                if not pbrt_only:
                                    self.lxsFile.write("%s " % counter)
                                counter += 1
                            if not pbrt_only:
                                self.lxsFile.write('\n')
                if not pbrt_only:
                    self.lxsFile.write('      ]\n')
            # triangles
            if len(triangles):
                counter = 0
                if not pbrt_only:
                    self.lxsFile.write('      "integer triindices" [\n')
                for pi in range(len(polygonMaterialIndices)):
                    pmi = polygonMaterialIndices[pi]
                    if pmi == matIndex:
                        polygon = polygons[pi]
                        if (len(polygon) == 4):
                            counter += 4
                        elif (len(polygon) == 3):
                            if not pbrt_only:
                                self.lxsFile.write('        ')
                            self.pbrtFile.write('        ')
                            for dummy in range(3):
                                if not pbrt_only:
                                    self.lxsFile.write("%s " % counter)
                                self.pbrtFile.write("%s " % counter)
                                counter += 1
                            if not pbrt_only:
                                self.lxsFile.write('\n')
                            self.pbrtFile.write('\n')
                if not pbrt_only:
                    self.lxsFile.write('      ]\n')
            self.pbrtFile.write('      ]\n')
            # uv-coordinates
            if len(uvs):
                if not pbrt_only:
                    self.lxsFile.write('      "float uv" [\n')
                self.pbrtFile.write('      "float uv" [\n')
                for pi in range(len(polygonMaterialIndices)):
                    pmi = polygonMaterialIndices[pi]
                    if pmi == matIndex:
                        uvs_poly = uvs[pi]
                        for uv in uvs_poly:
                            if not pbrt_only:
                                self.lxsFile.write('        %s %s\n' %
                                                   (uv[0], uv[1]))
                            self.pbrtFile.write('        %s %s\n' %
                                               (uv[0], uv[1]))
                if not pbrt_only:
                    self.lxsFile.write('      ]\n')
                self.pbrtFile.write('      ]\n')
        # AttributeEnd
        if not pbrt_only:
            self.lxsFile.write("  AttributeEnd\n")
        self.pbrtFile.write("  AttributeEnd\n")

    def writePointLight(self, name, transform, info):
        self.lxsFile.write('  # %s\n' % name)
        self.pbrtFile.write('  # %s\n' % name)
        # info
        color = info[0]
        energy = info[1]
        falloff = info[2]
        use_diffuse = info[3][0]
        use_specular = info[3][1]
        # lamp data
        lamp = bpy.data.lamps[name]
        # AttributeBegin
        self.lxsFile.write("  AttributeBegin\n")
        self.pbrtFile.write("  AttributeBegin\n")
        # LightGroup
        self.lxsFile.write('    LightGroup "%s"\n' % name)
        # LightSource
        loc, rot, scale = transform.decompose()
        self.lxsFile.write('    LightSource "point"\n')
        self.lxsFile.write('      "color L" [ %s %s %s ]\n' %
                           (energy * color[0],
                            energy * color[1],
                            energy * color[2]))
        self.lxsFile.write('      "point from" [ %s %s %s ]\n' %
                           (loc[0], loc[1], loc[2]))
        self.pbrtFile.write('    LightSource "point"\n')
        self.pbrtFile.write('      "color I" [ %s %s %s ]\n' % (color[0],
                                                                color[1],
                                                                color[2]))
        self.pbrtFile.write('      "color scale" [ %s %s %s ]\n' % (energy,
                                                                    energy,
                                                                    energy))
        self.pbrtFile.write('      "point from" [ %s %s %s ]\n' %
                            (loc[0], loc[1], loc[2]))
        # AttributeEnd
        self.lxsFile.write("  AttributeEnd\n")
        self.pbrtFile.write("  AttributeEnd\n")

    def writeRing(self, name, transform, info, mat):
        self.writeCommonPrimitive(name, transform, transform, mat)
        # NamedMaterial
        matName = mat.name
        self.lxsFile.write('    NamedMaterial "%s"\n' % matName)
        self.pbrtFile.write('    NamedMaterial "%s"\n' % matName)
        if mat.emit != 0.0:
            # LightGroup
            self.lxsFile.write('    LightGroup "%s"\n' % matName)
            # AreaLightSource
            self.lxsFile.write('    AreaLightSource "%s"\n' % "area")
            self.pbrtFile.write('    AreaLightSource "%s"\n' % "area")
            power = mat.emit
            self.lxsFile.write('      "float power" [ %s ]\n' % power)
            L = mat.diffuse_color
            self.lxsFile.write('      "color L" [ %s %s %s ]\n' %
                               (L[0], L[1], L[2]))
            self.pbrtFile.write('      "color L" [ %s %s %s ]\n' %
                               (L[0] * power, L[1] * power, L[2] * power))
        # disk
        obj = bpy.data.objects[name]
        p1 = obj.data.splines[0].points[0].co
        p2 = obj.data.splines[0].points[8].co
        radius = math.fabs(p2.y)
        innerradius = math.fabs(p1.y)
        if innerradius > radius:
            tmp = innerradius
            innerradius = radius
            radius = tmp
        # lxs
        self.lxsFile.write('    Shape "disk"\n')
        self.lxsFile.write('      "float height" [ 0 ]\n')
        self.lxsFile.write('      "float innerradius" [ %s ]\n' %
                           (self.scale_length * innerradius))
        self.lxsFile.write('      "float radius" [ %s ]\n' %
                           (self.scale_length * radius))
        self.lxsFile.write('      "float phimax" [ 360 ]\n')
        # pbrt
        self.pbrtFile.write('    Shape "disk"\n')
        self.pbrtFile.write('      "float height" [ 0 ]\n')
        self.pbrtFile.write('      "float innerradius" [ %s ]\n' %
                           (self.scale_length * innerradius))
        self.pbrtFile.write('      "float radius" [ %s ]\n' %
                           (self.scale_length * radius))
        self.pbrtFile.write('      "float phimax" [ 360 ]\n')
        # AttributeEnd
        self.lxsFile.write("  AttributeEnd\n")
        self.pbrtFile.write("  AttributeEnd\n")

    def writeSphere(self, name, transform, info, mat):
        loc, rot, scale = transform.decompose()
        if mat.emit != 0.0:
            # detect scale
            if scale[0] != 1.0 or scale[1] != 1.0 or scale[2] != 1.0:
                print("WARNING: scale = %s, apply to radius of sphere (for PBRT)" % scale)
                # take scaling out of the transform matrix
                print("BEFORE: ", transform)
                transform_copy = transform.copy()
                transform_copy[0][0] = transform_copy[0][0] * (1.0 / scale[0])
                transform_copy[1][1] = transform_copy[1][1] * (1.0 / scale[1])
                transform_copy[2][2] = transform_copy[2][2] * (1.0 / scale[2])
                print("AFTER: ", transform_copy)
                self.writeCommonPrimitive(name, transform, transform_copy, mat)
            else:
                self.writeCommonPrimitive(name, transform, transform, mat)
        else:
            self.writeCommonPrimitive(name, transform, transform, mat)
        # NamedMaterial
        matName = mat.name
        self.lxsFile.write('    NamedMaterial "%s"\n' % matName)
        self.pbrtFile.write('    NamedMaterial "%s"\n' % matName)
        if mat.emit != 0.0:
            # LightGroup
            self.lxsFile.write('    LightGroup "%s"\n' % matName)
            # AreaLightSource
            self.lxsFile.write('    AreaLightSource "%s"\n' % "area")
            self.pbrtFile.write('    AreaLightSource "%s"\n' % "area")
            power = mat.emit
            self.lxsFile.write('      "float power" [ %s ]\n' % power)
            L = mat.diffuse_color
            self.lxsFile.write('      "color L" [ %s %s %s ]\n' %
                               (L[0], L[1], L[2]))
            self.pbrtFile.write('      "color L" [ %s %s %s ]\n' %
                               (L[0] * power, L[1] * power, L[2] * power))
        # sphere
        # lxs
        self.lxsFile.write('    Shape "sphere"\n')
        self.lxsFile.write('      "float radius" [ %s ]\n' % self.scale_length)
        self.lxsFile.write('      "float zmin" [ -%s ]\n' % self.scale_length)
        self.lxsFile.write('      "float zmax" [ %s ]\n' % self.scale_length)
        self.lxsFile.write('      "float phimax" [ 360 ]\n')
        # pbrt
        self.pbrtFile.write('    Shape "sphere"\n')
        if mat.emit != 0.0:
            # detect scale
            if scale[0] != 1.0 or scale[1] != 1.0 or scale[2] != 1.0:
                # assume scale[0] == scale[1] == scale[2]
                print("DEBUG: scaling radius to %s" % (float(self.scale_length) * scale[0]))
                self.pbrtFile.write('      "float radius" [ %s ]\n' %
                                    (float(self.scale_length) * scale[0]))
                self.pbrtFile.write('      "float zmin" [ -%s ]\n' %
                                    (float(self.scale_length) * scale[0]))
                self.pbrtFile.write('      "float zmax" [ %s ]\n' %
                                    (float(self.scale_length) * scale[0]))
            else:
                self.pbrtFile.write('      "float radius" [ %s ]\n' % self.scale_length)
                self.pbrtFile.write('      "float zmin" [ -%s ]\n' % self.scale_length)
                self.pbrtFile.write('      "float zmax" [ %s ]\n' % self.scale_length)
        else:
            self.pbrtFile.write('      "float radius" [ %s ]\n' % self.scale_length)
            self.pbrtFile.write('      "float zmin" [ -%s ]\n' % self.scale_length)
            self.pbrtFile.write('      "float zmax" [ %s ]\n' % self.scale_length)
        self.pbrtFile.write('      "float phimax" [ 360 ]\n')
        # AttributeEnd
        self.lxsFile.write("  AttributeEnd\n")
        self.pbrtFile.write("  AttributeEnd\n")

    def writeSunLight(self, name, transform, info):
        self.lxsFile.write('  # %s\n' % name)
        self.pbrtFile.write('  # %s\n' % name)
        # info
        color = info[0]
        energy = info[1]
        falloff = info[2]
        use_diffuse = info[3][0]
        use_specular = info[3][1]
        # lamp data
        lamp = bpy.data.lamps[name]
        # AttributeBegin
        self.lxsFile.write("  AttributeBegin\n")
        self.pbrtFile.write("  AttributeBegin\n")
        # LightGroup
        self.lxsFile.write('    LightGroup "%s"\n' % name)
        # LightSource
        gain = 1.0
        importance = 1.0
        nsamples = 1
        turbidity = lamp.sky.atmosphere_turbidity
        # see export_arnold_ass.py for sundir calculation
        m = transform
        sunPos, rot, scale = m.decompose()
        obj = bpy.data.objects[name]
        # check for a constraint
        try:
            constraint = obj.constraints[0]
            if constraint.type == 'TRACK_TO':
                compass = constraint.target
                m = compass.matrix_world.copy()
                compassPos, rot, scale = m.decompose()
                sundir = [sunPos[0] - compassPos[0],
                          sunPos[1] - compassPos[1],
                          sunPos[2] - compassPos[2]]
                sundirLen = math.sqrt((sundir[0] * sundir[0] +
                                       sundir[1] * sundir[1] +
                                       sundir[2] * sundir[2]))
                sundir[0] = sundir[0] / sundirLen
                sundir[1] = sundir[1] / sundirLen
                sundir[2] = sundir[2] / sundirLen
        except IndexError:
            print("TODO: writeSunLight() without constraint")
            v_from = mathutils.Vector((0.0, 0.0, 0.0))
            v_from = transform * v_from
            # dir = v_from - v_to (with v_to = mathutils.Vector((0.0, 0.0, 0.0)))
            sundir = [v_from[0], v_from[1], v_from[2]]
            compassPos = [0.0, 0.0, 0.0]
        # lxs
        self.lxsFile.write('    LightSource "sunsky2"\n')
        self.lxsFile.write('      "float gain" [ %s ]\n' % gain)
        self.lxsFile.write('      "float importance" [ %s ]\n' % importance)
        self.lxsFile.write('      "integer nsamples" [ %s ]\n' % nsamples)
        self.lxsFile.write('      "float turbidity" [ %s ]\n' % turbidity)
        self.lxsFile.write('      "vector sundir" [ %s %s %s ]\n' %
                           (sundir[0], sundir[1], sundir[2]))
        # pbrt
        self.pbrtFile.write('    LightSource "distant"\n')
        self.pbrtFile.write('      "color L" [ %s %s %s ]\n' %
                            (energy * color[0],
                             energy * color[1],
                             energy * color[2]))
        self.pbrtFile.write('      "point from" [ %s %s %s ]\n' %
                            (sunPos[0], sunPos[1], sunPos[2]))
        self.pbrtFile.write('      "point to" [ %s %s %s ]\n' %
                            (compassPos[0], compassPos[1], compassPos[2]))
        # AttributeEnd
        self.lxsFile.write("  AttributeEnd\n")
        self.pbrtFile.write("  AttributeEnd\n")

    def writeTexture(self, name, param, tex_type, tex_path):
        basename = os.path.basename(tex_path)
        # lxs
        self.lxsFile.write('  Texture "%s::%s" ' %
                           (name, param) +
                           '"%s" "imagemap"\n' % tex_type)
        self.lxsFile.write('    "string wrap" ' +
                           '["repeat"]\n')
        self.lxsFile.write('    "string ' +
                           'filename" ' +
                           '["%s"]\n' % basename)
        self.lxsFile.write('    "float gamma" ' +
                           '[2.2]\n')
        self.lxsFile.write('    "float gain" ' +
                           '[1.000000]\n')
        self.lxsFile.write('    "string ' +
                           'filtertype" ' +
                           '["bilinear"]\n')
        self.lxsFile.write('    "string mapping" ' +
                           '["uv"]\n')
        self.lxsFile.write('    "float uscale" ' +
                           '[1.000000]\n')
        self.lxsFile.write('    "float vscale" ' +
                           '[-1.000000]\n')
        self.lxsFile.write('    "float udelta" ' +
                           '[0.000000]\n')
        self.lxsFile.write('    "float vdelta" ' +
                           '[0.000000]\n')
        # pbrt
        self.pbrtFile.write('  Texture "%s::%s" ' %
                           (name, param) +
                           '"%s" "imagemap"\n' % tex_type)
        self.pbrtFile.write('    "string ' +
                           'filename" ' +
                           '["textures/%s"]\n' % basename)
        self.pbrtFile.write('    "bool gamma" ' +
                            '["true"]\n') # TODO: decide if true or false

    def finishExport(self, sun = None):
        # WorldEnd
        self.lxsFile.write("WorldEnd\n")
        self.pbrtFile.write("WorldEnd\n")
        # close
        self.lxsFile.close()
        self.pbrtFile.close()
