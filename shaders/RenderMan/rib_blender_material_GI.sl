surface rib_blender_material_GI(float Ka = 1;
                                float Kd = .5;
                                float Ks = .5;
                                color Ks_color = 1;
                                float specular_hardness = 50.;
                                float Kr = 0;
                                color Kr_color = 1;
                                float Kt = 0;
                                float IOR = 1;
                                string texturename = "";
                                output varying normal aov_N = 0;
                                output varying point aov_P = 0;
                                output varying color aov_direct_diffuse = 0;
                                output varying color aov_direct_specular = 0;
                                output varying color aov_indirect_diffuse = 0;
                                output varying color aov_reflection = 0;
                                output varying color aov_refraction = 0;)
{
	normal Nf;
  vector In;
	vector V;
	color Ct;

  normal Nn = normalize(N);
	aov_N = transform("current", "world", Nn);
	aov_P = transform("current", "world", P);
	Nf = faceforward(Nn, I);
	In = normalize(I);
	V = -In;

	Ct = ( texturename != "" ) ?
		texture( texturename, mod(s, 1.0), mod(t, 1.0), "filter", "gaussian" ) : 1.0;

	Oi = Os;
  color diff = diffuse(Nf);
  aov_direct_diffuse = Cs * Ct * Kd * diff;
  color spec = 0;
  vector H;
  illuminance(P, Nf, PI/2) 
    {
      H = normalize(L) + V;
      H = normalize(H);
      spec += Cl * pow(max(0.0, Nf.H), specular_hardness);
    }
  aov_direct_specular = Ks_color * Ks * spec;
  vector R = reflect(In, Nf);
  color reflection = trace(P, R);
  aov_reflection = Kr_color * Kr * reflection;
  color refraction = 0;
	float eta = (In.Nn < 0.0) ? 1.0 / IOR : IOR;
  if (IOR != 1.0) {
    vector refr_dir = refract(In, Nf, eta);
    refraction = trace(P, refr_dir);
    aov_refraction = Kt * refraction;
  }
  float samples = 64;
  float maxdist = 151.0;
  string envmap = "";
  string envspace = "world";
  float environment_intensity = 1.0;
  color environment_color_gain = 1;
  varying color out_visibility = 1;
  color sampling_weight = 1;
  varying color out_environment_diffuse = 0;
  varying color out_color_bleeding = 0;
  aov_indirect_diffuse = trace(P, Nn,
                               "raytype", "diffuse",
                               "samples", samples,
                               "bsdf", "cosine",
                               "transmission", out_visibility,
                               "maxdist", maxdist,
                               "weight", sampling_weight,
                               "environment:tint", environment_color_gain * environment_intensity,
                               "environment:space", envspace,
                               "environment:map", envmap,
                               "environmentcontribution", out_environment_diffuse );
  out_color_bleeding = aov_indirect_diffuse - out_environment_diffuse;
  aov_indirect_diffuse = Cs * Ct * Kd * 0.1 * aov_indirect_diffuse;
	Ci = (Cs * Ct * (Ka * ambient() + Kd * diff) +
        aov_direct_specular + aov_reflection + aov_refraction + aov_indirect_diffuse);

	/* Ci *= Oi; */
}
