bl_info = {
    "name": "mental ray (Neuray/iray) MI format",
    "author": "Jan Walter",
    "blender": (2, 6, 2),
    "api": 35622,
    "location": "File > Import-Export",
    "description": "Export a scene in mental ray (neuray/iray) MI file format",
    "warning": "",
    "wiki_url": "",
    "tracker_url": "",
    "category": "Import-Export"}

if "bpy" in locals():
    import imp
    if "export_mental_ray" in locals():
        imp.reload(export_mental_ray)

import bpy
from bpy.props import BoolProperty, StringProperty, EnumProperty
from bpy_extras.io_utils import ExportHelper

class ExportMrMi(bpy.types.Operator, ExportHelper):
    bl_idname = "export_scene.mi"
    bl_label = "Export neuray/iray MI"
    bl_options = {'PRESET'}
    filename_ext = ".mi"
    filter_glob = StringProperty(default = "*.mi", options = {'HIDDEN'})

    # operator properties
    opt_selection = BoolProperty(name = "Selection Only", 
                                 description = "Export selected objects only", 
                                 default = False)
    opt_renderer = EnumProperty(items = [('iray', 
                                          'iray', 
                                          'iray'),
                                         ('mental_ray', 
                                          'mental_ray', 
                                          'mental_ray')],
                                name = "Select renderer",
                                description = ("Select the renderer being " +
                                               "used"),
                                default = 'mental_ray')
    opt_global_surface_shader = EnumProperty(items = [('none', 
                                                       'none', 
                                                       'none'),
                                                      ('ambient_occlusion', 
                                                       'ambient_occlusion',
                                                       'ambient_occlusion'),
                                                      ('depth_fade', 
                                                       'depth_fade',
                                                       'depth_fade'),
                                                      ('front_bright', 
                                                       'front_bright',
                                                       'front_bright'),
                                                      ('normals_as_colors', 
                                                       'normals_as_colors',
                                                       'normals_as_colors'),
                                                      ('one_color', 
                                                       'one_color',
                                                       'one_color'),
                                                      ('show_u', 
                                                       'show_u',
                                                       'show_u'),
                                                      ('show_uv', 
                                                       'show_uv',
                                                       'show_uv'),
                                                      ('show_v', 
                                                       'show_v',
                                                       'show_v')],
                                             name = "Global surface",
                                             description = ("Replace surface "
                                                            + "shaders " +
                                                            "globally"),
                                             default = 'none')
    opt_geometry = BoolProperty(name = "Output Geometry", 
                                description = "Write geomtry MI file (large)", 
                                default = True)
    opt_binary = BoolProperty(name = "Output Binary", 
                              description = 
                              "Write geomtry MI file partially binary", 
                              default = True)
    opt_textures = BoolProperty(name = "Gather Textures", 
                                description = 
                                "Gather textures within a local subdirectory", 
                                default = True)
    opt_lightprofiles = BoolProperty(name = "Gather LightProfiles", 
                                     description = 
                                     ("Gather light profiles within a local " +
                                      "subdirectory"), 
                                     default = True)
    opt_render = BoolProperty(name = "Start renderer", 
                              description = "Start renderer after export",
                              default = True)
    opt_ambient_fb = BoolProperty(name = "AMBIENT", 
                                  description = "Framebuffer AMBIENT",
                                  default = True)
    opt_occlusion_fb = BoolProperty(name = "OCCLUSION", 
                                    description = "Framebuffer OCCLUSION",
                                    default = False)
    opt_diffuse_fb = BoolProperty(name = "DIFFUSE", 
                                  description = "Framebuffer DIFFUSE",
                                  default = True)
    opt_specular_fb = BoolProperty(name = "SPECULAR", 
                                   description = "Framebuffer SPECULAR",
                                   default = True)
    opt_reflect_fb = BoolProperty(name = "REFLECT", 
                                  description = "Framebuffer REFLECT",
                                  default = True)
    opt_depth_fb = BoolProperty(name = "DEPTH", 
                                description = "Framebuffer DEPTH",
                                default = True)
    opt_normals_fb = BoolProperty(name = "NORMALS", 
                                  description = "Framebuffer NORMALS",
                                  default = True)
    opt_shadows_fb = BoolProperty(name = "SHADOWS", 
                                  description = "Framebuffer SHADOWS",
                                  default = True)

    def execute(self, context):
        keywords = self.as_keywords(ignore = (
                "check_existing", 
                "filter_glob"))
        from . import export_mental_ray
        return export_mental_ray.save(self, context, **keywords)

def menu_func(self, context):
    self.layout.operator(ExportMrMi.bl_idname, text = "mental ray MI (.mi)")

def register():
    bpy.utils.register_module(__name__)
    bpy.types.INFO_MT_file_export.append(menu_func)

def unregister():
    bpy.utils.unregister_module(__name__)
    bpy.types.INFO_MT_file_export.remove(menu_func)

if __name__ == "__main__":
    register()
