class CommonExporterInterface:
    def __init__(self, name, options):
        # name
        self.name = name
        # options
        self.options = options
        # scene
        self.scene = None
        # motion blur related
        self.use_motion_blur = False
        self.shutter_start = 0.0
        self.shutter_end = 0.0
        self.motion_blur_samples = 0

    def prepareExport(self, scene, bbox, directory, name, mblur, light_counter,
                      sun = None):
        print("TODO: %s.prepareExport()" % self.name)

    def writeAreaLight(self, name, transform, info):
        print("TODO: %s.writeAreaLight()" % self.name)

    def writeCamera(self, name, lens, angle, resolution, border, AA_samples,
                    tile_size, transform, isRenderCamera, sun = None):
        print("TODO: %s.writeCamera()" % self.name)

    def writeCone(self, name, transform, info, mat):
        print("TODO: %s.writeCone()" % self.name)

    def writeCylinder(self, name, transform, info, mat):
        print("TODO: %s.writeCylinder()" % self.name)

    def writeHdrLighting(self, hdrImagePath):
        print("TODO: %s.writeHdrLighting()" % self.name)

    def writeMesh(self, name, transform, info, user_matrix):
        print("TODO: %s.writeMesh()" % self.name)

    def writeNurbsSurface(self, name, transform, info):
        print("TODO: %s.writeNurbsSurface()" % self.name)

    def writePointLight(self, name, transform, info):
        print("TODO: %s.writePointLight()" % self.name)

    def writeRing(self, name, transform, info, mat):
        print("TODO: %s.writeRing()" % self.name)

    def writeSphere(self, name, transform, info, mat):
        print("TODO: %s.writeSphere()" % self.name)

    def writeSpotLight(self, name, transform, info):
        print("TODO: %s.writeSpotLight()" % self.name)

    def writeSunLight(self, name, transform, info):
        print("TODO: %s.writeSunLight()" % self.name)

    def finishExport(self, sun = None):
        print("TODO: %s.finishExport()" % self.name)
