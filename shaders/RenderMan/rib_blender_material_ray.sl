surface  rib_blender_material_ray(float Ka = 1;
                                  float Kd = .5;
                                  float Ks = .5;
                                  color Ks_color = 1;
                                  float specular_hardness = 50.;
                                  float Kr = 0;
                                  color Kr_color = 1;
                                  float Kt = 0;
                                  float IOR = 1;
                                  string texturename = "";
                                  output varying normal aov_N = 0;
                                  output varying point aov_P = 0;
                                  output varying color aov_direct_diffuse = 0;
                                  output varying color aov_direct_specular = 0;
                                  output varying color aov_reflection = 0;
                                  output varying color aov_refraction = 0;)
{
	normal Nf;
  vector In;
	vector V;
	color Ct;

  normal Nn = normalize(N);
	aov_N = transform("current", "world", Nn);
	aov_P = transform("current", "world", P);
	Nf = faceforward(Nn, I);
	In = normalize(I);
	V = -In;

	Ct = ( texturename != "" ) ?
		texture( texturename, "filter", "gaussian" ) : 1.0;

	Oi = Os;
  color diff = diffuse(Nf);
  aov_direct_diffuse = Cs * Ct * Kd * diff;
  color spec = 0;
  vector H;
  illuminance(P, Nf, PI/2) 
    {
      H = normalize(L) + V;
      H = normalize(H);
      spec += Cl * pow(max(0.0, Nf.H), specular_hardness);
    }
  aov_direct_specular = Ks_color * Ks * spec;
  vector refl_dir = reflect(In, Nf);
  color reflection = trace(P, refl_dir);
  aov_reflection = Kr_color * Kr * reflection;
  color refraction = 0;
	float eta = (In.Nn < 0.0) ? 1.0 / IOR : IOR;
  if (IOR != 1.0) {
    vector refr_dir = refract(In, Nf, eta);
    refraction = trace(P, refr_dir);
    aov_refraction = Kt * refraction;
  }
	Ci = (Cs * Ct * (Ka * ambient() + Kd * diff) +
        aov_direct_specular + aov_reflection + aov_refraction);

	/* Ci *= Oi; */
}
