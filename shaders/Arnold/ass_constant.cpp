#include <ai.h>
#include <cstring> // strcpy
// #include <stdio.h> // printf

AI_SHADER_NODE_EXPORT_METHODS(AssConstantMethods)

enum AssConstantParams
{
  p_Color,
  p_aov_emission
};

node_parameters
{
  AiParameterRGB("Color", 1.0f, 1.0f, 1.0f);
  AiParameterStr ("aov_emission", "emission");
}

node_initialize
{
}

node_update
{
  AiAOVRegister(AiNodeGetStr(node, "aov_emission"),
                AI_TYPE_RGB, AI_AOV_BLEND_OPACITY);
}

node_finish
{
}

shader_evaluate
{
  AtColor Color = AiShaderEvalParamRGB(p_Color);
  sg->out.RGB = Color;
  AiAOVSetRGB(sg, AiShaderEvalParamStr(p_aov_emission), Color);
}

node_loader
{
  if (i > 0) return false;

  node->methods = AssConstantMethods;
  node->output_type = AI_TYPE_RGB;
  node->name = "ass_constant";
  node->node_type = AI_NODE_SHADER;
  strcpy(node->version, AI_VERSION);
  return true;
}
