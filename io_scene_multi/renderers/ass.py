import os
import math
import ctypes
import platform
# common exporter interface
from ..common.interface import CommonExporterInterface
from ..common.unique import UniqueNames
# Arnold
import arnold
# Blender
import bpy

class AssExporter(CommonExporterInterface):
    # use Blender's ID prefix (see DNA_ID.h) for names
    def __init__(self, options):
        CommonExporterInterface.__init__(self, "AssExporter", options)
        self.filename = None
        self.plugin_searchpath = None
        self.usedMaterials = {}
        # unique names
        self.uniqueNames = UniqueNames()

    def uniqueName(self, proposal):
        return self.uniqueNames.uniqueName(proposal)

    def ccp(self, aString): # ccp = create_char_p
        ba = bytearray(aString, "utf8")
        sb = ctypes.create_string_buffer(len(aString)+1) # len + 1 !!!
        for i in range(len(aString)):
            sb[i] = ba[i]
        return sb

    def createMatrix(self, node, m, noScale = False):
        am = arnold.AtMatrix()
        arnold.AiM4Identity(am)
        if noScale:
            am[0][0] = 1.0
        else:
            am[0][0] = m[0][0]
        am[0][1] = m[1][0]
        am[0][2] = m[2][0]
        am[0][3] = m[3][0]
        am[1][0] = m[0][1]
        if noScale:
            am[1][1] = 1.0
        else:
            am[1][1] = m[1][1]
        am[1][2] = m[2][1]
        am[1][3] = m[3][1]
        am[2][0] = m[0][2]
        am[2][1] = m[1][2]
        if noScale:
            am[2][2] = 1.0
        else:
            am[2][2] = m[2][2]
        am[2][3] = m[3][2]
        am[3][0] = self.scale_length * m[0][3]
        am[3][1] = self.scale_length * m[1][3]
        am[3][2] = self.scale_length * m[2][3]
        am[3][3] = m[3][3]
        arnold.AiNodeSetMatrix(node, "matrix", am)

    def createUserMatrix(self, node, m):
        # declare Pref_matrix as constant MATRIX
        worked = arnold.AiNodeDeclare(node, "Pref_matrix", "constant MATRIX")
        if (worked):
            am = arnold.AtMatrix()
            arnold.AiM4Identity(am)
            am[0][0] = m[0][0]
            am[0][1] = m[1][0]
            am[0][2] = m[2][0]
            am[0][3] = m[3][0]
            am[1][0] = m[0][1]
            am[1][1] = m[1][1]
            am[1][2] = m[2][1]
            am[1][3] = m[3][1]
            am[2][0] = m[0][2]
            am[2][1] = m[1][2]
            am[2][2] = m[2][2]
            am[2][3] = m[3][2]
            am[3][0] = self.scale_length * m[0][3]
            am[3][1] = self.scale_length * m[1][3]
            am[3][2] = self.scale_length * m[2][3]
            am[3][3] = m[3][3]
            arnold.AiNodeSetMatrix(node, "Pref_matrix", am)
        else:
            print("WARNING: createUserMatrix(%s, %s) failed" % (node, m))

    def prepareExport(self, scene, bbox, directory, name, mblur, light_counter,
                      sun = None):
        self.filename = os.path.join(directory, name + ".ass")
        # scene related
        self.scene = scene
        self.bbox = bbox
        if scene.unit_settings.system != 'METRIC':
            print("WARNING: unit_settings == '%s'" % scene.unit_settings.system)
        self.scale_length = scene.unit_settings.scale_length
        # light related
        self.light_counter = light_counter
        # motion blur related
        self.use_motion_blur = mblur[0]
        self.shutter_start = mblur[1]
        self.shutter_end = mblur[2]
        self.motion_blur_samples = mblur[3]
        self.animatedObjects = mblur[4]
        self.motionMatrices = mblur[5]
        # delete .ass file (if it exists already)
        if os.path.exists(self.filename):
            os.remove(self.filename)
        # AiSetAppString
        appString = "Blender %s.%s.%s" % bpy.app.version
        sb = appString
        arnold.AiSetAppString(sb)
        # AiBegin
        arnold.AiBegin()

    def writeAreaLight(self, name, transform, info):
        # light_counter
        self.light_counter += 1
        # info
        color = info[0]
        energy = info[1]
        falloff = info[2]
        use_diffuse = info[3][0]
        use_specular = info[3][1]
        # quad_light
        quad_light = arnold.AiNode("quad_light")
        # name
        arnold.AiNodeSetStr(quad_light, "name",
                            "LA" + # ID_LA
                            name)
        # decay_type
        # values: constant quadratic
        arnold.AiNodeSetInt(quad_light,
                            "decay_type",
                            1)
        # one matrix
        m = transform
        self.createMatrix(quad_light, m)
        # color
        arnold.AiNodeSetRGB(quad_light, "color",
                            color[0],
                            color[1],
                            color[2])
        # intensity
        arnold.AiNodeSetFlt(quad_light,
                            "intensity",
                            energy * math.pi)
        # diffuse?
        arnold.AiNodeSetBool(quad_light,
                             "affect_diffuse",
                             use_diffuse)
        # specular?
        arnold.AiNodeSetBool(quad_light,
                             "affect_specular",
                             use_specular)
        # portal?
        obj = bpy.data.objects[name]
        try:
            is_portal = obj.data.cycles.is_portal
        except AttributeError:
            pass
        else:
            arnold.AiNodeSetBool(quad_light,
                                 "portal",
                                 is_portal)
        # vertices
        if obj.data.shape == 'RECTANGLE':
            sizeX = obj.data.size   / 2.0
            sizeY = obj.data.size_y / 2.0
        else:
            sizeX = obj.data.size / 2.0
            sizeY = obj.data.size / 2.0
        verts = [[-sizeX,  sizeY, 0.0],
                 [ sizeX,  sizeY, 0.0],
                 [ sizeX, -sizeY, 0.0],
                 [-sizeX, -sizeY, 0.0]]
        vertices = arnold.AiArrayAllocate(len(verts), 1,
                                          arnold.AI_TYPE_POINT)
        numVertexIndices = 0
        for v in verts:
            arnold.AiArraySetVec(vertices, numVertexIndices,
                                 arnold.AtPoint(self.scale_length * v[0],
                                                self.scale_length * v[1],
                                                self.scale_length * v[2]))
            numVertexIndices += 1
        arnold.AiNodeSetArray(quad_light, "vertices", vertices)

    def writeRaySwitchShader(self, mat, name):
        # return values
        shader = None
        opaque = True
        # ray_switch_rgba
        shader = arnold.AiNode("ray_switch_rgba")
        if shader == None:
            print("WARNING: %s" %
                  "'arnold.AiNode(ccp(\"ray_switch_rgba\"))' failed")
        else:
            # name
            arnold.AiNodeSetStr(shader,
                                "name",
                                "MA" + # ID_MA
                                mat.name)
            # camera rays
            color = mat.diffuse_color
            arnold.AiNodeSetRGBA(shader,
                                 "camera",
                                 color[0] * mat.emit,
                                 color[1] * mat.emit,
                                 color[2] * mat.emit,
                                 1.0)
            # set all others to 0 0 0 0
            arnold.AiNodeSetRGBA(shader,
                                 "shadow",
                                 0.0, 0.0, 0.0, 1.0)
            arnold.AiNodeSetRGBA(shader,
                                 "specular_reflection",
                                 color[0] * mat.emit,
                                 color[1] * mat.emit,
                                 color[2] * mat.emit,
                                 1.0)
            arnold.AiNodeSetRGBA(shader,
                                 "specular_transmission",
                                 color[0] * mat.emit,
                                 color[1] * mat.emit,
                                 color[2] * mat.emit,
                                 1.0)
            arnold.AiNodeSetRGBA(shader,
                                 "diffuse_reflection",
                                 0.0, 0.0, 0.0, 1.0)
            arnold.AiNodeSetRGBA(shader,
                                 "diffuse_transmission",
                                 0.0, 0.0, 0.0, 1.0)
        return shader, opaque

    def writeMeshLight(self, polymesh, mat, name):
        mesh_light = arnold.AiNode("mesh_light")
        # name
        arnold.AiNodeSetStr(mesh_light,
                            "name",
                            "MA" + # ID_MA
                            name + "_light")
        # mesh
        arnold.AiNodeSetPtr(mesh_light,
                            "mesh",
                            polymesh)
        # color
        color = mat.diffuse_color
        arnold.AiNodeSetRGB(mesh_light,
                            "color",
                            color[0], color[1], color[2])
        # intensity
        arnold.AiNodeSetFlt(mesh_light,
                            "intensity",
                            mat.emit)
        # samples
        arnold.AiNodeSetInt(mesh_light, "samples", 2)

    def writeCamera(self, name, lens, angle, resolution, border, AA_samples,
                    tile_size, transform, isRenderCamera, sun = None):
        # persp_camera
        camera = arnold.AiNode("persp_camera")
        if isRenderCamera:
            options = arnold.AiUniverseGetOptions()
            arnold.AiNodeSetPtr(options, "camera", camera)
            # provide a default shader for e.g. options.shader_override (see issue #56)
            shader = arnold.AiNode("standard_surface")
            arnold.AiNodeSetStr(shader,
                                "name",
                                "MA" + # ID_MA
                                "default") # call it MAdefault
        # name
        arnold.AiNodeSetStr(camera,
                            "name",
                            "CA" + # ID_CA
                            name)
        aspect = resolution[0] / float(resolution[1])
        if aspect >= 1.0:
            fov = math.degrees(angle)
        else:
            fov = 2 * math.degrees(math.atan((aspect * 16.0) / lens))
        arnold.AiNodeSetFlt(camera, "fov", fov)
        obj = bpy.data.objects[name]
        data = obj.data
        arnold.AiNodeSetBool(camera, "flat_field_focus", False)
        arnold.AiNodeSetFlt(camera, "focus_distance", data.dof_distance)
        arnold.AiNodeSetFlt(camera, "aperture_size", 0.0)
        m = transform
        self.createMatrix(camera, m)
        if self.use_motion_blur:
            arnold.AiNodeSetFlt(camera, "shutter_start", 0.0)
            # dont use self.shutter_end !!!
            arnold.AiNodeSetFlt(camera, "shutter_end", 1.0)
        if isRenderCamera:
            # count AOVs
            numOutputs = 1 # beauty
            if self.options.aov_Z:
                numOutputs += 1
            if self.options.aov_N:
                numOutputs += 1
            if self.options.aov_P:
                numOutputs += 1
            if self.options.aov_emission:
                numOutputs += 1
            if self.options.aov_direct_diffuse:
                numOutputs += 1
            if self.options.aov_direct_specular:
                numOutputs += 1
            if self.options.aov_reflection:
                numOutputs += 1
            if self.options.aov_refraction:
                numOutputs += 1
            if self.options.aov_indirect_diffuse:
                numOutputs += 1
            if self.options.aov_indirect_specular:
                numOutputs += 1
            # physicalsky
            if self.options.lighting == 'no_lights' and sun:
                useSky = False
                try:
                    sun_data = bpy.data.lamps['sun']
                except KeyError:
                    try:
                        sun_data = bpy.data.lamps['Sun']
                    except KeyError:
                        useSun = False
                        useSky = False
                    else:
                        useSun = True
                        useSky = sun_data.sky.use_sky
                else:
                    useSun = True
                    useSky = sun_data.sky.use_sky
                if useSun:
                    try:
                        sun = bpy.data.objects[sun_data.name]
                    except KeyError:
                        print("WARNING: No object called \"%s\" found" %
                              sun_data.name)
                    else:
                        m = sun.matrix_world.copy()
                        # UGLY: works only because getInfoLamp(...)
                        # uses no self !!!
                        info = MultiExporter.getInfoLamp(MultiExporter, sun)
                        if sun and sun.data.type == 'SUN':
                            self.writeSunLight("sun", m, info)
            # options
            options = arnold.AiUniverseGetOptions()
            # anti-aliasing
            arnold.AiNodeSetInt(options, "AA_samples", AA_samples) # ignore !!!
            arnold.AiNodeSetInt(options, "AA_samples", 2)
            # outputs
            outputs = arnold.AiArrayAllocate(numOutputs, 1,
                                             arnold.AI_TYPE_STRING)
            outIndex = 0
            # beauty
            arnold.AiArraySetStr(outputs, outIndex,
                                 "RGBA RGBA filter driver")
            outIndex += 1
            # other (standard) AOVs
            if self.options.aov_Z:
                arnold.AiArraySetStr(outputs, outIndex,
                                     "Z FLOAT closest_filter driver")
                outIndex += 1
            if self.options.aov_N:
                arnold.AiArraySetStr(outputs, outIndex,
                                     "N VECTOR filter driver_N")
                outIndex += 1
            if self.options.aov_P:
                arnold.AiArraySetStr(outputs, outIndex,
                                     "P POINT filter driver_P")
                outIndex += 1
            if self.options.aov_emission:
                arnold.AiArraySetStr(outputs, outIndex,
                                     "emission RGB filter %s" %
                                     "driver_emission")
                outIndex += 1
            if self.options.aov_direct_diffuse:
                arnold.AiArraySetStr(outputs, outIndex,
                                     "diffuse_direct RGB filter %s" %
                                     "driver_diffuse_direct")
                outIndex += 1
            if self.options.aov_direct_specular:
                arnold.AiArraySetStr(outputs, outIndex,
                                     "specular_direct RGB filter %s" %
                                     "driver_specular_direct")
                outIndex += 1
            # if self.options.aov_reflection:
            #     arnold.AiArraySetStr(outputs, outIndex,
            #                          "reflection RGB filter %s" %
            #                          "driver_reflection")
            #     outIndex += 1
            if self.options.aov_refraction:
                arnold.AiArraySetStr(outputs, outIndex,
                                     "transmission_direct RGB filter %s" %
                                     "driver_transmission_direct")
                outIndex += 1
                arnold.AiArraySetStr(outputs, outIndex,
                                     "transmission_indirect RGB filter %s" %
                                     "driver_transmission_indirect")
                outIndex += 1
            if self.options.aov_indirect_diffuse:
                arnold.AiArraySetStr(outputs, outIndex,
                                     "diffuse_indirect RGB filter %s" %
                                     "driver_diffuse_indirect")
                outIndex += 1
            if self.options.aov_indirect_specular:
                arnold.AiArraySetStr(outputs, outIndex,
                                     "specular_indirect " +
                                     "RGB filter %s" %
                                     "driver_specular_indirect")
                outIndex += 1
            arnold.AiNodeSetArray(options, "outputs", outputs)
            # xres
            arnold.AiNodeSetInt(options, "xres", resolution[0])
            # yres
            arnold.AiNodeSetInt(options, "yres", resolution[1])
            # border?
            if border:
                border_min_x = border[0]
                border_max_x = border[1]
                border_min_y = border[2]
                border_max_y = border[3]
                region_min_x = int(border_min_x * resolution[0])
                region_max_x = int(border_max_x * resolution[0])
                region_min_y = int((1.0-border_max_y) * resolution[1])
                region_max_y = int((1.0-border_min_y) * resolution[1])
                arnold.AiNodeSetInt(options, "region_min_x", region_min_x)
                arnold.AiNodeSetInt(options, "region_min_y", region_min_y)
                arnold.AiNodeSetInt(options, "region_max_x", region_max_x)
                arnold.AiNodeSetInt(options, "region_max_y", region_max_y)
            # bucket_size
            arnold.AiNodeSetInt(options, "bucket_size", tile_size[0])
            # bucket_scanning
            arnold.AiNodeSetStr(options, "bucket_scanning", "hilbert")
            # plugin_searchpath
            ##self.plugin_searchpath = os.getenv("HOME")
            self.plugin_searchpath = "/Users/jan" # use OS X path
            shaders = "Graphics/Rendering/Arnold/shaders/current" # TMP
            self.plugin_searchpath = os.path.join(self.plugin_searchpath,
                                                  shaders)
            arnold.AiNodeSetStr(options, "plugin_searchpath",
                                self.plugin_searchpath)
            # GI parameters
            if self.options.lighting == 'global_illumination':
                arnold.AiNodeSetInt(options, "GI_diffuse_depth", 1) # default: 0
                arnold.AiNodeSetInt(options, "GI_specular_depth", 1) # default: 0
                arnold.AiNodeSetInt(options, "GI_transmission_depth", 3) # default: 2
                arnold.AiNodeSetInt(options, "GI_volume_depth", 1) # default: 0
                arnold.AiNodeSetInt(options, "GI_total_depth", 11) # default: 10
                arnold.AiNodeSetInt(options, "GI_diffuse_samples", 3) # default: 2
                arnold.AiNodeSetInt(options, "GI_specular_samples", 3) # default: 2
                arnold.AiNodeSetInt(options, "GI_transmission_samples", 3) # default: 2
                arnold.AiNodeSetInt(options, "GI_sss_samples", 1) # default: 2
                arnold.AiNodeSetInt(options, "GI_volume_samples", 1) # default: 2
            # filter(s)
            filter = arnold.AiNode("gaussian_filter")
            arnold.AiNodeSetStr(filter, "name", "filter")
            if self.options.aov_Z:
                closest_filter = arnold.AiNode("closest_filter")
                arnold.AiNodeSetStr(closest_filter, "name",
                                    "closest_filter")
            # driver(s)
            driver = arnold.AiNode("driver_exr")
            arnold.AiNodeSetStr(driver, "name", "driver")
            arnold.AiNodeSetStr(driver, "filename",
                                "arnold.exr")
            if self.options.aov_N:
                driver_N = arnold.AiNode("driver_exr")
                arnold.AiNodeSetStr(driver_N, "name",
                                    "driver_N")
                arnold.AiNodeSetStr(driver_N, "filename",
                                    "arnold_aov_N.exr")
            if self.options.aov_P:
                driver_P = arnold.AiNode("driver_exr")
                arnold.AiNodeSetStr(driver_P, "name",
                                    "driver_P")
                arnold.AiNodeSetStr(driver_P, "filename",
                                    "arnold_aov_P.exr")
            if self.options.aov_emission:
                driver_emission = arnold.AiNode("driver_exr")
                arnold.AiNodeSetStr(driver_emission, "name",
                                    "driver_emission")
                arnold.AiNodeSetStr(driver_emission, "filename",
                                    "arnold_aov_emission.exr")
            if self.options.aov_direct_diffuse:
                driver_diffuse_direct = arnold.AiNode("driver_exr")
                arnold.AiNodeSetStr(driver_diffuse_direct, "name",
                                    "driver_diffuse_direct")
                arnold.AiNodeSetStr(driver_diffuse_direct, "filename",
                                    "arnold_aov_diffuse_direct.exr")
            if self.options.aov_direct_specular:
                driver_specular_direct = arnold.AiNode("driver_exr")
                arnold.AiNodeSetStr(driver_specular_direct, "name",
                                    "driver_specular_direct")
                arnold.AiNodeSetStr(driver_specular_direct,
                                    "filename",
                                    "arnold_aov_specular_direct.exr")
            # if self.options.aov_reflection:
            #     driver_reflection = arnold.AiNode("driver_exr")
            #     arnold.AiNodeSetStr(driver_reflection, "name",
            #                         "driver_reflection")
            #     arnold.AiNodeSetStr(driver_reflection, "filename",
            #                         "arnold_aov_reflection.exr")
            if self.options.aov_refraction:
                driver_transmission_direct = arnold.AiNode("driver_exr")
                arnold.AiNodeSetStr(driver_transmission_direct, "name",
                                    "driver_transmission_direct")
                arnold.AiNodeSetStr(driver_transmission_direct, "filename",
                                    "arnold_aov_transmission_direct.exr")
                driver_transmission_indirect = arnold.AiNode("driver_exr")
                arnold.AiNodeSetStr(driver_transmission_indirect, "name",
                                    "driver_transmission_indirect")
                arnold.AiNodeSetStr(driver_transmission_indirect, "filename",
                                    "arnold_aov_transmission_indirect.exr")
            if self.options.aov_indirect_diffuse:
                driver_diffuse_indirect = arnold.AiNode("driver_exr")
                arnold.AiNodeSetStr(driver_diffuse_indirect, "name",
                                    "driver_diffuse_indirect")
                arnold.AiNodeSetStr(driver_diffuse_indirect,
                                    "filename",
                                    "arnold_aov_diffuse_indirect.exr")
            if self.options.aov_indirect_specular:
                driver_specular_indirect = arnold.AiNode("driver_exr")
                arnold.AiNodeSetStr(driver_specular_indirect, "name",
                                    "driver_specular_indirect")
                arnold.AiNodeSetStr(driver_specular_indirect,
                                    "filename",
                                    "arnold_aov_" +
                                    "specular_indirect.exr")

    def writeCommonPrimitive(self, name, transform, mat, primitive):
        # material
        mat = bpy.data.materials[mat.name]
        shader = arnold.AiNodeLookUpByName("MA" + # ID_MA
                                           mat.name)
        if shader == None:
            # for Cycles nodes we might have used a '_mix'
            shader = arnold.AiNodeLookUpByName("MA" + # ID_MA
                                               mat.name +
                                               "_mix")
        if shader == None:
            if self.options.lighting == 'no_lights' or self.light_counter == 0:
                shader, opaque = self.writeDefaultSurface(mat, name)
            else:
                shader, opaque = self.writeStandardShader(mat, name)
            self.usedMaterials[mat.name] = opaque
        else:
            opaque = self.usedMaterials[mat.name]
        if shader != None:
            arnold.AiNodeSetPtr(primitive, "shader", shader)
        arnold.AiNodeSetBool(primitive,
                             "opaque",
                             opaque)
        # one matrix
        m = transform
        self.createMatrix(primitive, m)

    def writeCone(self, name, transform, info, mat):
        obj = bpy.data.objects[name]
        p1 = obj.data.splines[0].points[0].co
        p2 = obj.data.splines[0].points[8].co
        # cone
        cone = arnold.AiNode("cone")
        # name
        arnold.AiNodeSetStr(cone,
                            "name",
                            "CU" + # ID_CU
                            self.uniqueName(name))
        arnold.AiNodeSetVec(cone,
                            "bottom", 0.0, 0.0,
                            self.scale_length * p1.z)
        arnold.AiNodeSetVec(cone,
                            "top", 0.0, 0.0,
                            self.scale_length * p2.z)
        arnold.AiNodeSetFlt(cone, # default is 0.5
                            "bottom_radius",
                            math.fabs(self.scale_length * p1.y))
        arnold.AiNodeSetFlt(cone, # default is 0.0
                            "top_radius",
                            math.fabs(self.scale_length * p2.y))
        primitive = cone
        self.writeCommonPrimitive(name, transform, mat, primitive)

    def writeCylinder(self, name, transform, info, mat):
        # cylinder
        cylinder = arnold.AiNode("cylinder")
        # name
        arnold.AiNodeSetStr(cylinder,
                            "name",
                            "CU" + # ID_CU
                            self.uniqueName(name))
        arnold.AiNodeSetVec(cylinder,
                            "bottom", 0.0, 0.0, 0.0)
        arnold.AiNodeSetVec(cylinder,
                            "top", 0.0, 0.0,
                            self.scale_length * 1.0)
        arnold.AiNodeSetFlt(cylinder, # default is 0.5
                            "radius",
                            self.scale_length * 1.0)
        primitive = cylinder
        self.writeCommonPrimitive(name, transform, mat, primitive)

    def writeDefaultSurface(self, mat, name):
        # return values
        shader = None
        opaque = True
        # defaultsurface
        diffuse_intensity = mat.diffuse_intensity
        Kd = diffuse_intensity
        Ka = 1.0 - Kd
        textures_found = 0
        textures = {}
        if mat:
            # check for textures
            for index in list(range(len(mat.texture_slots))):
                slot = mat.texture_slots[index]
                if slot:
                    texture = slot.texture
                    if slot.texture_coords == 'UV':
                        if texture.type == 'IMAGE':
                            image = texture.image
                            src = image.filepath
                            if slot.use_map_color_diffuse:
                                # diffuse texture
                                textures["diffuse"] = src
                                textures_found += 1
        texturename = ""
        if textures_found:
            diffuse_tex = textures["diffuse"]
            paths = bpy.utils.blend_paths()
            if diffuse_tex in paths:
                index = paths.index(diffuse_tex)
                abs_paths = bpy.utils.blend_paths(absolute = True)
                abs_path = abs_paths[index]
                basename = os.path.basename(abs_path)
                # assume the texture was copied to the export directory
                texturename = basename
        if mat:
            if textures_found:
                # image
                image = arnold.AiNode("image")
                # name
                arnold.AiNodeSetStr(image, "name",
                                    "IM" + # ID_IM
                                    mat.name + "_diff_tex")
                arnold.AiNodeSetStr(image, "filename", texturename)
        # shader
        shader = arnold.AiNode("utility")
        # name
        arnold.AiNodeSetStr(shader,
                            "name",
                            "MA" + # ID_MA
                            mat.name)
        return shader, opaque

    def writeHdrLighting(self, hdrImagePath):
        # light_counter
        self.light_counter += 1
        # image
        image = arnold.AiNode("image")
        arnold.AiNodeSetStr(image, "name", "hdr_tex")
        arnold.AiNodeSetStr(image, "filename", hdrImagePath)
        # sky
        sky = arnold.AiNode("sky")
        arnold.AiNodeSetStr(sky,
                            "name",
                            "hdr_sky")
        intensity = 1.0 # TODO: adjust in Blender?
        arnold.AiNodeSetFlt(sky,
                            "intensity",
                            intensity)
        visibility = (arnold.AI_RAY_ALL &
                      ~arnold.AI_RAY_DIFFUSE &
                      ~arnold.AI_RAY_GLOSSY &
                      ~arnold.AI_RAY_REFLECTED &
                      ~arnold.AI_RAY_REFRACTED &
                      ~arnold.AI_RAY_SHADOW)
        arnold.AiNodeSetInt(sky,
                            "visibility",
                            visibility)
        latlong = 2 # TODO
        arnold.AiNodeSetInt(sky, "format", latlong)
        arnold.AiNodeSetFlt(sky, "Z_angle", 270.0)
        arnold.AiNodeSetVec(sky, "Y", 0.0, 0.0, 1.0)
        arnold.AiNodeSetVec(sky, "Z", 0.0, -1.0, 0.0)
        # skydome_light
        skydome_light = arnold.AiNode("skydome_light")
        arnold.AiNodeSetStr(skydome_light,
                            "name",
                            "hdr_skydome")
        arnold.AiNodeSetInt(skydome_light, "resolution", 4096)
        arnold.AiNodeSetInt(skydome_light, "format", latlong)
        intensity = 1.0
        arnold.AiNodeSetFlt(skydome_light,
                            "intensity",
                            intensity)
        arnold.AiNodeSetInt(skydome_light, "samples", 6)
        # m = [[0.0, 1.0, 0.0, 0.0],
        #      [0.0, 0.0, 1.0, 0.0],
        #      [1.0, 0.0, 0.0, 0.0],
        #      [0.0, 0.0, 0.0, 1.0]]
        am = arnold.AtMatrix()
        arnold.AiM4Identity(am)
        am[0][0] = 0.0
        am[0][1] = 1.0
        am[1][1] = 0.0
        am[1][2] = 1.0
        am[2][0] = 1.0
        am[2][2] = 0.0
        arnold.AiNodeSetMatrix(skydome_light, "matrix", am)
        # link image to sky and skydome_light
        arnold.AiNodeLink(image, "color", sky)
        arnold.AiNodeLink(image, "color", skydome_light)
        # link sky to options background
        options = arnold.AiUniverseGetOptions()
        arnold.AiNodeSetPtr(options, "background", sky)

    def writeMesh(self, name, transform, info, user_matrix = None):
        # info
        materials = info[0]
        vertices = info[1]
        vertexNormals = info[2]
        polygons = info[3]
        polygonNormals = info[4]
        smoothPolygons = info[5]
        polygonMaterialIndices = info[6]
        hasUVs, uvs = info[7]
        # if there's a single smooth polygon, we export normals
        useVertexNormals = False
        for smoothPolygon in smoothPolygons:
            if smoothPolygon:
                useVertexNormals = True
                break
        # polymesh
        polymesh = arnold.AiNode("polymesh")
        # name
        arnold.AiNodeSetStr(polymesh,
                            "name",
                            "ME" + # ID_ME
                            self.uniqueName(name))
        # nsides
        nsides = arnold.AiArrayAllocate(len(polygons), 1,
                                        arnold.AI_TYPE_UINT)
        numVertexIndices = 0
        for i in range(len(polygons)):
            polygon = polygons[i]
            arnold.AiArraySetUInt(nsides, i, len(polygon))
            numVertexIndices += len(polygon)
        arnold.AiNodeSetArray(polymesh, "nsides", nsides)
        numUVs = numVertexIndices # keep for later
        # vidxs
        vidxs = arnold.AiArrayAllocate(numVertexIndices, 1,
                                       arnold.AI_TYPE_UINT)
        numVertexIndices = 0 # reset
        for polygon in polygons:
            for vi in polygon:
                arnold.AiArraySetUInt(vidxs, numVertexIndices,
                                      vi)
                numVertexIndices += 1
        arnold.AiNodeSetArray(polymesh, "vidxs", vidxs)
        # nidxs
        if useVertexNormals:
            if len(vertices) != len(vertexNormals):
                print("WARNING: len(vertices)[%s] != len(vertexNormals)[%s]" %
                      (len(vertices), len(vertexNormals)))
            nidxs = arnold.AiArrayAllocate(numVertexIndices, 1,
                                           arnold.AI_TYPE_UINT)
            numVertexIndices = 0 # reset
            for polygon in polygons:
                for vi in polygon:
                    arnold.AiArraySetUInt(nidxs, numVertexIndices,
                                          vi)
                    numVertexIndices += 1
            arnold.AiNodeSetArray(polymesh, "nidxs", nidxs)
        # uvidxs
        if hasUVs:
            uvidxs = arnold.AiArrayAllocate(numVertexIndices, 1,
                                           arnold.AI_TYPE_UINT)
            numVertexIndices = 0 # reset
            for polygon in polygons:
                for vi in polygon:
                    arnold.AiArraySetUInt(uvidxs, numVertexIndices,
                                          numVertexIndices)
                    numVertexIndices += 1
            arnold.AiNodeSetArray(polymesh, "uvidxs", uvidxs)
        # shidxs
        shidxs = arnold.AiArrayAllocate(len(polygonMaterialIndices), 1,
                                        arnold.AI_TYPE_BYTE)
        for i in range(len(polygonMaterialIndices)):
            mat_idx = polygonMaterialIndices[i]
            arnold.AiArraySetByte(shidxs, i, mat_idx)
        arnold.AiNodeSetArray(polymesh, "shidxs", shidxs)
        # vlist
        vlist = arnold.AiArrayAllocate(len(vertices), 1,
                                       arnold.AI_TYPE_VECTOR)
        numVertexIndices = 0 # reset
        for v in vertices:
            arnold.AiArraySetVec(vlist, numVertexIndices,
                                 arnold.AtVector(self.scale_length * v[0],
                                                self.scale_length * v[1],
                                                self.scale_length * v[2]))
            numVertexIndices += 1
        arnold.AiNodeSetArray(polymesh, "vlist", vlist)
        # nlist
        if useVertexNormals:
            nlist = arnold.AiArrayAllocate(len(vertices), 1,
                                           arnold.AI_TYPE_VECTOR)
            numVertexIndices = 0 # reset
            for n in vertexNormals: # use vertex normal
                arnold.AiArraySetVec(nlist, numVertexIndices,
                                     arnold.AtVector(n[0], n[1], n[2]))
                numVertexIndices += 1
            arnold.AiNodeSetArray(polymesh, "nlist", nlist)
            # use smoothing
            arnold.AiNodeSetBool(polymesh,
                                 "smoothing",
                                 True)
        # uvlist
        if hasUVs:
            uvlist = arnold.AiArrayAllocate(numUVs, 1,
                                            arnold.AI_TYPE_VECTOR2)
            numVertexIndices = 0 # reset
            for polygon in uvs:
                for uv in polygon:
                    arnold.AiArraySetVec2(uvlist, numVertexIndices,
                                          arnold.AtVector2(uv[0], uv[1]))
                    numVertexIndices += 1
            arnold.AiNodeSetArray(polymesh, "uvlist", uvlist)
            # double check if that worked
            # dummy = arnold.AiNodeGetArray(polymesh, "uvlist")
            # if dummy.contents.data == None:
            #     print('WARNING: uvlist allocation for polymesh "%s" ' % name +
            #           'failed for unknown reason')
            #     # reset uvidxs
            #     uvidxs = arnold.AiArrayAllocate(0, 1, arnold.AI_TYPE_UINT)
            #     arnold.AiNodeSetArray(polymesh, "uvidxs", uvidxs)
        # material(s)
        isOpaque = True
        shaders = arnold.AiArray(len(materials),
                                 1,
                                 arnold.AI_TYPE_NODE)
        if len(materials) > 1:
            for mi in range(len(materials)):
                matName = materials[mi]
                mat = bpy.data.materials[matName]
                shader = arnold.AiNodeLookUpByName("MA" + # ID_MA
                                                   matName)
                if shader == None:
                    # for Cycles nodes we might have used a '_mix'
                    shader = arnold.AiNodeLookUpByName("MA" + # ID_MA
                                                       mat.name +
                                                       "_mix")
                if shader == None:
                    if (self.options.lighting == 'no_lights' or
                        self.light_counter == 0):
                        shader, opaque = self.writeDefaultSurface(mat, name)
                    else:
                        shader, opaque = self.writeStandardShader(mat, name, mi)
                    self.usedMaterials[mat.name] = opaque
                else:
                    opaque = self.usedMaterials[mat.name]
                isOpaque = isOpaque and opaque
                if shader != None:
                    arnold.AiArraySetPtr(shaders, mi, shader)
            arnold.AiNodeSetArray(polymesh,
                                  "shader",
                                  shaders)
            arnold.AiNodeSetBool(polymesh,
                                 "opaque",
                                 isOpaque)
        else:
            if len(materials):
                mat = bpy.data.materials[materials[0]]
            else:
                mat = None
            # check if material is light emitter
            if mat != None and mat.emit != 0.0:
                # emitter
                shader = arnold.AiNodeLookUpByName("MA" + # ID_MA
                                                   mat.name)
                if shader == None:
                    if (self.options.lighting == 'no_lights' or
                        self.light_counter == 0):
                        shader, opaque = self.writeRaySwitchShader(mat, name)
                    else:
                        shader, opaque = self.writeRaySwitchShader(mat, name)
                        # mesh_light
                        self.writeMeshLight(polymesh, mat, name)
                    self.usedMaterials[mat.name] = opaque
                else:
                    opaque = self.usedMaterials[mat.name]
                    if mat.emit != 0.0:
                        # mesh_light
                        self.writeMeshLight(polymesh, mat, name)
                if shader != None:
                    arnold.AiNodeSetPtr(polymesh, "shader", shader)
                    arnold.AiNodeSetBool(polymesh,
                                         "opaque",
                                         opaque)
            else:
                # no emitter
                shader = arnold.AiNodeLookUpByName("MA" + # ID_MA
                                                   mat.name)
                if shader == None:
                    # for Cycles nodes we might have used a '_mix'
                    shader = arnold.AiNodeLookUpByName("MA" + # ID_MA
                                                       mat.name +
                                                       "_mix")
                if shader == None:
                    if (self.options.lighting == 'no_lights' or
                        self.light_counter == 0):
                        shader, opaque = self.writeDefaultSurface(mat, name)
                    else:
                        shader, opaque = self.writeStandardShader(mat, name)
                    self.usedMaterials[mat.name] = opaque
                else:
                    opaque = self.usedMaterials[mat.name]
                if shader != None:
                    arnold.AiNodeSetPtr(polymesh, "shader", shader)
                    arnold.AiNodeSetBool(polymesh,
                                         "opaque",
                                         opaque)
        # one matrix
        m = transform
        self.createMatrix(polymesh, m)
        # user matrix?
        if (user_matrix != None):
            m = user_matrix
            self.createUserMatrix(polymesh, m)

    def writeNurbsSurface(self, name, transform, info):
        # info
        materials = info[0]
        splines = info[1]
        # for each spline ...
        for spline in splines:
            order_u = spline[0][0]
            order_v = spline[0][1]
            # ... check if bicubic
            if (order_u == 4 and order_v == 4):
                # nurbs
                nurbs = arnold.AiNode("nurbs")
                # name
                uniqueName = name # TODO
                arnold.AiNodeSetStr(nurbs, "name",
                                    "CU" + # ID_CU
                                    self.uniqueName(uniqueName))
                # knots
                knots_u = arnold.AiArrayAllocate(2 * 4, 1,
                                                 arnold.AI_TYPE_FLOAT)
                knots_v = arnold.AiArrayAllocate(2 * 4, 1,
                                                 arnold.AI_TYPE_FLOAT)
                # TODO: this is an assumption !!!
                for i in range(4):
                    arnold.AiArraySetFlt(knots_u, i, 0.0)
                    arnold.AiArraySetFlt(knots_v, i, 0.0)
                for i in range(4):
                    arnold.AiArraySetFlt(knots_u, i+4, 1.0)
                    arnold.AiArraySetFlt(knots_v, i+4, 1.0)
                arnold.AiNodeSetArray(nurbs, "knots_u", knots_u)
                arnold.AiNodeSetArray(nurbs, "knots_v", knots_v)
                # cvs
                points = spline[1]
                cvs = arnold.AiArrayAllocate(4 * len(points), 1,
                                             arnold.AI_TYPE_FLOAT)
                counter = 0
                for v in points:
                    for i in range(4):
                        arnold.AiArraySetFlt(cvs, counter * 4 + i,
                                             self.scale_length * v[i])
                    counter += 1
                arnold.AiNodeSetArray(nurbs, "cvs", cvs)
                # smoothing
                smoothing = True
                arnold.AiNodeSetBool(nurbs, "smoothing", smoothing)
                # assume ONE material
                matName = materials[0]
                mat = bpy.data.materials[matName]
                shader = arnold.AiNodeLookUpByName("MA" + # ID_MA
                                                   mat.name)
                if shader == None:
                    # for Cycles nodes we might have used a '_mix'
                    shader = arnold.AiNodeLookUpByName("MA" + # ID_MA
                                                       mat.name +
                                                       "_mix")
                if shader == None:
                    if (self.options.lighting == 'no_lights' or
                        self.light_counter == 0):
                        shader, opaque = self.writeDefaultSurface(mat, name)
                    else:
                        shader, opaque = self.writeStandardShader(mat, name)
                    self.usedMaterials[mat.name] = opaque
                else:
                    opaque = self.usedMaterials[mat.name]
                if shader != None:
                    arnold.AiNodeSetPtr(nurbs, "shader", shader)
                    arnold.AiNodeSetBool(nurbs,
                                         "opaque",
                                         opaque)
                # one matrix
                m = transform
                self.createMatrix(nurbs, m)
            else:
                print("WARNING: TODO: nurbs [%s, %s]" %
                      (order_u, order_v))

    def writePointLight(self, name, transform, info):
        # light_counter
        self.light_counter += 1
        # info
        color = info[0]
        energy = info[1]
        falloff = info[2]
        use_diffuse = info[3][0]
        use_specular = info[3][1]
        # falloff
        falloff_type = falloff[0]
        # point_light
        point_light = arnold.AiNode("point_light")
        # name
        arnold.AiNodeSetStr(point_light,
                            "name",
                            "LA" + # ID_LA
                            self.uniqueName(name))
        # decay_type
        # values: constant quadratic
        if falloff_type == 'INVERSE_SQUARE':
            arnold.AiNodeSetInt(point_light,
                                "decay_type",
                                1)
        elif (falloff_type == 'INVERSE_LINEAR' or
              falloff_type == 'CUSTOM_CURVE' or
              falloff_type == 'LINEAR_QUADRATIC_WEIGHTED'):
            print("WARNING: falling back to 'CONSTANT' falloff type")
            arnold.AiNodeSetInt(point_light,
                                "decay_type",
                                0)
        else: # 'CONSTANT'
            arnold.AiNodeSetInt(point_light,
                                "decay_type",
                                0)
        # one matrix
        m = transform
        self.createMatrix(point_light, m)
        # color
        arnold.AiNodeSetRGB(point_light, "color",
                            color[0],
                            color[1],
                            color[2])
        # intensity
        arnold.AiNodeSetFlt(point_light,
                            "intensity",
                            energy)
        # diffuse?
        arnold.AiNodeSetBool(point_light,
                             "affect_diffuse",
                             use_diffuse)
        # specular?
        arnold.AiNodeSetBool(point_light,
                             "affect_specular",
                             use_specular)

    def writeRing(self, name, transform, info, mat):
        # disk
        obj = bpy.data.objects[name]
        p1 = obj.data.splines[0].points[0].co
        p2 = obj.data.splines[0].points[8].co
        disk = arnold.AiNode("disk")
        r1 = self.scale_length * math.fabs(p1.y)
        r2 = self.scale_length * math.fabs(p2.y)
        if r1 < r2:
            minRadius = r1
            maxRadius = r2
        else:
            minRadius = r2
            maxRadius = r1
        # name
        arnold.AiNodeSetStr(disk,
                            "name",
                            "CU" + # ID_CU
                            self.uniqueName(name))
        arnold.AiNodeSetFlt(disk, # default is 0.5
                            "radius",
                            self.scale_length * 1.0)
        arnold.AiNodeSetFlt(disk, # default is 0.5
                            "radius", maxRadius)
        arnold.AiNodeSetFlt(disk, # default is 0.0
                            "hole", minRadius)
        primitive = disk
        self.writeCommonPrimitive(name, transform, mat, primitive)

    def writeSphere(self, name, transform, info, mat):
        # sphere
        sphere = arnold.AiNode("sphere")
        # name
        arnold.AiNodeSetStr(sphere,
                            "name",
                            "CU" + # ID_CU
                            self.uniqueName(name))
        # radius
        arnold.AiNodeSetFlt(sphere, # default is 0.5
                            "radius",
                            self.scale_length * 1.0)
        if mat.emit == 0.0:
            primitive = sphere
            self.writeCommonPrimitive(name, transform, mat, primitive)
        else:
            # material
            mat = bpy.data.materials[mat.name]
            shader = arnold.AiNodeLookUpByName("MA" + # ID_MA
                                               mat.name)
            if shader == None:
                # for Cycles nodes we might have used a '_mix'
                shader = arnold.AiNodeLookUpByName("MA" + # ID_MA
                                                   mat.name +
                                                   "_mix")
            if shader == None:
                opaque = True
                # ray_switch_rgba
                ray_switch_rgba = arnold.AiNode("ray_switch_rgba")
                if ray_switch_rgba == None:
                    print("WARNING: %s" %
                          "'arnold.AiNode(\"ray_switch_rgba\")' failed")
                else:
                    # name
                    arnold.AiNodeSetStr(ray_switch_rgba,
                                        "name",
                                        "MA" + # ID_MA
                                        mat.name)
                    # camera rays
                    color = mat.diffuse_color
                    arnold.AiNodeSetRGBA(ray_switch_rgba,
                                         "camera",
                                         color[0] * mat.emit,
                                         color[1] * mat.emit,
                                         color[2] * mat.emit,
                                         1.0)
                    # set all others to 0 0 0 0
                    arnold.AiNodeSetRGBA(ray_switch_rgba,
                                         "shadow",
                                         0.0, 0.0, 0.0, 1.0)
                    arnold.AiNodeSetRGBA(ray_switch_rgba,
                                         "diffuse_reflection",
                                         color[0] * mat.emit,
                                         color[1] * mat.emit,
                                         color[2] * mat.emit,
                                         1.0)
                    arnold.AiNodeSetRGBA(ray_switch_rgba,
                                         "diffuse_transmission",
                                         color[0] * mat.emit,
                                         color[1] * mat.emit,
                                         color[2] * mat.emit,
                                         1.0)
                    arnold.AiNodeSetRGBA(ray_switch_rgba,
                                         "specular_reflection",
                                         color[0] * mat.emit,
                                         color[1] * mat.emit,
                                         color[2] * mat.emit,
                                         1.0)
                    arnold.AiNodeSetRGBA(ray_switch_rgba,
                                         "specular_transmission",
                                         color[0] * mat.emit,
                                         color[1] * mat.emit,
                                         color[2] * mat.emit,
                                         1.0)
                    shader = ray_switch_rgba
                self.usedMaterials[mat.name] = opaque
            else:
                opaque = self.usedMaterials[mat.name]
            # sphere
            ########
            # visibility
            obj = bpy.data.objects[name]
            if obj.hide:
                # not visible to AI_RAY_ALL
                arnold.AiNodeSetByte(sphere, "visibility", 0)
            else:
                # not visible to AI_RAY_SHADOW
                arnold.AiNodeSetByte(sphere, "visibility", 253)
            if shader != None:
                # shader
                arnold.AiNodeSetPtr(sphere, "shader", shader)
            # opaque
            arnold.AiNodeSetBool(sphere,
                                 "opaque",
                                 opaque)
            # one matrix
            m = transform
            self.createMatrix(sphere, m)
            # point_light
            #############
            point_light = arnold.AiNode("point_light")
            # name
            arnold.AiNodeSetStr(point_light,
                                "name",
                                "LA" + # ID_LA
                                self.uniqueName(name))
            # radius
            m = transform
            center, rot, scale = m.decompose()
            if (scale[0] == scale[1] == scale[2]):
                radius = self.scale_length * scale[0]
            else:
                # print('WARNING: AssExporter.writeSphere' +
                #       '("%s") scale (%s, %s, %s)' %
                #       (mat.name, scale[0], scale[1], scale[2]))
                radius = self.scale_length * scale[0]
            arnold.AiNodeSetFlt(point_light,
                                "radius",
                                radius)
            # one matrix
            m = transform
            # no scaling (it's in the radius)
            self.createMatrix(point_light, m, True)
            # color
            color = mat.diffuse_color
            arnold.AiNodeSetRGB(point_light,
                                "color",
                                color[0],
                                color[1],
                                color[2])
            # intensity
            arnold.AiNodeSetFlt(point_light,
                                "intensity",
                                mat.emit)
            # samples
            arnold.AiNodeSetInt(point_light,
                                "samples",
                                1)
            # specular
            arnold.AiNodeSetFlt(point_light,
                                "specular",
                                1.0)

    def writeSpotLight(self, name, transform, info):
        # light_counter
        self.light_counter += 1
        # info
        color = info[0]
        energy = info[1]
        falloff = info[2]
        use_diffuse = info[3][0]
        use_specular = info[3][1]
        spot_size = info[4][0]
        spot_blend = info[4][1]
        # falloff
        falloff_type = falloff[0]
        # spot_light
        spot_light = arnold.AiNode("spot_light")
        # name
        arnold.AiNodeSetStr(spot_light,
                            "name",
                            "LA" + # ID_LA
                            name)
        arnold.AiNodeSetFlt(spot_light,
                            "cone_angle",
                            math.degrees(spot_size))
        # decay_type
        # values: constant quadratic
        if falloff_type == 'INVERSE_SQUARE':
            arnold.AiNodeSetInt(spot_light,
                                "decay_type",
                                1)
        elif (falloff_type == 'INVERSE_LINEAR' or
              falloff_type == 'CUSTOM_CURVE' or
              falloff_type == 'LINEAR_QUADRATIC_WEIGHTED'):
            print("WARNING: falling back to 'CONSTANT' falloff type")
            arnold.AiNodeSetInt(spot_light,
                                "decay_type",
                                0)
        else: # 'CONSTANT'
            arnold.AiNodeSetInt(spot_light,
                                "decay_type",
                                0)
        # one matrix
        m = transform
        self.createMatrix(spot_light, m)
        # color
        arnold.AiNodeSetRGB(spot_light, "color",
                            color[0],
                            color[1],
                            color[2])
        # intensity
        if (math.fabs(math.degrees(spot_size) - 180.0) < 0.0001):
            arnold.AiNodeSetFlt(spot_light,
                                "intensity",
                                energy * math.pi / 2.0)
        else:
            arnold.AiNodeSetFlt(spot_light,
                                "intensity",
                                energy * math.pi)
        # diffuse?
        arnold.AiNodeSetBool(spot_light,
                             "affect_diffuse",
                             use_diffuse)
        # specular?
        arnold.AiNodeSetBool(spot_light,
                             "affect_specular",
                             use_specular)

    def writeSunLight(self, name, transform, info):
        # light_counter
        self.light_counter += 1
        # info
        color = info[0]
        energy = info[1]
        falloff = info[2]
        use_diffuse = info[3][0]
        use_specular = info[3][1]
        # lamp data
        lamp = bpy.data.lamps[name]
        if name == 'sun' or name == 'Sun':
            # physical_sky
            physical_sky = arnold.AiNode("physical_sky")
            # name
            arnold.AiNodeSetStr(physical_sky,
                                "name",
                                name + "_sky")
            # turbidity
            turbidity = lamp.sky.atmosphere_turbidity
            arnold.AiNodeSetFlt(physical_sky,
                                "turbidity",
                                turbidity)
            # sun_direction
            arnold.AiNodeSetVec(physical_sky,
                                "sun_direction",
                                0.0, 1.0, 0.0)
            # calculate elevation and azimuth
            m = transform
            pos, rot, scale = m.decompose()
            euler = rot.to_euler()
            # elevation
            elevation = 90.0 - math.degrees(euler.x)
            arnold.AiNodeSetFlt(physical_sky,
                                "elevation",
                                elevation)
            # azimuth
            azimuth = math.degrees(euler.z) - 90.0
            arnold.AiNodeSetFlt(physical_sky,
                                "azimuth",
                                azimuth)
            # intensity
            intensity = energy
            arnold.AiNodeSetFlt(physical_sky,
                                "intensity",
                                intensity)
            # X
            arnold.AiNodeSetVec(physical_sky,
                                "X",
                                1.0, 0.0, 0.0)
            # Y (swap)
            arnold.AiNodeSetVec(physical_sky,
                                "Y",
                                0.0, 0.0, 1.0)
            # Z (swap)
            arnold.AiNodeSetVec(physical_sky,
                                "Z",
                                0.0, 1.0, 0.0)
            # options
            options = arnold.AiNodeLookUpByName("options")
            arnold.AiNodeSetPtr(options, "background", physical_sky)
            if self.options.lighting == 'global_illumination':
                # skydome_light
                skydome_light = arnold.AiNode("skydome_light")
                # name
                arnold.AiNodeSetStr(skydome_light,
                                    "name",
                                    name + "_skydome")
                # resolution
                arnold.AiNodeSetInt(skydome_light,
                                    "resolution", 4096)
                # intensity
                intensity = energy
                arnold.AiNodeSetFlt(skydome_light,
                                    "intensity",
                                    intensity)
                # samples
                arnold.AiNodeSetInt(skydome_light, "samples", 6)
                # link to skydome_light
                arnold.AiNodeLink(physical_sky, "color",
                                  skydome_light)
                return
        if self.options.lighting != 'no_lights':
            # distant_light
            distant_light = arnold.AiNode("distant_light")
            # name
            arnold.AiNodeSetStr(distant_light,
                                "name",
                                "LA" + # ID_LA
                                name)
            # one matrix
            m = transform
            self.createMatrix(distant_light, m)
            # color
            arnold.AiNodeSetRGB(distant_light, "color",
                                color[0],
                                color[1],
                                color[2])
            # intensity
            arnold.AiNodeSetFlt(distant_light,
                                "intensity",
                                energy * math.pi)
            # diffuse?
            arnold.AiNodeSetBool(distant_light,
                                 "affect_diffuse",
                                 use_diffuse)
            # specular?
            arnold.AiNodeSetBool(distant_light,
                                 "affect_specular",
                                 use_specular)

    def writeStandardShader(self, mat, name, mi = None):
        # return values
        shader = None
        opaque = True
        # try first to use Cycles nodes
        if self.options.cycles and mat.use_nodes:
            surface = mat.node_tree.nodes['Material Output'].inputs['Surface']
            if len(surface.links) >= 1:
                try:
                    color = surface.links[0].from_node.inputs['Color']
                except KeyError:
                    # handle mix of diffuse and glossy/transparent
                    fac = surface.links[0].from_node.inputs['Fac']
                    surface1 = surface.links[0].from_node.inputs[1]
                    surface2 = surface.links[0].from_node.inputs[2]
                    if surface1.links[0].from_node.type != 'BSDF_DIFFUSE':
                        print("WARNING: BSDF_DIFFUSE expected, %s found" %
                              surface1.links[0].from_node.type)
                    if surface2.links[0].from_node.type != 'BSDF_GLOSSY':
                        if surface2.links[0].from_node.type != 'BSDF_TRANSPARENT':
                            print("WARNING: BSDF_GLOSSY|TRANSPARENT expected, " +
                                  "%s found" %
                                  surface2.links[0].from_node.type)
                    diff = surface1.links[0].from_node.inputs['Color']
                    if surface2.links[0].from_node.type != 'BSDF_GLOSSY':
                        spec = [1, 1, 1, 1]
                    else:
                        spec = surface2.links[0].from_node.inputs['Color']
                    diff_rough = surface1.links[0].from_node.inputs['Roughness']
                    diffuse_roughness = diff_rough.default_value
                    if surface2.links[0].from_node.type != 'BSDF_GLOSSY':
                        spec_rough = None
                        specular_roughness = 0.0
                    else:
                        spec_rough = surface2.links[0].from_node.inputs['Roughness']
                        specular_roughness = spec_rough.default_value
                    Kd = 1.0 - fac.default_value
                    r = diff.default_value[0]
                    g = diff.default_value[1]
                    b = diff.default_value[2]
                    a = diff.default_value[3]
                    Kd_color = [r, g, b]
                    Ks = fac.default_value
                    Kr = fac.default_value
                    if surface2.links[0].from_node.type != 'BSDF_GLOSSY':
                        r = 1.0
                        g = 1.0
                        b = 1.0
                        a = 1.0
                        Ks_color = [r, g, b]
                        Kr_color = [r, g, b]
                    else:
                        r = spec.default_value[0]
                        g = spec.default_value[1]
                        b = spec.default_value[2]
                        a = spec.default_value[3]
                        Ks_color = [r, g, b]
                        Kr_color = [r, g, b]
                    # mix
                    mix = arnold.AiNode("mix_shader")
                    # shader
                    shader1 = arnold.AiNode("standard_surface")
                    shader2 = arnold.AiNode("standard_surface")
                    # name
                    arnold.AiNodeSetStr(mix,
                                        "name",
                                        "MA" + # ID_MA
                                        mat.name + "_mix")
                    arnold.AiNodeSetStr(shader1,
                                        "name",
                                        "MA" + # ID_MA
                                        mat.name + "_diffuse")
                    arnold.AiNodeSetStr(shader2,
                                        "name",
                                        "MA" + # ID_MA
                                        mat.name + "_glossy")
                    # base
                    arnold.AiNodeSetFlt(shader1,
                                        "base",
                                        1.0) # diffuse
                    arnold.AiNodeSetFlt(shader2,
                                        "base",
                                        0.0) # glossy
                    # base_color
                    if diff.is_linked:
                        # image
                        diff_image = arnold.AiNode("image")
                        arnold.AiNodeSetStr(diff_image, "name",
                                            "IM" + # ID_IM
                                            name + "_diff_tex")
                        image = diff.links[0].from_node.image
                        src = image.filepath
                        paths = bpy.utils.blend_paths()
                        if src in paths:
                            index = paths.index(src)
                            abs_paths = bpy.utils.blend_paths(absolute = True)
                            abs_path = abs_paths[index]
                            arnold.AiNodeSetStr(diff_image, "filename", abs_path)
                        # base_color
                        arnold.AiNodeLink(diff_image,
                                          "base_color",
                                          shader1)
                    else:
                        arnold.AiNodeSetRGB(shader1,
                                            "base_color",
                                            Kd_color[0],
                                            Kd_color[1],
                                            Kd_color[2])
                    # diffuse_roughness
                    if diffuse_roughness:
                        diffuse_roughness = math.sqrt(diffuse_roughness)
                    else:
                        diffuse_roughness = 0.0
                    arnold.AiNodeSetFlt(shader1,
                                        "diffuse_roughness",
                                        diffuse_roughness)
                    # specular
                    arnold.AiNodeSetFlt(shader1,
                                        "specular",
                                        0.0) # diffuse
                    if surface2.links[0].from_node.type != 'BSDF_GLOSSY':
                        arnold.AiNodeSetFlt(shader2,
                                            "specular",
                                            0.0)
                        # metalness
                        arnold.AiNodeSetFlt(shader2,
                                            "metalness",
                                            0.0)
                        # transmission
                        arnold.AiNodeSetFlt(shader2,
                                            "transmission",
                                            1.0)
                        # transmission_color
                        Kt_color = [1.0, 1.0, 1.0]
                        arnold.AiNodeSetRGB(shader2,
                                            "transmission_color",
                                            Kt_color[0],
                                            Kt_color[1],
                                            Kt_color[2])
                        # specular_IOR
                        IOR = 1.0
                        arnold.AiNodeSetFlt(shader2,
                                            "specular_IOR",
                                            IOR)
                        # opaque
                        opaque = False
                    else:
                        arnold.AiNodeSetFlt(shader2,
                                            "specular",
                                            1.0) # glossy
                    # specular_color
                    arnold.AiNodeSetRGB(shader2,
                                        "specular_color",
                                        Ks_color[0],
                                        Ks_color[1],
                                        Ks_color[2])
                    # specular_roughness
                    if specular_roughness:
                        specular_roughness = math.sqrt(specular_roughness)
                    else:
                        specular_roughness = 0.0
                    if (surface2.links[0].from_node.type == 'BSDF_GLOSSY' and
                        spec_rough.is_linked):
                        # image
                        spec_rough_image = arnold.AiNode("image")
                        arnold.AiNodeSetStr(spec_rough_image, "name",
                                            "IM" + # ID_IM
                                            name + "_spec_rough_tex")
                        image = spec_rough.links[0].from_node.image
                        src = image.filepath
                        paths = bpy.utils.blend_paths()
                        if src in paths:
                            index = paths.index(src)
                            abs_paths = bpy.utils.blend_paths(absolute = True)
                            abs_path = abs_paths[index]
                            arnold.AiNodeSetStr(spec_rough_image,
                                                "filename",
                                                abs_path)
                        arnold.AiNodeSetBool(spec_rough_image,
                                             "single_channel",
                                             True)
                        # specular_roughness
                        arnold.AiNodeLink(spec_rough_image,
                                          "specular_roughness",
                                          shader2)
                    else:
                        arnold.AiNodeSetFlt(shader2,
                                            "specular_roughness",
                                            specular_roughness)
                    if specular_roughness > 0.0:
                        # specular
                        arnold.AiNodeSetFlt(shader2,
                                            "specular",
                                            1.0)
                        # specular_color
                        arnold.AiNodeSetRGB(shader2,
                                            "specular_color",
                                            Ks_color[0],
                                            Ks_color[1],
                                            Ks_color[2])
                    # metalness
                    arnold.AiNodeSetFlt(shader2,
                                        "metalness",
                                        1.0)
                    # Kr_color
                    # arnold.AiNodeSetRGB(shader2,
                    #                     "Kr_color",
                    #                     Kr_color[0],
                    #                     Kr_color[1],
                    #                     Kr_color[2])
                    # mix
                    arnold.AiNodeLink(shader1, "shader1", mix)
                    arnold.AiNodeLink(shader2, "shader2", mix)
                    if fac.is_linked:
                        # image
                        fac_image = arnold.AiNode("image")
                        arnold.AiNodeSetStr(fac_image, "name",
                                            "IM" + # ID_IM
                                            name + "_fac_tex")
                        image = fac.links[0].from_node.image
                        src = image.filepath
                        paths = bpy.utils.blend_paths()
                        if src in paths:
                            index = paths.index(src)
                            abs_paths = bpy.utils.blend_paths(absolute = True)
                            abs_path = abs_paths[index]
                            arnold.AiNodeSetStr(fac_image, "filename",
                                                abs_path)
                        arnold.AiNodeSetBool(fac_image,
                                             "single_channel",
                                             True)
                        # mix
                        arnold.AiNodeLink(fac_image,
                                          "mix",
                                          mix)
                    else:
                        arnold.AiNodeSetFlt(mix,
                                            "mix",
                                            fac.default_value)
                    # return
                    return mix, opaque
                else:
                    if len(color.links) >= 1:
                        inputs = color.links[0].from_node.inputs
                        try:
                            filepath = color.links[0].from_node.script.filepath
                        except AttributeError:
                            # assume diffuse BRDF
                            diff = color
                            Kd = 1.0
                            Ks = 0.0
                            Kr = 0.0
                            r = diff.default_value[0]
                            g = diff.default_value[1]
                            b = diff.default_value[2]
                            a = diff.default_value[3]
                            Kd_color = [r, g, b]
                            # shader
                            shader = arnold.AiNode("standard_surface")
                            # name
                            arnold.AiNodeSetStr(shader,
                                                "name",
                                                "MA" + # ID_MA
                                                mat.name)
                            # base
                            arnold.AiNodeSetFlt(shader,
                                                "base",
                                                Kd)
                            # base_color
                            if diff.is_linked:
                                # image
                                diff_image = arnold.AiNode("image")
                                arnold.AiNodeSetStr(diff_image, "name",
                                                    "IM" + # ID_IM
                                                    name + "_diff_tex")
                                image = diff.links[0].from_node.image
                                src = image.filepath
                                paths = bpy.utils.blend_paths()
                                if src in paths:
                                    index = paths.index(src)
                                    abs_paths = bpy.utils.blend_paths(absolute = True)
                                    abs_path = abs_paths[index]
                                    arnold.AiNodeSetStr(diff_image, "filename",
                                                        abs_path)
                                # base_color
                                arnold.AiNodeLink(diff_image,
                                                  "base_color",
                                                  shader)
                            else:
                                arnold.AiNodeSetRGB(shader,
                                                    "base_color",
                                                    Kd_color[0],
                                                    Kd_color[1],
                                                    Kd_color[2])
                            # specular
                            arnold.AiNodeSetFlt(shader,
                                                "specular",
                                                Ks)
                            # metalness
                            arnold.AiNodeSetFlt(shader,
                                                "metalness",
                                                Kr)
                            arnold.AiNodeSetFlt(shader,
                                                "base", # set base to same value!!!
                                                Kr)
                            # return
                            return shader, opaque
                        else:
                            print("INFO: filepath = %s" % filepath)
                            # shader
                            basename = os.path.basename(filepath)
                            root, ext = os.path.splitext(basename)
                            ext = ".oso" # compiled OSL shader
                            osoFilepath = os.path.join(self.plugin_searchpath,
                                                       root + ext)
                            arnold.AiLoadPlugin("%s" % osoFilepath)
                            oslShader = arnold.AiNode(root)
                            if oslShader == None:
                                print("WARNING: can't load %s" % osoFilepath)
                                # return shader, opaque
                            else:
                                # name
                                arnold.AiNodeSetStr(oslShader,
                                                    "name",
                                                    "MAOSL" + # ID_MA
                                                    mat.name)
                                for p in inputs:
                                    print("%s %s" % (p.type, p.name))
                                    if p.type == 'VECTOR':
                                        x = p.default_value[0]
                                        y = p.default_value[1]
                                        z = p.default_value[2]
                                        arnold.AiNodeSetVec(oslShader,
                                                            p.name,
                                                            x, y, z)
                                    elif p.type == 'VALUE':
                                        arnold.AiNodeSetFlt(oslShader,
                                                            p.name,
                                                            p.default_value)
                                    elif p.type == 'RGBA':
                                        r = p.default_value[0]
                                        g = p.default_value[1]
                                        b = p.default_value[2]
                                        a = p.default_value[3]
                                        # ignore alpha
                                        arnold.AiNodeSetRGB(oslShader,
                                                            p.name,
                                                            r, g, b)
                                    else:
                                        print("TODO: %s %s" % (p.type, p.name))
                                # create standard shader
                                standard = arnold.AiNode("standard_surface")
                                arnold.AiNodeSetStr(standard,
                                                    "name",
                                                    "MA" + # ID_MA
                                                    mat.name)
                                # link standard shader and OSL pattern
                                arnold.AiNodeLink(oslShader, "base_color",
                                                  standard)
                                return standard, opaque
        # check for proper diffuse and specular model
        if mat.diffuse_shader != 'LAMBERT':
            print("WARNING: switching %s to 'LAMBERT'" % mat.name)
            mat.diffuse_shader = 'LAMBERT'
        if mat.specular_shader != 'PHONG':
            print("WARNING: switching %s to 'PHONG'" % mat.name)
            mat.specular_shader = 'PHONG'
        # textures?
        textures_found = 0
        textures = {}
        if mat:
            # check for textures
            for index in list(range(len(mat.texture_slots))):
                slot = mat.texture_slots[index]
                if slot:
                    texture = slot.texture
                    if slot.texture_coords == 'UV':
                        if texture.type == 'IMAGE':
                            image = texture.image
                            src = image.filepath
                            if slot.use_map_color_diffuse:
                                # diffuse texture
                                textures["diffuse"] = src
                                textures_found += 1
        # shader
        shader = arnold.AiNode("standard_surface")
        # name
        arnold.AiNodeSetStr(shader,
                            "name",
                            "MA" + # ID_MA
                            mat.name)
        # Kd
        Kd = mat.diffuse_intensity
        try:
            diffuse_tex = textures["diffuse"]
        except KeyError:
            arnold.AiNodeSetFlt(shader,
                                "base",
                                Kd)
        else:
            paths = bpy.utils.blend_paths()
            if diffuse_tex in paths:
                index = paths.index(diffuse_tex)
                abs_paths = bpy.utils.blend_paths(absolute = True)
                abs_path = abs_paths[index]
                # image
                image = arnold.AiNode("image")
                if mi == None:
                    arnold.AiNodeSetStr(image, "name",
                                        "IM" + # ID_IM
                                        name + "_diff_tex")
                else:
                    arnold.AiNodeSetStr(image, "name",
                                        "IM" + # ID_IM
                                        name + "_%02d_diff_tex" %
                                        mi)
                arnold.AiNodeSetStr(image, "filename",
                                    abs_path)
                # base
                arnold.AiNodeSetFlt(shader,
                                    "base",
                                    Kd)
                # using image
                arnold.AiNodeLink(image, "base_color", shader)
            else:
                print("WARNING: couldn't find '%s' in paths" % diffuse_tex)
                arnold.AiNodeSetFlt(shader,
                                    "base",
                                    Kd)
        # base_color
        Kd_color = mat.diffuse_color
        arnold.AiNodeSetRGB(shader,
                            "base_color",
                            Kd_color[0],
                            Kd_color[1],
                            Kd_color[2])
        # specular
        Ks = mat.specular_intensity
        arnold.AiNodeSetFlt(shader,
                            "specular",
                            Ks)
        # specular_color
        Ks_color = mat.specular_color
        arnold.AiNodeSetRGB(shader,
                            "specular_color",
                            Ks_color[0],
                            Ks_color[1],
                            Ks_color[2])
        if mat.use_transparency and mat.transparency_method == 'RAYTRACE':
            # base
            Kd = 0.0
            arnold.AiNodeSetFlt(shader,
                                "base",
                                Kd)
            # specular
            Ks = 0.0
            arnold.AiNodeSetFlt(shader,
                                "specular",
                                Ks)
            # metalness
            Kr = mat.alpha
            arnold.AiNodeSetFlt(shader,
                                "metalness",
                                Kr)
            arnold.AiNodeSetFlt(shader,
                                "base", # set base to same value!!!
                                Kr)
            # transmission
            Kt = 1.0 - mat.alpha
            arnold.AiNodeSetFlt(shader,
                                "transmission",
                                Kt)
            # transmission_color
            maxColor = Kd_color[0]
            if Kd_color[1] > maxColor:
                maxColor = Kd_color[1]
            if Kd_color[2] > maxColor:
                maxColor = Kd_color[2]
            Kt_color = [Kd_color[0] / maxColor,
                        Kd_color[1] / maxColor,
                        Kd_color[2] / maxColor]
            arnold.AiNodeSetRGB(shader,
                                "transmission_color",
                                Kt_color[0],
                                Kt_color[1],
                                Kt_color[2])
            # specular_IOR
            IOR = mat.raytrace_transparency.ior
            arnold.AiNodeSetFlt(shader,
                                "specular_IOR",
                                IOR)
            # opaque
            opaque = False
        if mat.raytrace_mirror.use:
            # specular_roughness
            specular_roughness = math.sqrt(mat.roughness)
            arnold.AiNodeSetFlt(shader,
                                "specular_roughness",
                                specular_roughness)
            if specular_roughness > 0.0:
                # specular
                Ks = mat.raytrace_mirror.reflect_factor
                arnold.AiNodeSetFlt(shader,
                                    "specular",
                                    Ks)
                # specular_color
                Ks_color = mat.mirror_color
                arnold.AiNodeSetRGB(shader,
                                    "specular_color",
                                    Ks_color[0],
                                    Ks_color[1],
                                    Ks_color[2])
            # metalness
            Kr = mat.raytrace_mirror.reflect_factor
            arnold.AiNodeSetFlt(shader,
                                "metalness",
                                Kr)
            arnold.AiNodeSetFlt(shader,
                                "base", # set base to same value!!!
                                Kr)
            # Kr_color
            # Kr_color = mat.mirror_color
            # arnold.AiNodeSetRGB(shader,
            #                     "Kr_color",
            #                     Kr_color[0],
            #                     Kr_color[1],
            #                     Kr_color[2])
        elif Ks > 0.0:
            # specular_roughness
            specular_roughness = math.sqrt(mat.roughness)
            arnold.AiNodeSetFlt(shader,
                                "specular_roughness",
                                specular_roughness)
            if specular_roughness == 0.0:
                # metalness
                Kr = Ks
                arnold.AiNodeSetFlt(shader,
                                    "metalness",
                                    Kr)
                arnold.AiNodeSetFlt(shader,
                                    "base", # set base to same value!!!
                                    Kr)
                # Kr_color
                # Kr_color = Ks_color
                # arnold.AiNodeSetRGB(shader,
                #                     "Kr_color",
                #                     Kr_color[0],
                #                     Kr_color[1],
                #                     Kr_color[2])
        if mat.emit != 0.0:
            # emission
            emission = mat.emit
            arnold.AiNodeSetFlt(shader,
                                "emission",
                                emission)
            # emission_color
            emission_color = mat.diffuse_color
            arnold.AiNodeSetRGB(shader,
                                "emission_color",
                                emission_color[0],
                                emission_color[1],
                                emission_color[2])
            # opacity
            opacity = [1.0, 1.0, 1.0]
            arnold.AiNodeSetRGB(shader,
                                "opacity",
                                opacity[0],
                                opacity[1],
                                opacity[2])
            # return
            opaque = True
        return shader, opaque

    def finishExport(self, sun = None):
        # motion blur
        if self.use_motion_blur:
            # for all animated objects ...
            for objIdx in range(len(self.animatedObjects)):
                objName = self.animatedObjects[objIdx]
                matrices = self.motionMatrices[objName]
                # and store them in an Arnold array
                m_array = arnold.AiArrayAllocate(1,
                                                 self.motion_blur_samples,
                                                 arnold.AI_TYPE_MATRIX)
                for midx in range(len(matrices)):
                    m = matrices[midx][0]
                    am = arnold.AtMatrix()
                    arnold.AiM4Identity(am)
                    am[0][0] = m[0][0]
                    am[0][1] = m[1][0]
                    am[0][2] = m[2][0]
                    am[0][3] = m[3][0]
                    am[1][0] = m[0][1]
                    am[1][1] = m[1][1]
                    am[1][2] = m[2][1]
                    am[1][3] = m[3][1]
                    am[2][0] = m[0][2]
                    am[2][1] = m[1][2]
                    am[2][2] = m[2][2]
                    am[2][3] = m[3][2]
                    am[3][0] = self.scale_length * m[0][3]
                    am[3][1] = self.scale_length * m[1][3]
                    am[3][2] = self.scale_length * m[2][3]
                    am[3][3] = m[3][3]
                    arnold.AiArraySetMtx(m_array, midx, am)
                # now find the Arnold node
                obj = bpy.data.objects[objName]
                if obj.type == 'MESH':
                    objName = "ME" + objName # ID_ME
                elif obj.type == 'SURFACE':
                    objName = "CU" + objName # ID_CU
                elif obj.type == 'CAMERA':
                    objName = "CA" + objName # ID_CA
                node = arnold.AiNodeLookUpByName(objName)
                if node != None:
                    arnold.AiNodeSetArray(node, "matrix", m_array)
                else:
                    if obj.type != 'EMPTY':
                        print("WARNING: couldn't look up Arnold node by name " +
                              "\"%s\"" % objName)
        # AiASSWrite
        sb = self.filename
        node_mask = arnold.AI_NODE_ALL
        open_procs = False
        binary = False
        intRet = arnold.AiASSWrite(sb, node_mask, open_procs, binary)
        print("INFO: %s" % self.filename)
        # AiEnd
        arnold.AiEnd()
