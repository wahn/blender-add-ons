import os
import math
# common exporter interface
from ..common.interface import CommonExporterInterface
# Blender
import bpy

class RibExporter(CommonExporterInterface):
    def __init__(self, options):
        CommonExporterInterface.__init__(self, "RibExporter", options)
        self.ribFile = None

    def prepareExport(self, scene, bbox, directory, name, mblur, light_counter,
                      sun = None):
        # scene related
        self.scene = scene
        self.bbox = bbox
        self.scene_name = name
        self.scale_length = scene.unit_settings.scale_length
        # light related
        self.light_counter = light_counter
        # motion blur related
        self.use_motion_blur = mblur[0]
        self.shutter_start = mblur[1]
        self.shutter_end = mblur[2]
        self.motion_blur_samples = mblur[3]
        self.animatedObjects = mblur[4]
        self.motionMatrices = mblur[5]
        # open
        filename = os.path.join(directory, name + ".rib")
        print('INFO: %s' % filename)
        self.ribFile = open(filename, "w")

    def writeCyclesMaterial(self, mat, primitive = "rect"):
        # return value
        aspect = 1.0
        patternWritten = False
        # try first to use Cycles nodes
        surface = mat.node_tree.nodes['Material Output'].inputs['Surface']
        if len(surface.links) >= 1:
            try:
                color = surface.links[0].from_node.inputs['Color']
            except KeyError:
                # handle mix of diffuse and glossy/transparent
                fac = surface.links[0].from_node.inputs['Fac']
                surface1 = surface.links[0].from_node.inputs[1]
                surface2 = surface.links[0].from_node.inputs[2]
                if surface1.links[0].from_node.type != 'BSDF_DIFFUSE':
                    print("WARNING: BSDF_DIFFUSE expected, %s found" %
                          surface1.links[0].from_node.type)
                if surface2.links[0].from_node.type != 'BSDF_GLOSSY':
                    if surface2.links[0].from_node.type != 'BSDF_TRANSPARENT':
                        print("WARNING: BSDF_GLOSSY|TRANSPARENT expected, " +
                              "%s found" %
                              surface2.links[0].from_node.type)
                diff = surface1.links[0].from_node.inputs['Color']
                diff_rough = surface1.links[0].from_node.inputs['Roughness']
                diffuse_roughness = diff_rough.default_value
                if surface2.links[0].from_node.type != 'BSDF_GLOSSY':
                    spec = [1, 1, 1, 1]
                    spec_rough = None
                    specular_roughness = 0.0
                else:
                    spec = surface2.links[0].from_node.inputs['Color']
                    spec_rough = surface2.links[0].from_node.inputs['Roughness']
                    specular_roughness = spec_rough.default_value
                Kd = 1.0 - fac.default_value
                r = diff.default_value[0]
                g = diff.default_value[1]
                b = diff.default_value[2]
                a = diff.default_value[3]
                Kd_color = [r, g, b]
                Ks = fac.default_value
                Kr = fac.default_value
                if surface2.links[0].from_node.type != 'BSDF_GLOSSY':
                    r = 1.0
                    g = 1.0
                    b = 1.0
                    a = 1.0
                    Ks_color = [r, g, b]
                    Kr_color = [r, g, b]
                else:
                    r = spec.default_value[0]
                    g = spec.default_value[1]
                    b = spec.default_value[2]
                    a = spec.default_value[3]
                    Ks_color = [r, g, b]
                    Kr_color = [r, g, b]
                # diffuse texture
                if diff.is_linked:
                    # image
                    image = diff.links[0].from_node.image
                    src = image.filepath
                    paths = bpy.utils.blend_paths()
                    if src in paths:
                        index = paths.index(src)
                        abs_paths = bpy.utils.blend_paths(absolute = True)
                        abs_path = abs_paths[index]
                        self.writeTexture(mat.name,
                                          "Kd", "color",
                                          abs_path)
                # glossy texture
                if (surface2.links[0].from_node.type == 'BSDF_GLOSSY' and
                    spec_rough.is_linked):
                    # image
                    image = spec_rough.links[0].from_node.image
                    src = image.filepath
                    paths = bpy.utils.blend_paths()
                    if src in paths:
                        index = paths.index(src)
                        abs_paths = bpy.utils.blend_paths(absolute = True)
                        abs_path = abs_paths[index]
                        self.writeTexture(mat.name,
                                          "Kr", "color",
                                          abs_path)
                # mix texture
                if fac.is_linked:
                    # image
                    image = fac.links[0].from_node.image
                    src = image.filepath
                    paths = bpy.utils.blend_paths()
                    if src in paths:
                        index = paths.index(src)
                        abs_paths = bpy.utils.blend_paths(absolute = True)
                        abs_path = abs_paths[index]
                        self.writeTexture(mat.name,
                                          "fac", "float",
                                          abs_path)
                # mix
                if fac.is_linked:
                    print("TODO: fac.is_linked in rib.writeCyclesMaterial()")
                else:
                    # layer1
                    self.ribFile.write('    Pattern "%s" "%s_%s"\n' %
                                       ("PxrLayer", mat.name, "PxrLayer1"))
                    diffuse_color = Kd_color
                    self.ribFile.write('      "color diffuseColor" [%s %s %s]\n' %
                                       (diffuse_color[0],
                                        diffuse_color[1],
                                        diffuse_color[2]))
                    self.ribFile.write('      "float diffuseRoughness" [%s]\n' %
                                       diffuse_roughness)
                    # layer2
                    self.ribFile.write('    Pattern "%s" "%s_%s"\n' %
                                       ("PxrLayer", mat.name, "PxrLayer2"))
                    self.ribFile.write('      "int enableSpecular" [1]\n')
                    specular_color = Kd_color
                    self.ribFile.write('      "color specularFaceColor" [%s %s %s]\n' %
                                       (specular_color[0],
                                        specular_color[1],
                                        specular_color[2]))
                    self.ribFile.write('      "float specularRoughness" [%s]\n' %
                                       specular_roughness)
                    # mix
                    self.ribFile.write('    Pattern "%s" "%s_%s"\n' %
                                       ("PxrLayerMixer", mat.name, "PxrLayerMixer"))
                    self.ribFile.write('      "float layer1Mask" [%s]\n' %
                                       fac.default_value)
                    self.ribFile.write('      "reference color baselayer_diffuseColor" ' +
                                       '["%s_%s:pxrMaterialOut_diffuseColor"]\n' %
                                       (mat.name, "PxrLayer1"))
                    self.ribFile.write('      "reference float baselayer_diffuseRoughness" ' +
                                       '["%s_%s:pxrMaterialOut_diffuseRoughness"]\n' %
                                       (mat.name, "PxrLayer1"))
                    self.ribFile.write('      "reference int layer1_enableSpecular" ' +
                                       '["%s_%s:pxrMaterialOut_enableSpecular"]\n' %
                                       (mat.name, "PxrLayer2"))
                    self.ribFile.write('      "reference color layer1_specularFaceColor" ' +
                                       '["%s_%s:pxrMaterialOut_specularFaceColor"]\n' %
                                       (mat.name, "PxrLayer2"))
                    self.ribFile.write('      "reference float layer1_specularRoughness" ' +
                                       '["%s_%s:pxrMaterialOut_specularRoughness"]\n' %
                                       (mat.name, "PxrLayer2"))
                    # surface
                    self.ribFile.write('    Bxdf "%s" "%s_%s"\n' %
                                       ("PxrSurface", mat.name, "PxrSurface"))
                    self.ribFile.write('      "int diffuseBackUseDiffuseColor" [1]\n')
                    self.ribFile.write('      "reference color diffuseColor" ' +
                                       '["%s_%s:pxrMaterialOut_diffuseColor"]\n' %
                                       (mat.name, "PxrLayerMixer"))
                    self.ribFile.write('      "reference color specularFaceColor" ' +
                                       '["%s_%s:pxrMaterialOut_specularFaceColor"] \n' %
                                       (mat.name, "PxrLayerMixer"))
                    self.ribFile.write('      "reference float diffuseRoughness" ' +
                                       '["%s_%s:pxrMaterialOut_diffuseRoughness"]\n' %
                                       (mat.name, "PxrLayerMixer"))
                return aspect
            else:
                if len(color.links) >= 1:
                    inputs = color.links[0].from_node.inputs
                    try:
                        filepath = color.links[0].from_node.script.filepath
                    except AttributeError:
                        pass
                    else:
                        print("INFO: filepath = %s" % filepath)
                        root, ext = os.path.splitext(filepath)
                        # oslinfo
                        execString = "oslinfo %s" % root
                        print(execString)
                        info = os.popen(execString)
                        lines = info.readlines()
                        info.close()
                        if len(lines):
                            basename = os.path.basename(filepath)
                            root, ext = os.path.splitext(basename)
                            self.ribFile.write('    Pattern "PxrOSL" ' +
                                               '"%s_pattern" "shader" "%s"\n' %
                                               (mat.name, root))
                        for line in lines:
                            words = line[:-1].split()
                            if words[0] == "surface":
                                # ignore
                                pass
                            elif words[0] == "output":
                                returnType = words[1]
                                name = words[2]
                            elif words[0] == "float":
                                name = words[1]
                                value = float(words[2]) # default
                                try:
                                    attr = inputs[name]
                                except KeyError:
                                    pass # use default
                                else:
                                    value = attr.default_value # actual value
                                self.ribFile.write('      "float %s" [%s]\n' %
                                                   (name, value))
                            elif words[0] == "color":
                                name = words[1]
                                # default
                                r = float(words[3])
                                g = float(words[4])
                                b = float(words[5])
                                try:
                                    attr = inputs[name]
                                except KeyError:
                                    pass # use default
                                else:
                                    # actual value
                                    r = attr.default_value[0]
                                    g = attr.default_value[1]
                                    b = attr.default_value[2]
                                self.ribFile.write('      "color %s" [%s %s %s]\n' %
                                                   (name, r, g, b))
                            else:
                                print("TODO: %s", words)
                        patternWritten = True
        if patternWritten:
            # TODO: for now just use diffuse
            if self.options.lighting != 'no_lights':
                self.ribFile.write('    Bxdf "PxrLMDiffuse" "%s"\n' %
                                   mat.name)
                self.ribFile.write('      "reference color frontColor" ' +
                                   '["%s_pattern:Color"]\n' % mat.name)
                self.ribFile.write('      "reference color backColor" ' +
                                   '["%s_pattern:Color"]\n' % mat.name)
        else:
            # do not just looks for named nodes, follow link from
            # 'Material Output' instead
            output = mat.node_tree.nodes['Material Output']
            surface = output.inputs['Surface']
            if surface.links[0].from_node.name == 'Emission':
                emission = mat.node_tree.nodes['Emission']
                # Emission node
                nodes = mat.node_tree.nodes
                surface = nodes['Material Output'].inputs['Surface']
                if len(surface.links) >= 1:
                    # Color
                    color = emission.inputs['Color']
                    strength = emission.inputs['Strength']
                    emit = strength.default_value
                    if len(color.links) == 0:
                        r = color.default_value[0]
                        g = color.default_value[1]
                        b = color.default_value[2]
                        a = color.default_value[3]
                        diffuse_color = [r, g, b]
                        # area light
                        if self.options.lighting != 'no_lights':
                            self.ribFile.write('    Bxdf "PxrBlack" "%s.PxrBlack" ' %
                                               (mat.name,) +
                                               '"__instanceid" ["%s.PxrBlack"]\n' %
                                               (mat.name,))
                            self.ribFile.write('    Light "PxrMeshLight" "%s"\n' % mat.name)
                            self.ribFile.write('      "float temperature" [6500]\n')
                            self.ribFile.write('      "__instanceid" ["%s"]\n' % mat.name)
                            self.ribFile.write('      "string lightGroup" [""]\n')
                            self.ribFile.write('      "color lightColor" [%s %s %s]\n' %
                                               (diffuse_color[0],
                                                diffuse_color[1],
                                                diffuse_color[2]))
                            self.ribFile.write('      "color textureColor" [1 1 1]\n')
                            self.ribFile.write('      "int fixedSampleCount" [0]\n')
                            self.ribFile.write('      "int traceLightPaths" [0]\n')
                            self.ribFile.write('      "int thinShadow" [1]\n')
                            self.ribFile.write('      "int enableTemperature" [0]\n')
                            self.ribFile.write('      "int areaNormalize" [0]\n')
                            self.ribFile.write('      "float exposure" [0]\n')
                            self.ribFile.write('      "float intensity" [%s]\n' % emit)
                            self.ribFile.write('      "float diffuse" [1]\n')
                            self.ribFile.write('      "float specular" [1]\n')
            elif surface.links[0].from_node.name == 'Diffuse BSDF':
                diff = surface.links[0].from_node.inputs['Color']
                diff_rough = surface.links[0].from_node.inputs['Roughness']
                r = diff.default_value[0]
                g = diff.default_value[1]
                b = diff.default_value[2]
                a = diff.default_value[3]
                Kd_color = [r, g, b]
                # diffuse
                if diff.is_linked:
                    # image
                    image = diff.links[0].from_node.image
                    src = image.filepath
                    paths = bpy.utils.blend_paths()
                    if src in paths:
                        index = paths.index(src)
                        abs_paths = bpy.utils.blend_paths(absolute = True)
                        abs_path = abs_paths[index]
                        self.writeTexture(mat.name,
                                          "Kd", "color",
                                          abs_path)
                    # use texture
                    self.ribFile.write('    Bxdf "PxrLMDiffuse" "%s"\n' %
                                       mat.name)
                    self.ribFile.write('      "reference color frontColor" ' +
                                       '["%s_diff_tex:resultRGB"]\n' %
                                       mat.name)
                    self.ribFile.write('      "reference color backColor" ' +
                                       '["%s_diff_tex:resultRGB"]\n' %
                                       mat.name)
                else:
                    self.ribFile.write('    Bxdf "PxrLMDiffuse" "%s"\n' %
                                       mat.name)
                    # no texture
                    diffuse_color = Kd_color
                    self.ribFile.write('      "color frontColor" [%s %s %s]\n' %
                                       (diffuse_color[0],
                                        diffuse_color[1],
                                        diffuse_color[2]))
                    self.ribFile.write('      "color backColor" [%s %s %s]\n' %
                                       (diffuse_color[0],
                                        diffuse_color[1],
                                        diffuse_color[2]))
            else:
                print("TODO: use other nodes than 'Emission' or 'Diffuse BSDF'")
                aspect = self.writeBlenderMaterial(mat, primitive)
        return aspect

    def writeBlenderMaterial(self, mat, primitive = "rect"):
        # return value
        aspect = 1.0
        if self.options.lighting != 'global_illumination':
            if mat.emit != 0.0:
                self.ribFile.write("    Surface \"rib_constant\"\n")
            else:
                diffuse_intensity = mat.diffuse_intensity
                Kd = diffuse_intensity
                Ka = 1.0 - Kd
                textures_found = 0
                textures = {}
                if mat:
                    # check for textures
                    for index in list(range(len(mat.texture_slots))):
                        slot = mat.texture_slots[index]
                        if slot:
                            texture = slot.texture
                            if slot.texture_coords == 'UV':
                                if texture.type == 'IMAGE':
                                    image = texture.image
                                    iszx = image.size[0]
                                    iszy = image.size[1]
                                    aspect = iszx / float(iszy)
                                    src = image.filepath
                                    if slot.use_map_color_diffuse:
                                        # diffuse texture
                                        textures["diffuse"] = src
                                        textures_found += 1
                texturename = ""
                prman_texturename = ""
                if textures_found:
                    diffuse_tex = textures["diffuse"]
                    paths = bpy.utils.blend_paths()
                    if diffuse_tex in paths:
                        index = paths.index(diffuse_tex)
                        abs_paths = bpy.utils.blend_paths(absolute = True)
                        abs_path = abs_paths[index]
                        basename = os.path.basename(abs_path)
                        root, ext = os.path.splitext(basename)
                        # assume the texture was copied to the export directory
                        texturename = root + "_tdlmake" + ext
                        prman_texturename = root + "_txmake" + ext
                if (self.options.lighting == 'no_lights' or
                    self.light_counter == 0):
                    self.ribFile.write("    Surface \"rib_defaultsurface\" " +
                                       "\"Ka\" [%s] \"Kd\" [%s]\n" % (Ka, Kd))
                    self.ribFile.write("           \"texturename\" " +
                                       "[\"%s\"]\n" %
                                       texturename)
                else:
                    # check for proper diffuse and specular model
                    if mat.diffuse_shader != 'LAMBERT':
                        print("WARNING: switching %s to 'LAMBERT'" % mat.name)
                        mat.diffuse_shader = 'LAMBERT'
                    if mat.specular_shader != 'PHONG':
                        print("WARNING: switching %s to 'PHONG'" % mat.name)
                        mat.specular_shader = 'PHONG'
                    # check if we need raytracing
                    use_raytracing = False
                    if mat.raytrace_mirror.use or mat.use_transparency:
                        use_raytracing = True
                    Ka = 0.0
                    specular_intensity = mat.specular_intensity
                    Ks = specular_intensity
                    specular_hardness = mat.specular_hardness
                    Ks_color = mat.specular_color
                    if self.options.lighting == 'global_illumination':
                        self.ribFile.write("    Surface " +
                                           "\"rib_blender_material_GI\"\n")
                    else:
                        if use_raytracing:
                            self.ribFile.write("    Surface " +
                                               "\"rib_blender_material_ray\"\n")
                        else:
                            self.ribFile.write("    Surface " +
                                               "\"rib_blender_material\"\n")
                    self.ribFile.write("            \"float Ka\" [%s]\n" % Ka +
                                       "            \"float Kd\" [%s]\n" % Kd +
                                       "            \"float Ks\" [%s]\n" % Ks)
                    self.ribFile.write("            \"color Ks_color\" " +
                                       "[%s %s %s]\n" %
                                       (Ks_color[0],
                                        Ks_color[1],
                                        Ks_color[2]))
                    self.ribFile.write("            " +
                                       "\"float specular_hardness\" " +
                                       "[%s]\n" %
                                       specular_hardness)
                    self.ribFile.write("            \"string texturename\" " +
                                       "[\"%s\"]\n" %
                                       texturename)
                    # PRMan
                    if prman_texturename:
                        self.ribFile.write('    # PRMan\n')
                        self.ribFile.write("    # Surface " +
                                           "\"texmap\"\n")
                        self.ribFile.write("            # \"string texname\" " +
                                           "[\"%s\"]\n" %
                                           prman_texturename)
                    if mat.raytrace_mirror.use:
                        Kr = mat.raytrace_mirror.reflect_factor
                        self.ribFile.write("            \"float Kr\" [%s]\n" %
                                           Kr)
                        Kr_color = mat.mirror_color
                        self.ribFile.write("            \"color Kr_color\" " +
                                           "[%s %s %s]\n" %
                                           (Kr_color[0],
                                            Kr_color[1],
                                            Kr_color[2]))
                    if mat.use_transparency:
                        Kt = 1.0 - mat.alpha
                        self.ribFile.write("            \"float Kt\" " +
                                           "[%s]\n" % Kt)
                        IOR = mat.raytrace_transparency.ior
                        self.ribFile.write("            \"float IOR\" " +
                                           "[%s]\n" % IOR)
            return aspect
        else:
            if mat.emit > 0.0:
                diffuse_color = mat.diffuse_color
                # area light
                self.ribFile.write('    Bxdf "PxrBlack" "%s.PxrBlack" ' %
                                   (mat.name,) +
                                   '"__instanceid" ["%s.PxrBlack"]\n' %
                                   (mat.name,))
                self.ribFile.write('    Light "PxrMeshLight" "%s"\n' % mat.name)
                self.ribFile.write('      "float temperature" [6500]\n')
                self.ribFile.write('      "__instanceid" ["%s"]\n' % mat.name)
                self.ribFile.write('      "string lightGroup" [""]\n')
                self.ribFile.write('      "color lightColor" [%s %s %s]\n' %
                                   (diffuse_color[0],
                                    diffuse_color[1],
                                    diffuse_color[2]))
                self.ribFile.write('      "color textureColor" [1 1 1]\n')
                self.ribFile.write('      "int fixedSampleCount" [0]\n')
                self.ribFile.write('      "int traceLightPaths" [0]\n')
                self.ribFile.write('      "int thinShadow" [1]\n')
                self.ribFile.write('      "int enableTemperature" [0]\n')
                self.ribFile.write('      "int areaNormalize" [0]\n')
                self.ribFile.write('      "float exposure" [0]\n')
                self.ribFile.write('      "float intensity" [%s]\n' % mat.emit)
                self.ribFile.write('      "float diffuse" [1]\n')
                self.ribFile.write('      "float specular" [1]\n')
            elif mat.use_transparency:
                refractionColor = mat.diffuse_color
                # glass
                self.ribFile.write('    Attribute "trace" ' +
                                   '"constant int maxspeculardepth" [6]\n')
                self.ribFile.write('    Attribute "visibility"\n')
                self.ribFile.write('      "constant int transmission" [1]\n')
                self.ribFile.write('      "constant int indirect" [1]\n')
                self.ribFile.write('    Bxdf "PxrLMGlass" "%s"\n' %
                                   mat.name)
                ior = mat.raytrace_transparency.ior
                self.ribFile.write('      "float eta" [%s]\n' % ior)
                reflectionColor = [1.0, 1.0, 1.0]
                self.ribFile.write('      "color reflectionColor" ' +
                                   '[%s %s %s]\n' %
                                   (reflectionColor[0],
                                    reflectionColor[1],
                                    reflectionColor[2]))
                self.ribFile.write('      "color refractionColor" ' +
                                   '[%s %s %s]\n' %
                                   (refractionColor[0],
                                    refractionColor[1],
                                    refractionColor[2]))
                self.ribFile.write('      "int shadows" [1]\n')
            elif mat.raytrace_mirror.use:
                if mat.diffuse_intensity > 0.0:
                    diffuse_color = mat.diffuse_color
                    mirror_color = mat.mirror_color
                    roughness = math.sqrt(mat.roughness)
                    # plastic
                    self.ribFile.write('    Bxdf "PxrLMPlastic" "%s"\n' %
                                       mat.name)
                    self.ribFile.write('      "float diffuseRoughness" [%s]\n' %
                                       roughness)
                    self.ribFile.write('      "float specularRoughness" ' +
                                       '[%s]\n' %
                                       roughness)
                    if self.options.cycles and mat.use_nodes:
                        try: # mix (assumes diffuse and glossy)
                            mix = mat.node_tree.nodes['Mix Shader']
                        except KeyError:
                            self.ribFile.write('      "color diffuseColor" ' +
                                               '[%s %s %s]\n' %
                                               (diffuse_color[0],
                                                diffuse_color[1],
                                                diffuse_color[2]))
                            self.ribFile.write('      "color specularColor" ' +
                                               '[%s %s %s]\n' %
                                               (mirror_color[0],
                                                mirror_color[1],
                                                mirror_color[2]))
                        else:
                            clearcoat = mix.inputs['Fac'].default_value
                            self.ribFile.write('      "color diffuseColor" ' +
                                               '[%s %s %s]\n' %
                                               ((1.0 - clearcoat) *
                                                diffuse_color[0],
                                                (1.0 - clearcoat) *
                                                diffuse_color[1],
                                                (1.0 - clearcoat) *
                                                diffuse_color[2]))
                            self.ribFile.write('      "color specularColor" ' +
                                               '[%s %s %s]\n' %
                                               (clearcoat * mirror_color[0],
                                                clearcoat * mirror_color[1],
                                                clearcoat * mirror_color[2]))
                            self.ribFile.write('      "color clearcoatColor" ' +
                                               '[%s %s %s]\n' %
                                               (clearcoat,
                                                clearcoat,
                                                clearcoat))
                            self.ribFile.write('      "float clearcoatEta"' +
                                               ' [3.0]\n') # TODO
                    else:
                        clearcoat = mat.raytrace_mirror.reflect_factor
                        self.ribFile.write('      "color diffuseColor" ' +
                                           '[%s %s %s]\n' %
                                           ((1.0 - clearcoat) *
                                            diffuse_color[0],
                                            (1.0 - clearcoat) *
                                            diffuse_color[1],
                                            (1.0 - clearcoat) *
                                            diffuse_color[2]))
                        self.ribFile.write('      "color specularColor" ' +
                                           '[%s %s %s]\n' %
                                           (clearcoat * mirror_color[0],
                                            clearcoat * mirror_color[1],
                                            clearcoat * mirror_color[2]))
                        self.ribFile.write('      "color clearcoatColor" ' +
                                           '[%s %s %s]\n' %
                                           (clearcoat,
                                            clearcoat,
                                            clearcoat))
                        self.ribFile.write('      "float clearcoatEta"' +
                                           ' [3.0]\n') # TODO
                else:
                    mirror_color = mat.mirror_color
                    # metal
                    self.ribFile.write('    Bxdf "PxrLMMetal" "%s"\n' %
                                       mat.name)
                    self.ribFile.write('      "color specularColor" ' +
                                       '[%s %s %s]\n' %
                                       (mirror_color[0],
                                        mirror_color[1],
                                        mirror_color[2]))
                    roughness = math.sqrt(mat.roughness)
                    self.ribFile.write('      "float roughness" [%s]\n' %
                                       roughness)
            elif mat.specular_intensity > 0.0:
                diffuse_color = mat.diffuse_color
                specular_color = mat.specular_color
                roughness = math.sqrt(mat.roughness)
                # plastic
                self.ribFile.write('    Bxdf "PxrLMPlastic" "%s"\n' %
                                   mat.name)
                self.ribFile.write('      "color diffuseColor" [%s %s %s]\n' %
                                   (diffuse_color[0],
                                    diffuse_color[1],
                                    diffuse_color[2]))
                self.ribFile.write('      "color specularColor" [%s %s %s]\n' %
                                   (specular_color[0],
                                    specular_color[1],
                                    specular_color[2]))
                self.ribFile.write('      "float diffuseRoughness" [%s]\n' %
                                   roughness)
                self.ribFile.write('      "float specularRoughness" [%s]\n' %
                                   roughness)
            else:
                textures_found = 0
                textures = {}
                # check for textures
                for index in list(range(len(mat.texture_slots))):
                    slot = mat.texture_slots[index]
                    if slot:
                        texture = slot.texture
                        if slot.texture_coords == 'UV':
                            if texture.type == 'IMAGE':
                                image = texture.image
                                iszx = image.size[0]
                                iszy = image.size[1]
                                aspect = iszx / float(iszy)
                                src = image.filepath
                                if slot.use_map_color_diffuse:
                                    # diffuse texture
                                    textures["diffuse"] = src
                                    textures_found += 1
                texturename = ""
                prman_texturename = ""
                if textures_found:
                    diffuse_tex = textures["diffuse"]
                    paths = bpy.utils.blend_paths()
                    if diffuse_tex in paths:
                        index = paths.index(diffuse_tex)
                        abs_paths = bpy.utils.blend_paths(absolute = True)
                        abs_path = abs_paths[index]
                        basename = os.path.basename(abs_path)
                        root, ext = os.path.splitext(basename)
                        # assume the texture was copied to the export directory
                        texturename = root + "_tdlmake" + ext
                        prman_texturename = root + "_txmake" + ext
                # diffuse
                if prman_texturename:
                    # define texture
                    self.ribFile.write('    Pattern "PxrTexture" ' +
                                       '"%s_diff_tex"\n' %
                                       mat.name)
                    self.ribFile.write('      "string filename" ["%s"]\n' %
                                       prman_texturename)
                    self.ribFile.write('      "int invertT" [0] "int linearize" [1]\n')
                    # use texture
                    self.ribFile.write('    Bxdf "PxrLMDiffuse" "%s"\n' %
                                       mat.name)
                    self.ribFile.write('      "reference color frontColor" ' +
                                       '["%s_diff_tex:resultRGB"]\n' %
                                       mat.name)
                    self.ribFile.write('      "reference color backColor" ' +
                                       '["%s_diff_tex:resultRGB"]\n' %
                                       mat.name)
                else:
                    self.ribFile.write('    Bxdf "PxrLMDiffuse" "%s"\n' %
                                       mat.name)
                    # no texture
                    diffuse_color = mat.diffuse_color
                    self.ribFile.write('      "color frontColor" [%s %s %s]\n' %
                                       (diffuse_color[0],
                                        diffuse_color[1],
                                        diffuse_color[2]))
                    self.ribFile.write('      "color backColor" [%s %s %s]\n' %
                                       (diffuse_color[0],
                                        diffuse_color[1],
                                        diffuse_color[2]))
        return aspect

    def writeCamera(self, name, lens, angle, resolution, border, AA_samples,
                    tile_size, transform, isRenderCamera, sun = None):
        RMANTREE = "/mill3d/server/apps/PIXAR/linux-x86-64/RenderManProServer-20.9"
        if not isRenderCamera:
            m = transform.copy()
            m.invert()
            self.ribFile.write("# %s\n" % name)
            self.ribFile.write("# ConcatTransform [\n" +
                               "#   %s %s %s %s\n" %
                               (m[0][0], m[1][0], m[2][0], m[3][0]) +
                               "#   %s %s %s %s\n" %
                               (m[0][1], m[1][1], m[2][1], m[3][1]) +
                               "#   %s %s %s %s\n" %
                               (m[0][2], m[1][2], m[2][2], m[3][2]) +
                               "#   %s %s %s %s\n#   ]\n" %
                               (self.scale_length * m[0][3],
                                self.scale_length * m[1][3],
                                self.scale_length * m[2][3],
                                m[3][3]))
        else:
            self.ribFile.write('##RenderMan RIB\n')
            self.ribFile.write('#Generated by export_multi for Blender (by Jan Walter), PRMan v21.2.0 \n')
            self.ribFile.write('version 3.04\n')
            # statistics (works for PRMan and 3Delight)
            self.ribFile.write('Option "statistics" "endofframe" [1]\n')
            if self.options.lighting == 'global_illumination':
                # https://renderman.pixar.com/resources/current/RenderMan/risLights.html
                self.ribFile.write('Option "shading" ' +
                                   '"int directlightinglocalizedsampling" ' +
                                   '[4]\n')
            else:
                # PRMan
                self.ribFile.write('# PRMan\n')
                self.ribFile.write('Option "searchpath" "shader"\n')
                path1 = "/mill3d/users/jan/git/blender-add-ons/shaders/RenderMan"
                path2 = "%s/lib/shaders" % RMANTREE
                self.ribFile.write('  "%s:%s"\n' % (path1, path2))
            # Exposure
            self.ribFile.write('Exposure 1.0 1.0\n')
            # bucket
            self.ribFile.write('# bucket\n')
            self.ribFile.write('Option "bucket" "string order" ["spiral"]\n')
            # Display (3Delight)
            self.ribFile.write('# 3Delight\n')
            self.ribFile.write('# Display "%s.exr" "%s" "%s" ' %
                               ("renderman", "exr", "rgba") +
                               ' "quantize" [0 0 0 0]\n')
            if self.options.aov_Z:
                # additional EXR layers Z (1 channels, float)
                self.ribFile.write('# Display "%s" "%s" "%s" ' %
                                   ("+renderman.exr", "exr", "z") +
                                   '"quantize" [0 0 0 0]\n')
            if self.options.aov_N:
                # write aov_N into a EXR file
                self.ribFile.write('# Declare "aov_N" "varying normal"\n')
                self.ribFile.write('# Display "%s" "%s" "%s" ' %
                                   ("+renderman_aov_N.exr", "exr", "aov_N") +
                                   '"quantize" [0 0 0 0]\n')
            if self.options.aov_P:
                # write aov_P into a EXR file
                self.ribFile.write('# Declare "aov_P" "varying point"\n')
                self.ribFile.write('# Display "%s" "%s" "%s" ' %
                                   ("+renderman_aov_P.exr", "exr", "aov_P") +
                                   '"quantize" [0 0 0 0]\n')
            # Display (PRMan)
            self.ribFile.write('# PRMan\n')
            self.ribFile.write('Display "%s.exr" "%s" "%s"\n' %
                               ("prman", "openexr", "rgba"))
            if self.options.aov_Z:
                # additional EXR layers Z (1 channels, float)
                self.ribFile.write('Display "%s" "%s" "%s" ' %
                                   ("+prman_aov_Z.tif", "zfile", "z") +
                                   '"quantize" [0 0 0 0]\n')
            if self.options.aov_N:
                # write aov_N into a EXR file
                self.ribFile.write('Declare "aov_N" "varying normal"\n')
                self.ribFile.write('Display "%s" "%s" "%s" ' %
                                   ("+prman_aov_N.tif", "file", "aov_N") +
                                   '"quantize" [0 0 0 0]\n')
            if self.options.aov_P:
                # write aov_P into a EXR file
                self.ribFile.write('Declare "aov_P" "varying point"\n')
                self.ribFile.write('Display "%s" "%s" "%s" ' %
                                   ("+prman_aov_P.tif", "file", "aov_P") +
                                   '"quantize" [0 0 0 0]\n')
            if self.options.lighting == 'global_illumination':
                self.ribFile.write('Hider "raytrace"\n')
                self.ribFile.write('  "constant int incremental" [1]\n')
                self.ribFile.write('Integrator "PxrVCM" "PxrVCM"\n')
                self.ribFile.write('  "int maxPathLength" [10]\n')
                self.ribFile.write('  "int mergePaths" [1]\n')
                self.ribFile.write('  "int connectPaths" [1]\n')
                self.ribFile.write('PixelVariance .001\n')
            elif self.options.lighting == 'direct_lighting':
                self.ribFile.write('Hider "raytrace"\n')
            else:
                self.ribFile.write('# Hider "raytrace"\n')
            self.ribFile.write('Format %s %s 1.0\n' %
                               (resolution[0], resolution[1]))
            # border?
            if border:
                xmin = border[0]
                xmax = border[1]
                ymin = 1.0 - border[3]
                ymax = 1.0 - border[2]
                self.ribFile.write('CropWindow %s %s %s %s\n' %
                                   (xmin, xmax, ymin, ymax))
            aspect = resolution[0] / float(resolution[1])
            self.ribFile.write("ShadingRate 1.0\n")
            # self.ribFile.write('Option "limits" "integer bucketsize[2]" ')
            # self.ribFile.write('[%s %s]\n' % (tile_size[0], tile_size[1]))
            # self.ribFile.write('Option "limits" "integer gridsize" ')
            # self.ribFile.write('[%s]\n' % (tile_size[0] * tile_size[1]))
            # camera
            if self.use_motion_blur:
                self.ribFile.write("Shutter %s %s\n" %
                                   (self.shutter_start, self.shutter_end))
            if aspect >= 1.0:
                fov = 2 * math.degrees(math.atan(16.0 / (aspect * lens)))
            else:
                fov = 2 * math.degrees(math.atan((aspect * 16.0) / lens))
            self.ribFile.write("# lens %s, aspect %s\n" %
                               (lens, aspect))
            self.ribFile.write("Projection \"%s\" \"%s\" [ %s ]\n" %
                               ("perspective", "fov", fov))
            if self.use_motion_blur and (name in self.animatedObjects):
                # for motion blur increase pixel samples
                self.ribFile.write("PixelSamples 4 4\n")
            self.ribFile.write("Rotate 180 0 1 0 # right handed\n")
            self.ribFile.write("Scale -1 1 1 # right handed\n")
            m = transform.copy()
            m.invert()
            self.ribFile.write("# %s\n" % name)
            if self.use_motion_blur and (name in self.animatedObjects):
                # WorldBegin first !!!
                self.ribFile.write("WorldBegin\n")
                # many matrices
                matrices = self.motionMatrices[name]
                self.ribFile.write("  MotionBegin [ ")
                frame_step = 1.0 / float(self.motion_blur_samples - 1)
                for i in range(self.motion_blur_samples):
                    subframe = self.shutter_end * frame_step * i
                    if i == 0:
                        self.ribFile.write("  %s\n" % subframe)
                    elif i == (self.motion_blur_samples - 1):
                        self.ribFile.write("                %s ]\n" % subframe)
                    else:
                        self.ribFile.write("                %s\n" % subframe)
                for midx in range(len(matrices)):
                    m = matrices[midx][1] # inverse = [1]
                    self.ribFile.write("    ConcatTransform [\n" +
                                       "      %s %s %s %s\n" %
                                       (m[0][0], m[1][0], m[2][0], m[3][0]) +
                                       "      %s %s %s %s\n" %
                                       (m[0][1], m[1][1], m[2][1], m[3][1]) +
                                       "      %s %s %s %s\n" %
                                       (m[0][2], m[1][2], m[2][2], m[3][2]) +
                                       "      %s %s %s %s\n  ]\n" %
                                       (self.scale_length * m[0][3],
                                        self.scale_length * m[1][3],
                                        self.scale_length * m[2][3],
                                        m[3][3]))
                self.ribFile.write("  MotionEnd\n")
                # Volume?
                self.writeVolume()
            else:
                self.ribFile.write("ConcatTransform [\n" +
                                   "  %s %s %s %s\n" %
                                   (m[0][0], m[1][0], m[2][0], m[3][0]) +
                                   "  %s %s %s %s\n" %
                                   (m[0][1], m[1][1], m[2][1], m[3][1]) +
                                   "  %s %s %s %s\n" %
                                   (m[0][2], m[1][2], m[2][2], m[3][2]) +
                                   "  %s %s %s %s\n  ]\n" %
                                   (self.scale_length * m[0][3],
                                    self.scale_length * m[1][3],
                                    self.scale_length * m[2][3],
                                    m[3][3]))
                self.ribFile.write("WorldBegin\n")
                # Volume?
                self.writeVolume()
            if self.options.lighting == 'direct_lighting':
                # Attribute
                self.ribFile.write('  Attribute "visibility" ' +
                                   '"int diffuse" [1]\n')
                self.ribFile.write('  Attribute "visibility" ' +
                                   '"int specular" [1]\n')
                self.ribFile.write('  Attribute "visibility" ' +
                                   '"int reflection" [1] # 3Delight\n')
                self.ribFile.write('  Attribute "visibility" ' +
                                   '"int transmission" [1]\n')
                self.ribFile.write('  Attribute "light" "shadows" "on"\n')

    def writeCommonPrimitive(self, name, transform, mat, primitive = "rect"):
        self.ribFile.write("  # %s\n" % name)
        # AttributeBegin
        self.ribFile.write("  AttributeBegin\n")
        self.ribFile.write("    Attribute " +
                           "\"%s\" \"%s\" [\"%s\"]\n" %
                           ("identifier", "string name", name))
        if self.use_motion_blur and (name in self.animatedObjects):
            # many matrices
            matrices = self.motionMatrices[name]
            self.ribFile.write("    MotionBegin [ ")
            frame_step = 1.0 / float(self.motion_blur_samples - 1)
            for i in range(self.motion_blur_samples):
                subframe = self.shutter_end * frame_step * i
                if i == 0:
                    self.ribFile.write("%s\n" % subframe)
                elif i == (self.motion_blur_samples - 1):
                    self.ribFile.write("                  %s ]\n" % subframe)
                else:
                    self.ribFile.write("                  %s\n" % subframe)
            for midx in range(len(matrices)):
                m = matrices[midx][0]
                self.ribFile.write("      ConcatTransform [\n" +
                                   "        %s %s %s %s\n" %
                                   (m[0][0], m[1][0], m[2][0], m[3][0]) +
                                   "        %s %s %s %s\n" %
                                   (m[0][1], m[1][1], m[2][1], m[3][1]) +
                                   "        %s %s %s %s\n" %
                                   (m[0][2], m[1][2], m[2][2], m[3][2]) +
                                   "        %s %s %s %s\n      ]\n" %
                                   (self.scale_length * m[0][3],
                                    self.scale_length * m[1][3],
                                    self.scale_length * m[2][3],
                                    m[3][3]))
            self.ribFile.write("    MotionEnd\n")
        else:
            # one matrix
            m = transform
            self.ribFile.write("    ConcatTransform [\n" +
                  "      %s %s %s %s\n" %
                  (m[0][0], m[1][0], m[2][0], m[3][0]) +
                  "      %s %s %s %s\n" %
                  (m[0][1], m[1][1], m[2][1], m[3][1]) +
                  "      %s %s %s %s\n" %
                  (m[0][2], m[1][2], m[2][2], m[3][2]) +
                  "      %s %s %s %s\n    ]\n" %
                               (self.scale_length * m[0][3],
                                self.scale_length * m[1][3],
                                self.scale_length * m[2][3],
                                m[3][3]))
        if mat != None:
            self.ribFile.write("    # %s\n" % mat.name)
            # traceable?
            if not mat.use_raytrace:
                self.ribFile.write('    Attribute "visibility" ' +
                                   '"string transmission" ["transparent"]\n')
                self.ribFile.write('    Attribute "visibility" ' +
                                   '"int transmission" [0]\n')
            # Sides (two-sided)
            self.ribFile.write('    Sides 2\n')
            # Opacity
            if mat.use_transparency:
                alpha = mat.alpha
                self.ribFile.write("    Opacity [%s %s %s]\n" %
                                   (alpha, alpha, alpha))
            else:
                self.ribFile.write("    Opacity [1 1 1]\n")
            # Color
            diffuse_color = mat.diffuse_color
            self.ribFile.write("    Color [%s %s %s]\n" %
                               (diffuse_color[0],
                                diffuse_color[1],
                                diffuse_color[2]))
            # Surface
            self.writeBlenderMaterial(mat, primitive)

    def writeCone(self, name, transform, info, mat):
        self.writeCommonPrimitive(name, transform, mat)
        # Hyperboloid
        obj = bpy.data.objects[name]
        p1 = obj.data.splines[0].points[0].co
        p2 = obj.data.splines[0].points[8].co
        self.ribFile.write("    Hyperboloid %s %s %s %s %s %s 360\n" %
                           (self.scale_length * math.fabs(p1.y), 0.0, self.scale_length * p1.z,
                            self.scale_length * math.fabs(p2.y), 0.0, self.scale_length * p2.z))
        # AttributeEnd
        self.ribFile.write("  AttributeEnd\n")

    def writeCylinder(self, name, transform, info, mat):
        self.writeCommonPrimitive(name, transform, mat)
        # Cylinder
        self.ribFile.write("    Cylinder %s 0 %s 360\n" %
                           (self.scale_length, self.scale_length))
        # AttributeEnd
        self.ribFile.write("  AttributeEnd\n")

    def writeMesh(self, name, transform, info, user_matrix = None):
        # info
        materials = info[0]
        vertices = info[1]
        vertexNormals = info[2]
        polygons = info[3]
        polygonNormals = info[4]
        smoothPolygons = info[5]
        polygonMaterialIndices = info[6]
        hasUVs, uvs = info[7]
        # name
        self.ribFile.write("  # %s\n" % name)
        # AttributeBegin
        self.ribFile.write("  AttributeBegin\n")
        self.ribFile.write("    Attribute " +
                           "\"%s\" \"%s\" [\"%s\"]\n" %
                           ("identifier", "string name", name))
        if self.use_motion_blur and (name in self.animatedObjects):
            # many matrices
            matrices = self.motionMatrices[name]
            self.ribFile.write("    MotionBegin [ ")
            frame_step = 1.0 / float(self.motion_blur_samples - 1)
            for i in range(self.motion_blur_samples):
                subframe = self.shutter_end * frame_step * i
                if i == 0:
                    self.ribFile.write("%s\n" % subframe)
                elif i == (self.motion_blur_samples - 1):
                    self.ribFile.write("                  %s ]\n" % subframe)
                else:
                    self.ribFile.write("                  %s\n" % subframe)
            for midx in range(len(matrices)):
                m = matrices[midx][0]
                self.ribFile.write("      ConcatTransform [\n" +
                                   "        %s %s %s %s\n" %
                                   (m[0][0], m[1][0], m[2][0], m[3][0]) +
                                   "        %s %s %s %s\n" %
                                   (m[0][1], m[1][1], m[2][1], m[3][1]) +
                                   "        %s %s %s %s\n" %
                                   (m[0][2], m[1][2], m[2][2], m[3][2]) +
                                   "        %s %s %s %s\n      ]\n" %
                                   (self.scale_length * m[0][3],
                                    self.scale_length * m[1][3],
                                    self.scale_length * m[2][3],
                                    m[3][3]))
            self.ribFile.write("    MotionEnd\n")
        else:
            # one matrix
            m = transform
            self.ribFile.write("    ConcatTransform [\n" +
                  "      %s %s %s %s\n" %
                  (m[0][0], m[1][0], m[2][0], m[3][0]) +
                  "      %s %s %s %s\n" %
                  (m[0][1], m[1][1], m[2][1], m[3][1]) +
                  "      %s %s %s %s\n" %
                  (m[0][2], m[1][2], m[2][2], m[3][2]) +
                  "      %s %s %s %s\n    ]\n" %
                               (self.scale_length * m[0][3],
                                self.scale_length * m[1][3],
                                self.scale_length * m[2][3],
                                m[3][3]))
        # sort by materials
        infoMaterial = {}
        for matIndex in range(len(materials)):
            matName = materials[matIndex]
            # store for each material a list with len(vertices) 0 entries
            matIndices = [0] * len(vertices)
            infoMaterial[matIndex] = matIndices
        for pi in range(len(polygonMaterialIndices)):
            pmi = polygonMaterialIndices[pi]
            polygon = polygons[pi]
            for vi in polygon:
                # count how often a vertex is used per material
                infoMaterial[pmi][vi] += 1
        for matIndex in range(len(materials)):
            counter = 0
            for index in range(len(infoMaterial[matIndex])):
                vi = infoMaterial[matIndex][index]
                # replace the counter by ...
                if vi == 0:
                    # ... -1 if the material doesn't use this vertex
                    infoMaterial[matIndex][index] = -1
                else:
                    # ... or by an increasing index
                    infoMaterial[matIndex][index] = counter
                    counter += 1
        # for each material ...
        for matIndex in range(len(materials)):
            matName = materials[matIndex]
            mat = bpy.data.materials[matName]
            if len(materials) > 1:
                self.ribFile.write("    # %s [%s]\n" % (matName, matIndex))
            else:
                self.ribFile.write("    # %s\n" % matName)
            # traceable?
            if not mat.use_raytrace:
                self.ribFile.write('    Attribute "visibility" ' +
                                   '"string transmission" ["transparent"]\n')
                self.ribFile.write('    Attribute "visibility" ' +
                                   '"int transmission" [0]\n')
            # Sides (two-sided)
            self.ribFile.write('    Sides 2\n')
            # Opacity
            if mat.use_transparency:
                alpha = mat.alpha
            else:
                alpha = 1.0
            self.ribFile.write("    Opacity [%s %s %s]\n" %
                               (alpha, alpha, alpha))
            # Color
            diffuse_color = mat.diffuse_color
            self.ribFile.write("    Color [%s %s %s]\n" %
                               (diffuse_color[0],
                                diffuse_color[1],
                                diffuse_color[2]))
            # Surface
            if self.options.cycles and mat.use_nodes:
                aspect = self.writeCyclesMaterial(mat)
            else:
                aspect = self.writeBlenderMaterial(mat)
            # if there's a single smooth polygon, we export normals
            useVertexNormals = False
            for pi in range(len(polygonMaterialIndices)):
                pmi = polygonMaterialIndices[pi]
                if pmi == matIndex:
                    smoothPolygon = smoothPolygons[pi]
                    if smoothPolygon:
                        useVertexNormals = True
                        break
            # PointsPolygons
            self.ribFile.write("    PointsPolygons\n")
            # number of vertices per face
            self.ribFile.write("      [ ")
            for pi in range(len(polygonMaterialIndices)):
                pmi = polygonMaterialIndices[pi]
                if pmi == matIndex:
                    polygon = polygons[pi]
                    self.ribFile.write("%s " % len(polygon))
            self.ribFile.write("]\n")
            # indices per triangle/quad
            self.ribFile.write("      [ ")
            counter = 0
            for pi in range(len(polygonMaterialIndices)):
                pmi = polygonMaterialIndices[pi]
                if pmi == matIndex:
                    polygon = polygons[pi]
                    for vi in polygon:
                        # infoMaterial[matIndex][vi])
                        self.ribFile.write("%s " % counter)
                        counter += 1
            self.ribFile.write("]\n")
            # coordinates
            self.ribFile.write("      \"P\" [\n")
            for pi in range(len(polygonMaterialIndices)):
                pmi = polygonMaterialIndices[pi]
                if pmi == matIndex:
                    polygon = polygons[pi]
                    for vi in polygon:
                        v = vertices[vi] # use original index
                        self.ribFile.write("        %s %s %s\n" %
                                           (self.scale_length * v[0],
                                            self.scale_length * v[1],
                                            self.scale_length * v[2]))
            self.ribFile.write("      ]\n")
            # normals (based on smoothPolygons)
            if useVertexNormals:
                # normals
                self.ribFile.write("      \"N\" [\n")
                for pi in range(len(polygonMaterialIndices)):
                    pmi = polygonMaterialIndices[pi]
                    smoothPolygon = smoothPolygons[pi]
                    if pmi == matIndex:
                        if not smoothPolygon:
                            # use face normal
                            normal = polygonNormals[pi]
                        polygon = polygons[pi]
                        for vi in polygon:
                            if smoothPolygon:
                                # use vertex normal
                                n = vertexNormals[vi]
                            else:
                                n = normal # face normal
                            self.ribFile.write("        %s %s %s\n" %
                                               (n[0], n[1], n[2]))
                self.ribFile.write("      ]\n")
            # uv-coordinates
            if len(uvs):
                self.ribFile.write("      \"st\" [\n")
                for pi in range(len(polygonMaterialIndices)):
                    pmi = polygonMaterialIndices[pi]
                    if pmi == matIndex:
                        polygon = uvs[pi]
                        for uv in polygon:
                            if aspect < 1.0:
                                u = uv[0] * aspect + (1.0 - aspect) / 2.0
                                v = uv[1]
                            else:
                                inv_aspect = 1.0 / aspect
                                u = uv[0]
                                v = uv[1] * inv_aspect + (1.0 - inv_aspect) / 2.0
                            v = 1.0 - v
                            # TODO: don't use modulo, find a better solution
                            if u != 1.0 and v != 1.0:
                                self.ribFile.write('        %s %s\n' %
                                                   (u % 1, v % 1))
                            else:
                                if u != 1.0 and v == 1.0:
                                    self.ribFile.write('        %s %s\n' %
                                                       (u % 1, v))
                                elif u == 1.0 and v != 1.0:
                                    self.ribFile.write('        %s %s\n' %
                                                       (u, v % 1))
                                else:                                    
                                    self.ribFile.write('        %s %s\n' %
                                                       (u, v))
                self.ribFile.write("      ]\n")
        # AttributeEnd
        self.ribFile.write("  AttributeEnd\n")

    def writeNurbsSurface(self, name, transform, info):
        # info
        materials = info[0]
        splines = info[1]
        # name
        self.ribFile.write("  # %s\n" % name)
        # AttributeBegin
        self.ribFile.write("  AttributeBegin\n")
        self.ribFile.write("    Attribute " +
                           "\"%s\" \"%s\" [\"%s\"]\n" %
                           ("identifier", "string name", name))
        if self.use_motion_blur and (name in self.animatedObjects):
            # many matrices
            matrices = self.motionMatrices[name]
            self.ribFile.write("    MotionBegin [ ")
            frame_step = 1.0 / float(self.motion_blur_samples - 1)
            for i in range(self.motion_blur_samples):
                subframe = self.shutter_end * frame_step * i
                if i == 0:
                    self.ribFile.write("%s\n" % subframe)
                elif i == (self.motion_blur_samples - 1):
                    self.ribFile.write("                  %s ]\n" % subframe)
                else:
                    self.ribFile.write("                  %s\n" % subframe)
            for midx in range(len(matrices)):
                m = matrices[midx][0]
                self.ribFile.write("      ConcatTransform [\n" +
                                   "        %s %s %s %s\n" %
                                   (m[0][0], m[1][0], m[2][0], m[3][0]) +
                                   "        %s %s %s %s\n" %
                                   (m[0][1], m[1][1], m[2][1], m[3][1]) +
                                   "        %s %s %s %s\n" %
                                   (m[0][2], m[1][2], m[2][2], m[3][2]) +
                                   "        %s %s %s %s\n      ]\n" %
                                   (self.scale_length * m[0][3],
                                    self.scale_length * m[1][3],
                                    self.scale_length * m[2][3],
                                    m[3][3]))
            self.ribFile.write("    MotionEnd\n")
        else:
            # one matrix
            m = transform
            self.ribFile.write("    ConcatTransform [\n" +
                  "      %s %s %s %s\n" %
                  (m[0][0], m[1][0], m[2][0], m[3][0]) +
                  "      %s %s %s %s\n" %
                  (m[0][1], m[1][1], m[2][1], m[3][1]) +
                  "      %s %s %s %s\n" %
                  (m[0][2], m[1][2], m[2][2], m[3][2]) +
                  "      %s %s %s %s\n    ]\n" %
                               (self.scale_length * m[0][3],
                                self.scale_length * m[1][3],
                                self.scale_length * m[2][3],
                                m[3][3]))
        # assume ONE material
        matName = materials[0]
        mat = bpy.data.materials[matName]
        if len(materials) > 1:
            self.ribFile.write("    # %s [%s]\n" % (matName, matIndex))
        else:
            self.ribFile.write("    # %s\n" % matName)
        # traceable?
        if not mat.use_raytrace:
            self.ribFile.write('    Attribute "visibility" ' +
                               '"string transmission" ["transparent"]\n')
            self.ribFile.write('    Attribute "visibility" ' +
                               '"int transmission" [0]\n')
        # Sides (two-sided)
        self.ribFile.write('    Sides 2\n')
        # Opacity
        if mat.use_transparency:
            alpha = mat.alpha
        else:
            alpha = 1.0
        self.ribFile.write("    Opacity [%s %s %s]\n" %
                           (alpha, alpha, alpha))
        # Color
        diffuse_color = mat.diffuse_color
        self.ribFile.write("    Color [%s %s %s]\n" %
                           (diffuse_color[0],
                            diffuse_color[1],
                            diffuse_color[2]))
        # Surface
        if self.options.cycles and mat.use_nodes:
            self.writeCyclesMaterial(mat)
        else:
            self.writeBlenderMaterial(mat)
        # Patches
        for spline in splines:
            order_u = spline[0][0]
            order_v = spline[0][1]
            if (order_u == 4 and order_v == 4):
                self.ribFile.write('    Patch "bicubic" "Pw" [\n')
                points = spline[1]
                for point in points:
                    self.ribFile.write("      %s %s %s %s\n" %
                                       (point[0], point[1], point[2], point[3]))
                self.ribFile.write("    ]\n")
            else:
                self.ribFile.write("    # TODO: Patch [%s, %s]" %
                                   (order_u, order_v))
        # AttributeEnd
        self.ribFile.write("  AttributeEnd\n")

    def writePointLight(self, name, transform, info):
        # light_counter
        self.light_counter += 1
        # info
        color = info[0]
        energy = info[1]
        falloff = info[2]
        use_diffuse = info[3][0]
        use_specular = info[3][1]
        # falloff
        falloff_type = falloff[0]
        # name
        self.ribFile.write("  # %s\n" % name)
        self.ribFile.write("  Attribute " +
                           "\"%s\" \"%s\" [\"%s\"]\n" %
                           ("identifier", "string name", name))
        if self.options.lighting != 'global_illumination':
            # LightSource
            loc, rot, scale = transform.decompose()
            self.ribFile.write('  LightSource "rib_blender_pointlight" ' +
                               '%s\n' % self.light_counter +
                               '    "point from" [%s %s %s]\n' %
                               (loc[0], loc[1], loc[2]) +
                               '    "float intensity" %s\n' %
                               energy +
                               '    "color lightcolor" ' +
                               '[%s %s %s]\n' % (color[0],
                                                 color[1],
                                                 color[2]))
            # falloff_type
            if falloff_type == 'INVERSE_SQUARE':
                self.ribFile.write('    "float decay" 2\n')
            elif falloff_type == 'INVERSE_LINEAR':
                self.ribFile.write('    "float decay" 1\n')
            elif (falloff_type == 'CUSTOM_CURVE' or
                  falloff_type == 'LINEAR_QUADRATIC_WEIGHTED'):
                print("WARNING: falling back to 'CONSTANT' falloff type")
                self.ribFile.write('    "float decay" 0\n')
            else: # 'CONSTANT'
                self.ribFile.write('    "float decay" 0\n')
            # diffuse?
            if not use_diffuse:
                self.ribFile.write('    "float __nondiffuse" 1\n')
            # specular?
            if not use_specular:
                self.ribFile.write('    "float __nonspecular" 1\n')

    def writeRing(self, name, transform, info, mat):
        self.writeCommonPrimitive(name, transform, mat)
        # Hyperboloid
        obj = bpy.data.objects[name]
        p1 = obj.data.splines[0].points[0].co
        p2 = obj.data.splines[0].points[8].co
        self.ribFile.write("    Hyperboloid " +
                           "%s %s %s %s %s %s 360\n" %
                           (self.scale_length * math.fabs(p1.y), 0.0, self.scale_length * p1.z,
                            self.scale_length * math.fabs(p2.y), 0.0, self.scale_length * p2.z))
        # AttributeEnd
        self.ribFile.write("  AttributeEnd\n")

    def writeSphere(self, name, transform, info, mat):
        self.writeCommonPrimitive(name, transform, mat, "sphere")
        # Sphere
        self.ribFile.write("    Sphere %s -%s %s 360\n" % (self.scale_length,
                                                           self.scale_length,
                                                           self.scale_length))
        # AttributeEnd
        self.ribFile.write("  AttributeEnd\n")

    def writeSpotLight(self, name, transform, info):
        # light_counter
        self.light_counter += 1
        # info
        color = info[0]
        energy = info[1]
        falloff = info[2]
        use_diffuse = info[3][0]
        use_specular = info[3][1]
        spot_size = info[4][0]
        spot_blend = info[4][1]
        # falloff
        falloff_type = falloff[0]
        # name
        self.ribFile.write('  # %s\n' % name)
        self.ribFile.write('  AttributeBegin\n')
        self.ribFile.write('    Attribute ' +
                           '"%s" "%s" ["%s"]\n' %
                           ("identifier", "string name", name))
        # Transform
        m = transform
        self.ribFile.write('    Transform [\n')
        self.ribFile.write('      %s %s %s %s\n' %
                           (m[0][0],
                            m[1][0],
                            m[2][0],
                            m[3][0]))
        self.ribFile.write('      %s %s %s %s\n' %
                           (m[0][1],
                            m[1][1],
                            m[2][1],
                            m[3][1]))
        self.ribFile.write('      %s %s %s %s\n' %
                           (m[0][2],
                            m[1][2],
                            m[2][2],
                            m[3][2]))
        self.ribFile.write('      %s %s %s %s\n' %
                           (self.scale_length * m[0][3],
                            self.scale_length * m[1][3],
                            self.scale_length * m[2][3],
                            m[3][3]))
        self.ribFile.write('    ]\n')
        # LightSource
        self.ribFile.write('    AreaLightSource "PxrStdAreaLight" ' +
                           '"%s"\n' % name +
                           '      "string rman__Shape" ["%s"]\n' % "spot" +
                           # TODO: exposure
                           '      "float exposure" [%s]\n' % 10.0 +
                           '      "color lightColor" [%s %s %s]\n' %
                           (color[0], color[1], color[2]) +
                           '      "float coneAngle" [%s]\n' %
                           (math.degrees(spot_size) / 2.0) +
                           # TODO: penumbraAngle
                           '      "float penumbraAngle" [%s]\n' % 0.0)
        self.ribFile.write('    Bxdf "PxrLightEmission" "%s_emit"\n' % name)
        self.ribFile.write('    Disk 0 0.5 360\n') # TODO
        self.ribFile.write('  AttributeEnd\n')

    def writeSunLight(self, name, transform, info):
        # light_counter
        self.light_counter += 1
        # info
        color = info[0]
        energy = info[1]
        falloff = info[2]
        use_diffuse = info[3][0]
        use_specular = info[3][1]
        # lamp data
        lamp = bpy.data.lamps[name]
        if name == 'sun' or name == 'Sun':
            # name
            self.ribFile.write("  # %s\n" % name)
            self.ribFile.write("  Attribute " +
                               "\"%s\" \"%s\" [\"%s\"]\n" %
                               ("identifier", "string name", name))
            # AreaLightSource
            haziness = lamp.sky.atmosphere_turbidity
            # see export_arnold_ass.py for sundir calculation
            m = transform
            sunPos, rot, scale = m.decompose()
            obj = bpy.data.objects[name]
            constraint = obj.constraints[0]
            if constraint.type == 'TRACK_TO':
                compass = constraint.target
                m = compass.matrix_world.copy()
                compassPos, rot, scale = m.decompose()
                sundir = [sunPos[0] - compassPos[0],
                          sunPos[1] - compassPos[1],
                          sunPos[2] - compassPos[2]]
                sundirLen = math.sqrt((sundir[0] * sundir[0] +
                                       sundir[1] * sundir[1] +
                                       sundir[2] * sundir[2]))
                sundir[0] = sundir[0] / sundirLen
                sundir[1] = sundir[1] / sundirLen
                sundir[2] = sundir[2] / sundirLen
            self.ribFile.write('  AttributeBegin\n')
            self.ribFile.write('    Attribute "visibility" "int transmission" [0] "int camera" [1] "int indirect" [0]\n')
            self.ribFile.write('    Light "PxrEnvDayLight" ' +
                               '"%s"\n' % name)
            self.ribFile.write('      "float intensity" [%s]\n' % energy)
            self.ribFile.write('      "vector sunDirection" ' +
                               '[%s %s %s]\n' %
                               (sundir[0], sundir[1], sundir[2]))
            self.ribFile.write('      "float haziness" [%s]\n' % haziness)
            self.ribFile.write('    Rotate 180 0 1 0 # right handed\n')
            self.ribFile.write('    Scale -1 1 1 # right handed\n')
            self.ribFile.write('    Geometry "envsphere"\n')
            self.ribFile.write('  AttributeEnd\n')
            # diffuse?
            if not use_diffuse:
                self.ribFile.write("    \"float __nondiffuse\" 1\n")
            # specular?
            if not use_specular:
                self.ribFile.write("    \"float __nonspecular\" 1\n")

    def writeTexture(self, name, param, tex_type, tex_path):
        basename = os.path.basename(tex_path)
        root, ext = os.path.splitext(basename)
        # assume the texture was copied to the export directory
        prman_texturename = root + "_txmake" + ext
        # define texture
        if param == "Kd":
            self.ribFile.write('    Pattern "PxrTexture" ' +
                               '"%s_diff_tex"\n' %
                               name)
        elif param == "Kr":
            self.ribFile.write('    Pattern "PxrTexture" ' +
                               '"%s_spec_tex"\n' %
                               name)
        elif param == "fac":
            self.ribFile.write('    Pattern "PxrTexture" ' +
                               '"%s_fac_tex"\n' %
                               name)
        else:
            print("TODO: rib.writeTexture(%s, %s, ...)" %
                  (name, param))
        self.ribFile.write('      "string filename" ["%s"]\n' %
                           prman_texturename)
        self.ribFile.write('      "int invertT" [0] "int linearize" [1]\n')

    def writeVolume(self):
        if self.scene.world:
            world = self.scene.world
            if world.node_tree == None:
                return
            try:
                inSlots = world.node_tree.nodes['Volume Scatter'].inputs
            except KeyError:
                return
            else:
                color = inSlots['Color']
                density = inSlots['Density']
                ##anisotropy = inSlots['Anisotropy']
                self.ribFile.write('  AttributeBegin\n')
                self.ribFile.write('    Identity\n')
                self.ribFile.write('    Bxdf "PxrVolume" "volume"\n')
                self.ribFile.write('      "color diffuseColor" [%s %s %s]\n' %
                                   (color.default_value[0],
                                    color.default_value[1],
                                    color.default_value[2]))
                self.ribFile.write('      "float densityFloat" [%s]\n' %
                                   density.default_value)
                self.ribFile.write('      "int multiScatter" [1]\n')
                self.ribFile.write('    Volume "box"\n')
                self.ribFile.write('      [%s %s %s %s %s %s]\n' %
                                   (self.bbox[0],  # minX
                                    self.bbox[1],  # maxX
                                    self.bbox[2],  # minY
                                    self.bbox[3],  # maxY
                                    self.bbox[4],  # minZ
                                    self.bbox[5])) # maxZ
                self.ribFile.write('      [0 0 0]\n')
                self.ribFile.write('  AttributeEnd\n')

    def finishExport(self, sun = None):
        # WorldEnd
        self.ribFile.write("WorldEnd\n")
        # close
        self.ribFile.close()
