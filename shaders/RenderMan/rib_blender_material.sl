surface  rib_blender_material(float Ka = 1;
                              float Kd = .5;
                              float Ks = .5;
                              color Ks_color = 1;
                              float specular_hardness = 50.;
                              string texturename = "";
                              output varying normal aov_N = 0;
                              output varying point aov_P = 0;
                              output varying color aov_direct_diffuse = 0;
                              output varying color aov_direct_specular = 0;)
{
	normal Nf;
	vector V;
	color Ct;

  normal Nn = normalize(N);
	aov_N = transform("current", "world", Nn);
	aov_P = transform("current", "world", P);
	Nf = faceforward(Nn, I);
	V = normalize(-I);

	Ct = ( texturename != "" ) ?
		texture( texturename, mod(s, 1.0), mod(t, 1.0), "filter", "gaussian" ) : 1.0;

	Oi = Os;
  color diff = diffuse(Nf);
  aov_direct_diffuse = Cs * Ct * Kd * diff;
  color spec = 0;
  vector H;
  illuminance(P, Nf, PI/2) 
    {
      H = normalize(L) + V;
      H = normalize(H);
      spec += Cl * pow(max(0.0, Nf.H), specular_hardness);
    }
  aov_direct_specular = Ks_color * Ks * spec;
	Ci = (Cs * Ct * (Ka * ambient() + Kd * diff) +
        aov_direct_specular);

	Ci *= Oi;
}
