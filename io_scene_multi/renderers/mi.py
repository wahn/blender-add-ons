class MiExporter(CommonExporterInterface):
    def __init__(self, options):
        CommonExporterInterface.__init__(self, "MiExporter", options)
        self.miFile = None
        self.mdlFile = None
        self.cameraName = None
        self.cameraNames = []
        self.instances = []
        self.usedMaterials = {}
        self.usedMeshes = {}
        self.usedNurbsSurfaces = {}
        self.usedPrimitives = {}

    def prepareExport(self, scene, bbox, directory, name, mblur, light_counter,
                      sun = None):
        # scene related
        self.scene = scene
        self.bbox = bbox
        # light related
        self.light_counter = light_counter
        # motion blur related
        self.use_motion_blur = mblur[0]
        self.shutter_start = mblur[1]
        self.shutter_end = mblur[2]
        self.motion_blur_samples = mblur[3]
        self.animatedObjects = mblur[4]
        self.motionMatrices = mblur[5]
        # default values (should be overwritten by environment variables)
        if platform.system() == "Linux":
            MI_ROOT = "/mill3d/server/apps/MENTAL/MI2/mr_3.13.0.12"
            OSSWITCH = "linux-x86-64"
        else:
            MI_ROOT = "/usr/local/mental_ray"
            OSSWITCH = "macosx-x86-64-clang"
        WMRS_ROOT = "/usr/people/jan/git/mental_ray_shaders"
        MULTI_ROOT = "/usr/people/jan/git/blender-add-ons/shaders/mental_ray"
        # create an .rayrc file
        filename = os.path.join(directory, ".rayrc")
        print('open("%s", "w")' % filename)
        rayrc = open(filename, "w")
        rayrc.write("registry \"{%s}\"\n" % "_MI_REG_INCLUDE")
        rayrc.write("  value \".;%s/%s;%s/%s;%s/%s\"\n" %
                         (MI_ROOT, "common/include",
                          WMRS_ROOT, "include",
                          MULTI_ROOT, "include"))
        rayrc.write("end registry\n")
        rayrc.write("\n")
        rayrc.write("registry \"{%s}\"\n" % "_MI_REG_LIBRARY")
        rayrc.write("  value \".;%s/%s/%s;%s/%s;%s/%s;%s/%s/%s\"\n" %
                         (MI_ROOT, OSSWITCH, "shaders",
                          WMRS_ROOT, "library",
                          MULTI_ROOT, "library",
                          MI_ROOT, OSSWITCH, "bin",)) # libmdl
        rayrc.write("end registry\n")
        rayrc.write("\n")
        rayrc.write("# convenience include/link lines\n")
        rayrc.write("\n")
        rayrc.write("# include mental images' shaders " +
                         "(coming with mental ray)\n")
        rayrc.write('link \"base.so\"\n')
        rayrc.write('$include \"base.mi\"\n')
        rayrc.write('link \"contour.so\"\n')
        rayrc.write('$include \"contour.mi\"\n')
        rayrc.write('link \"physics.so\"\n')
        rayrc.write('$include \"physics.mi\"\n')
        rayrc.write('link \"subsurface.so\"\n')
        rayrc.write('$include \"subsurface.mi\"\n')
        rayrc.write("# include shaders from " +
                         "Andy Kopra's book\n")
        rayrc.write('link \"wmrs.so\"\n')
        rayrc.write('$include \"wmrs.mi\"\n')
        rayrc.write('link \"newblocks.so\"\n')
        rayrc.write('$include \"newblocks.mi\"\n')
        rayrc.write("# include shaders for Massive\n")
        rayrc.write('link \"mill_massive.so\"\n')
        rayrc.write('$include \"mill_massive.mi\"\n')
        rayrc.write("# include shaders for io_scene_multi\n")
        rayrc.write('link \"mi_blender_material.so\"\n')
        rayrc.write('$include \"mi_blender_material.mi\"\n')
        rayrc.write('link \"mi_defaultsurface.so\"\n')
        rayrc.write('$include \"mi_defaultsurface.mi\"\n')
        rayrc.close()
        # open
        filename = os.path.join(directory, name + ".mi")
        print('open("%s", "w")' % filename)
        self.miFile = open(filename, "w")
        filename = os.path.join(directory, "textured.mdl")
        print('open("%s", "w")' % filename)
        self.mdlFile = open(filename, "w")
        # import MDL stuff
        self.mdlFile.write('\nmdl 1.0;\n')
        self.mdlFile.write('import df::*;\n')
        self.mdlFile.write('import base::*;\n')
        self.mdlFile.write('import math::*;\n\n')
        # include Brenton's MDL stuff
        self.miFile.write('$include "export_multi.mdl"\n')
        self.miFile.write('$include "textured.mdl"\n')
        self.miFile.write('$include "base.mdl"\n')
        self.miFile.write('\n')
        # sun
        if sun and self.options.lighting == 'global_illumination':
            m = sun.matrix_world.copy()
            sunPos, rot, scale = m.decompose()
            if len(sun.constraints) > 0:
                constraint = sun.constraints[0]
                if constraint.type == 'TRACK_TO':
                    compass = constraint.target
                    m = compass.matrix_world.copy()
                    compassPos, rot, scale = m.decompose()
                    sundir = [sunPos[0] - compassPos[0],
                              sunPos[1] - compassPos[1],
                              sunPos[2] - compassPos[2]]
                    sundirLen = math.sqrt((sundir[0] * sundir[0] +
                                           sundir[1] * sundir[1] +
                                           sundir[2] * sundir[2]))
                    sundir[0] = sundir[0] / sundirLen
                    sundir[1] = sundir[1] / sundirLen
                    sundir[2] = sundir[2] / sundirLen
                    self.miFile.write('# %s\n' % sun.name)
                    self.miFile.write('shader "%s" "%s" (\n' %
                                      ("sun_sky", "mdl::base::sun_and_sky"))
                    self.miFile.write('  "%s" %s,\n' % ("on", "on"))
                    self.miFile.write('  "%s" %s,\n' % ("multiplier", "0.025"))
                    self.miFile.write('  "%s" %s,\n' % ("rgb_unit_conversion",
                                                        "%s %s %s" %
                                                        (0.000666667,
                                                         0.000666667,
                                                         0.000666667)))
                    self.miFile.write('  "%s" %s, # 0.5\n' % ("haze", "1.0"))
                    self.miFile.write('  "%s" %s,\n' % ("redblueshift", "0.0"))
                    self.miFile.write('  "%s" %s,\n' % ("saturation", "0.5"))
                    self.miFile.write('  "%s" %s,\n' %
                                      ("horizon_height", "0.001"))
                    self.miFile.write('  "%s" %s,\n' % ("horizon_blur", "0.1"))
                    self.miFile.write('  "%s" %s,\n' % ("ground_color",
                                                        "%s %s %s" %
                                                        (0.4,
                                                         0.4,
                                                         0.4)))
                    self.miFile.write('  "%s" %s,\n' % ("night_color",
                                                        "%s %s %s" %
                                                        (0.0,
                                                         0.0,
                                                         0.0)))
                    self.miFile.write('  "%s" %s,\n' % ("sun_direction",
                                                        "%s %s %s" %
                                                        (sundir[0],
                                                         sundir[2],
                                                         -sundir[1])))
                    self.miFile.write('  "%s" %s, # 0.01\n' %
                                      ("sun_disk_intensity", "1.0"))
                    self.miFile.write('  "%s" %s, # 0.5\n' %
                                      ("sun_disk_scale", "1.0"))
                    self.miFile.write('  "%s" %s,\n' %
                                      ("sun_glow_intensity", "1.0"))
                    self.miFile.write('  "%s" %s,\n' % ("y_is_up", "on"))
                    self.miFile.write('  "%s" %s,\n' % ("flags", "0"))
                    self.miFile.write('  "%s" %s\n' %
                                      ("physically_scaled_sun", "on"))
                    self.miFile.write(')\n\n')
        # options
        self.miFile.write('options "%s"\n' % "opt")
        if (self.options.lighting == 'no_lights' or
            self.options.lighting == 'direct_lighting'):
            self.miFile.write('  samples 1\n')
            self.miFile.write('  filter gauss 3.0\n')
            self.miFile.write('  trace depth 4 4 8\n')
        self.miFile.write('  "unified sampling" on\n')
        self.miFile.write('  "samples quality" 1.0\n')
        self.miFile.write('  "samples min" 1.0\n')
        self.miFile.write('  "samples max" 100.0\n')
        if self.options.lighting == 'global_illumination':
            self.miFile.write('  "samples error cutoff" 0.005\n')
            self.miFile.write('  "samples improved pattern" on\n')
            self.miFile.write('  "progressive" off\n')
            self.miFile.write('  "progressive max time" 0\n')
            self.miFile.write('  # "progressive subsampling size" 4\n')
            self.miFile.write('\n')
            self.miFile.write('  "gi" on\n')
            self.miFile.write('  "gi rays" 100\n')
            self.miFile.write('  "gi depth" 3\n')
            self.miFile.write('  "gi freeze" off\n')
            self.miFile.write('\n')
            self.miFile.write('  "light importance sampling" on\n')
            self.miFile.write('  "multiple importance sampling" off\n')
            self.miFile.write('  "ray differentials" on\n')
            self.miFile.write('  "environment lighting mode" "automatic"\n')
            self.miFile.write('\n')
            self.miFile.write('  filter gauss 2.0 2.0\n')
            self.miFile.write('  trace depth 2 4 5\n')
            self.miFile.write('\n')
            self.miFile.write('  "light relative scale" 0.31831\n')
            self.miFile.write('  luminance weight 0.212671 0.71516 0.072169\n')
            self.miFile.write('\n')
            self.miFile.write('  "trace camera clip" on\n')
            self.miFile.write('  "trace camera motion vectors" on\n')
            self.miFile.write('\n')
            self.miFile.write('  object space\n')
            self.miFile.write('  desaturate off\n')
            self.miFile.write('  colorclip raw\n')
            self.miFile.write('  dither off\n')
            self.miFile.write('  shadow segments\n')
            self.miFile.write('\n')
            self.miFile.write('  diagnostic samples on\n')
        if self.use_motion_blur:
            inv = 1.0 / float(self.motion_blur_samples)
            self.miFile.write('  # 1.0/%d = %f\n' %
                              (self.motion_blur_samples, inv))
            self.miFile.write('  # time contrast %s %s %s %s\n' %
                              (inv, inv, inv, inv))
            self.miFile.write('  # diagnostic samples on\n')
            self.miFile.write('  shutter %s\n' % 1.0) # self.shutter_end
            self.miFile.write('  # motion steps are not used for ' +
                              'motion transformations\n')
            self.miFile.write('  motion steps %s\n' % self.motion_blur_samples)
        self.miFile.write('end options\n')
        self.miFile.write('\n')

    def writeBlenderMaterial(self, mat, primitive = None):
        # return values
        textures_found = 0
        texture_name = ""
        opaque = True
        mesh_light = False
        # check for proper diffuse and specular model
        if mat.diffuse_shader != 'LAMBERT':
            print("WARNING: switching %s to 'LAMBERT'" % mat.name)
            mat.diffuse_shader = 'LAMBERT'
        if mat.specular_shader != 'PHONG':
            print("WARNING: switching %s to 'PHONG'" % mat.name)
            mat.specular_shader = 'PHONG'
        # search for texture(s)
        textures = {}
        if mat:
            if mat.use_transparency:
                alpha = mat.alpha
                if alpha == 1.0:
                    opaque = True
                else:
                    opaque = False
            # check for textures
            for index in list(range(len(mat.texture_slots))):
                slot = mat.texture_slots[index]
                if slot:
                    texture = slot.texture
                    if slot.texture_coords == 'UV':
                        if texture.type == 'IMAGE':
                            image = texture.image
                            src = image.filepath
                            if slot.use_map_color_diffuse:
                                # diffuse texture
                                textures["diffuse"] = src
                                textures_found += 1
        texturename = ""
        if textures_found:
            diffuse_tex = textures["diffuse"]
            paths = bpy.utils.blend_paths()
            if diffuse_tex in paths:
                index = paths.index(diffuse_tex)
                abs_paths = bpy.utils.blend_paths(absolute = True)
                abs_path = abs_paths[index]
                basename = os.path.basename(abs_path)
                root, ext = os.path.splitext(basename)
                texture_name = root + "_" + ext[1:]
                # assume the texture was copied to the export directory
                texturename = basename
                if 0:
                    # texture
                    self.miFile.write('color texture "%s" "%s"\n\n' %
                                      (texture_name,
                                       basename))
                    # texture_uv
                    self.miFile.write('shader "%s_diff_tex" "%s" (\n' %
                                      (texture_name,
                                       "texture_uv"))
                    self.miFile.write('  "tex" "%s",\n' % (texture_name))
                    self.miFile.write('  "u_scale" 1.0,\n')
                    self.miFile.write('  "v_scale" 1.0,\n')
                    self.miFile.write('  "u_offset" 0.0,\n')
                    self.miFile.write('  "v_offset" 0.0)\n\n')
                else:
                    # start writing texture info into MDL file
                    self.mdlFile.write('export material %s_%s(\n' %
                                       (mat.name, texture_name))
                    self.mdlFile.write('  uniform ' +
                                       'texture_2d diffuse_texture = ')
                    self.mdlFile.write('texture_2d("%s"),\n' % basename)
        if mat.emit != 0.0:
            # light emitter
            # name
            # TODO: Implement and use fix_name(...) wherever it makes sense
            fixed_name = mat.name
            self.miFile.write('# %s\n' % mat.name)
            if self.options.lighting == 'global_illumination':
                # shader
                if 0:
                    # FIXME: MDL light is broken
                    self.miFile.write('shader "%s_mat" "%s" (\n' %
                                      (mat.name,
                                       "mdl::export_multi::diffuse_light"))
                    diffuse_color = mat.diffuse_color
                    self.miFile.write('  tint %s %s %s,\n' %
                                      (diffuse_color[0],
                                       diffuse_color[1],
                                       diffuse_color[2]))
                    self.miFile.write('  intensity %s,\n' % mat.emit)
                    self.miFile.write('  exposure %s\n' % 0.0)
                    self.miFile.write('  )\n')
                    self.miFile.write('\n')
                else:
                    # FIXME: use a physical_light for now
                    self.miFile.write('shader "%s_mat" "%s" (\n' %
                                      (mat.name,
                                       "physical_light"))
                    diffuse_color = mat.diffuse_color
                    self.miFile.write('  "color" %s %s %s\n' %
                                      (diffuse_color[0] * mat.emit,
                                       diffuse_color[1] * mat.emit,
                                       diffuse_color[2] * mat.emit))
                    self.miFile.write(')\n')
                    self.miFile.write('\n')
                    if primitive != "sphere":
                        # material
                        self.miFile.write('material "%s_one_color_mat"\n' %
                                          mat.name)
                        self.miFile.write('  "one_color" (\n')
                        diffuse_color = mat.diffuse_color
                        self.miFile.write('    "color" %s %s %s 1.0,\n' %
                                          (diffuse_color[0],
                                           diffuse_color[1],
                                           diffuse_color[2]))
                        self.miFile.write('  )\n')
                        self.miFile.write('end material\n')
                        self.miFile.write('\n')
                    mesh_light = True
                opaque = True
                return textures_found, texture_name, opaque, mesh_light
            else:
                # material
                self.miFile.write('material "%s_mat"\n' % mat.name)
                self.miFile.write('  "one_color" (\n')
                diffuse_color = mat.diffuse_color
                self.miFile.write('    "color" %s %s %s 1.0,\n' %
                                  (diffuse_color[0],
                                   diffuse_color[1],
                                   diffuse_color[2]))
                self.miFile.write('  )\n')
                self.miFile.write('end material\n')
                self.miFile.write('\n')
                opaque = True
                return textures_found, texture_name, opaque, mesh_light
        # name
        # TODO: Implement and use fix_name(...) wherever it makes sense
        fixed_name = mat.name
        self.miFile.write('# %s\n' % mat.name)
        # material
        if self.options.lighting == 'global_illumination':
            if mat.use_transparency:
                # glass
                self.miFile.write('shader "%s_mat" "%s" (\n' %
                                  (mat.name, "mdl::export_multi::glass"))
                diffuse_color = mat.diffuse_color
                self.miFile.write('  "%s" %s %s %s,\n' %
                                  ("tint",
                                   diffuse_color[0],
                                   diffuse_color[1],
                                   diffuse_color[2]))
                ior = mat.raytrace_transparency.ior
                self.miFile.write('  "%s" %s,\n' % ("ior", ior))
                # TODO: base on something within Blender
                self.miFile.write('  "%s" %s\n' % ("distance_scale", 0.1))
                self.miFile.write(')\n')
            elif mat.raytrace_mirror.use:
                if texture_name == "":
                    # (reflective) metal
                    self.miFile.write('shader "%s_mat" "%s" (\n' %
                                      (mat.name, "mdl::export_multi::metal"))
                    diffuse_color = mat.diffuse_color
                    self.miFile.write('  "%s" %s %s %s,\n' %
                                      ("tint",
                                       diffuse_color[0],
                                       diffuse_color[1],
                                       diffuse_color[2]))
                    self.miFile.write('  "%s" %s,\n' % ("roughness", 0.0))
                    ior = 100
                    self.miFile.write('  "%s" %s\n' % ("ior", ior))
                    self.miFile.write(')\n')
                else:
                    # finish texture info in MDL file
                    self.mdlFile.write('  uniform float roughness = 0.3,\n')
                    self.mdlFile.write('  uniform float ior = 2.4\n')
                    self.mdlFile.write(') = let {\n')
                    self.mdlFile.write('  base::texture_coordinate_info ')
                    self.mdlFile.write('coordinate = ')
                    self.mdlFile.write('base::transform_coordinate(\n')
                    self.mdlFile.write('    transform: base::')
                    self.mdlFile.write('rotation_translation_scale(')
                    self.mdlFile.write('0.0, 0.0, float3(1, 1, 1))\n')
                    self.mdlFile.write('  );\n')
                    self.mdlFile.write('  base::texture_return diffuse_map = ')
                    self.mdlFile.write('base::file_texture(\n')
                    self.mdlFile.write('    texture: diffuse_texture,\n')
                    self.mdlFile.write('    uvw: coordinate\n')
                    self.mdlFile.write('  );\n')
                    self.mdlFile.write('} in  material (\n')
                    self.mdlFile.write('  surface: material_surface(\n')
                    self.mdlFile.write('    scattering: df::fresnel_layer(\n')
                    self.mdlFile.write('      layer: df::simple_glossy_bsdf(\n')
                    self.mdlFile.write('        mode: df::scatter_reflect,\n')
                    self.mdlFile.write('        roughness_u: roughness,\n')
                    self.mdlFile.write('        roughness_v: roughness,\n')
                    self.mdlFile.write('        tint: diffuse_map.tint),\n')
                    self.mdlFile.write('      ior: ior\n')
                    self.mdlFile.write('    )\n')
                    self.mdlFile.write('  )\n')
                    self.mdlFile.write(');\n\n')
                    # (reflective) metal
                    self.miFile.write('shader "%s_mat" "%s" (\n' %
                                      (mat.name, "mdl::textured::%s_%s" %
                                       (mat.name, texture_name)))
                    self.miFile.write('  "%s" %s,\n' % ("roughness", 0.0))
                    ior = 100
                    self.miFile.write('  "%s" %s\n' % ("ior", ior))
                    self.miFile.write(')\n')
            elif mat.specular_intensity > 0.0:
                if texture_name == "":
                    # plastic
                    self.miFile.write('shader "%s_mat" "%s" (\n' %
                                      (mat.name, "mdl::export_multi::plastic"))
                    diffuse_color = mat.diffuse_color
                    self.miFile.write('  "%s" %s %s %s,\n' %
                                      ("tint",
                                       diffuse_color[0],
                                       diffuse_color[1],
                                       diffuse_color[2]))
                    self.miFile.write('  "%s" %s,\n' % ("roughness_glossy",
                                                        mat.roughness))
                    self.miFile.write('  "%s" %s,\n' % ("roughness_diffuse",
                                                        mat.roughness))
                    ior = 1.46
                    self.miFile.write('  "%s" %s\n' % ("ior", ior))
                    self.miFile.write(')\n')
                else:
                    # finish texture info in MDL file
                    self.mdlFile.write('  uniform float ' +
                                       'roughness_glossy = 0.2,\n')
                    self.mdlFile.write('  uniform float ' +
                                       'roughness_diffuse = 0.1,\n')
                    self.mdlFile.write('  uniform float ior = 1.46\n')
                    self.mdlFile.write(') = let {\n')
                    self.mdlFile.write('  base::texture_coordinate_info ')
                    self.mdlFile.write('coordinate = ')
                    self.mdlFile.write('base::transform_coordinate(\n')
                    self.mdlFile.write('    transform: base::')
                    self.mdlFile.write('rotation_translation_scale(')
                    self.mdlFile.write('0.0, 0.0, float3(1, 1, 1))\n')
                    self.mdlFile.write('  );\n')
                    self.mdlFile.write('  base::texture_return diffuse_map = ')
                    self.mdlFile.write('base::file_texture(\n')
                    self.mdlFile.write('    texture: diffuse_texture,\n')
                    self.mdlFile.write('    uvw: coordinate\n')
                    self.mdlFile.write('  );\n')
                    self.mdlFile.write('} in  material (\n')
                    self.mdlFile.write('  surface: material_surface(\n')
                    self.mdlFile.write('    scattering: df::fresnel_layer(\n')
                    self.mdlFile.write('      layer: df::specular_bsdf(\n')
                    self.mdlFile.write('        mode: df::scatter_reflect,\n')
                    self.mdlFile.write('        tint: color(0.9)),\n')
                    self.mdlFile.write('      base: df::fresnel_layer(\n')
                    self.mdlFile.write('        layer: ' +
                                       'df::simple_glossy_bsdf(\n')
                    self.mdlFile.write('          mode: df::scatter_reflect,\n')
                    self.mdlFile.write('          roughness_u: ' +
                                       'roughness_glossy,\n')
                    self.mdlFile.write('          roughness_v: ' +
                                       'roughness_glossy,\n')
                    self.mdlFile.write('          tint: color(0.9)),\n')
                    self.mdlFile.write('        base: ' +
                                       'df::diffuse_reflection_bsdf(\n')
                    self.mdlFile.write('          roughness: ' +
                                       'roughness_diffuse,\n')
                    self.mdlFile.write('          tint: diffuse_map.tint),\n')
                    self.mdlFile.write('        ior: ior\n')
                    self.mdlFile.write('      ),\n')
                    self.mdlFile.write('      ior: ior\n')
                    self.mdlFile.write('    )\n')
                    self.mdlFile.write('  )\n')
                    self.mdlFile.write(');\n')
                    # plastic
                    self.miFile.write('shader "%s_mat" "%s" (\n' %
                                      (mat.name, "mdl::textured::%s_%s" %
                                       (mat.name, texture_name)))
                    self.miFile.write('  "%s" %s,\n' % ("roughness_glossy",
                                                        mat.roughness))
                    self.miFile.write('  "%s" %s,\n' % ("roughness_diffuse",
                                                        mat.roughness))
                    ior = 1.46
                    self.miFile.write('  "%s" %s\n' % ("ior", ior))
                    self.miFile.write(')\n')
            else:
                if texture_name == "":
                    # matte
                    self.miFile.write('shader "%s_mat" "%s" (\n' %
                                      (mat.name, "mdl::export_multi::matte"))
                    diffuse_color = mat.diffuse_color
                    self.miFile.write('  "%s" %s %s %s,\n' %
                                      ("tint",
                                       diffuse_color[0],
                                       diffuse_color[1],
                                       diffuse_color[2]))
                    self.miFile.write('  "%s" %s\n' %
                                      ("roughness", mat.roughness))
                    self.miFile.write(')\n')
                else:
                    # finish texture info in MDL file
                    self.mdlFile.write('  uniform float roughness = 0.1\n')
                    self.mdlFile.write(') = let {\n')
                    self.mdlFile.write('  base::texture_coordinate_info ')
                    self.mdlFile.write('coordinate = ')
                    self.mdlFile.write('base::transform_coordinate(\n')
                    self.mdlFile.write('    transform: base::')
                    self.mdlFile.write('rotation_translation_scale(')
                    self.mdlFile.write('0.0, 0.0, float3(1, 1, 1))\n')
                    self.mdlFile.write('  );\n')
                    self.mdlFile.write('  base::texture_return diffuse_map = ')
                    self.mdlFile.write('base::file_texture(\n')
                    self.mdlFile.write('    texture: diffuse_texture,\n')
                    self.mdlFile.write('    uvw: coordinate\n')
                    self.mdlFile.write('  );\n')
                    self.mdlFile.write('} in  material (\n')
                    self.mdlFile.write('  surface: material_surface(\n')
                    self.mdlFile.write('    scattering: ' +
                                       'df::diffuse_reflection_bsdf(\n')
                    self.mdlFile.write('      roughness: roughness,\n')
                    self.mdlFile.write('      tint: diffuse_map.tint)\n')
                    self.mdlFile.write('  )\n')
                    self.mdlFile.write(');\n')
                    # matte
                    self.miFile.write('shader "%s_mat" "%s" (\n' %
                                      (mat.name, "mdl::textured::%s_%s" %
                                       (mat.name, texture_name)))
                    self.miFile.write('  "%s" %s\n' % ("roughness",
                                                       mat.roughness))
                    self.miFile.write(')\n')
        else:
            self.miFile.write('material "%s_mat"\n' % mat.name)
            self.miFile.write('  "mi_blender_material" (\n')
            diffuse_color = mat.diffuse_color
            self.miFile.write('    "Color" %s %s %s,\n' %
                                   (diffuse_color[0],
                                    diffuse_color[1],
                                    diffuse_color[2]))
            if mat.use_transparency:
                alpha = mat.alpha
            else:
                alpha = 1.0
            self.miFile.write('    "Opacity" %s %s %s,\n' %
                              (alpha, alpha, alpha))
            diffuse_intensity = mat.diffuse_intensity
            Ka = 0.0
            Kd = diffuse_intensity
            specular_intensity = mat.specular_intensity
            Ks = specular_intensity
            Ks_color = mat.specular_color
            specular_hardness = mat.specular_hardness
            self.miFile.write('    "Ka" %s,\n' % Ka)
            self.miFile.write('    "Kd" %s,\n' % Kd)
            self.miFile.write('    "Ks" %s,\n' % Ks)
            self.miFile.write('    "Ks_color" %s %s %s,\n' %
                              (Ks_color[0], Ks_color[1], Ks_color[2]))
            self.miFile.write('    "specular_hardness" %s,\n' %
                              specular_hardness)
            if mat.raytrace_mirror.use:
                Kr = mat.raytrace_mirror.reflect_factor
                self.miFile.write('    "Kr" %s,\n' % Kr)
                Kr_color = mat.mirror_color
                self.miFile.write('    "Kr_color" %s %s %s,\n' %
                                  (Kr_color[0], Kr_color[1], Kr_color[2]))
            if mat.use_transparency:
                Kt = 1.0 - alpha
                self.miFile.write('    "Kt" %s,\n' % Kt)
                IOR = mat.raytrace_transparency.ior
                self.miFile.write('    "IOR" %s,\n' % IOR)
            if textures_found and texture_name != "":
                self.miFile.write('    "texture" = "%s_diff_tex",\n' %
                                  (texture_name))
            self.miFile.write('  )\n')
            if mat.use_transparency:
                self.miFile.write('  shadow "transparent_shadow" (\n')
                self.miFile.write('    "color" 1.0 1.0 1.0,\n')
                self.miFile.write('    "transparency" %s %s %s,\n' %
                                  (Kt, Kt, Kt))
                self.miFile.write('    "breakpoint" 0.5,\n')
                self.miFile.write('    "center" 0.5,\n')
                self.miFile.write('    "extent" 0.5,\n')
                self.miFile.write('  )\n')
            self.miFile.write('end material\n')
        self.miFile.write('\n')
        return textures_found, texture_name, opaque, mesh_light

    def writeCamera(self, name, lens, angle, resolution, border, AA_samples,
                    tile_size, transform, isRenderCamera, sun = None):
        # name
        # TODO: Implement and use fix_name(...) wherever it makes sense
        fixed_name = name
        self.miFile.write('# %s\n' % name)
        # camera
        if isRenderCamera:
            self.cameraName = name
        self.cameraNames.append(name)
        self.miFile.write('camera "%s_cam"\n' % name)
        self.miFile.write('  framebuffer "%s"\n' % "primary")
        self.miFile.write('    datatype "%s"\n' % "rgba_h")
        self.miFile.write('    filtering on\n')
        self.miFile.write('    primary on\n')
        self.miFile.write('    user off\n')
        self.miFile.write('    filename "%s"\n' % "mental_ray.exr")
        if self.options.aov_Z:
            self.miFile.write('  framebuffer "%s"\n' % "Z")
            self.miFile.write('    datatype "%s"\n' % "z")
            self.miFile.write('    filtering off\n')
            self.miFile.write('    primary on\n')
            self.miFile.write('    user off\n')
            self.miFile.write('    filename "%s"\n' % "mental_ray.exr")
        if self.options.aov_N:
            self.miFile.write('  framebuffer "%s"\n' % "aov_N")
            self.miFile.write('    datatype "%s"\n' % "rgb_h")
            self.miFile.write('    filtering off\n')
            self.miFile.write('    primary off\n')
            self.miFile.write('    user on\n')
            self.miFile.write('    filename "%s"\n' % "mental_ray_aov_N.exr")
        if self.options.aov_P:
            self.miFile.write('  framebuffer "%s"\n' % "aov_P")
            self.miFile.write('    datatype "%s"\n' % "rgb_h")
            self.miFile.write('    filtering off\n')
            self.miFile.write('    primary off\n')
            self.miFile.write('    user on\n')
            self.miFile.write('    filename "%s"\n' % "mental_ray_aov_P.exr")
        if self.options.aov_direct_diffuse:
            self.miFile.write('  framebuffer "%s"\n' % "aov_direct_diffuse")
            self.miFile.write('    datatype "%s"\n' % "rgb_h")
            self.miFile.write('    filtering on\n')
            self.miFile.write('    primary off\n')
            self.miFile.write('    user on\n')
            self.miFile.write('    filename "%s"\n' %
                              "mental_ray_aov_direct_diffuse.exr")
        if self.options.aov_direct_specular:
            self.miFile.write('  framebuffer "%s"\n' % "aov_direct_specular")
            self.miFile.write('    datatype "%s"\n' % "rgb_h")
            self.miFile.write('    filtering on\n')
            self.miFile.write('    primary off\n')
            self.miFile.write('    user on\n')
            self.miFile.write('    filename "%s"\n' %
                              "mental_ray_aov_direct_specular.exr")
        if self.options.aov_reflection:
            self.miFile.write('  framebuffer "%s"\n' % "aov_reflection")
            self.miFile.write('    datatype "%s"\n' % "rgb_h")
            self.miFile.write('    filtering on\n')
            self.miFile.write('    primary off\n')
            self.miFile.write('    user on\n')
            self.miFile.write('    filename "%s"\n' %
                              "mental_ray_aov_reflection.exr")
        aspect = resolution[0] / float(resolution[1])
        if aspect >= 1.0:
            self.miFile.write('  focal %s\n' % lens)
            self.miFile.write('  aperture %s\n' % (2 * 16.0))
        else:
            self.miFile.write('  focal %s\n' % (lens / aspect))
            self.miFile.write('  aperture %s\n' % (2 * 16.0))
        self.miFile.write('  aspect %s\n' % aspect)
        self.miFile.write('  resolution %s %s\n' %
                          (resolution[0], resolution[1]))
        # sun
        if sun and self.options.lighting == 'global_illumination':
            self.miFile.write('  environment = "%s"\n' % "sun_sky")
        self.miFile.write('end camera\n')
        self.miFile.write('\n')
        # instance
        self.miFile.write('instance "%s_inst" "%s_cam"\n' %
                          (name, name))
        if self.use_motion_blur and (name in self.animatedObjects):
            # multiple instance motion transformations were introduced
            # in version 3.11.0.4
            matrices = self.motionMatrices[name]
            for midx in range(len(matrices)):
                m = matrices[midx][1] # inverse = [1]
                if midx == 0: # skip (see transform above)
                    self.miFile.write('  transform\n')
                else:
                    self.miFile.write('  motion transform\n')
                self.miFile.write('    %s %s %s %s\n' %
                                  (m[0][0],
                                   m[1][0],
                                   m[2][0],
                                   m[3][0]))
                self.miFile.write('    %s %s %s %s #  z\n' % #  z
                                  (m[0][2],
                                   m[1][2],
                                   m[2][2],
                                   m[3][2]))
                self.miFile.write('    %s %s %s %s # -y\n' % # -y
                                  (-m[0][1],
                                   -m[1][1],
                                   -m[2][1],
                                   -m[3][1]))
                self.miFile.write('    %s %s %s %s\n' %
                                  (m[0][3],
                                   m[1][3],
                                   m[2][3],
                                   m[3][3]))
        else:
            m = transform
            m.invert()
            self.miFile.write('  transform\n')
            self.miFile.write('    %s %s %s %s\n' %
                              (m[0][0],
                               m[1][0],
                               m[2][0],
                               m[3][0]))
            self.miFile.write('    %s %s %s %s #  z\n' % #  z
                              (m[0][2],
                               m[1][2],
                               m[2][2],
                               m[3][2]))
            self.miFile.write('    %s %s %s %s # -y\n' % # -y
                              (-m[0][1],
                                -m[1][1],
                                -m[2][1],
                                -m[3][1]))
            self.miFile.write('    %s %s %s %s\n' %
                              (m[0][3],
                               m[1][3],
                               m[2][3],
                               m[3][3]))
        self.miFile.write('end instance\n')
        self.miFile.write('\n')

    def writeCone(self, name, transform, info, mat):
        ## self.writeCommonPrimitive(name, transform, mat, "bw_hyperboloid")
        # convert to mesh
        obj = bpy.data.objects[name]
        mesh = obj.to_mesh(self.scene, True, 'RENDER')
        info = MultiExporter.getInfoMesh(self, mesh)
        self.writeMesh(name, transform, info)
        # remove temporary mesh
        bpy.data.meshes.remove(mesh)

    def writeCylinder(self, name, transform, info, mat):
        ## self.writeCommonPrimitive(name, transform, mat, "bw_open_cylinder")
        # convert to mesh
        obj = bpy.data.objects[name]
        mesh = obj.to_mesh(self.scene, True, 'RENDER')
        info = MultiExporter.getInfoMesh(self, mesh)
        self.writeMesh(name, transform, info)
        # remove temporary mesh
        bpy.data.meshes.remove(mesh)

    def writeDefaultSurface(self, mat):
        # return values
        textures_found = 0
        texture_name = ""
        opaque = True
        # search for texture(s)
        textures = {}
        if mat:
            if mat.use_transparency:
                alpha = mat.alpha
                if alpha == 1.0:
                    opaque = True
                else:
                    opaque = False
            # check for textures
            for index in list(range(len(mat.texture_slots))):
                slot = mat.texture_slots[index]
                if slot:
                    texture = slot.texture
                    if slot.texture_coords == 'UV':
                        if texture.type == 'IMAGE':
                            image = texture.image
                            src = image.filepath
                            if slot.use_map_color_diffuse:
                                # diffuse texture
                                textures["diffuse"] = src
                                textures_found += 1
        texturename = ""
        if textures_found:
            diffuse_tex = textures["diffuse"]
            paths = bpy.utils.blend_paths()
            if diffuse_tex in paths:
                index = paths.index(diffuse_tex)
                abs_paths = bpy.utils.blend_paths(absolute = True)
                abs_path = abs_paths[index]
                basename = os.path.basename(abs_path)
                root, ext = os.path.splitext(basename)
                texture_name = root + "_" + ext[1:]
                # assume the texture was copied to the export directory
                texturename = basename
                # texture
                self.miFile.write('color texture "%s" "%s"\n\n' %
                                  (texture_name,
                                   basename))
                # texture_uv
                self.miFile.write('shader "%s_diff_tex" "%s" (\n' %
                                  (texture_name,
                                   "texture_uv"))
                self.miFile.write('  "tex" "%s",\n' % (texture_name))
                self.miFile.write('  "u_scale" 1.0,\n')
                self.miFile.write('  "v_scale" 1.0,\n')
                self.miFile.write('  "u_offset" 0.0,\n')
                self.miFile.write('  "v_offset" 0.0)\n\n')
        # name
        # TODO: Implement and use fix_name(...) wherever it makes sense
        fixed_name = mat.name
        self.miFile.write('# %s\n' % mat.name)
        # material
        self.miFile.write('material "%s_mat"\n' % mat.name)
        self.miFile.write('  "mi_defaultsurface" (\n')
        diffuse_color = mat.diffuse_color
        self.miFile.write('    "Color" %s %s %s,\n' %
                               (diffuse_color[0],
                                diffuse_color[1],
                                diffuse_color[2]))
        if mat.use_transparency:
            alpha = mat.alpha
        else:
            alpha = 1.0
        self.miFile.write('    "Opacity" %s %s %s,\n' %
                          (alpha, alpha, alpha))
        diffuse_intensity = mat.diffuse_intensity
        Kd = diffuse_intensity
        Ka = 1.0 - Kd
        self.miFile.write('    "Kd" %s,\n' % Kd)
        self.miFile.write('    "Ka" %s,\n' % Ka)
        if textures_found and texture_name != "":
            self.miFile.write('    "texture" = "%s_diff_tex",\n' %
                              (texture_name))
        self.miFile.write('  )\n')
        self.miFile.write('end material\n')
        self.miFile.write('\n')
        return textures_found, texture_name, opaque

    def writeMesh(self, name, transform, info, user_matrix = None,
                  primitive = None):
        mesh_light = False
        # info
        materials = info[0]
        vertices = info[1]
        vertexNormals = info[2]
        polygons = info[3]
        polygonNormals = info[4]
        smoothPolygons = info[5]
        polygonMaterialIndices = info[6]
        hasUVs, uvs = info[7]
        # if there's a single smooth polygon, we export normals
        useVertexNormals = False
        for smoothPolygon in smoothPolygons:
            if smoothPolygon:
                useVertexNormals = True
                break
        # material(s)
        isOpaque = True
        if len(materials) > 1:
            for mi in range(len(materials)):
                matName = materials[mi]
                mat = bpy.data.materials[matName]
                try:
                    [textures_found,
                     texture_name,
                     opaque,
                     mesh_light] = self.usedMaterials[mat.name]
                except KeyError:
                    if (self.options.lighting == 'no_lights' or
                        self.light_counter == 0):
                        [textures_found,
                         texture_name,
                         opaque] = self.writeDefaultSurface(mat)
                    else:
                        [textures_found,
                         texture_name,
                         opaque,
                         mesh_light] = self.writeBlenderMaterial(mat, primitive)
                    self.usedMaterials[mat.name] = [textures_found,
                                                    texture_name,
                                                    opaque,
                                                    mesh_light]
                isOpaque = isOpaque and opaque
        else:
            if len(materials):
                mat = bpy.data.materials[materials[0]]
                try:
                    [textures_found,
                     texture_name,
                     opaque,
                     mesh_light] = self.usedMaterials[mat.name]
                except KeyError:
                    if (self.options.lighting == 'no_lights' or
                        self.light_counter == 0):
                        [textures_found,
                         texture_name,
                         opaque] = self.writeDefaultSurface(mat)
                        self.usedMaterials[mat.name] = [textures_found,
                                                        texture_name,
                                                        opaque,
                                                        mesh_light]
                    else:
                        [textures_found,
                         texture_name,
                         opaque,
                         mesh_light] = self.writeBlenderMaterial(mat, primitive)
                        if primitive != "sphere":
                            self.usedMaterials[mat.name] = [textures_found,
                                                            texture_name,
                                                            opaque,
                                                            mesh_light]
            else:
                mat = None
                if (self.options.lighting == 'no_lights' or
                    self.light_counter == 0):
                    [textures_found,
                     texture_name,
                     opaque] = self.writeDefaultSurface(mat)
                else:
                    [textures_found,
                     texture_name,
                     opaque,
                     mesh_light] = self.writeBlenderMaterial(mat, primitive)
        # check if the mesh was exported already
        try:
            instances = self.usedMeshes[name]
        except KeyError:
            instances = 0
            self.usedMeshes[name] = 1
        else:
            self.usedMeshes[name] = instances + 1
            name = name + "%s" % instances
        instances = 0 # TMP: Turn off instancing
        if instances:
            if primitive != "sphere" or (not mesh_light):
                self.instances.append(name + "un%s" % (instances))
            # instance
            self.miFile.write('instance "%sun%s_inst" "%s_obj"\n' %
                              (name, instances, name))
        else:
            if primitive != "sphere" or (not mesh_light):
                self.instances.append(name)
            # name
            # TODO: Implement and use fix_name(...) wherever it makes sense
            fixed_name = name
            self.miFile.write('# %s\n' % name)
            self.miFile.write('object "%s_obj"\n' % fixed_name)
            if primitive != "sphere" and mesh_light:
                self.miFile.write('  finalgather %s\n' % "off") # TMP
            self.miFile.write('  visible %s\n' % "on")
            self.miFile.write('  shadow %s\n' % "on")
            self.miFile.write('  trace %s\n' % "on")
            self.miFile.write('  tagged %s\n' % "on")
            # TODO: use opacity for transparency mode
            # TODO: bounding box
            self.miFile.write('  group\n')
            # vertices (coordinates)
            self.miFile.write('    # vertices [%s]\n' % len(vertices))
            for i in list(range(len(vertices))):
                vertex = vertices[i]
                x = vertex[0]
                y = vertex[1]
                z = vertex[2]
                self.miFile.write('    %s %s %s\n' % (x, y, z))
            # normals
            if useVertexNormals or mesh_light:
                if len(vertices) != len(vertexNormals):
                    print("WARNING: len(vertices)" +
                          "[%s] != len(vertexNormals)[%s]" %
                          (len(vertices), len(vertexNormals)))
                self.miFile.write('    # vertex normals [%s]\n' %
                                  len(vertexNormals))
                for i in list(range(len(vertexNormals))):
                    vertexNormal = vertexNormals[i]
                    x = vertexNormal[0]
                    y = vertexNormal[1]
                    z = vertexNormal[2]
                    self.miFile.write('    %s %s %s\n' % (x, y, z))
            # uv-coordinates
            numVertexIndices = 0
            for i in range(len(polygons)):
                polygon = polygons[i]
                numVertexIndices += len(polygon)
            numUVs = numVertexIndices # keep for later
            if len(uvs) == len(polygons):
                self.miFile.write('    # uv coordinates [%s]\n' % numUVs)
                for pi in list(range(len(polygons))): # polygon index
                    polygon = polygons[pi]
                    for vi in list(range(len(polygon))):
                        uv = uvs[pi][vi]
                        self.miFile.write('    %s %s 0.0\n' %
                                          (uv[0], uv[1]))
            # vertex information (relation between vertices, normals,
            # and uv-coords)
            if len(uvs) == len(polygons):
                self.miFile.write('    # vertex information [%s]\n' % numUVs)
                counter = 0
                for polygon in polygons:
                    for vi in polygon:
                        if useVertexNormals or mesh_light:
                            self.miFile.write('    v %s n %s t %s\n' %
                                              (vi, vi + len(vertices),
                                               counter + 2 * len(vertices)))
                        else:
                            self.miFile.write('    v %s t %s\n' %
                                              (vi, counter + len(vertices)))
                        counter += 1
            else:
                self.miFile.write('    # vertex information [%s]\n' % numUVs)
                counter = 0
                for polygon in polygons:
                    for vi in polygon:
                        if useVertexNormals or mesh_light:
                            self.miFile.write('    v %s n %s # vn\n' %
                                              (vi, vi + len(vertices)))
                        else:
                            self.miFile.write('    v %s\n' % vi)
                        counter += 1
            # triangles/quads
            self.miFile.write('    # triangles/quads [%s]\n' % len(polygons))
            counter = 0
            for pi in list(range(len(polygons))): # polygon index
                polygon = polygons[pi]
                mat_idx = polygonMaterialIndices[pi]
                if len(polygon) == 3:
                    self.miFile.write('    c %s  %s %s %s\n' % # c
                                      (mat_idx, counter, counter+1, counter+2))
                    counter += 3
                elif len(polygon) == 4:
                    self.miFile.write('    p %s  %s %s %s %s\n' % # p
                                      (mat_idx,
                                       counter,
                                       counter+1,
                                       counter+2,
                                       counter+3))
                    counter += 4
                else:
                    self.miFile.write('# no triangle/quad found\n')
            self.miFile.write('  end group\n')
            self.miFile.write('end object\n\n')
            # instance
            self.miFile.write('instance "%s_inst" "%s_obj"\n' % (name, name))
        # material(s)
        isOpaque = True
        if len(materials) > 1:
            self.miFile.write('  material [ ')
            for mi in range(len(materials)):
                matName = materials[mi]
                if mi == 0:
                    self.miFile.write('"%s_mat"' % matName)
                else:
                    self.miFile.write(', "%s_mat"' % matName)
            self.miFile.write(' ]\n')
        else:
            if not mesh_light:
                # material
                self.miFile.write('  material [ "%s_mat" ]\n' % mat.name)
            elif primitive != "sphere":
                # material
                self.miFile.write('  material [ "%s_one_color_mat" ]\n'
                                  % mat.name)
        m = transform
        m.invert()
        self.miFile.write('  transform\n')
        self.miFile.write('    %s %s %s %s\n' %
                          (m[0][0],
                           m[1][0],
                           m[2][0],
                           m[3][0]))
        self.miFile.write('    %s %s %s %s\n' %
                          (m[0][1],
                           m[1][1],
                           m[2][1],
                           m[3][1]))
        self.miFile.write('    %s %s %s %s\n' %
                          (m[0][2],
                           m[1][2],
                           m[2][2],
                           m[3][2]))
        self.miFile.write('    %s %s %s %s\n' %
                          (m[0][3],
                           m[1][3],
                           m[2][3],
                           m[3][3]))
        self.miFile.write('end instance\n')
        self.miFile.write('\n')
        if mesh_light:
            # create a mesh light
            self.miFile.write('light "%s_light"\n' % name)
            self.miFile.write('  = "%s_mat"\n' % mat.name)
            if primitive != "sphere":
                self.miFile.write('  origin 0.0 0.0 0.0\n')
                self.miFile.write('  object "%s_inst"\n' % name)
            else:
                self.miFile.write('  sphere 1.0\n')
                self.miFile.write('  # object "%s_inst"\n' % name)
            self.miFile.write('  16 1 1 1 1 # 16 is the number of samples\n')
            self.miFile.write('  visible on\n')
            self.miFile.write('end light\n')
            self.miFile.write('\n')
            self.miFile.write('instance "%s_light_inst" "%s_light" \n' %
                              (name, name))
            if primitive == "sphere":
                # repeat transform from above
                self.miFile.write('  transform\n')
                self.miFile.write('    %s %s %s %s\n' %
                                  (m[0][0],
                                   m[1][0],
                                   m[2][0],
                                   m[3][0]))
                self.miFile.write('    %s %s %s %s\n' %
                                  (m[0][1],
                                   m[1][1],
                                   m[2][1],
                                   m[3][1]))
                self.miFile.write('    %s %s %s %s\n' %
                                  (m[0][2],
                                   m[1][2],
                                   m[2][2],
                                   m[3][2]))
                self.miFile.write('    %s %s %s %s\n' %
                                  (m[0][3],
                                   m[1][3],
                                   m[2][3],
                                   m[3][3]))
            self.miFile.write('end instance\n')
            self.miFile.write('\n')
            self.instances.append(name + "_light")

    def writeNurbsSurface(self, name, transform, info):
        mesh_light = False
        # info
        materials = info[0]
        splines = info[1]
        # assume ONE material
        isOpaque = True
        matName = materials[0]
        mat = bpy.data.materials[matName]
        try:
            (textures_found,
             texture_name,
             opaque,
             mesh_light) = self.usedMaterials[mat.name]
        except KeyError:
            if self.options.lighting == 'no_lights' or self.light_counter == 0:
                [textures_found,
                 texture_name,
                 opaque] = self.writeDefaultSurface(mat)
            else:
                [textures_found,
                 texture_name,
                 opaque,
                 mesh_light] = self.writeBlenderMaterial(mat)
            self.usedMaterials[mat.name] = [textures_found,
                                            texture_name,
                                            opaque,
                                            mesh_light]
        # check if the NURBS surface was exported already
        try:
            instances = self.usedNurbsSurfaces[name]
        except KeyError:
            instances = 0
        if instances:
            self.instances.append(name + "un%s" % (instances))
            # instance
            self.miFile.write('instance "%sun%s_inst" "%s_obj"\n' %
                              (name, instances, name))
        else:
            self.instances.append(name)
            # name
            # TODO: Implement and use fix_name(...) wherever it makes sense
            fixed_name = name
            self.miFile.write('# %s\n' % name)
            self.miFile.write('object "%s_obj"\n' % fixed_name)
            self.miFile.write('  visible %s\n' % "on")
            self.miFile.write('  shadow %s\n' % "on")
            self.miFile.write('  trace %s\n' % "on")
            self.miFile.write('  tagged %s\n' % "on")
            # TODO: use opacity for transparency mode
            # TODO: bounding box
            # "BSpline1" is need for textures
            self.miFile.write('  basis \"BSpline1\" ' +
                              'rational bspline 1\n') # textures
            # for each spline ...
            for spline in splines:
                order_u = spline[0][0]
                order_v = spline[0][1]
                # ... check if bicubic
                if (order_u == 4 and order_v == 4): # bicubic
                    points = spline[1]
                    self.miFile.write('  basis \"BSpline3\" ' +
                                      'rational bspline 3\n')
                    self.miFile.write('  group\n')
                    self.miFile.write('    # control vertices\n')
                    for v in points:
                        w = v[3] # not used yet
                        self.miFile.write('    %s %s %s\n' % (v[0], v[1], v[2]))
                    self.miFile.write('    # texture vectors\n')
                    self.miFile.write('    %s %s 0.0\n' % (0.0, 0.0))
                    self.miFile.write('    %s %s 0.0\n' % (1.0, 0.0))
                    self.miFile.write('    %s %s 0.0\n' % (0.0, 1.0))
                    self.miFile.write('    %s %s 0.0\n' % (1.0, 1.0))
                    self.miFile.write('    # vertices\n')
                    for i in range(len(points)):
                        self.miFile.write('    v %s\n' % i)
                    self.miFile.write('    # texture vertices\n')
                    for i in range(4):
                       self.miFile.write('    v %s\n' % (len(points) + i))
                    self.miFile.write('    # surface\n')
                    # 0 = mat index
                    self.miFile.write('    surface \"Surface\" 0\n')
                    # TODO: this is an assumption !!!
                    self.miFile.write('      # u-knots\n')
                    self.miFile.write('      \"BSpline%d\" %s %s\n' %
                                      (3, 0.0, 1.0))
                    for i in range(4):
                        self.miFile.write('      %s\n' % 0.0)
                    for i in range(4):
                        self.miFile.write('      %s\n' % 1.0)
                    self.miFile.write('      # v-knots\n')
                    self.miFile.write('      \"BSpline%d\" %s %s\n' %
                                      (3, 0.0, 1.0))
                    for i in range(4):
                        self.miFile.write('      %s\n' % 0.0)
                    for i in range(4):
                        self.miFile.write('      %s\n' % 1.0)
                    self.miFile.write('      # CV indices\n')
                    for i in range(len(points)):
                      self.miFile.write('      %s\n' % i)
                    self.miFile.write('      # texture surface\n')
                    self.miFile.write('      volume vector texture\n')
                    self.miFile.write('        \"BSpline1\"\n')
                    self.miFile.write('        %s\n' % 0.0)
                    self.miFile.write('        %s\n' % 0.0)
                    self.miFile.write('        %s\n' % 1.0)
                    self.miFile.write('        %s\n' % 1.0)
                    self.miFile.write('        \"BSpline1\"\n')
                    self.miFile.write('        %s\n' % 0.0)
                    self.miFile.write('        %s\n' % 0.0)
                    self.miFile.write('        %s\n' % 1.0)
                    self.miFile.write('        %s\n' % 1.0)
                    self.miFile.write('        # texture CV indices\n')
                    for i in range(4):
                      self.miFile.write('        %s\n' % (len(points) + i))
                    self.miFile.write('    approximate surface ' +
                                      'parametric 6.0 6.0 \"Surface\"\n')
                    self.miFile.write('  end group\n')
                else:
                    self.miFile.write("# TODO: NURBS [%s, %s]" %
                                      (order_u, order_v))
            self.miFile.write('end object\n\n')
            # instance
            self.miFile.write('instance "%s_inst" "%s_obj"\n' % (name, name))
        # material(s)
        isOpaque = True
        if not mesh_light:
            # material
            self.miFile.write('  material [ "%s_mat" ]\n' % mat.name)
        if self.use_motion_blur and (name in self.animatedObjects):
            # multiple instance motion transformations were introduced
            # in version 3.11.0.4
            matrices = self.motionMatrices[name]
            for midx in range(len(matrices)):
                m = matrices[midx][1] # inverse = [1]
                if midx == 0: # skip (see transform above)
                    self.miFile.write('  transform\n')
                else:
                    self.miFile.write('  motion transform\n')
                self.miFile.write('    %s %s %s %s\n' %
                                  (m[0][0],
                                   m[1][0],
                                   m[2][0],
                                   m[3][0]))
                self.miFile.write('    %s %s %s %s\n' %
                                  (m[0][1],
                                   m[1][1],
                                   m[2][1],
                                   m[3][1]))
                self.miFile.write('    %s %s %s %s\n' %
                                  (m[0][2],
                                   m[1][2],
                                   m[2][2],
                                   m[3][2]))
                self.miFile.write('    %s %s %s %s\n' %
                                  (m[0][3],
                                   m[1][3],
                                   m[2][3],
                                   m[3][3]))
        else:
            m = transform
            m.invert()
            self.miFile.write('  transform\n')
            self.miFile.write('    %s %s %s %s\n' %
                              (m[0][0],
                               m[1][0],
                               m[2][0],
                               m[3][0]))
            self.miFile.write('    %s %s %s %s\n' %
                              (m[0][1],
                               m[1][1],
                               m[2][1],
                               m[3][1]))
            self.miFile.write('    %s %s %s %s\n' %
                              (m[0][2],
                               m[1][2],
                               m[2][2],
                               m[3][2]))
            self.miFile.write('    %s %s %s %s\n' %
                              (m[0][3],
                               m[1][3],
                               m[2][3],
                               m[3][3]))
        self.miFile.write('end instance\n')
        self.miFile.write('\n')
        if not instances:
            self.usedNurbsSurfaces[name] = 1
        else:
            self.usedNurbsSurfaces[name] = instances + 1

    def writePointLight(self, name, transform, info):
        self.instances.append(name)
        # light_counter
        self.light_counter += 1
        # info
        color = info[0]
        energy = info[1]
        falloff = info[2]
        use_diffuse = info[3][0]
        use_specular = info[3][1]
        # falloff
        falloff_type = falloff[0]
        # name
        # TODO: Implement and use fix_name(...) wherever it makes sense
        fixed_name = name
        self.miFile.write('# %s\n' % name)
        # light
        self.miFile.write('light "%s_lgt"\n' % fixed_name)
        self.miFile.write('  "point_light_falloff" (\n')
        self.miFile.write('    "light_color" %s %s %s)\n' %
                          (color[0] * energy,
                           color[1] * energy,
                           color[2] * energy))
        self.miFile.write('  origin 0.0 0.0 0.0\n')
        if falloff_type == 'INVERSE_SQUARE':
            self.miFile.write('  exponent 2\n')
        else:
            self.miFile.write('  exponent 0\n')
        self.miFile.write('end light\n')
        self.miFile.write('\n')
        # instance
        self.miFile.write('instance "%s_inst" "%s_lgt"\n' % (name, name))
        m = transform
        m.invert()
        self.miFile.write('  transform\n')
        self.miFile.write('    %s %s %s %s\n' %
                          (m[0][0],
                           m[1][0],
                           m[2][0],
                           m[3][0]))
        self.miFile.write('    %s %s %s %s\n' %
                          (m[0][1],
                           m[1][1],
                           m[2][1],
                           m[3][1]))
        self.miFile.write('    %s %s %s %s\n' %
                          (m[0][2],
                           m[1][2],
                           m[2][2],
                           m[3][2]))
        self.miFile.write('    %s %s %s %s\n' %
                          (m[0][3],
                           m[1][3],
                           m[2][3],
                           m[3][3]))
        self.miFile.write('end instance\n')
        self.miFile.write('\n')

    def writeRing(self, name, transform, info, mat):
        ## self.writeCommonPrimitive(name, transform, mat, "bw_hyperboloid")
        # convert to mesh
        obj = bpy.data.objects[name]
        mesh = obj.to_mesh(self.scene, True, 'RENDER')
        info = MultiExporter.getInfoMesh(self, mesh)
        self.writeMesh(name, transform, info)
        # remove temporary mesh
        bpy.data.meshes.remove(mesh)

    def writeSphere(self, name, transform, info, mat):
        ## self.writeCommonPrimitive(name, transform, mat, "bw_ball")
        # convert to mesh
        obj = bpy.data.objects[name]
        mesh = obj.to_mesh(self.scene, True, 'RENDER')
        info = MultiExporter.getInfoMesh(self, mesh)
        self.writeMesh(name, transform, info, None, "sphere")
        # remove temporary mesh
        bpy.data.meshes.remove(mesh)

    def writeSpotLight(self, name, transform, info):
        self.instances.append(name)
        # light_counter
        self.light_counter += 1
        # info
        color = info[0]
        energy = info[1]
        falloff = info[2]
        use_diffuse = info[3][0]
        use_specular = info[3][1]
        spot_size = info[4][0]
        spot_blend = info[4][1]
        # falloff
        falloff_type = falloff[0]
        # spread
        spread = math.cos(spot_size / 2.0)
        if spread < 0.0:
            spread = 0.0
        # name
        # TODO: Implement and use fix_name(...) wherever it makes sense
        fixed_name = name
        self.miFile.write('# %s\n' % name)
        # light
        self.miFile.write('light "%s_lgt"\n' % fixed_name)
        self.miFile.write('  "soft_spotlight_falloff" (\n')
        self.miFile.write('    "light_color" %s %s %s,\n' %
                          (color[0] * energy,
                           color[1] * energy,
                           color[2] * energy))
        self.miFile.write('    "inner_spread" %s)\n' % spread)
        self.miFile.write('  origin 0.0 0.0 0.0\n')
        self.miFile.write('  direction 0.0 0.0 -1.0\n')
        self.miFile.write('  spread %s\n' % spread)
        if falloff_type == 'INVERSE_SQUARE':
            self.miFile.write('  exponent 2\n')
        else:
            self.miFile.write('  exponent 0\n')
        self.miFile.write('end light\n')
        self.miFile.write('\n')
        # instance
        self.miFile.write('instance "%s_inst" "%s_lgt"\n' % (name, name))
        m = transform
        m.invert()
        self.miFile.write('  transform\n')
        self.miFile.write('    %s %s %s %s\n' %
                          (m[0][0],
                           m[1][0],
                           m[2][0],
                           m[3][0]))
        self.miFile.write('    %s %s %s %s\n' %
                          (m[0][1],
                           m[1][1],
                           m[2][1],
                           m[3][1]))
        self.miFile.write('    %s %s %s %s\n' %
                          (m[0][2],
                           m[1][2],
                           m[2][2],
                           m[3][2]))
        self.miFile.write('    %s %s %s %s\n' %
                          (m[0][3],
                           m[1][3],
                           m[2][3],
                           m[3][3]))
        self.miFile.write('end instance\n')
        self.miFile.write('\n')

    def writeSunLight(self, name, transform, info):
        self.instances.append(name)
        # light_counter
        self.light_counter += 1
        # info
        color = info[0]
        energy = info[1]
        falloff = info[2]
        use_diffuse = info[3][0]
        use_specular = info[3][1]
        # name
        # TODO: Implement and use fix_name(...) wherever it makes sense
        fixed_name = name
        self.miFile.write('# %s\n' % name)
        # light
        self.miFile.write('light "%s_lgt"\n' % fixed_name)
        self.miFile.write('  "mib_light_infinite" (\n')
        self.miFile.write('    "color" %s %s %s,\n' %
                          (color[0] * energy,
                           color[1] * energy,
                           color[2] * energy))
        self.miFile.write('    "shadow" on,\n')
        self.miFile.write('    "factor" 0.0)\n')
        self.miFile.write('  direction 0.0 0.0 -1.0\n')
        self.miFile.write('end light\n')
        self.miFile.write('\n')
        # instance
        self.miFile.write('instance "%s_inst" "%s_lgt"\n' % (name, name))
        m = transform
        m.invert()
        self.miFile.write('  transform\n')
        self.miFile.write('    %s %s %s %s\n' %
                          (m[0][0],
                           m[1][0],
                           m[2][0],
                           m[3][0]))
        self.miFile.write('    %s %s %s %s\n' %
                          (m[0][1],
                           m[1][1],
                           m[2][1],
                           m[3][1]))
        self.miFile.write('    %s %s %s %s\n' %
                          (m[0][2],
                           m[1][2],
                           m[2][2],
                           m[3][2]))
        self.miFile.write('    %s %s %s %s\n' %
                          (m[0][3],
                           m[1][3],
                           m[2][3],
                           m[3][3]))
        self.miFile.write('end instance\n')
        self.miFile.write('\n')

    def finishExport(self, sun = None):
        # group collected instances
        self.miFile.write('instgroup "%s"\n' % "rotated_root_grp")
        for instance in self.instances:
            if (sun and sun.name == instance and
                self.options.lighting == 'global_illumination'):
                self.miFile.write('# "%s_inst"\n' % instance)
            else:
                self.miFile.write('  "%s_inst"\n' % instance)
        self.miFile.write('end instgroup\n')
        self.miFile.write('\n')
        # compensate for different coordinate systems
        self.miFile.write('instance "%s_inst" "%s"\n' %
                          ("rotated_root_grp", "rotated_root_grp"))
        if self.use_motion_blur:
            for i in range(self.motion_blur_samples):
                if i == 0:
                    self.miFile.write('  transform\n')
                else:
                    self.miFile.write('  motion transform\n')
                self.miFile.write('    1.0  0.0  0.0  0.0\n')
                self.miFile.write('    0.0  0.0  1.0  0.0 #  z\n') #  z
                self.miFile.write('    0.0 -1.0  0.0  0.0 # -y\n') # -y
                self.miFile.write('    0.0  0.0  0.0  1.0\n')
        else:
            self.miFile.write('  transform\n')
            self.miFile.write('    1.0  0.0  0.0  0.0\n')
            self.miFile.write('    0.0  0.0  1.0  0.0 #  z\n') #  z
            self.miFile.write('    0.0 -1.0  0.0  0.0 # -y\n') # -y
            self.miFile.write('    0.0  0.0  0.0  1.0\n')
        self.miFile.write('end instance\n')
        self.miFile.write('\n')
        # root_grp
        self.miFile.write('instgroup "root_grp"\n')
        for cameraName in self.cameraNames:
            self.miFile.write('  "%s_inst"\n' % cameraName)
        self.miFile.write('  "%s_inst"\n' % "rotated_root_grp")
        self.miFile.write('end instgroup\n')
        self.miFile.write('\n')
        # render statement
        self.miFile.write('render "%s" "%s_inst" "%s"\n' %
                          ("root_grp", self.cameraName, "opt"))
        for cameraName in self.cameraNames:
            self.miFile.write('# render "%s" "%s_inst" "%s"\n' %
                              ("root_grp", cameraName, "opt"))
        # close
        self.miFile.close()
        self.mdlFile.close()
