import os
import math
# common exporter interface
from ..common.interface import CommonExporterInterface
from ..common.unique import UniqueNames
# Blender
import bpy
import mathutils

class RadExporter(CommonExporterInterface):
    def __init__(self, options):
        CommonExporterInterface.__init__(self, "RadExporter", options)
        self.radFile = None
        self.usedMaterials = {}
        # unique names
        self.uniqueNames = UniqueNames()

    def uniqueName(self, proposal):
        return self.uniqueNames.uniqueName(proposal)

    def prepareExport(self, scene, bbox, directory, name, mblur, light_counter,
                      sun = None):
        # scene related
        self.scene = scene
        self.bbox = bbox
        self.scene_name = name
        self.scale_length = scene.unit_settings.scale_length
        # light related
        self.light_counter = light_counter
        # open
        filename = os.path.join(directory, name + ".rad")
        print('INFO: %s' % filename)
        self.radFile = open(filename, "w")
        # .rif file
        filename = os.path.join(directory, name + ".rif")
        print('INFO: %s' % filename)
        self.rifFile = open(filename, "w")

    def writeBlenderMaterial(self, mat):
        matType = None
        # TODO: textures
        if mat.emit != 0.0:
            # light emitter
            matType = "emitter"
            color = mat.diffuse_color
            red   = color[0] * mat.emit
            green = color[1] * mat.emit
            blue  = color[2] * mat.emit
            self.radFile.write("void light %s\n" % mat.name)
            self.radFile.write("0\n")
            self.radFile.write("0\n")
            self.radFile.write("3 %s %s %s\n" % (red, green, blue))
            self.radFile.write("\n")
        else:
            if mat.use_transparency:
                # glass
                matType = "glass"
                color = mat.diffuse_color
                rtn = color[0]
                gtn = color[1]
                btn = color[2]
                self.radFile.write("void glass %s\n" % mat.name)
                self.radFile.write("0\n")
                self.radFile.write("0\n")
                self.radFile.write("3 %s %s %s\n" % (rtn, gtn, btn))
                self.radFile.write("\n")
            elif mat.raytrace_mirror.use:
                # mirror
                matType = "mirror"
                color = mat.diffuse_color
                red   = color[0]
                green = color[1]
                blue  = color[2]
                self.radFile.write("void mirror %s\n" % mat.name)
                self.radFile.write("0\n")
                self.radFile.write("0\n")
                self.radFile.write("3 %s %s %s\n" %
                                   (red, green, blue))
                self.radFile.write("\n")
            elif mat.specular_intensity > 0.0:
                # phong (plastic)
                matType = "phong"
                color = mat.diffuse_color
                red   = color[0]
                green = color[1]
                blue  = color[2]
                rough = mat.roughness
                spec = mat.specular_intensity
                self.radFile.write("void plastic %s\n" % mat.name)
                self.radFile.write("0\n")
                self.radFile.write("0\n")
                self.radFile.write("5 %s %s %s %s %s\n" %
                                   (red, green, blue, spec, rough))
                self.radFile.write("\n")
            else:
                # diffuse (plastic)
                matType = "diffuse"
                color = mat.diffuse_color
                red   = color[0]
                green = color[1]
                blue  = color[2]
                rough = 0.0
                spec = 0.0
                self.radFile.write("void plastic %s\n" % mat.name)
                self.radFile.write("0\n")
                self.radFile.write("0\n")
                self.radFile.write("5 %s %s %s %s %s\n" %
                                   (red, green, blue, spec, rough))
                self.radFile.write("\n")
        return matType

    def writeCamera(self, name, lens, angle, resolution, border, AA_samples,
                    tile_size, transform, isRenderCamera, sun = None):
        # see lxs.py
        m = transform
        matrix = m.transposed()
        pos = matrix[3]
        forwards = -matrix[2]
        target = (pos + forwards)
        up = matrix[1]
        # fov (vh and vv)
        aspect = resolution[0] / float(resolution[1])
        cam = bpy.data.cameras[name]
        if aspect >= 1.0:
            vh = math.degrees(2.0 * math.atan((cam.sensor_width  / 2.0) / lens))
            vv = 2.0 * math.degrees(math.atan(16.0 / (aspect * lens))) # lxs.py
        else:
            vh = 2.0 * math.degrees(math.atan((aspect * 16.0) / lens))
            vv = math.degrees(2.0 * math.atan((cam.sensor_width  / 2.0) / lens))
        if not isRenderCamera:
            self.rifFile.write('# %s\n' % name)
            self.rifFile.write('# view ' +
                               '-vp %s %s %s ' %
                               (self.scale_length * pos[0],
                                self.scale_length * pos[1],
                                self.scale_length * pos[2]) +
                               '-vd %s %s %s ' %
                               (self.scale_length * forwards[0],
                                self.scale_length * forwards[1],
                                self.scale_length * forwards[2]) +
                               '-vu %s %s %s ' %
                               (up[0], up[1], up[2]) +
                               '-vh %s -vv %s\n' %
                               (vh, vv))
        else:
            self.rifFile.write('# %s \n' % self.scene_name)
            self.rifFile.write('ZONE = I %s %s %s %s %s %s\n' %
                               (self.scale_length * self.bbox[0],
                                self.scale_length * self.bbox[1],
                                self.scale_length * self.bbox[2],
                                self.scale_length * self.bbox[3],
                                self.scale_length * self.bbox[4],
                                self.scale_length * self.bbox[5]))
            self.rifFile.write('scene = %s.rad\n' % self.scene_name)
            self.rifFile.write('VAR  = L\n')
            self.rifFile.write('DET  = L\n')
            self.rifFile.write('QUAL = L\n')
            self.rifFile.write('AMB  = %s.amb\n' % self.scene_name)
            self.rifFile.write('INDIRECT = 1\n')
            self.rifFile.write('# %s\n' % name)
            self.rifFile.write('view ' +
                               '-vp %s %s %s ' %
                               (self.scale_length * pos[0],
                                self.scale_length * pos[1],
                                self.scale_length * pos[2]) +
                               '-vd %s %s %s ' %
                               (self.scale_length * forwards[0],
                                self.scale_length * forwards[1],
                                self.scale_length * forwards[2]) +
                               '-vu %s %s %s ' %
                               (up[0], up[1], up[2]) +
                               '-vh %s -vv %s\n' %
                               (vh, vv))
            self.rifFile.write('RESOLUTION = %s %s\n' %
                               (resolution[0], resolution[1]))

    def writeCone(self, name, transform, info, mat):
        if mat != None:
            matName = mat.name
            self.radFile.write("# %s\n" % matName)
            # material
            try:
                matType = self.usedMaterials[matName]
                self.radFile.write("\n")
            except KeyError:
                matType = self.writeBlenderMaterial(mat)
                self.usedMaterials[matName] = matType
            # get center and radius from transform
            loc, rot, scale = transform.decompose()
            obj = bpy.data.objects[name]
            p1 = obj.data.splines[0].points[0].co
            p2 = obj.data.splines[0].points[8].co
            v1 = mathutils.Vector((0.0, 0.0, p1.z))
            tv1 = transform * v1
            v2 = mathutils.Vector((0.0, 0.0, p2.z))
            tv2 = transform * v2
            x0 = tv1.x
            y0 = tv1.y
            z0 = tv1.z
            x1 = tv2.x
            y1 = tv2.y
            z1 = tv2.z
            self.radFile.write('%s cone %s\n' %
                               (matName, name))
            self.radFile.write('0\n')
            self.radFile.write('0\n')
            self.radFile.write('8 %s %s %s\n' %
                               (self.scale_length * x0,
                                self.scale_length * y0,
                                self.scale_length * z0))
            self.radFile.write('  %s %s %s\n' %
                               (self.scale_length * x1,
                                self.scale_length * y1,
                                self.scale_length * z1))
            self.radFile.write('  %s %s\n' %
                               (scale[0] * self.scale_length * math.fabs(p1.y),
                                scale[0] * self.scale_length * math.fabs(p2.y)))
            self.radFile.write('\n')
        else:
            print('TODO: rad.writeCone(%s, %s, %s)' %
                  (name, transform, mat))

    def writeCylinder(self, name, transform, info, mat):
        if mat != None:
            matName = mat.name
            self.radFile.write("# %s\n" % matName)
            # material
            try:
                matType = self.usedMaterials[matName]
                self.radFile.write("\n")
            except KeyError:
                matType = self.writeBlenderMaterial(mat)
                self.usedMaterials[matName] = matType
            # get center and radius from transform
            loc, rot, scale = transform.decompose()
            obj = bpy.data.objects[name]
            p1 = obj.data.splines[0].points[0].co
            p2 = obj.data.splines[0].points[8].co
            # assume p0 is loc
            v1 = mathutils.Vector((0.0, 0.0, p1.z))
            tv1 = transform * v1
            x0 = tv1.x
            y0 = tv1.y
            z0 = tv1.z
            # assume p1 only differs in z-value
            v2 = mathutils.Vector((0.0, 0.0, p2.z))
            tv2 = transform * v2
            x1 = tv2.x
            y1 = tv2.y
            z1 = tv2.z
            # rad TODO: check scale[1]
            rad = scale[0] * self.scale_length
            # center
            (x0, y0, z0) = loc
            self.radFile.write('%s cylinder %s\n' %
                               (matName, name))
            self.radFile.write('0\n')
            self.radFile.write('0\n')
            self.radFile.write('7 %s %s %s\n' %
                               (self.scale_length * x0,
                                self.scale_length * y0,
                                self.scale_length * z0))
            self.radFile.write('  %s %s %s\n' %
                               (self.scale_length * x1,
                                self.scale_length * y1,
                                self.scale_length * z1))
            self.radFile.write('  %s\n' %
                               rad)
            self.radFile.write('\n')
        else:
            print('TODO: rad.writeCylinder(%s, %s, %s)' %
                  (name, transform, mat))

    def writeMesh(self, name, transform, info, user_matrix = None):
        # info
        materials = info[0]
        vertices = info[1]
        vertexNormals = info[2]
        polygons = info[3]
        polygonNormals = info[4]
        smoothPolygons = info[5]
        polygonMaterialIndices = info[6]
        hasUVs, uvs = info[7]
        # sort by materials
        infoMaterial = {}
        for matIndex in range(len(materials)):
            matName = materials[matIndex]
            # store for each material a list with len(vertices) 0 entries
            matIndixes = [0] * len(vertices)
            infoMaterial[matIndex] = matIndixes
        for pi in range(len(polygonMaterialIndices)):
            pmi = polygonMaterialIndices[pi]
            polygon = polygons[pi]
            for vi in polygon:
                # count how often a vertex is used per material
                infoMaterial[pmi][vi] += 1
        for matIndex in range(len(materials)):
            counter = 0
            for index in range(len(infoMaterial[matIndex])):
                vi = infoMaterial[matIndex][index]
                # replace the counter by ...
                if vi == 0:
                    # ... -1 if the material doesn't use this vertex
                    infoMaterial[matIndex][index] = -1
                else:
                    # ... or by an increasing index
                    infoMaterial[matIndex][index] = counter
                    counter += 1
        # for each material ...
        for matIndex in range(len(materials)):
            matName = materials[matIndex]
            mat = bpy.data.materials[matName]
            if len(materials) > 1:
                self.radFile.write("# %s [%s]\n" % (matName, matIndex))
            else:
                self.radFile.write("# %s\n" % matName)
            # material
            try:
                matType = self.usedMaterials[matName]
                self.radFile.write("\n")
            except KeyError:
                matType = self.writeBlenderMaterial(mat)
                self.usedMaterials[matName] = matType
        for pi in range(len(polygons)):
            polygon = polygons[pi]
            self.radFile.write("%s polygon %s\n" %
                               (materials[polygonMaterialIndices[pi]],
                                (self.uniqueName(name) + "_%s" % pi)))
            self.radFile.write("0\n")
            self.radFile.write("0\n")
            self.radFile.write("%s\n" % (3 * len(polygon)))
            for vi in polygon:
                v = vertices[vi]
                vec = mathutils.Vector((v[0], v[1], v[2]))
                tv = transform * vec
                self.radFile.write("  %s %s %s\n" %
                                   (self.scale_length * tv.x,
                                    self.scale_length * tv.y,
                                    self.scale_length * tv.z))
            self.radFile.write("\n")

    def writeRing(self, name, transform, info, mat):
        if mat != None:
            matName = mat.name
            self.radFile.write("# %s\n" % matName)
            # material
            try:
                matType = self.usedMaterials[matName]
                self.radFile.write("\n")
            except KeyError:
                matType = self.writeBlenderMaterial(mat)
                self.usedMaterials[matName] = matType
            # get center and radius from transform
            loc, rot, scale = transform.decompose()
            # calculate both radii from NURBS CVs
            obj = bpy.data.objects[name]
            p1 = obj.data.splines[0].points[0].co
            p2 = obj.data.splines[0].points[8].co
            y1 = math.fabs(p1.y)
            y2 = math.fabs(p2.y)
            if p1.z == p2.z:
                if y1 < y2:
                    r0 = scale[0] * y1
                    r1 = scale[0] * y2
                else:
                    r1 = scale[0] * y1
                    r0 = scale[0] * y2
                # assume z is up
                xdir = 0
                ydir = 0
                zdir = 1
                vec = mathutils.Vector((xdir, ydir, zdir))
                tv = rot * vec
                # center
                (xcent, ycent, zcent) = loc
                self.radFile.write('%s ring %s\n' %
                                   (matName, name))
                self.radFile.write('0\n')
                self.radFile.write('0\n')
                self.radFile.write('8 %s %s %s\n' %
                                   (self.scale_length * xcent,
                                    self.scale_length * ycent,
                                    self.scale_length * zcent))
                self.radFile.write('  %s %s %s\n' %
                                   (tv.x, tv.y, tv.z))
                self.radFile.write('  %s %s\n' %
                                   (self.scale_length * r0, self.scale_length * r1))
                self.radFile.write('\n')
            else:
                print('TODO: rad.writeRing(%s, %s, %s)' %
                      (name, transform, mat))
        else:
            print('TODO: rad.writeRing(%s, %s, %s)' %
                  (name, transform, mat))

    def writeSphere(self, name, transform, info, mat):
        if mat != None:
            matName = mat.name
            self.radFile.write("# %s\n" % matName)
            # material
            try:
                matType = self.usedMaterials[matName]
                self.radFile.write("\n")
            except KeyError:
                matType = self.writeBlenderMaterial(mat)
                self.usedMaterials[matName] = matType
            # get center and radius from transform
            loc, rot, scale = transform.decompose()
            # radius TODO: check scale[1] and scale[2]
            radius = scale[0]
            # center
            (xcent, ycent, zcent) = loc
            self.radFile.write('%s sphere %s\n' %
                               (matName, name))
            self.radFile.write('0\n')
            self.radFile.write('0\n')
            self.radFile.write('4 %s %s %s %s\n' %
                               (self.scale_length * xcent,
                                self.scale_length * ycent,
                                self.scale_length * zcent,
                                self.scale_length * radius))
            self.radFile.write('\n')
        else:
            print('TODO: rad.writeSphere(%s, %s, %s)' %
                  (name, transform, mat))

    def writeSunLight(self, name, transform, info):
        # light_counter
        self.light_counter += 1
        # info
        color = info[0]
        energy = info[1]
        falloff = info[2]
        use_diffuse = info[3][0]
        use_specular = info[3][1]
        # lamp data
        lamp = bpy.data.lamps[name]
        if name == 'sun' or name == 'Sun':
            # calculate altitude and azimuth (see ass.py)
            m = transform
            pos, rot, scale = m.decompose()
            euler = rot.to_euler()
            # altitude
            altitude = 90.0 - math.degrees(euler.x)
            # azimuth (is different to Arnold)
            azimuth = -math.degrees(euler.z)
            # call gensky
            self.radFile.write('!gensky -ang %s %s\n' %
                               (altitude, azimuth))
            self.radFile.write('\n')
            # use example from Scene 0 Tutorial (Radiance book)
            self.radFile.write('skyfunc glow sky_glow\n')
            self.radFile.write('0\n')
            self.radFile.write('0\n')
            self.radFile.write('4 .9 .9 1.15 0\n')
            self.radFile.write('\n')
            self.radFile.write('sky_glow source sky\n')
            self.radFile.write('0\n')
            self.radFile.write('0\n')
            self.radFile.write('4 0 0 1 180\n')
            self.radFile.write('\n')
            self.radFile.write('skyfunc glow ground_glow\n')
            self.radFile.write('0\n')
            self.radFile.write('0\n')
            self.radFile.write('4 1.4 .9 .6 0\n')
            self.radFile.write('\n')
            self.radFile.write('ground_glow source ground\n')
            self.radFile.write('0\n')
            self.radFile.write('0\n')
            self.radFile.write('4 0 0 -1 180\n')
            self.radFile.write('\n')

    def finishExport(self, sun = None):
        # close
        self.radFile.close()
        self.rifFile.close()
