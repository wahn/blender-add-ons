#include <ai.h>
#include <cstring> // strcpy

typedef void* (*InitBrdfFunc)(const AtShaderGlobals* sg,
                              const AtVector* U,
                              const AtVector* V,
                              float rx,
                              float ry);

struct BrdfMethods {
  BrdfMethods(InitBrdfFunc init,
              AtBRDFEvalSampleFunc sample,
              AtBRDFEvalBrdfFunc brdf,
              AtBRDFEvalPdfFunc pdf) :
    init_func(init),
    sample_func(sample),
    brdf_func(brdf),
    pdf_func(pdf) {}

  InitBrdfFunc         init_func;
  AtBRDFEvalSampleFunc sample_func;
  AtBRDFEvalBrdfFunc   brdf_func;
  AtBRDFEvalPdfFunc    pdf_func;
};

static void* CookTorranceBrdfInit(const AtShaderGlobals* sg,
                                  const AtVector* U,
                                  const AtVector* V,
                                  float rx,
                                  float ry) {
  return AiCookTorranceMISCreateData(sg, U, V, rx, ry);
}


static BrdfMethods cook_torrance_methods(CookTorranceBrdfInit,
                                         AiCookTorranceMISSample,
                                         AiCookTorranceMISBRDF,
                                         AiCookTorranceMISPDF);

AI_SHADER_NODE_EXPORT_METHODS(AssBlenderMaterialMethods)

enum AssBlenderMaterialParams
{
  p_Color,
  p_Opacity,
  p_Ka,
  p_Kd,
  p_Ks,
  p_Ks_color,
  p_specular_hardness,
  p_Kr,
  p_Kr_color,
  p_texture,
  p_aov_direct_diffuse,
  p_aov_direct_specular,
  p_aov_indirect_diffuse,
  p_aov_reflection
};

struct ShaderData {
  struct RenderOptions {
    int GI_reflection_depth;
  } options;
};

node_parameters
{
  AiParameterRGB("Color", 1.0f, 1.0f, 1.0f);
  AiParameterRGB("Opacity", 1.0f, 1.0f, 1.0f);
  AiParameterFLT("Ka", 1.0f);
  AiParameterFLT("Kd", 0.5f);
  AiParameterFLT("Ks", 0.5f);
  AiParameterRGB("Ks_color", 1.0f, 1.0f, 1.0f);
  AiParameterFLT("specular_hardness", 50.0f);
  AiParameterFLT("Kr", 0.0f);
  AiParameterRGB("Kr_color", 1.0f, 1.0f, 1.0f);
  AiParameterRGB("texture", 1.0f, 1.0f, 1.0f);
  AiParameterStr("aov_direct_diffuse", "direct_diffuse");
  AiParameterStr("aov_direct_specular", "direct_specular");
  AiParameterStr("aov_reflection", "reflection");
}

node_initialize
{
  ShaderData* data = (ShaderData*) AiMalloc(sizeof(ShaderData));
  AiNodeSetLocalData(node, data);
}

node_update
{
  // global options
  ShaderData* data = (ShaderData*) AiNodeGetLocalData(node);
  AtNode* options = AiUniverseGetOptions();
  data->options.GI_reflection_depth = AiNodeGetInt(options, "GI_reflection_depth");
  // AOVs
  AiAOVRegister(AiNodeGetStr(node, "aov_direct_diffuse"),
                AI_TYPE_RGB, AI_AOV_BLEND_OPACITY);
  AiAOVRegister(AiNodeGetStr(node, "aov_direct_specular"),
                AI_TYPE_RGB, AI_AOV_BLEND_OPACITY);
  AiAOVRegister(AiNodeGetStr(node, "aov_reflection"),
                AI_TYPE_RGB, AI_AOV_BLEND_OPACITY);
}

node_finish
{
}

shader_evaluate
{
  ShaderData* data = (ShaderData*) AiNodeGetLocalData(node);
  sg->out_opacity = AiShaderEvalParamRGB(p_Opacity);

  // early out for shadow rays
  if (sg->Rt & AI_RAY_SHADOW) {
    return;
  }
  // early out for nearly transparent surfaces
  if (AiColorIsZero(sg->out_opacity)) {
    return;
  }

  sg->out.RGB = AI_RGB_BLACK;

  float Ka = AiShaderEvalParamFlt(p_Ka);
  AtColor Kd = AiShaderEvalParamFlt(p_Kd) * AiShaderEvalParamRGB(p_Color);
  AtColor Ks = AiShaderEvalParamFlt(p_Ks) * AiShaderEvalParamRGB(p_Ks_color);
  AtColor Kr = AiShaderEvalParamFlt(p_Kr) * AiShaderEvalParamRGB(p_Kr_color);

  // direct diffuse layer
  if (!AiColorIsZero(Kd)) {
    AtColor direct_diffuse = AI_RGB_BLACK;
    void* oren_nayar_brdf = AiOrenNayarMISCreateData(sg, 0.0f);
    AiLightsPrepare(sg);
    while (AiLightsGetSample(sg)) {
      if (AiLightGetAffectDiffuse(sg->Lp)) {
        float light_diffuse = AiLightGetDiffuse(sg->Lp);
        if (light_diffuse > 0) {
          direct_diffuse += light_diffuse *
            AiEvaluateLightSample(sg, oren_nayar_brdf,
                                  AiOrenNayarMISSample,
                                  AiOrenNayarMISBRDF,
                                  AiOrenNayarMISPDF);
        }
      }
    }
    direct_diffuse *= Kd * AiShaderEvalParamRGB(p_texture);
    sg->out.RGB += direct_diffuse;
    AiAOVSetRGB(sg, AiShaderEvalParamStr(p_aov_direct_diffuse),
                direct_diffuse);
  }

  // direct specular layer
  if (!AiColorIsZero(Ks)) {
    AtColor direct_specular = AI_RGB_BLACK;
    AtVector rot_U, rot_V; // doing nothing (for cook_torrance)
    float specular_rx, specular_ry;
    const float specular_roughness = AiShaderEvalParamFlt(p_specular_hardness) /
      511.0f;
    const float specular_roughness_clamped = CLAMP(specular_roughness, 0.f, 1.f);
    const float specular_roughness_squared = SQR(specular_roughness_clamped);
    specular_rx = specular_ry = specular_roughness_squared;
    void* brdf_data = AiCookTorranceMISCreateData(sg, &rot_U, &rot_V,
                                                  specular_rx, specular_ry);
    AiLightsPrepare(sg);
    while (AiLightsGetSample(sg)) {
      if (AiLightGetAffectSpecular(sg->Lp)) {
        float light_specular = AiLightGetSpecular(sg->Lp);
        if (light_specular > 0) {
          direct_specular += light_specular *
            AiEvaluateLightSample(sg, brdf_data,
                                  AiCookTorranceMISSample,
                                  AiCookTorranceMISBRDF,
                                  AiCookTorranceMISPDF);
        }
      }
    }
    direct_specular *= Ks;
    sg->out.RGB += direct_specular;
    AiAOVSetRGB(sg, AiShaderEvalParamStr(p_aov_direct_specular),
                direct_specular);
  }

  // indirect diffuse layer
  if (!AiColorIsZero(Kd)) {
    AtColor indirect_diffuse = AiIndirectDiffuse(&sg->Nf, sg);
    indirect_diffuse *= Kd;
    sg->out.RGB += indirect_diffuse;
    AiAOVSetRGB(sg, AiShaderEvalParamStr(p_aov_indirect_diffuse),
                indirect_diffuse);
  }

  // mirror reflection layer
  if (!AiColorIsZero(Kr)) {
    AtVector R;
    AiReflectSafe(&sg->Rd, &sg->Nf, &sg->Ngf, &R);
    AtRay ray;
    AiMakeRay(&ray, AI_RAY_REFLECTED, &sg->P, &R, AI_BIG, sg);
    AtColor reflection = AI_RGB_BLACK;
    if (sg->Rr_refl < data->options.GI_reflection_depth) {
      AtScrSample sample;
      AiTrace(&ray, &sample);
      reflection = sample.color;
    }
    reflection *= Kr;
    sg->out.RGB += reflection;
    AiAOVSetRGB(sg, AiShaderEvalParamStr(p_aov_reflection), reflection);
  }
}

node_loader
{
  if (i > 0) return false;

  node->methods = AssBlenderMaterialMethods;
  node->output_type = AI_TYPE_RGB;
  node->name = "ass_blender_material";
  node->node_type = AI_NODE_SHADER;
  strcpy(node->version, AI_VERSION);
  return true;
}
