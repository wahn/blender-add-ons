# (c) Jan Walter 2008
#
# $Author: jan $
# $Date: 2008-10-19 10:27:20 -0700 (Sun, 19 Oct 2008) $
# $Revision: 150 $

import os
import re
import sys
import string
import inspect
# my own modules
import rad

VERBOSE = 0

def usage():
    print("usage: python %s " % inspect.getfile(usage) +
          "filename.rad {test|commands|create} > filename.py")

class State:
    def __init__(self, parent = None):
        self.parent = parent
        self.directory = None
        self.filename = None
        self.ext = None
        self.mode = None
        self.currentSurface = None
        self.currentMaterial = None
        self.surfaces = []
        self.materials = {}
        self.commands = []
        self.includeNames = []
        self.includes = {}
        self.camera = None

def test(iFile, state):
    global VERBOSE
    # regular expressions
    commentRex = re.compile("^\s*#")
    emptyLineRex = re.compile("^\s*$")
    zeroLineRex = re.compile("^\s*0")
    coneRex = re.compile("^\s*[a-zA-Z_0-9\.\-]+\s+cone\s+")
    cupRex = re.compile("^\s*[a-zA-Z_0-9\.\-]+\s+cup\s+")
    cylinderRex = re.compile("^\s*[a-zA-Z_0-9\.\-]+\s+cylinder\s+")
    polygonRex = re.compile("^\s*[a-zA-Z_0-9\.\-]+\s+polygon\s+")
    sphereRex = re.compile("^\s*[a-zA-Z_0-9\.\-]+\s+sphere\s+")
    ringRex = re.compile("^\s*[a-zA-Z_0-9\.\-]+\s+ring\s+")
    tubeRex = re.compile("^\s*[a-zA-Z_0-9\.\-]+\s+tube\s+")
    dielectricRex = re.compile("^\s*[a-zA-Z_0-9\.\-]+\s+dielectric\s+")
    glassRex = re.compile("^\s*[a-zA-Z_0-9\.\-]+\s+glass\s+")
    glowRex = re.compile("^\s*[a-zA-Z_0-9\.\-]+\s+glow\s+")
    illumRex = re.compile("^\s*[a-zA-Z_0-9\.\-]+\s+illum\s+")
    lightRex = re.compile("^\s*[a-zA-Z_0-9\.\-]+\s+light\s+")
    metalRex = re.compile("^\s*[a-zA-Z_0-9\.\-]+\s+metal\s+")
    metal2Rex = re.compile("^\s*[a-zA-Z_0-9\.\-]+\s+metal2\s+")
    mirrorRex = re.compile("^\s*[a-zA-Z_0-9\.\-]+\s+mirror\s+")
    plasticRex = re.compile("^\s*[a-zA-Z_0-9\.\-]+\s+plastic\s+")
    plastic2Rex = re.compile("^\s*[a-zA-Z_0-9\.\-]+\s+plastic2\s+")
    sourceRex = re.compile("^\s*[a-zA-Z_0-9\.\-]+\s+source\s+")
    spotlightRex = re.compile("^\s*[a-zA-Z_0-9\.\-]+\s+spotlight\s+")
    texfuncRex = re.compile("^\s*[a-zA-Z_0-9\.\-]+\s+texfunc\s+")
    colorfuncRex = re.compile("^\s*[a-zA-Z_0-9\.\-]+\s+colorfunc\s+")
    brightfuncRex = re.compile("^\s*[a-zA-Z_0-9\.\-]+\s+brightfunc\s+")
    brightdataRex = re.compile("^\s*[a-zA-Z_0-9\.\-]+\s+brightdata\s+")
    antimatterRex = re.compile("^\s*[a-zA-Z_0-9\.\-]+\s+antimatter\s+")
    colorpictRex = re.compile("^\s*[a-zA-Z_0-9\.\-]+\s+colorpict\s+")
    transRex = re.compile("^\s*[a-zA-Z_0-9\.\-]+\s+trans\s+")
    commandRex = re.compile("^!")
    # read RAD file line by line
    lineNumber = 0
    while 1:
        lineNumber = lineNumber + 1
        line = iFile.readline()
        if line == "":
            # EOF
            break
        else:
            # comment
            if commentRex.search(line):
                ##print("[comment]", line[:-1], "[handled]")
                pass
            # command
            elif commandRex.search(line):
                mod = None
                id = None
                words = line[:-1].split()
                while words[-1] == '\\' or words[-1][-1] == '\\':
                    # read next line
                    lineNumber = lineNumber + 1
                    if words[-1] == '\\':
                        line = " ".join(words[:-1]) + " " + iFile.readline()
                    else:
                        line = " ".join(words)[:-1] + " " + iFile.readline()
                    words = line[:-1].split()
                # check first word
                if words[0] == "!cat":
                    if VERBOSE:
                        print(line[:-1])
                    iFilename2 = os.path.join(state.directory, words[1])
                    if VERBOSE:
                        print("opening %s" % words[1])
                    iFile2 = open(iFilename2, "r")
                    dir, base = os.path.split(iFilename2)
                    test(iFile2, state)
                    iFile2.close()
                    if VERBOSE:
                        print("closing %s" % words[1])
                elif words[0] == "!xform":
                    if VERBOSE:
                        print(line[:-1])
                    # find name
                    for i in range(len(words)):
                        word = words[i]
                        if word == "-n":
                            id = words[i+1]
                            break
                    iFilename2 = os.path.join(state.directory, words[-1])
                    try:
                        includedNames = state.includes[words[-1]]
                    except KeyError:
                        if VERBOSE:
                            print("opening %s" % words[-1])
                        state.includes[words[-1]] = []
                        iFile2 = open(iFilename2, "r")
                        dir, base = os.path.split(iFilename2)
                        state2 = State(parent = state) # new state
                        state2.directory = dir # new directory
                        test(iFile2, state2)
                        # create includeNames from scratch
                        state2.includeNames = []
                        for key in state2.includes.keys():
                            includeNames = state2.includes[key]
                            for includeName in includeNames:
                                state2.includeNames.append(includeName)
                        for surface in state2.surfaces:
                            state.surfaces.append(surface)
                            if not (surface.id in state2.includeNames):
                                state.includes[words[-1]].append(surface.id)
                        for key in state2.materials.keys():
                            state.materials[key] = state2.materials[key]
                        for command in state2.commands:
                            state.commands.append(command)
                            if not (command.id in state2.includeNames):
                                state.includes[words[-1]].append(command.id)
                        for key in state2.includes.keys():
                            state.includes[key] = state2.includes[key]
                        iFile2.close()
                        if VERBOSE:
                            print("closing %s" % words[-1])
                        # create includeNames from scratch
                        state.includeNames = []
                        for key in state.includes.keys():
                            includeNames = state.includes[key]
                            for includeName in includeNames:
                                state.includeNames.append(includeName)
                elif words[0].startswith("!gen"):
                    if VERBOSE:
                        print(line[:-1])
                    ##state.names.append(words[2])
                    mod = words[1]
                    id = words[2]
                else:
                    if VERBOSE:
                        print(line[:-1])
                line = " ".join(words)
                state.commands.append(rad.RadCommand(mod, id, line[1:]))
            # empty line
            elif emptyLineRex.search(line):
                pass
            # sphere
            elif sphereRex.search(line):
                if VERBOSE:
                    print(line[:-1])
                words = line[:-1].split()
                mod = words[0]
                sphere = words[1]
                id = words[2]
                radSphere = rad.RadSphere(mod, id)
                # state
                state.currentSurface = "sphere"
                if len(words) > 5:
                    line = " ".join(words[5:]) + "\n"
                else:
                    # we expect two lines with zeros
                    lineNumber = lineNumber + 1
                    line = iFile.readline()
                    if not zeroLineRex.search(line):
                        print(line[:-1])
                        print('ERROR (line %s): "0\\n" expected' % lineNumber)
                        sys.exit()
                    lineNumber = lineNumber + 1
                    line = iFile.readline()
                    if not zeroLineRex.search(line):
                        print(line[:-1])
                        print('ERROR (line %s): "0\\n" expected' % lineNumber)
                        sys.exit()
                # 4 xcent ycent zcent radius
                lineNumber = lineNumber + 1
                line = iFile.readline()
                words = line[:-1].split()
                if len(words) in [3, 4, 5]:
                    if len(words) == 5:
                        offset = 1 # skip '4'
                    else:
                        offset = 0
                    xcent = float(words[0+offset])
                    ycent = float(words[1+offset])
                    zcent = float(words[2+offset])
                    if len(words) in [4, 5]:
                        radius = float(words[3+offset])
                    else:
                        lineNumber = lineNumber + 1
                        line = iFile.readline()
                        words = line[:-1].split()
                        radius = float(words[0])
                    radSphere.setParams(xcent, ycent, zcent, radius)
                    # store in state
                    state.surfaces.append(radSphere)
                    ##state.names.append(radSphere.id)
                else:
                    print(line[:-1])
                    print('ERROR (line %s): "%s\\n" expected'  %
                          (lineNumber, "4 xcent ycent zcent radius"))
                    sys.exit()
            # cone
            elif coneRex.search(line):
                if VERBOSE:
                    print(line[:-1])
                words = line[:-1].split()
                mod = words[0]
                cone = words[1]
                id = words[2]
                radCone = rad.RadCone(mod, id)
                # state
                state.currentSurface = "cone"
                # we expect two lines with zeros
                lineNumber = lineNumber + 1
                line = iFile.readline()
                if not zeroLineRex.search(line):
                    print(line[:-1])
                    print('ERROR (line %s): "0\\n" expected' % lineNumber)
                    sys.exit()
                lineNumber = lineNumber + 1
                line = iFile.readline()
                if not zeroLineRex.search(line):
                    print(line[:-1])
                    print('ERROR (line %s): "0\\n" expected' % lineNumber)
                    sys.exit()
                # 8 x0 y0 z0 x1 y1 z1 r0 r1
                lineNumber = lineNumber + 1
                line = iFile.readline()
                words = line[:-1].split()
                if words[0] != '8':
                    print(line[:-1])
                    print('ERROR (line %s): "%s\\n" expected'  %
                          (lineNumber, "8 x0 y0 z0 x1 y1 z1 r0 r1"))
                    sys.exit()
                else:
                    # 8
                    if len(words) == 1:
                        lineNumber = lineNumber + 1
                        line = iFile.readline()
                        words = line[:-1].split()
                        # x0 y0 z0
                        if len(words) == 3:
                            x0 = float(words[0])
                            y0 = float(words[1])
                            z0 = float(words[2])
                            lineNumber = lineNumber + 1
                            line = iFile.readline()
                            words = line[:-1].split()
                    else:
                        # 8 x0 y0 z0
                        if len(words) == 4:
                            x0 = float(words[1])
                            y0 = float(words[2])
                            z0 = float(words[3])
                            lineNumber = lineNumber + 1
                            line = iFile.readline()
                            words = line[:-1].split()
                        elif len(words) == 9:
                            # 8 x0 y0 z0 x1 y2 z1 r0 r1
                            x0 = float(words[1])
                            y0 = float(words[2])
                            z0 = float(words[3])
                            x1 = float(words[4])
                            y1 = float(words[5])
                            z1 = float(words[6])
                            r0 = float(words[7])
                            r1 = float(words[8])
                            words = []
                    if len(words) == 0:
                        # 8 x0 y0 z0 x1 y2 z1 r0 r1
                        pass
                    elif len(words) == 3:
                        # x1 y1 z1
                        x1 = float(words[0])
                        y1 = float(words[1])
                        z1 = float(words[2])
                        lineNumber = lineNumber + 1
                        line = iFile.readline()
                        words = line[:-1].split()
                        # r0 r1
                        if len(words) == 2:
                            r0 = float(words[0])
                            r1 = float(words[1])
                    elif len(words) == 5:
                        # x1 y2 z1 r0 r1
                        x1 = float(words[0])
                        y1 = float(words[1])
                        z1 = float(words[2])
                        r0 = float(words[3])
                        r1 = float(words[4])
                    else:
                        print('ERROR (line %s): "%s\\n" expected'  %
                              (lineNumber, "x1 y1 z1 r0 r1"))
                        sys.exit()
                    radCone.setParams(x0, y0, z0, x1, y1, z1, r0, r1)
                    # store in state
                    state.surfaces.append(radCone)
                    ##state.names.append(radCone.id)
            # cup
            elif cupRex.search(line):
                if VERBOSE:
                    print(line[:-1])
                words = line[:-1].split()
                mod = words[0]
                cup = words[1]
                id = words[2]
                radCup = rad.RadCup(mod, id)
                # state
                state.currentSurface = "cup"
                # we expect two lines with zeros
                lineNumber = lineNumber + 1
                line = iFile.readline()
                if not zeroLineRex.search(line):
                    print(line[:-1])
                    print('ERROR (line %s): "0\\n" expected' % lineNumber)
                    sys.exit()
                lineNumber = lineNumber + 1
                line = iFile.readline()
                if not zeroLineRex.search(line):
                    print(line[:-1])
                    print('ERROR (line %s): "0\\n" expected' % lineNumber)
                    sys.exit()
                # 8 x0 y0 z0 x1 y1 z1 r0 r1
                lineNumber = lineNumber + 1
                line = iFile.readline()
                words = line[:-1].split()
                if words[0] != '8':
                    print(line[:-1])
                    print('ERROR (line %s): "%s\\n" expected'  %
                          (lineNumber, "8 x0 y0 z0 x1 y1 z1 r0 r1"))
                    sys.exit()
                else:
                    # 8
                    if len(words) == 1:
                        lineNumber = lineNumber + 1
                        line = iFile.readline()
                        words = line[:-1].split()
                        # x0 y0 z0
                        if len(words) == 3:
                            x0 = float(words[0])
                            y0 = float(words[1])
                            z0 = float(words[2])
                            lineNumber = lineNumber + 1
                            line = iFile.readline()
                            words = line[:-1].split()
                    else:
                        # 8 x0 y0 z0
                        if len(words) == 4:
                            x0 = float(words[1])
                            y0 = float(words[2])
                            z0 = float(words[3])
                            lineNumber = lineNumber + 1
                            line = iFile.readline()
                            words = line[:-1].split()
                    # x1 y1 z1
                    if len(words) == 3:
                        x1 = float(words[0])
                        y1 = float(words[1])
                        z1 = float(words[2])
                        lineNumber = lineNumber + 1
                        line = iFile.readline()
                        words = line[:-1].split()
                    # r0 r1
                    if len(words) == 2:
                        r0 = float(words[0])
                        r1 = float(words[1])
                    radCup.setParams(x0, y0, z0, x1, y1, z1, r0, r1)
                    # store in state
                    state.surfaces.append(radCup)
                    ##state.names.append(radCup.id)
            # ring
            elif ringRex.search(line):
                if VERBOSE:
                    print(line[:-1])
                words = line[:-1].split()
                mod = words[0]
                ring = words[1]
                id = words[2]
                radRing = rad.RadRing(mod, id)
                # state
                state.currentSurface = "ring"
                # we expect two lines with zeros
                lineNumber = lineNumber + 1
                line = iFile.readline()
                if not zeroLineRex.search(line):
                    print(line[:-1])
                    print('ERROR (line %s): "0\\n" expected' % lineNumber)
                    sys.exit()
                lineNumber = lineNumber + 1
                line = iFile.readline()
                if not zeroLineRex.search(line):
                    print(line[:-1])
                    print('ERROR (line %s): "0\\n" expected' % lineNumber)
                    sys.exit()
                # 8 xcent ycent zcent xdir ydir zdir r0 r1
                lineNumber = lineNumber + 1
                line = iFile.readline()
                words = line[:-1].split()
                if words[0] != '8':
                    print(line[:-1])
                    print('ERROR (line %s): "%s\\n" expected'  %
                          (lineNumber,
                           "8 xcent ycent zcent xdir ydir zdir r0 r1"))
                    sys.exit()
                else:
                    # 8
                    if len(words) == 1:
                        lineNumber = lineNumber + 1
                        line = iFile.readline()
                        words = line[:-1].split()
                        # xcent ycent zcent
                        if len(words) == 3:
                            xcent = float(words[0])
                            ycent = float(words[1])
                            zcent = float(words[2])
                            lineNumber = lineNumber + 1
                            line = iFile.readline()
                            words = line[:-1].split()
                    else:
                        # 8 xcent ycent zcent
                        if len(words) == 4:
                            xcent = float(words[1])
                            ycent = float(words[2])
                            zcent = float(words[3])
                            lineNumber = lineNumber + 1
                            line = iFile.readline()
                            words = line[:-1].split()
                    # xdir ydir zdir
                    if len(words) == 3:
                        xdir = float(words[0])
                        ydir = float(words[1])
                        zdir = float(words[2])
                        lineNumber = lineNumber + 1
                        line = iFile.readline()
                        words = line[:-1].split()
                    # r0 r1
                    if len(words) == 2:
                        r0 = float(words[0])
                        r1 = float(words[1])
                    radRing.setParams(xcent, ycent, zcent, xdir, ydir, zdir,
                                      r0, r1)
                    # store in state
                    state.surfaces.append(radRing)
                    ##state.names.append(radRing.id)
            # cylinder
            elif cylinderRex.search(line):
                if VERBOSE:
                    print(line[:-1])
                words = line[:-1].split()
                mod = words[0]
                cylinder = words[1]
                id = words[2]
                radCylinder = rad.RadCylinder(mod, id)
                # state
                state.currentSurface = "cylinder"
                if len(words) > 5:
                    line = " ".join(words[5:]) + "\n"
                else:
                    # we expect two lines with zeros
                    lineNumber = lineNumber + 1
                    line = iFile.readline()
                    if not zeroLineRex.search(line):
                        print(line[:-1])
                        print('ERROR (line %s): "0\\n" expected' % lineNumber)
                        sys.exit()
                    lineNumber = lineNumber + 1
                    line = iFile.readline()
                    if not zeroLineRex.search(line):
                        print(line[:-1])
                        print('ERROR (line %s): "0\\n" expected' % lineNumber)
                        sys.exit()
                    lineNumber = lineNumber + 1
                    line = iFile.readline()
                # 7 x0 y0 z0 x1 y1 z1 rad
                words = line[:-1].split()
                if words[0] != '7':
                    print(line[:-1])
                    print('ERROR (line %s): "%s\\n" expected'  %
                          (lineNumber, "7 x0 y0 z0 x1 y1 z1 rad"))
                    sys.exit()
                else:
                    # 7
                    if len(words) == 1:
                        lineNumber = lineNumber + 1
                        line = iFile.readline()
                        words = line[:-1].split()
                        # x0 y0 z0
                        if len(words) == 3:
                            x0 = float(words[0])
                            y0 = float(words[1])
                            z0 = float(words[2])
                            lineNumber = lineNumber + 1
                            line = iFile.readline()
                            words = line[:-1].split()
                    else:
                        if len(words) == 4:
                            # 7 x0 y0 z0
                            x0 = float(words[1])
                            y0 = float(words[2])
                            z0 = float(words[3])
                            lineNumber = lineNumber + 1
                            line = iFile.readline()
                            words = line[:-1].split()
                        elif len(words) == 8:
                            # 7 x0 y0 z0 x1 y1 z1 rad
                            x0 = float(words[1])
                            y0 = float(words[2])
                            z0 = float(words[3])
                            x1 = float(words[4])
                            y1 = float(words[5])
                            z1 = float(words[6])
                            radius = float(words[7]) # don't call it rad !!!
                    if len(words) == 3:
                        # x1 y1 z1
                        x1 = float(words[0])
                        y1 = float(words[1])
                        z1 = float(words[2])
                        lineNumber = lineNumber + 1
                        line = iFile.readline()
                        words = line[:-1].split()
                        if len(words) == 1:
                            # rad
                            radius = float(words[0]) # don't call it rad !!!
                    elif len(words) == 4:
                        # x1 y1 z1 rad
                        x1 = float(words[0])
                        y1 = float(words[1])
                        z1 = float(words[2])
                        radius = float(words[3]) # don't call it rad !!!
                    radCylinder.setParams(x0, y0, z0, x1, y1, z1, radius)
                    # store in state
                    state.surfaces.append(radCylinder)
                    ##state.names.append(radCylinder.id)
            # tube
            elif tubeRex.search(line):
                if VERBOSE:
                    print(line[:-1])
                words = line[:-1].split()
                mod = words[0]
                tube = words[1]
                id = words[2]
                radTube = rad.RadTube(mod, id)
                # state
                state.currentSurface = "tube"
                # we expect two lines with zeros
                lineNumber = lineNumber + 1
                line = iFile.readline()
                if not zeroLineRex.search(line):
                    print(line[:-1])
                    print('ERROR (line %s): "0\\n" expected' % lineNumber)
                    sys.exit()
                lineNumber = lineNumber + 1
                line = iFile.readline()
                if not zeroLineRex.search(line):
                    print(line[:-1])
                    print('ERROR (line %s): "0\\n" expected' % lineNumber)
                    sys.exit()
                # 7 x0 y0 z0 x1 y1 z1 rad
                lineNumber = lineNumber + 1
                line = iFile.readline()
                words = line[:-1].split()
                if words[0] != '7':
                    print(line[:-1])
                    print('ERROR (line %s): "%s\\n" expected'  %
                          (lineNumber, "7 x0 y0 z0 x1 y1 z1 rad"))
                    sys.exit()
                else:
                    # 7
                    if len(words) == 1:
                        lineNumber = lineNumber + 1
                        line = iFile.readline()
                        words = line[:-1].split()
                        # x0 y0 z0
                        if len(words) == 3:
                            x0 = float(words[0])
                            y0 = float(words[1])
                            z0 = float(words[2])
                            lineNumber = lineNumber + 1
                            line = iFile.readline()
                            words = line[:-1].split()
                    else:
                        # 7 x0 y0 z0
                        if len(words) == 4:
                            x0 = float(words[1])
                            y0 = float(words[2])
                            z0 = float(words[3])
                            lineNumber = lineNumber + 1
                            line = iFile.readline()
                            words = line[:-1].split()
                    # x1 y1 z1
                    if len(words) == 3:
                        x1 = float(words[0])
                        y1 = float(words[1])
                        z1 = float(words[2])
                        lineNumber = lineNumber + 1
                        line = iFile.readline()
                        words = line[:-1].split()
                    # rad
                    if len(words) == 1:
                        radius = float(words[0]) # don't call rad !!!
                    radTube.setParams(x0, y0, z0, x1, y1, z1, radius)
                    # store in state
                    state.surfaces.append(radTube)
                    ##state.names.append(radTube.id)
            # polygon
            elif polygonRex.search(line):
                if VERBOSE:
                    print(line[:-1])
                words = line[:-1].split()
                mod = words[0]
                polygon = words[1]
                id = words[2]
                radPolygon = rad.RadPolygon(mod, id)
                # state
                state.currentSurface = "polygon"
                if len(words) > 5:
                    line = " ".join(words[5:]) + "\n"
                else:
                    # we expect two lines with zeros
                    lineNumber = lineNumber + 1
                    line = iFile.readline()
                    if not zeroLineRex.search(line):
                        print(line[:-1])
                        print('ERROR (line %s): "0\\n" expected' % lineNumber)
                        sys.exit()
                    lineNumber = lineNumber + 1
                    line = iFile.readline()
                    if not zeroLineRex.search(line):
                        print(line[:-1])
                        print('ERROR (line %s): "0\\n" expected' % lineNumber)
                        sys.exit()
                    lineNumber = lineNumber + 1
                    line = iFile.readline()
                # 3n [x0 y0 z0]
                words = line[:-1].split()
                if len(words) == 4:
                    # 3n
                    n = int(int(words[0]) / 3)
                    # vertices
                    vertices = []
                    x = float(words[1])
                    y = float(words[2])
                    z = float(words[3])
                    vertices.append([x, y, z])
                    for index in range(1, n):
                        lineNumber = lineNumber + 1
                        line = iFile.readline()
                        words = line[:-1].split()
                        x = float(words[0])
                        y = float(words[1])
                        z = float(words[2])
                        vertices.append([x, y, z])
                elif len(words) == 1:
                    # 3n
                    n = int(int(words[0]) / 3)
                    # vertices
                    vertices = []
                    for index in range(n):
                        lineNumber = lineNumber + 1
                        line = iFile.readline()
                        words = line[:-1].split()
                        if words == []:
                            # allow one empty line here
                            lineNumber = lineNumber + 1
                            line = iFile.readline()
                            words = line[:-1].split()
                        x = float(words[0])
                        y = float(words[1])
                        z = float(words[2])
                        vertices.append([x, y, z])
                else:
                    # 3n
                    n = int(int(words[0]) / 3)
                    # vertices
                    vertices = []
                    if len(words) == (n * 3 + 1):
                        words = words[1:]
                        for i in range(n):
                            x = float(words[0])
                            y = float(words[1])
                            z = float(words[2])
                            words = words[3:]
                            vertices.append([x, y, z])
                    else:
                        print(line[:-1])
                        print('ERROR (line %s): "%s\\n" expected'  %
                              (lineNumber, "3n [x0 y0 z0]"))
                        sys.exit()
                radPolygon.setParams(n, vertices)
                # store in state
                state.surfaces.append(radPolygon)
                ##state.names.append(radPolygon.id)
            # light
            elif lightRex.search(line):
                if VERBOSE:
                    print(line[:-1])
                words = line[:-1].split()
                mod = words[0]
                light = words[1]
                id = words[2]
                radLight = rad.RadLight(mod, id)
                # state
                state.currentMaterial = "light"
                # we expect two lines with zeros
                lineNumber = lineNumber + 1
                line = iFile.readline()
                if not zeroLineRex.search(line):
                    print(line[:-1])
                    print('ERROR (line %s): "0\\n" expected' % lineNumber)
                    sys.exit()
                lineNumber = lineNumber + 1
                line = iFile.readline()
                if not zeroLineRex.search(line):
                    print(line[:-1])
                    print('ERROR (line %s): "0\\n" expected' % lineNumber)
                    sys.exit()
                # 3 red green blue
                lineNumber = lineNumber + 1
                line = iFile.readline()
                words = line[:-1].split()
                if words[0] != '3':
                    print(line[:-1])
                    print('ERROR (line %s): "%s\\n" expected'  %
                          (lineNumber, "3 red green blue"))
                    sys.exit()
                else:
                    red = float(words[1])
                    green = float(words[2])
                    blue = float(words[3])
                    radLight.setParams(red, green, blue)
                    # store in state
                    state.materials[id] = radLight
            # mirror
            elif mirrorRex.search(line):
                if VERBOSE:
                    print(line[:-1])
                words = line[:-1].split()
                mod = words[0]
                mirror = words[1]
                id = words[2]
                radMirror = rad.RadMirror(mod, id)
                # state
                state.currentMaterial = "mirror"
                # we expect two lines with zeros
                lineNumber = lineNumber + 1
                line = iFile.readline()
                if not zeroLineRex.search(line):
                    print(line[:-1])
                    print('ERROR (line %s): "0\\n" expected' % lineNumber)
                    sys.exit()
                lineNumber = lineNumber + 1
                line = iFile.readline()
                if not zeroLineRex.search(line):
                    print(line[:-1])
                    print('ERROR (line %s): "0\\n" expected' % lineNumber)
                    sys.exit()
                # 3 red green blue
                lineNumber = lineNumber + 1
                line = iFile.readline()
                words = line[:-1].split()
                if words[0] != '3':
                    print(line[:-1])
                    print('ERROR (line %s): "%s\\n" expected'  %
                          (lineNumber, "3 red green blue"))
                    sys.exit()
                else:
                    red = float(words[1])
                    green = float(words[2])
                    blue = float(words[3])
                    radMirror.setParams(red, green, blue)
                    # store in state
                    state.materials[id] = radMirror
            # plastic
            elif plasticRex.search(line):
                if VERBOSE:
                    print(line[:-1])
                words = line[:-1].split()
                mod = words[0]
                plastic = words[1]
                id = words[2]
                radPlastic = rad.RadPlastic(mod, id)
                # state
                state.currentMaterial = "plastic"
                if len(words) > 5:
                    line = " ".join(words[5:]) + "\n"
                else:
                    # we expect two lines with zeros
                    lineNumber = lineNumber + 1
                    line = iFile.readline()
                    if not zeroLineRex.search(line):
                        print(line[:-1])
                        print('ERROR (line %s): "0\\n" expected' % lineNumber)
                        sys.exit()
                    lineNumber = lineNumber + 1
                    line = iFile.readline()
                    if not zeroLineRex.search(line):
                        print(line[:-1])
                        print('ERROR (line %s): "0\\n" expected' % lineNumber)
                        sys.exit()
                    lineNumber = lineNumber + 1
                    line = iFile.readline()
                # 5 red green blue spec rough
                words = line[:-1].split()
                if words[0] != '5':
                    print(line[:-1])
                    print('ERROR (line %s): "%s\\n" expected'  %
                          (lineNumber, "5 red green blue spec rough"))
                    sys.exit()
                else:
                    red = float(words[1])
                    green = float(words[2])
                    blue = float(words[3])
                    spec = float(words[4])
                    try:
                        rough = float(words[5])
                    except IndexError:
                        lineNumber = lineNumber + 1
                        line = iFile.readline()
                        words = line[:-1].split()
                        rough = float(words[0])
                    radPlastic.setParams(red, green, blue, spec, rough)
                    # store in state
                    state.materials[id] = radPlastic
            # plastic2
            elif plastic2Rex.search(line):
                if VERBOSE:
                    print(line[:-1])
                words = line[:-1].split()
                mod = words[0]
                plastic = words[1]
                id = words[2]
                radPlastic2 = rad.RadPlastic2(mod, id)
                # state
                state.currentMaterial = "plastic2"
                if len(words) > 5:
                    line = " ".join(words[5:]) + "\n"
                else:
                    # we expect two lines with zeros
                    lineNumber = lineNumber + 1
                    line = iFile.readline()
                    if not zeroLineRex.search(line):
                        # TODO: 4+ ux uy uz funcfile transform
                        pass
                    lineNumber = lineNumber + 1
                    line = iFile.readline()
                    if not zeroLineRex.search(line):
                        print(line[:-1])
                        print('ERROR (line %s): "0\\n" expected' % lineNumber)
                        sys.exit()
                    lineNumber = lineNumber + 1
                    line = iFile.readline()
                # 6 red green blue spec urough vrough
                words = line[:-1].split()
                if words[0] != '6':
                    print(line[:-1])
                    print('ERROR (line %s): "%s\\n" expected'  %
                          (lineNumber, "6 red green blue spec urough vrough"))
                    sys.exit()
                else:
                    red = float(words[1])
                    green = float(words[2])
                    blue = float(words[3])
                    spec = float(words[4])
                    urough = float(words[5])
                    vrough = float(words[6])
                    radPlastic2.setParams(red, green, blue, spec, urough, vrough)
                    # store in state
                    state.materials[id] = radPlastic2
            # trans
            elif transRex.search(line):
                if VERBOSE:
                    print(line[:-1])
                words = line[:-1].split()
                mod = words[0]
                trans = words[1]
                id = words[2]
                radTrans = rad.RadTrans(mod, id)
                # state
                state.currentMaterial = "trans"
                if len(words) > 5:
                    line = " ".join(words[5:]) + "\n"
                else:
                    # we expect two lines with zeros
                    lineNumber = lineNumber + 1
                    line = iFile.readline()
                    if not zeroLineRex.search(line):
                        print(line[:-1])
                        print('ERROR (line %s): "0\\n" expected' % lineNumber)
                        sys.exit()
                    lineNumber = lineNumber + 1
                    line = iFile.readline()
                    if not zeroLineRex.search(line):
                        print(line[:-1])
                        print('ERROR (line %s): "0\\n" expected' % lineNumber)
                        sys.exit()
                    lineNumber = lineNumber + 1
                    line = iFile.readline()
                # 7 red green blue spec rough trans tspec
                words = line[:-1].split()
                if words[0] != '7':
                    print(line[:-1])
                    print('ERROR (line %s): "%s\\n" expected'  %
                          (lineNumber,
                           "7 red green blue spec rough trans tspec"))
                    sys.exit()
                else:
                    red = float(words[1])
                    green = float(words[2])
                    blue = float(words[3])
                    spec = float(words[4])
                    rough = float(words[5])
                    trans = float(words[6])
                    tspec = float(words[7])
                    radTrans.setParams(red, green, blue, spec, rough,
                                       trans, tspec)
                    # store in state
                    state.materials[id] = radTrans
            # source
            elif sourceRex.search(line):
                if VERBOSE:
                    print(line[:-1])
                words = line[:-1].split()
                mod = words[0]
                source = words[1]
                id = words[2]
                radSource = rad.RadSource(mod, id)
                # state
                state.currentMaterial = "source"
                # we expect two lines with zeros
                lineNumber = lineNumber + 1
                line = iFile.readline()
                if not zeroLineRex.search(line):
                    print(line[:-1])
                    print('ERROR (line %s): "0\\n" expected' % lineNumber)
                    sys.exit()
                lineNumber = lineNumber + 1
                line = iFile.readline()
                if not zeroLineRex.search(line):
                    print(line[:-1])
                    print('ERROR (line %s): "0\\n" expected' % lineNumber)
                    sys.exit()
                # 4 xdir ydir zdir angle
                lineNumber = lineNumber + 1
                line = iFile.readline()
                words = line[:-1].split()
                if words[0] != '4':
                    print(line[:-1])
                    print('ERROR (line %s): "%s\\n" expected'  %
                          (lineNumber, "4 xdir ydir zdir angle"))
                    sys.exit()
                else:
                    xdir = float(words[1])
                    ydir = float(words[2])
                    zdir = float(words[3])
                    angle = float(words[4])
                    radSource.setParams(xdir, ydir, zdir, angle)
                    # store in state
                    state.materials[id] = radSource
            # spotlight
            elif spotlightRex.search(line):
                if VERBOSE:
                    print(line[:-1])
                words = line[:-1].split()
                mod = words[0]
                spotlight = words[1]
                id = words[2]
                radSpotlight = rad.RadSpotlight(mod, id)
                # state
                state.currentMaterial = "spotlight"
                # we expect two lines with zeros
                lineNumber = lineNumber + 1
                line = iFile.readline()
                if not zeroLineRex.search(line):
                    print(line[:-1])
                    print('ERROR (line %s): "0\\n" expected' % lineNumber)
                    sys.exit()
                lineNumber = lineNumber + 1
                line = iFile.readline()
                if not zeroLineRex.search(line):
                    print(line[:-1])
                    print('ERROR (line %s): "0\\n" expected' % lineNumber)
                    sys.exit()
                # 7 red green blue angle xdir ydir zdir
                lineNumber = lineNumber + 1
                line = iFile.readline()
                words = line[:-1].split()
                if words[0] != '7':
                    print(line[:-1])
                    print('ERROR (line %s): "%s\\n" expected'  %
                          (lineNumber,
                           "7 red green blue angle xdir ydir zdir"))
                    sys.exit()
                else:
                    red = float(words[1])
                    green = float(words[2])
                    blue = float(words[3])
                    angle = float(words[4])
                    xdir = float(words[5])
                    ydir = float(words[6])
                    zdir = float(words[7])
                    radSpotlight.setParams(red, green, blue,
                                           angle, xdir, ydir, zdir)
                    # store in state
                    state.materials[id] = radSpotlight
            # dielectric
            elif dielectricRex.search(line):
                if VERBOSE:
                    print(line[:-1])
                words = line[:-1].split()
                mod = words[0]
                dielectric = words[1]
                id = words[2]
                radDielectric = rad.RadDielectric(mod, id)
                # state
                state.currentMaterial = "dielectric"
                # we expect two lines with zeros
                lineNumber = lineNumber + 1
                line = iFile.readline()
                if not zeroLineRex.search(line):
                    print(line[:-1])
                    print('ERROR (line %s): "0\\n" expected' % lineNumber)
                    sys.exit()
                lineNumber = lineNumber + 1
                line = iFile.readline()
                if not zeroLineRex.search(line):
                    print(line[:-1])
                    print('ERROR (line %s): "0\\n" expected' % lineNumber)
                    sys.exit()
                # 3 rtn gtn btn n hc
                lineNumber = lineNumber + 1
                line = iFile.readline()
                words = line[:-1].split()
                if words[0] != '5':
                    print(line[:-1])
                    print('ERROR (line %s): "%s\\n" expected'  %
                          (lineNumber, "5 rtn gtn btn n hc"))
                    sys.exit()
                else:
                    rtn = float(words[1])
                    gtn = float(words[2])
                    btn = float(words[3])
                    n = float(words[4])
                    hc = float(words[5])
                    radDielectric.setParams(rtn, gtn, btn, n, hc)
                    # store in state
                    state.materials[id] = radDielectric
            # glass
            elif glassRex.search(line):
                if VERBOSE:
                    print(line[:-1])
                words = line[:-1].split()
                mod = words[0]
                glass = words[1]
                id = words[2]
                radGlass = rad.RadGlass(mod, id)
                # state
                state.currentMaterial = "glass"
                # we expect two lines with zeros
                lineNumber = lineNumber + 1
                line = iFile.readline()
                if not zeroLineRex.search(line):
                    print(line[:-1])
                    print('ERROR (line %s): "0\\n" expected' % lineNumber)
                    sys.exit()
                lineNumber = lineNumber + 1
                line = iFile.readline()
                if not zeroLineRex.search(line):
                    print(line[:-1])
                    print('ERROR (line %s): "0\\n" expected' % lineNumber)
                    sys.exit()
                # 3 rtn gtn btn
                lineNumber = lineNumber + 1
                line = iFile.readline()
                words = line[:-1].split()
                if words[0] != '3':
                    print(line[:-1])
                    print('ERROR (line %s): "%s\\n" expected'  %
                          (lineNumber, "3 rtn gtn btn"))
                    sys.exit()
                else:
                    rtn = float(words[1])
                    gtn = float(words[2])
                    btn = float(words[3])
                    radGlass.setParams(rtn, gtn, btn)
                    # store in state
                    state.materials[id] = radGlass
            # glow
            elif glowRex.search(line):
                if VERBOSE:
                    print(line[:-1])
                words = line[:-1].split()
                mod = words[0]
                glow = words[1]
                id = words[2]
                radGlow = rad.RadGlow(mod, id)
                # state
                state.currentMaterial = "glow"
                # we expect two lines with zeros
                lineNumber = lineNumber + 1
                line = iFile.readline()
                if not zeroLineRex.search(line):
                    print(line[:-1])
                    print('ERROR (line %s): "0\\n" expected' % lineNumber)
                    sys.exit()
                lineNumber = lineNumber + 1
                line = iFile.readline()
                if not zeroLineRex.search(line):
                    print(line[:-1])
                    print('ERROR (line %s): "0\\n" expected' % lineNumber)
                    sys.exit()
                # 4 red green blue maxrad
                lineNumber = lineNumber + 1
                line = iFile.readline()
                words = line[:-1].split()
                if words[0] != '4':
                    print(line[:-1])
                    print('ERROR (line %s): "%s\\n" expected'  %
                          (lineNumber, "4 red green blue maxrad"))
                    sys.exit()
                else:
                    red = float(words[1])
                    green = float(words[2])
                    blue = float(words[3])
                    maxrad = float(words[4])
                    radGlow.setParams(red, green, blue, maxrad)
                    # store in state
                    state.materials[id] = radGlow
            # illum
            elif illumRex.search(line):
                if VERBOSE:
                    print(line[:-1])
                words = line[:-1].split()
                mod = words[0]
                illum = words[1]
                id = words[2]
                radIllum = rad.RadIllum(mod, id)
                # state
                state.currentMaterial = "illum"
                # we expect: 1 material
                lineNumber = lineNumber + 1
                line = iFile.readline()
                # ... but for now ignore the line (TODO)
                # we expect a zero line
                lineNumber = lineNumber + 1
                line = iFile.readline()
                if not zeroLineRex.search(line):
                    print(line[:-1])
                    print('ERROR (line %s): "0\\n" expected' % lineNumber)
                    sys.exit()
                # 3 red green blue
                lineNumber = lineNumber + 1
                line = iFile.readline()
                words = line[:-1].split()
                if words[0] != '3':
                    print(line[:-1])
                    print('ERROR (line %s): "%s\\n" expected'  %
                          (lineNumber, "3 red green blue"))
                    sys.exit()
                else:
                    red = float(words[1])
                    green = float(words[2])
                    blue = float(words[3])
                    radIllum.setParams(red, green, blue)
                    # store in state
                    state.materials[id] = radIllum
            # metal
            elif metalRex.search(line):
                if VERBOSE:
                    print(line[:-1])
                words = line[:-1].split()
                mod = words[0]
                metal = words[1]
                id = words[2]
                radMetal = rad.RadMetal(mod, id)
                # state
                state.currentMaterial = "metal"
                if len(words) > 5:
                    line = " ".join(words[5:]) + "\n"
                else:
                    # we expect two lines with zeros
                    lineNumber = lineNumber + 1
                    line = iFile.readline()
                    if not zeroLineRex.search(line):
                        print(line[:-1])
                        print('ERROR (line %s): "0\\n" expected' % lineNumber)
                        sys.exit()
                    lineNumber = lineNumber + 1
                    line = iFile.readline()
                    if not zeroLineRex.search(line):
                        print(line[:-1])
                        print('ERROR (line %s): "0\\n" expected' % lineNumber)
                        sys.exit()
                    lineNumber = lineNumber + 1
                    line = iFile.readline()
                # 5 red green blue spec rough
                words = line[:-1].split()
                if words[0] != '5':
                    print(line[:-1])
                    print('ERROR (line %s): "%s\\n" expected'  %
                          (lineNumber, "5 red green blue spec rough"))
                    sys.exit()
                else:
                    red = float(words[1])
                    green = float(words[2])
                    blue = float(words[3])
                    spec = float(words[4])
                    try:
                        rough = float(words[5])
                    except IndexError:
                        lineNumber = lineNumber + 1
                        line = iFile.readline()
                        words = line[:-1].split()
                        rough = float(words[0])
                    radMetal.setParams(red, green, blue, spec, rough)
                    # store in state
                    state.materials[id] = radMetal
            # metal2
            elif metal2Rex.search(line):
                if VERBOSE:
                    print(line[:-1])
                words = line[:-1].split()
                mod = words[0]
                metal2 = words[1]
                id = words[2]
                radMetal2 = rad.RadMetal2(mod, id)
                # state
                state.currentMaterial = "metal2"
                if len(words) > 6:
                    line = " ".join(words[6:])
                else:
                    # we expect two lines with zeros
                    lineNumber = lineNumber + 1
                    line = iFile.readline()
                    if not zeroLineRex.search(line):
                        # TODO: 4+ ux uy uz funcfile transform
                        pass
                    lineNumber = lineNumber + 1
                    line = iFile.readline()
                    if not zeroLineRex.search(line):
                        print(line[:-1])
                        print('ERROR (line %s): "0\\n" expected' % lineNumber)
                        sys.exit()
                    lineNumber = lineNumber + 1
                    line = iFile.readline()
                # 5 red green blue spec urough vrough
                words = line[:-1].split()
                if words[0] != '6':
                    print(line[:-1])
                    print('ERROR (line %s): "%s\\n" expected'  %
                          (lineNumber, "6 red green blue spec urough vrough"))
                    sys.exit()
                else:
                    red = float(words[1])
                    green = float(words[2])
                    blue = float(words[3])
                    spec = float(words[4])
                    urough = float(words[5])
                    vrough = float(words[6])
                    radMetal2.setParams(red, green, blue, spec, urough, vrough)
                    # store in state
                    state.materials[id] = radMetal2
            # antimatter
            elif antimatterRex.search(line):
                if VERBOSE:
                    print(line[:-1])
                words = line[:-1].split()
                mod = words[0]
                antimatter = words[1]
                id = words[2]
                radAntimatter = rad.RadAntimatter(mod, id)
                # state
                state.currentMaterial = "antimatter"
                # N mod1 mod2 ... modN
                lineNumber = lineNumber + 1
                line = iFile.readline()
                words = line[:-1].split()
                N = int(words[0])
                mods = []
                for i in range(N):
                    mods.append(words[i+1])
                radAntimatter.setParams(mods)
                # store in state
                state.materials[id] = radAntimatter
                # we expect two lines with zeros
                lineNumber = lineNumber + 1
                line = iFile.readline()
                if not zeroLineRex.search(line):
                    print(line[:-1])
                    print('ERROR (line %s): "0\\n" expected' % lineNumber)
                    sys.exit()
                lineNumber = lineNumber + 1
                line = iFile.readline()
                if not zeroLineRex.search(line):
                    print(line[:-1])
                    print('ERROR (line %s): "0\\n" expected' % lineNumber)
                    sys.exit()
            # texfunc or colorfunc or brightfunc or brightdata or colorpict
            elif (texfuncRex.search(line) or colorfuncRex.search(line) or
                  brightfuncRex.search(line) or brightdataRex.search(line) or
                  colorpictRex.search(line)):
                # just ignore the next three lines (TODO)
                lineNumber = lineNumber + 1
                line = iFile.readline()
                lineNumber = lineNumber + 1
                line = iFile.readline()
                lineNumber = lineNumber + 1
                line = iFile.readline()
            elif len(line) > len("rview") and line.startswith("rview"):
                if VERBOSE:
                    print(line[:-1])
                words = line[:-1].split("-vp")
                words = words[1].split()
                vp = [float(words[0]), float(words[1]), float(words[2])]
                words = line[:-1].split("-vd")
                words = words[1].split()
                vd = [float(words[0]), float(words[1]), float(words[2])]
                words = line[:-1].split("-vh")
                words = words[1].split()
                vh = float(words[0])
                words = line[:-1].split("-vv")
                words = words[1].split()
                vv = float(words[0])
                state.camera = rad.RadCamera(state.filename, vp, vd, vh, vv)
            else:
                print(line[:-1])
    # close RAD file
    iFile.close()

def commands(iFilename, state):
    # regular expressions
    commandRex = re.compile("^!")
    # open RAD file
    iFile = open(iFilename, "r")
    # read RAD file line by line
    lineNumber = 0
    while 1:
        lineNumber = lineNumber + 1
        line = iFile.readline()
        if line == "":
            # EOF
            break
        else:
            # comment
            if commandRex.search(line):
                words = line[:-1].split()
                while words[-1] == '\\':
                    # read next line
                    lineNumber = lineNumber + 1
                    line = " ".join(words[:-1]) + iFile.readline()
                    words = line[:-1].split()
                line = " ".join(words)
                execString = line[1:]
                info = os.popen(execString)
                lines = info.readlines()
                info.close()
                for line in lines:
                    print(line[:-1])
            else:
                print(line[:-1])
                pass

def create(state):
    # import rad module
    print("import rad")
    # all materials first
    print("# materials")
    print("materials = {}")
    for materialName in state.materials.keys():
        material =  state.materials[materialName]
        print('materials["%s"] = rad.%s' % (material.id, material))
    # after that we define the surfaces
    print("# surfaces")
    print("surfaces = []")
    surfaceNames = {}
    for surface in state.surfaces:
        print("surfaces.append(rad.%s)" % surface)
    # now the commands
    print("# commands")
    print("commands = []")
    for command in state.commands:
        words = command.commandLine.split()
        if words[0] == "cat":
            pass
        elif words[0] == "xform":
            print('commands.append(rad.%s)' % command)
        else:
            print('commands.append(rad.%s)' % command)
    # then the includes
    print("# includes")
    print("includes = {}")
    for include in state.includes:
        print('includes["%s"] = %s' % (include, state.includes[include]))
    # finally the camera
    print("# camera")
    if (state.camera == None):
        print("camera = %s" % state.camera)
    else:
        print("camera = rad.%s" % state.camera)

if __name__ == "__main__":
    if len(sys.argv) != 3:
        usage()
    else:
        # state
        state = State()
        # check argument
        if sys.argv[2] not in ["test", "commands", "create"]:
            usage()
            sys.exit()
        else:
            state.mode = sys.argv[2]
        iFilename = sys.argv[1]
        dir, base = os.path.split(iFilename)
        state.directory = dir
        state.filename, state.ext = os.path.splitext(base)
        if state.mode == "test":
            iFile = open(iFilename, "r")
            test(iFile, state)
            iFile.close()
            for command in state.commands:
                print(command)
        elif state.mode == "commands":
            commands(iFilename, state)
        if state.mode == "create":
            # do test first ...
            iFile = open(iFilename, "r")
            test(iFile, state)
            iFile.close()
            # ... then create
            create(state)
