import os
import math
import array
import ctypes
# common exporter interface
from ..common.interface import CommonExporterInterface
# Blender
import bpy

class IgsExporter(CommonExporterInterface):
    def __init__(self, options):
        CommonExporterInterface.__init__(self, "IgsExporter", options)
        self.igsFile = None
        self.directory = None
        self.usedMaterials = {}
        self.uids = 0
        self.layers = 0

    def prepareExport(self, scene, bbox, directory, name, mblur, light_counter,
                      sun = None):
        self.directory = directory
        # scene related
        self.scene = scene
        self.bbox = bbox
        if scene.unit_settings.system != 'METRIC':
            print("WARNING: unit_settings == '%s'" % scene.unit_settings.system)
        self.scale_length = scene.unit_settings.scale_length
        # light related
        self.light_counter = light_counter
        # GUI related
        self.uids = 0
        # layer related
        self.layers = 0
        # create directory for meshes
        directoryPath = os.path.join(self.directory, "igmesh")
        if not os.path.exists(directoryPath):
            print("INFO: creating directory:")
            print("INFO:    \"%s\"" % directoryPath)
            os.mkdir(directoryPath)
        # open igmesh/objects.igs
        self.objFile = open(os.path.join(self.directory,
                                         "igmesh",
                                         "objects.igs"),
                            "w")
        self.objFile.write('<scenedata>\n')
        # open
        filename = os.path.join(directory, name + ".igs")
        print('open("%s", "w")' % filename)
        self.igsFile = open(filename, "w")
        # header
        self.igsFile.write('<?xml version="1.0" encoding="utf-8"?>\n')
        # begin scene
        self.igsFile.write('<scene>\n')

    def write_list_of_vec2s(self, oFile, vec2_list):
        # length of vector
        self.write_uint32(oFile, len(vec2_list))
        # convert the list of vec3s to a list of floats
        component_list = []
        for v in vec2_list:
            component_list.extend([v[0], v[1]])
        # the list of floats
        a = array.array('f', component_list)
        a.tofile(oFile)

    def write_list_of_vec3s(self, oFile, vec3_list):
        # length of vector
        self.write_uint32(oFile, len(vec3_list))
        # convert the list of vec3s to a list of floats
        component_list = []
        for v in vec3_list:
            component_list.extend([self.scale_length * v[0],
                                   self.scale_length * v[1],
                                   self.scale_length * v[2]])
        # the list of floats
        a = array.array('f', component_list)
        a.tofile(oFile)

    def write_string(self, oFile, s):
        string_bytes = bytearray(s.encode(encoding='UTF-8'))
        # length of string
        self.write_uint32(oFile, len(string_bytes))
        # string bytes.
        a = array.array('b', string_bytes)
        a.tofile(oFile)

    def write_uint32(self, oFile, x):
        # see Blendigo igmesh.py
        a = array.array('i', [x]) # signed
        a.tofile(oFile)

    def writeBlenderMaterial(self, mat):
        matType = None
        # try first to use Cycles nodes
        if self.options.cycles and mat.use_nodes:
            surface = mat.node_tree.nodes['Material Output'].inputs['Surface']
            if len(surface.links) >= 1:
                try:
                    color = surface.links[0].from_node.inputs['Color']
                except KeyError:
                    # handle mix of diffuse and glossy/transparent
                    fac = surface.links[0].from_node.inputs['Fac']
                    surface1 = surface.links[0].from_node.inputs[1]
                    surface2 = surface.links[0].from_node.inputs[2]
                    if surface1.links[0].from_node.type != 'BSDF_DIFFUSE':
                        print("WARNING: BSDF_DIFFUSE expected, %s found" %
                              surface1.links[0].from_node.type)
                    if surface2.links[0].from_node.type != 'BSDF_GLOSSY':
                        if surface2.links[0].from_node.type != 'BSDF_TRANSPARENT':
                            print("WARNING: BSDF_GLOSSY|TRANSPARENT expected, " +
                                  "%s found" %
                                  surface2.links[0].from_node.type)
                    diff = surface1.links[0].from_node.inputs['Color']
                    if surface2.links[0].from_node.type != 'BSDF_GLOSSY':
                        spec = [1, 1, 1, 1]
                    else:
                        spec = surface2.links[0].from_node.inputs['Color']
                    # mix
                    matType = "blend"
                    self.igsFile.write('  <material>\n')
                    self.uids += 1 # increment before usage
                    self.igsFile.write('    <uid>%s</uid>\n' % self.uids)
                    self.igsFile.write('    <name>%s</name>\n' % mat.name)
                    self.igsFile.write('    <blend>\n')
                    self.igsFile.write('      <a_mat_uid>%s</a_mat_uid>\n' %
                                       (self.uids + 1))
                    if surface2.links[0].from_node.type == 'BSDF_GLOSSY':
                        self.igsFile.write('      <b_mat_uid>%s</b_mat_uid>\n' %
                                           (self.uids + 2))
                    elif surface2.links[0].from_node.type == 'BSDF_TRANSPARENT':
                        # medium id = self.uids + 2
                        self.igsFile.write('      <b_mat_uid>%s</b_mat_uid>\n' %
                                           (self.uids + 3))
                    else:
                        # TODO: handle other BSDFs
                        print("WARNING: BSDF_GLOSSY|TRANSPARENT expected, " +
                              "%s found" %
                              surface2.links[0].from_node.type)
                        self.igsFile.write('      <b_mat_uid>%s</b_mat_uid>\n' %
                                           (self.uids + 1)) # TMP: re-use ID
                    if fac.is_linked:
                        image = fac.links[0].from_node.image
                        src = image.filepath
                        paths = bpy.utils.blend_paths()
                        if src in paths:
                            index = paths.index(src)
                            abs_paths = bpy.utils.blend_paths(absolute = True)
                            abs_path = abs_paths[index]
                            self.igsFile.write('      <texture>\n')
                            self.igsFile.write('        <path>%s</path>\n' % abs_path)
                            self.igsFile.write('        <a>0</a>\n')
                            self.igsFile.write('        <b>1</b>\n')
                            self.igsFile.write('        <c>0</c>\n')
                            self.igsFile.write('        <uv_set_index>0' +
                                               '</uv_set_index>\n')
                            self.igsFile.write('        <exponent>2.2</exponent>\n')
                            self.igsFile.write('        <tex_coord_generation>\n')
                            self.igsFile.write('          <uv>\n')
                            self.igsFile.write('            <matrix>1 0 0 1</matrix>\n')
                            self.igsFile.write('            <translation>0 0' +
                                               '</translation>\n')
                            self.igsFile.write('          </uv>\n')
                            self.igsFile.write('        </tex_coord_generation>\n')
                            self.igsFile.write('        <smooth>false</smooth>\n')
                            self.igsFile.write('      </texture>\n')
                            self.igsFile.write('      <blend>\n')
                            self.igsFile.write('        <texture>\n')
                            self.igsFile.write('          <texture_index>\n')
                            self.igsFile.write('            0\n')
                            self.igsFile.write('          </texture_index>\n')
                            self.igsFile.write('        </texture>\n')
                        else:                    
                            self.igsFile.write('      <blend>\n')
                            self.igsFile.write('        <constant>\n')
                            self.igsFile.write('          0.5\n') # TODO
                            self.igsFile.write('        </constant>\n')
                    else:                    
                        self.igsFile.write('      <blend>\n')
                        self.igsFile.write('        <constant>\n')
                        self.igsFile.write('          0.5\n') # TODO
                        self.igsFile.write('        </constant>\n')
                    self.igsFile.write('      </blend>\n')
                    self.igsFile.write('      <step_blend>false</step_blend>\n')
                    self.igsFile.write('    </blend>\n')
                    self.igsFile.write('  </material>\n')
                    # matType = "diffuse"
                    r = diff.default_value[0]
                    g = diff.default_value[1]
                    b = diff.default_value[2]
                    rgb = [r, g, b]
                    self.igsFile.write('  <material>\n')
                    self.uids += 1 # increment before usage
                    self.igsFile.write('    <uid>%s</uid>\n' % self.uids)
                    self.igsFile.write('    <name>%s_%s</name>\n' %
                                       (mat.name, "diff"))
                    self.igsFile.write('    <diffuse>\n')
                    if diff.is_linked:
                        image = diff.links[0].from_node.image
                        src = image.filepath
                        paths = bpy.utils.blend_paths()
                        if src in paths:
                            index = paths.index(src)
                            abs_paths = bpy.utils.blend_paths(absolute = True)
                            abs_path = abs_paths[index]
                            self.igsFile.write('      <albedo>\n')
                            self.igsFile.write('        <texture>\n')
                            self.igsFile.write('          <texture_index>\n')
                            self.igsFile.write('            0\n')
                            self.igsFile.write('          </texture_index>\n')
                            self.igsFile.write('        </texture>\n')
                            self.igsFile.write('      </albedo>\n')
                            self.igsFile.write('      <texture>\n')
                            self.igsFile.write('        <path>%s</path>\n' % abs_path)
                            self.igsFile.write('        <a>0</a>\n')
                            self.igsFile.write('        <b>1</b>\n')
                            self.igsFile.write('        <c>0</c>\n')
                            self.igsFile.write('        <uv_set_index>0' +
                                               '</uv_set_index>\n')
                            self.igsFile.write('        <exponent>2.2</exponent>\n')
                            self.igsFile.write('        <tex_coord_generation>\n')
                            self.igsFile.write('          <uv>\n')
                            self.igsFile.write('            <matrix>1 0 0 1</matrix>\n')
                            self.igsFile.write('            <translation>0 0' +
                                               '</translation>\n')
                            self.igsFile.write('          </uv>\n')
                            self.igsFile.write('        </tex_coord_generation>\n')
                            self.igsFile.write('        <smooth>false</smooth>\n')
                            self.igsFile.write('      </texture>\n')
                        else:
                            self.igsFile.write('      <albedo>\n')
                            self.igsFile.write('        <constant>\n')
                            self.igsFile.write('          <rgb>\n')
                            self.igsFile.write('            <rgb>%s %s %s</rgb>\n' %
                                               (rgb[0], rgb[1], rgb[2]))
                            self.igsFile.write('            <gamma>1.0</gamma>\n')
                            self.igsFile.write('          </rgb>\n')
                            self.igsFile.write('        </constant>\n')
                            self.igsFile.write('      </albedo>\n')
                    else:
                        self.igsFile.write('      <albedo>\n')
                        self.igsFile.write('        <constant>\n')
                        self.igsFile.write('          <rgb>\n')
                        self.igsFile.write('            <rgb>%s %s %s</rgb>\n' %
                                           (rgb[0], rgb[1], rgb[2]))
                        self.igsFile.write('            <gamma>1.0</gamma>\n')
                        self.igsFile.write('          </rgb>\n')
                        self.igsFile.write('        </constant>\n')
                        self.igsFile.write('      </albedo>\n')
                    self.igsFile.write('    </diffuse>\n')
                    self.igsFile.write('  </material>\n')
                    # BSDF_GLOSSY or BSDF_TRANSPARENT?
                    if surface2.links[0].from_node.type == 'BSDF_GLOSSY':
                        # matType = "phong"
                        r = spec.default_value[0]
                        g = spec.default_value[1]
                        b = spec.default_value[2]
                        rgb = [r, g, b]
                        spec_rough = surface2.links[0].from_node.inputs['Roughness']
                        self.igsFile.write('  <material>\n')
                        self.uids += 1 # increment before usage
                        self.igsFile.write('    <uid>%s</uid>\n' % self.uids)
                        self.igsFile.write('    <name>%s_%s</name>\n' %
                                           (mat.name, "spec"))
                        self.igsFile.write('    <phong>\n')
                        self.igsFile.write('      <ior>1.5</ior>\n')
                        if spec_rough.is_linked:
                            image = spec_rough.links[0].from_node.image
                            src = image.filepath
                            paths = bpy.utils.blend_paths()
                            if src in paths:
                                index = paths.index(src)
                                abs_paths = bpy.utils.blend_paths(absolute = True)
                                abs_path = abs_paths[index]
                                self.igsFile.write('      <exponent>\n')
                                self.igsFile.write('        <texture>\n')
                                self.igsFile.write('          <texture_index>\n')
                                self.igsFile.write('            0\n')
                                self.igsFile.write('          </texture_index>\n')
                                self.igsFile.write('        </texture>\n')
                                self.igsFile.write('      </exponent>\n')
                                self.igsFile.write('      <texture>\n')
                                self.igsFile.write('        <path>%s</path>\n' % abs_path)
                                self.igsFile.write('        <a>0</a>\n')
                                self.igsFile.write('        <b>1</b>\n')
                                self.igsFile.write('        <c>0</c>\n')
                                self.igsFile.write('        <uv_set_index>0' +
                                                   '</uv_set_index>\n')
                                self.igsFile.write('        <exponent>2.2</exponent>\n')
                                self.igsFile.write('        <tex_coord_generation>\n')
                                self.igsFile.write('          <uv>\n')
                                self.igsFile.write('            <matrix>1 0 0 1</matrix>\n')
                                self.igsFile.write('            <translation>0 0' +
                                                   '</translation>\n')
                                self.igsFile.write('          </uv>\n')
                                self.igsFile.write('        </tex_coord_generation>\n')
                                self.igsFile.write('        <smooth>false</smooth>\n')
                                self.igsFile.write('      </texture>\n')
                            else:
                                self.igsFile.write('      <exponent>\n')
                                self.igsFile.write('        <constant>1000000.0</constant>\n')
                                self.igsFile.write('      </exponent>\n')
                        else:
                            self.igsFile.write('      <exponent>\n')
                            self.igsFile.write('        <constant>1000000.0</constant>\n')
                            self.igsFile.write('      </exponent>\n')
                        self.igsFile.write('      <specular_reflectivity>\n')
                        self.igsFile.write('        <constant>\n')
                        self.igsFile.write('          <rgb>\n')
                        self.igsFile.write('            <gamma>1.0</gamma>\n')
                        self.igsFile.write('            <rgb>%s %s %s</rgb>\n' %
                                           (rgb[0], rgb[1], rgb[2]))
                        self.igsFile.write('          </rgb>\n')
                        self.igsFile.write('        </constant>\n')
                        self.igsFile.write('      </specular_reflectivity>\n')
                        self.igsFile.write('    </phong>\n')
                        self.igsFile.write('  </material>\n')
                    elif surface2.links[0].from_node.type == 'BSDF_TRANSPARENT':
                        # matType = "glass"
                        ior = 1.0
                        newRgb = [0.96, 0.96, 0.96]
                        self.igsFile.write('  <medium>\n')
                        self.uids += 1 # increment before usage
                        self.igsFile.write('    <uid>%s</uid>\n' % self.uids)
                        self.igsFile.write('    <name>%s_medium</name>\n' % mat.name)
                        self.igsFile.write('    <basic>\n')
                        self.igsFile.write('      <ior>%s</ior>\n' % ior)
                        self.igsFile.write('      <cauchy_b_coeff>0.0</cauchy_b_coeff>\n')
                        self.igsFile.write('      <absorption_coefficient>\n')
                        self.igsFile.write('        <constant>\n')
                        self.igsFile.write('          <rgb>\n')
                        self.igsFile.write('            <gamma>1.0</gamma>\n')
                        self.igsFile.write('            <rgb>%s %s %s</rgb>\n' %
                                           (newRgb[0], newRgb[1], newRgb[2]))
                        self.igsFile.write('          </rgb>\n')
                        self.igsFile.write('        </constant>\n')
                        self.igsFile.write('      </absorption_coefficient>\n')
                        self.igsFile.write('    </basic>\n')
                        self.igsFile.write('    <precedence>2</precedence>\n')
                        self.igsFile.write('  </medium>\n')
                        self.igsFile.write('  <material>\n')
                        self.uids += 1 # increment before usage
                        self.igsFile.write('    <uid>%s</uid>\n' % self.uids)
                        self.igsFile.write('    <name>%s_%s</name>\n' %
                                           (mat.name, "trans"))
                        self.igsFile.write('    <specular>\n')
                        self.igsFile.write('      <transparent>true</transparent>\n')
                        self.igsFile.write('      <internal_medium_name>%s_medium'  %
                                           mat.name + '</internal_medium_name>\n')
                        self.igsFile.write('      <arch_glass>true</arch_glass>\n')
                        self.igsFile.write('      <single_face>true</single_face>\n')
                        self.igsFile.write('    </specular>\n')
                        self.igsFile.write('  </material>\n')
                    else:
                        print("WARNING: BSDF_GLOSSY|TRANSPARENT expected, " +
                              "%s found" %
                              surface2.links[0].from_node.type)
                    return matType
                else:
                    if surface.links[0].from_node.type == 'BSDF_DIFFUSE':
                        # diffuse BRDF
                        matType = "diffuse"
                        rgb = mat.diffuse_color
                        self.igsFile.write('  <material>\n')
                        self.uids += 1 # increment before usage
                        self.igsFile.write('    <uid>%s</uid>\n' % self.uids)
                        self.igsFile.write('    <name>%s</name>\n' % mat.name)
                        self.igsFile.write('    <diffuse>\n')
                        diff = surface.links[0].from_node.inputs['Color']
                        if diff.is_linked:
                            image = diff.links[0].from_node.image
                            src = image.filepath
                            paths = bpy.utils.blend_paths()
                            if src in paths:
                                index = paths.index(src)
                                abs_paths = bpy.utils.blend_paths(absolute = True)
                                abs_path = abs_paths[index]
                                self.igsFile.write('      <albedo>\n')
                                self.igsFile.write('        <texture>\n')
                                self.igsFile.write('          <texture_index>\n')
                                self.igsFile.write('            0\n')
                                self.igsFile.write('          </texture_index>\n')
                                self.igsFile.write('        </texture>\n')
                                self.igsFile.write('      </albedo>\n')
                                self.igsFile.write('      <texture>\n')
                                self.igsFile.write('        <path>%s</path>\n' % abs_path)
                                self.igsFile.write('        <a>0</a>\n')
                                self.igsFile.write('        <b>1</b>\n')
                                self.igsFile.write('        <c>0</c>\n')
                                self.igsFile.write('        <uv_set_index>0' +
                                                   '</uv_set_index>\n')
                                self.igsFile.write('        <exponent>2.2</exponent>\n')
                                self.igsFile.write('        <tex_coord_generation>\n')
                                self.igsFile.write('          <uv>\n')
                                self.igsFile.write('            <matrix>1 0 0 1</matrix>\n')
                                self.igsFile.write('            <translation>0 0' +
                                                   '</translation>\n')
                                self.igsFile.write('          </uv>\n')
                                self.igsFile.write('        </tex_coord_generation>\n')
                                self.igsFile.write('        <smooth>false</smooth>\n')
                                self.igsFile.write('      </texture>\n')
                            else:
                                self.igsFile.write('      <albedo>\n')
                                self.igsFile.write('        <constant>\n')
                                self.igsFile.write('          <rgb>\n')
                                self.igsFile.write('            <rgb>%s %s %s</rgb>\n' %
                                                   (rgb[0], rgb[1], rgb[2]))
                                self.igsFile.write('            <gamma>1.0</gamma>\n')
                                self.igsFile.write('          </rgb>\n')
                                self.igsFile.write('        </constant>\n')
                                self.igsFile.write('      </albedo>\n')
                        else:
                            self.igsFile.write('      <albedo>\n')
                            self.igsFile.write('        <constant>\n')
                            self.igsFile.write('          <rgb>\n')
                            self.igsFile.write('            <rgb>%s %s %s</rgb>\n' %
                                               (rgb[0], rgb[1], rgb[2]))
                            self.igsFile.write('            <gamma>1.0</gamma>\n')
                            self.igsFile.write('          </rgb>\n')
                            self.igsFile.write('        </constant>\n')
                            self.igsFile.write('      </albedo>\n')
                        self.igsFile.write('    </diffuse>\n')
                        self.igsFile.write('  </material>\n')
                        return matType
                    elif surface.links[0].from_node.type == 'EMISSION':
                        # light emitter
                        matType = "emitter"
                        # layer
                        self.igsFile.write('  <layer_name>\n')
                        self.igsFile.write('    <layer_name>%s</layer_name>\n' % mat.name)
                        self.layers += 1 # increment before usage
                        self.igsFile.write('    <layer_index>%s</layer_index>\n' %
                                           self.layers)
                        self.igsFile.write('  </layer_name>\n')
                        # material
                        diff = surface.links[0].from_node.inputs['Color']
                        r = diff.default_value[0]
                        g = diff.default_value[1]
                        b = diff.default_value[2]
                        rgb = [r, g, b]
                        strength = surface.links[0].from_node.inputs['Strength'].default_value
                        base_emission = strength * 10000 # TODO: replace '* 10000'
                        self.igsFile.write('  <material>\n')
                        self.uids += 1 # increment before usage
                        self.igsFile.write('    <uid>%s</uid>\n' % self.uids)
                        self.igsFile.write('    <name>%s</name>\n' % mat.name)
                        self.igsFile.write('    <diffuse>\n')
                        self.igsFile.write('      <base_emission>\n')
                        self.igsFile.write('        <constant>\n')
                        self.igsFile.write('          <uniform>\n')
                        self.igsFile.write('            <value>%s</value>\n' %
                                           base_emission)
                        self.igsFile.write('          </uniform>\n')
                        self.igsFile.write('        </constant>\n')
                        self.igsFile.write('      </base_emission>\n')
                        self.igsFile.write('      <emission>\n')
                        self.igsFile.write('        <constant>\n')
                        self.igsFile.write('          <rgb>\n')
                        self.igsFile.write('            <gamma>1.0</gamma>\n')
                        self.igsFile.write('            <rgb>%s %s %s</rgb>\n' %
                                           (rgb[0], rgb[1], rgb[2]))
                        self.igsFile.write('          </rgb>\n')
                        self.igsFile.write('        </constant>\n')
                        self.igsFile.write('      </emission>\n')
                        self.igsFile.write('      <albedo>\n')
                        self.igsFile.write('        <constant>\n')
                        self.igsFile.write('          <rgb>\n')
                        self.igsFile.write('            <gamma>1.0</gamma>\n')
                        self.igsFile.write('            <rgb>%s %s %s</rgb>\n' %
                                           (rgb[0], rgb[1], rgb[2]))
                        self.igsFile.write('          </rgb>\n')
                        self.igsFile.write('        </constant>\n')
                        self.igsFile.write('      </albedo>\n')
                        self.igsFile.write('      <layer>%s</layer>\n' % self.layers)
                        self.igsFile.write('    </diffuse>\n')
                        self.igsFile.write('  </material>\n')
                        return matType
                    elif surface.links[0].from_node.type == 'BSDF_GLOSSY':
                        # phong
                        matType = "phong"
                        diff = surface.links[0].from_node.inputs['Color']
                        r = diff.default_value[0]
                        g = diff.default_value[1]
                        b = diff.default_value[2]
                        rgb = [r, g, b]
                        roughness = surface.links[0].from_node.inputs['Roughness'].default_value
                        self.igsFile.write('  <material>\n')
                        self.uids += 1 # increment before usage
                        self.igsFile.write('    <uid>%s</uid>\n' % self.uids)
                        self.igsFile.write('    <name>%s</name>\n' % mat.name)
                        self.igsFile.write('    <phong>\n')
                        self.igsFile.write('      <ior>1.5</ior>\n')
                        self.igsFile.write('      <exponent>\n')
                        if roughness == 0.0:
                            self.igsFile.write('        <constant>1000000.0</constant>\n')
                        else:
                            # http://www.indigorenderer.com/node/1169
                            # Typically the exponent varies from about 10 to 10000
                            self.igsFile.write('        <constant>%s</constant>\n' %
                                               (10.0 + math.pow(100.0 * (1.0 - math.pow(roughness, 0.05)), 2)))
                        self.igsFile.write('      </exponent>\n')
                        self.igsFile.write('      <specular_reflectivity>\n')
                        self.igsFile.write('        <constant>\n')
                        self.igsFile.write('          <rgb>\n')
                        self.igsFile.write('            <gamma>1.0</gamma>\n')
                        self.igsFile.write('            <rgb>%s %s %s</rgb>\n' %
                                           (rgb[0], rgb[1], rgb[2]))
                        self.igsFile.write('          </rgb>\n')
                        self.igsFile.write('        </constant>\n')
                        self.igsFile.write('      </specular_reflectivity>\n')
                        self.igsFile.write('    </phong>\n')
                        self.igsFile.write('  </material>\n')
                        return matType
        # textures?
        textures_found = 0
        textures = {}
        if mat:
            # check for textures
            for index in list(range(len(mat.texture_slots))):
                slot = mat.texture_slots[index]
                if slot:
                    texture = slot.texture
                    if slot.texture_coords == 'UV':
                        if texture.type == 'IMAGE':
                            image = texture.image
                            src = image.filepath
                            if slot.use_map_color_diffuse:
                                # diffuse texture
                                textures["diffuse"] = src
                                textures_found += 1
        if mat.use_transparency:
            # glass
            matType = "glass"
            ior = mat.raytrace_transparency.ior
            rgb = mat.diffuse_color
            maxColor = rgb[0]
            if rgb[1] > maxColor:
                maxColor = rgb[1]
            if rgb[2] > maxColor:
                maxColor = rgb[2]
            if maxColor > 0.7:
                # multiplier 5.0 works with lotus in gallery scene
                newRgb = [(1.0 - rgb[0] / maxColor) * 5.0,
                          (1.0 - rgb[1] / maxColor) * 5.0,
                          (1.0 - rgb[2] / maxColor) * 5.0]
            else:
                # multiplier 200.0 works with tapestry in gallery scene
                newRgb = [(1.0 - 0.96 * rgb[0] / maxColor) * 200.0,
                          (1.0 - 0.96 * rgb[1] / maxColor) * 200.0,
                          (1.0 - 0.96 * rgb[2] / maxColor) * 200.0]
            self.igsFile.write('  <medium>\n')
            self.uids += 1 # increment before usage
            self.igsFile.write('    <uid>%s</uid>\n' % self.uids)
            self.igsFile.write('    <name>%s_medium</name>\n' % mat.name)
            self.igsFile.write('    <basic>\n')
            self.igsFile.write('      <ior>%s</ior>\n' % ior)
            self.igsFile.write('      <cauchy_b_coeff>0.0</cauchy_b_coeff>\n')
            self.igsFile.write('      <absorption_coefficient>\n')
            self.igsFile.write('        <constant>\n')
            self.igsFile.write('          <rgb>\n')
            self.igsFile.write('            <gamma>1.0</gamma>\n')
            self.igsFile.write('            <rgb>%s %s %s</rgb>\n' %
                               (newRgb[0], newRgb[1], newRgb[2]))
            self.igsFile.write('          </rgb>\n')
            self.igsFile.write('        </constant>\n')
            self.igsFile.write('      </absorption_coefficient>\n')
            self.igsFile.write('    </basic>\n')
            self.igsFile.write('    <precedence>2</precedence>\n')
            self.igsFile.write('  </medium>\n')
            self.igsFile.write('  <material>\n')
            self.uids += 1 # increment before usage
            self.igsFile.write('    <uid>%s</uid>\n' % self.uids)
            self.igsFile.write('    <name>%s</name>\n' % mat.name)
            self.igsFile.write('    <specular>\n')
            self.igsFile.write('      <transparent>true</transparent>\n')
            self.igsFile.write('      <internal_medium_name>%s_medium'  %
                               mat.name + '</internal_medium_name>\n')
            self.igsFile.write('    </specular>\n')
            self.igsFile.write('  </material>\n')
        elif mat.emit != 0.0:
            # light emitter
            matType = "emitter"
            # layer
            self.igsFile.write('  <layer_name>\n')
            self.igsFile.write('    <layer_name>%s</layer_name>\n' % mat.name)
            self.layers += 1 # increment before usage
            self.igsFile.write('    <layer_index>%s</layer_index>\n' %
                               self.layers)
            self.igsFile.write('  </layer_name>\n')
            # material
            rgb = mat.diffuse_color
            base_emission = mat.emit * 10000 # TODO: replace '* 10000'
            self.igsFile.write('  <material>\n')
            self.uids += 1 # increment before usage
            self.igsFile.write('    <uid>%s</uid>\n' % self.uids)
            self.igsFile.write('    <name>%s</name>\n' % mat.name)
            self.igsFile.write('    <diffuse>\n')
            self.igsFile.write('      <base_emission>\n')
            self.igsFile.write('        <constant>\n')
            self.igsFile.write('          <uniform>\n')
            self.igsFile.write('            <value>%s</value>\n' %
                               base_emission)
            self.igsFile.write('          </uniform>\n')
            self.igsFile.write('        </constant>\n')
            self.igsFile.write('      </base_emission>\n')
            self.igsFile.write('      <emission>\n')
            self.igsFile.write('        <constant>\n')
            self.igsFile.write('          <rgb>\n')
            self.igsFile.write('            <gamma>1.0</gamma>\n')
            self.igsFile.write('            <rgb>%s %s %s</rgb>\n' %
                               (rgb[0], rgb[1], rgb[2]))
            self.igsFile.write('          </rgb>\n')
            self.igsFile.write('        </constant>\n')
            self.igsFile.write('      </emission>\n')
            self.igsFile.write('      <albedo>\n')
            self.igsFile.write('        <constant>\n')
            self.igsFile.write('          <rgb>\n')
            self.igsFile.write('            <gamma>1.0</gamma>\n')
            self.igsFile.write('            <rgb>%s %s %s</rgb>\n' %
                               (rgb[0], rgb[1], rgb[2]))
            self.igsFile.write('          </rgb>\n')
            self.igsFile.write('        </constant>\n')
            self.igsFile.write('      </albedo>\n')
            self.igsFile.write('      <layer>%s</layer>\n' % self.layers)
            self.igsFile.write('    </diffuse>\n')
            self.igsFile.write('  </material>\n')
        elif mat.raytrace_mirror.use:
            # phong
            matType = "phong"
            rgb = mat.diffuse_color
            rgb2 = mat.mirror_color
            self.igsFile.write('  <material>\n')
            self.uids += 1 # increment before usage
            self.igsFile.write('    <uid>%s</uid>\n' % self.uids)
            self.igsFile.write('    <name>%s</name>\n' % mat.name)
            self.igsFile.write('    <phong>\n')
            try:
                diffuse_tex = textures["diffuse"]
            except KeyError:
                pass
            else:
                paths = bpy.utils.blend_paths()
                if diffuse_tex in paths:
                    index = paths.index(diffuse_tex)
                    abs_paths = bpy.utils.blend_paths(absolute = True)
                    abs_path = abs_paths[index]
                    self.igsFile.write('      <texture>\n')
                    self.igsFile.write('        <path>%s</path>\n' % abs_path)
                    self.igsFile.write('        <a>0</a>\n')
                    self.igsFile.write('        <b>1</b>\n')
                    self.igsFile.write('        <c>0</c>\n')
                    self.igsFile.write('        <uv_set_index>0' +
                                       '</uv_set_index>\n')
                    self.igsFile.write('        <exponent>2.2</exponent>\n')
                    self.igsFile.write('        <tex_coord_generation>\n')
                    self.igsFile.write('          <uv>\n')
                    self.igsFile.write('            <matrix>1 0 0 1</matrix>\n')
                    self.igsFile.write('            <translation>0 0' +
                                       '</translation>\n')
                    self.igsFile.write('          </uv>\n')
                    self.igsFile.write('        </tex_coord_generation>\n')
                    self.igsFile.write('        <smooth>false</smooth>\n')
                    self.igsFile.write('      </texture>\n')
            self.igsFile.write('      <ior>1.5</ior>\n')
            self.igsFile.write('      <exponent>\n')
            self.igsFile.write('        <constant>1000000.0</constant>\n')
            self.igsFile.write('      </exponent>\n')
            self.igsFile.write('      <specular_reflectivity>\n')
            try:
                diffuse_tex = textures["diffuse"]
            except KeyError:
                self.igsFile.write('        <constant>\n')
                self.igsFile.write('          <rgb>\n')
                self.igsFile.write('            <gamma>1.0</gamma>\n')
                self.igsFile.write('            <rgb>%s %s %s</rgb>\n' %
                                   (rgb[0], rgb[1], rgb[2]))
                self.igsFile.write('          </rgb>\n')
                self.igsFile.write('        </constant>\n')
            else:
                self.igsFile.write('        <texture>\n')
                self.igsFile.write('          <texture_index>0' +
                                   '</texture_index>\n')
                self.igsFile.write('        </texture>\n')
            self.igsFile.write('      </specular_reflectivity>\n')
            self.igsFile.write('    </phong>\n')
            self.igsFile.write('  </material>\n')
        elif mat.specular_intensity > 0.0:
            # phong
            matType = "phong"
            rgb = mat.diffuse_color
            self.igsFile.write('  <material>\n')
            self.uids += 1 # increment before usage
            self.igsFile.write('    <uid>%s</uid>\n' % self.uids)
            self.igsFile.write('    <name>%s</name>\n' % mat.name)
            self.igsFile.write('    <phong>\n')
            try:
                diffuse_tex = textures["diffuse"]
            except KeyError:
                pass
            else:
                paths = bpy.utils.blend_paths()
                if diffuse_tex in paths:
                    index = paths.index(diffuse_tex)
                    abs_paths = bpy.utils.blend_paths(absolute = True)
                    abs_path = abs_paths[index]
                    self.igsFile.write('      <texture>\n')
                    self.igsFile.write('        <path>%s</path>\n' % abs_path)
                    self.igsFile.write('        <a>0</a>\n')
                    self.igsFile.write('        <b>1</b>\n')
                    self.igsFile.write('        <c>0</c>\n')
                    self.igsFile.write('        <uv_set_index>0' +
                                       '</uv_set_index>\n')
                    self.igsFile.write('        <exponent>2.2</exponent>\n')
                    self.igsFile.write('        <tex_coord_generation>\n')
                    self.igsFile.write('          <uv>\n')
                    self.igsFile.write('            <matrix>1 0 0 1</matrix>\n')
                    self.igsFile.write('            <translation>0 0' +
                                       '</translation>\n')
                    self.igsFile.write('          </uv>\n')
                    self.igsFile.write('        </tex_coord_generation>\n')
                    self.igsFile.write('        <smooth>false</smooth>\n')
                    self.igsFile.write('      </texture>\n')
            if mat.specular_intensity > 0.0 and mat.specular_intensity <= 0.1:
                self.igsFile.write('      <ior>%s</ior>\n' %
                                   (mat.specular_intensity * 10.0 + 1.0))
            else:
                self.igsFile.write('      <ior>1.5</ior>\n')
            self.igsFile.write('      <exponent>\n')
            if mat.roughness > 0.0 and mat.roughness <= 0.2:
                # scale (0.0, 0.2] into (10000, 10]
                scale = 1.0 - mat.roughness / 0.2 # [0.0, 1.0)
                exponent = 10000 * scale # [10, 10000)
                self.igsFile.write('        <constant>%s</constant>\n' %
                                   (exponent))
            else:
                self.igsFile.write('        <constant>10000.0</constant>\n')
            self.igsFile.write('      </exponent>\n')
            self.igsFile.write('      <diffuse_albedo>\n')
            try:
                diffuse_tex = textures["diffuse"]
            except KeyError:
                self.igsFile.write('        <constant>\n')
                self.igsFile.write('          <rgb>\n')
                self.igsFile.write('            <gamma>1.0</gamma>\n')
                self.igsFile.write('            <rgb>%s %s %s</rgb>\n' %
                                   (rgb[0], rgb[1], rgb[2]))
                self.igsFile.write('          </rgb>\n')
                self.igsFile.write('        </constant>\n')
            else:
                self.igsFile.write('        <texture>\n')
                self.igsFile.write('          <texture_index>0' +
                                   '</texture_index>\n')
                self.igsFile.write('        </texture>\n')
            self.igsFile.write('      </diffuse_albedo>\n')
            self.igsFile.write('    </phong>\n')
            self.igsFile.write('  </material>\n')
        else:
            # diffuse
            matType = "diffuse"
            rgb = mat.diffuse_color
            self.igsFile.write('  <material>\n')
            self.uids += 1 # increment before usage
            self.igsFile.write('    <uid>%s</uid>\n' % self.uids)
            self.igsFile.write('    <name>%s</name>\n' % mat.name)
            self.igsFile.write('    <diffuse>\n')
            try:
                diffuse_tex = textures["diffuse"]
            except KeyError:
                self.igsFile.write('      <albedo>\n')
                self.igsFile.write('        <constant>\n')
                self.igsFile.write('          <rgb>\n')
                self.igsFile.write('            <rgb>%s %s %s</rgb>\n' %
                                   (rgb[0], rgb[1], rgb[2]))
                self.igsFile.write('            <gamma>1.0</gamma>\n')
                self.igsFile.write('          </rgb>\n')
                self.igsFile.write('        </constant>\n')
                self.igsFile.write('      </albedo>\n')
            else:
                paths = bpy.utils.blend_paths()
                if diffuse_tex in paths:
                    index = paths.index(diffuse_tex)
                    abs_paths = bpy.utils.blend_paths(absolute = True)
                    abs_path = abs_paths[index]
                    self.igsFile.write('      <albedo>\n')
                    self.igsFile.write('        <texture>\n')
                    self.igsFile.write('          <texture_index>\n')
                    self.igsFile.write('            0\n')
                    self.igsFile.write('          </texture_index>\n')
                    self.igsFile.write('        </texture>\n')
                    self.igsFile.write('      </albedo>\n')
                    self.igsFile.write('      <texture>\n')
                    self.igsFile.write('        <path>%s</path>\n' % abs_path)
                    self.igsFile.write('        <a>0</a>\n')
                    self.igsFile.write('        <b>1</b>\n')
                    self.igsFile.write('        <c>0</c>\n')
                    self.igsFile.write('        <uv_set_index>0' +
                                       '</uv_set_index>\n')
                    self.igsFile.write('        <exponent>2.2</exponent>\n')
                    self.igsFile.write('        <tex_coord_generation>\n')
                    self.igsFile.write('          <uv>\n')
                    self.igsFile.write('            <matrix>1 0 0 1</matrix>\n')
                    self.igsFile.write('            <translation>0 0' +
                                       '</translation>\n')
                    self.igsFile.write('          </uv>\n')
                    self.igsFile.write('        </tex_coord_generation>\n')
                    self.igsFile.write('        <smooth>false</smooth>\n')
                    self.igsFile.write('      </texture>\n')
            self.igsFile.write('    </diffuse>\n')
            self.igsFile.write('  </material>\n')
        return matType

    def writeIgMesh(self, name, transform):
        # see Blendigo igmesh.py write_mesh(...)
        worked = False
        # convert to mesh
        obj = bpy.data.objects[name]
        mesh = obj.to_mesh(self.scene, True, 'RENDER')
        # stop early
        if len(mesh.tessfaces) < 1: return worked
        if len(mesh.vertices) < 1: return worked
        # filename
        filename = '%s/igmesh/%s.igmesh' % (self.directory, name)
        # open
        oFile = open(filename, 'wb')
        # magic number
        self.write_uint32(oFile, 5456751)
        # format version
        self.write_uint32(oFile, 3)
        # uv_textures
        render_uvs = [uvt for uvt in mesh.tessface_uv_textures]
        num_uv_sets = len(render_uvs)
        if num_uv_sets == 0:
            self.write_uint32(oFile, 1)
        else:
            self.write_uint32(oFile, num_uv_sets)
        # used materials
        used_mat_indices = set()
        mats = []
        num_mats = len(obj.material_slots)
        for mi in range(num_mats):
            mat = obj.material_slots[mi].material
            if mat:
                mats.append(mat)
                used_mat_indices.add(mi)
        if len(mats) == 0:
            # num used materials
            self.write_uint32(oFile, 1)
            # material name
            self.write_string(oFile, 'default_material')
        else:
            # num used materials
            self.write_uint32(oFile, len(mats))
        for mat in mats:
            # material name
            self.write_string(oFile, mat.name)
            # check if material was exported already
            try:
                matType = self.usedMaterials[mat.name]
            except KeyError:
                # if not, export it
                matType = self.writeBlenderMaterial(mat)
                self.usedMaterials[mat.name] = matType
            if matType == None:
                print("TODO: handle material '%s'" % mat.name)
        # num uv set expositions (not used anymore)
        self.write_uint32(oFile, 0)
        # count smooth faces
        num_smooth = 0
        for face in mesh.tessfaces:
            if face.use_smooth:
                num_smooth += 1
        # vertices
        vertices = []
        for v in mesh.vertices:
            vertices.append(v.co)
        self.write_list_of_vec3s(oFile, vertices)
        del vertices
        # normals
        normals = []
        if num_smooth != 0:
            for v in mesh.vertices:
                normals.append(v.normal)
        self.write_list_of_vec3s(oFile, normals)
        del normals
        # UV sets
        uv_data = []
        if num_uv_sets > 0:
            for uv_index in range(num_uv_sets):
                layer_uvs = render_uvs[uv_index]
                for face in mesh.tessfaces:
                    face_uvs = layer_uvs.data[face.index]
                    if len(face.vertices) == 3:
                        uv_data.extend([face_uvs.uv[0],
                                        face_uvs.uv[1],
                                        face_uvs.uv[2],
                                        (0, 0)])
                    else:
                        uv_data.extend([face_uvs.uv[0],
                                        face_uvs.uv[1],
                                        face_uvs.uv[2],
                                        face_uvs.uv[3]])
        else:
            uv_data.extend([(0,0)])
        # UV layout
        self.write_uint32(oFile, 1)
        # UV data
        self.write_list_of_vec2s(oFile, uv_data)
        del uv_data
        # triangles and quads
        tri_data = []
        quad_data = []
        if num_uv_sets > 0:
            for face in mesh.tessfaces:
                uv_idx = face.index * 4
                fv = face.vertices
                if len(face.vertices) == 3:
                    tri_data.extend([fv[0], fv[1], fv[2],
                                     uv_idx, uv_idx + 1, uv_idx + 2,
                                     face.material_index])
                else:
                    quad_data.extend([fv[0], fv[1], fv[2], fv[3],
                                      uv_idx,
                                      uv_idx + 1,
                                      uv_idx + 2,
                                      uv_idx + 3,
                                      face.material_index])
        else:
            for face in mesh.tessfaces:
                fv = face.vertices
                if len(face.vertices) == 3:
                    tri_data.extend([fv[0], fv[1], fv[2],
                                     0, 0, 0,
                                     face.material_index])
                else:
                    quad_data.extend([fv[0], fv[1], fv[2], fv[3],
                                      0, 0, 0, 0,
                                      face.material_index])
        num_tris = len(tri_data) // 7
        self.write_uint32(oFile, num_tris)
        tri_array = array.array('i', tri_data)
        tri_array.tofile(oFile)
        del tri_data
        num_quads = len(quad_data) // 9
        self.write_uint32(oFile, num_quads)
        quad_array = array.array('i', quad_data)
        quad_array.tofile(oFile)
        # remove temporary mesh
        bpy.data.meshes.remove(mesh)
        # close
        oFile.close()
        # igmesh/objects.igs
        m = transform
        self.objFile.write('  <model>\n')
        self.objFile.write('    <scale>1.0</scale>\n')
        self.objFile.write('    <rotation>\n')
        self.objFile.write('      <matrix>' +
                           '%s %s %s %s %s %s %s %s %s' %
                           (m[0][0], m[0][1], m[0][2],
                            m[1][0], m[1][1], m[1][2],
                            m[2][0], m[2][1], m[2][2]) +
                           '</matrix>\n')
        self.objFile.write('    </rotation>\n')
        self.objFile.write('    <mesh_name>uid%s_%s</mesh_name>\n' %
                           (self.uids + 1, name))
        self.objFile.write('    <pos>%s %s %s</pos>\n' %
                           (self.scale_length * m[0][3],
                            self.scale_length * m[1][3],
                            self.scale_length * m[2][3]))
        self.objFile.write('  </model>\n')
        # indicate that everything worked fine
        worked = True
        return worked

    def writeCamera(self, name, lens, angle, resolution, border, AA_samples,
                    tile_size, transform, isRenderCamera, sun = None):
        if isRenderCamera:
            # renderer_settings
            self.igsFile.write('  <renderer_settings>\n')
            self.uids += 1 # increment before usage
            self.igsFile.write('    <uid>%s</uid>\n' % self.uids)
            self.igsFile.write('    <watermark>false</watermark>\n')
            self.igsFile.write('    <save_tonemapped_exr>false' +
                               '</save_tonemapped_exr>\n')
            self.igsFile.write('    <hybrid>false</hybrid>\n')
            self.igsFile.write('    <glass_acceleration>true' +
                               '</glass_acceleration>\n')
            self.igsFile.write('    <splat_filter>\n')
            self.igsFile.write('      <fastbox/>\n')
            self.igsFile.write('    </splat_filter>\n')
            self.igsFile.write('    <auto_choose_num_threads>true' +
                               '</auto_choose_num_threads>\n')
            self.igsFile.write('    <vignetting>true</vignetting>\n')
            self.igsFile.write('    <post_process_diffraction>false' +
                               '</post_process_diffraction>\n')
            self.igsFile.write('    <info_overlay>false</info_overlay>\n')
            self.igsFile.write('    <width>%s</width>\n' % resolution[0])
            self.igsFile.write('    <height>%s</height>\n' % resolution[1])
            self.igsFile.write('    <aperture_diffraction>false' +
                               '</aperture_diffraction>\n')
            self.igsFile.write('    <render_foreground_alpha>false' +
                               '</render_foreground_alpha>\n')
            self.igsFile.write('    <material_id_tracer>false' +
                               '</material_id_tracer>\n')
            self.igsFile.write('    <save_igi>false</save_igi>\n')
            self.igsFile.write('    <metropolis>true</metropolis>\n')
            self.igsFile.write('    <gpu>false</gpu>\n')
            self.igsFile.write('    <image_save_period>60' +
                               '</image_save_period>\n')
            self.igsFile.write('    <bidirectional>true</bidirectional>\n')
            self.igsFile.write('    <bih_tri_threshold>1100000' +
                               '</bih_tri_threshold>\n')
            self.igsFile.write('    <logging>false</logging>\n')
            self.igsFile.write('    <super_sample_factor>2' +
                               '</super_sample_factor>\n')
            self.igsFile.write('    <halt_samples_per_pixel>-1' +
                               '</halt_samples_per_pixel>\n')
            self.igsFile.write('    <save_untonemapped_exr>false' +
                               '</save_untonemapped_exr>\n')
            self.igsFile.write('    <downsize_filter>\n')
            self.igsFile.write('      <mn_cubic>\n')
            self.igsFile.write('        <blur>1.0</blur>\n')
            self.igsFile.write('        <ring>0.0</ring>\n')
            self.igsFile.write('        <radius>1.649999976158142</radius>\n')
            self.igsFile.write('      </mn_cubic>\n')
            self.igsFile.write('    </downsize_filter>\n')
            self.igsFile.write('    <halt_time>-1</halt_time>\n')
            self.igsFile.write('  </renderer_settings>\n')
            # name
            self.igsFile.write("<!-- %s -->\n" % name)
            # camera
            camData = bpy.data.cameras[name]
            # Blendigo properties/camera.py lens_sensor_dist(...)
            aspect = resolution[0] / float(resolution[1])
            film = 0.001 * camData.sensor_width
            FOV = camData.angle
            if aspect < 1.0:
                FOV = 2 * math.degrees(math.atan(16.0 / (aspect * lens)))
                sensor_width = film * aspect
                lens_sensor_dist = 0.001 * lens
            else:
                sensor_width = 0.032
                lens_sensor_dist = film / (2.0 * math.tan(FOV / 2.0))
            fstop = 8 # default in camera.py
            aperture_radius = lens_sensor_dist / (2.0 * fstop)
            self.igsFile.write('  <camera>\n')
            self.uids += 1 # increment before usage
            self.igsFile.write('    <uid>%s</uid>\n' % self.uids)
            self.igsFile.write('    <name>uid%s_Camera</name>\n' % self.uids)
            self.igsFile.write('    <aperture_radius>' +
                               '%s</aperture_radius>\n' % aperture_radius)
            self.igsFile.write('    <focus_distance>10</focus_distance>\n')
            self.igsFile.write('    <sensor_width>%s' % sensor_width +
                               '</sensor_width>\n')
            self.igsFile.write('    <lens_sensor_dist>%s' % lens_sensor_dist +
                               '</lens_sensor_dist>\n')
            self.igsFile.write('    <exposure_duration>0.008' +
                               '</exposure_duration>\n')
            self.igsFile.write('    <lens_shift_up_distance>0' +
                               '</lens_shift_up_distance>\n')
            self.igsFile.write('    <lens_shift_right_distance>0' +
                               '</lens_shift_right_distance>\n')
            self.igsFile.write('    <autofocus/>\n')
            self.igsFile.write('    <aperture_shape>\n')
            self.igsFile.write('      <circular/>\n')
            self.igsFile.write('    </aperture_shape>\n')
            self.igsFile.write('    <keyframe>\n')
            self.igsFile.write('      <time>0</time>\n')
            ws = 1.0 # TODO: get world scale from Blender
            cam_mat = transform.transposed()
            pos = [ i*ws for i in cam_mat[3][0:3]]
            self.igsFile.write('      <pos>%s %s %s</pos>\n' %
                               (self.scale_length * pos[0],
                                self.scale_length * pos[1],
                                self.scale_length * pos[2]))
            self.igsFile.write('      <rotation_quaternion>\n')
            self.igsFile.write('        <axis>0 0 1</axis>\n')
            self.igsFile.write('        <angle>0</angle>\n')
            self.igsFile.write('      </rotation_quaternion>\n')
            self.igsFile.write('    </keyframe>\n')
            up = [ i*ws for i in cam_mat[1][0:3]]
            self.igsFile.write('    <up>%s %s %s</up>\n' %
                               (up[0], up[1], up[2]))
            forwards = [-i*ws for i in cam_mat[2][0:3]]
            self.igsFile.write('    <forwards>%s %s %s</forwards>\n' %
                               (forwards[0], forwards[1], forwards[2]))
            self.igsFile.write('    <camera_type>thin-lens-perspective' +
                               '</camera_type>\n')
            self.igsFile.write('  </camera>\n')
            # tonemapping
            self.igsFile.write('  <tonemapping>\n')
            self.uids += 1 # increment before usage
            self.igsFile.write('    <uid>%s</uid>\n' % self.uids)
            self.igsFile.write('    <name>Tone Mapping</name>\n')
            self.igsFile.write('    <reinhard>\n')
            self.igsFile.write('      <pre_scale>4</pre_scale>\n')
            self.igsFile.write('      <post_scale>1</post_scale>\n')
            self.igsFile.write('      <burn>6</burn>\n')
            self.igsFile.write('    </reinhard>\n')
            self.igsFile.write('    <linear>\n')
            self.igsFile.write('      <scale>1</scale>\n')
            self.igsFile.write('    </linear>\n')
            self.igsFile.write('  </tonemapping>\n')
            # layers
            self.igsFile.write('  <layer_name>\n')
            self.igsFile.write('    <layer_index>0</layer_index>\n')
            self.igsFile.write('    <layer_name>default</layer_name>\n')
            self.igsFile.write('  </layer_name>\n')
            self.igsFile.write('  <layer_name>\n')
            self.igsFile.write('    <layer_index>1</layer_index>\n')
            self.igsFile.write('    <layer_name>sun</layer_name>\n')
            self.igsFile.write('  </layer_name>\n')
            self.igsFile.write('  <layer_name>\n')
            self.igsFile.write('    <layer_index>2</layer_index>\n')
            self.igsFile.write('    <layer_name>sky</layer_name>\n')
            self.igsFile.write('  </layer_name>\n')
            self.layers += 2
            # default material
            self.igsFile.write('  <material>\n')
            self.igsFile.write('    <name>default_material</name>\n')
            self.uids += 1 # increment before usage
            self.igsFile.write('    <uid>%s</uid>\n' % self.uids)
            self.igsFile.write('    <diffuse>\n')
            self.igsFile.write('      <albedo>\n')
            self.igsFile.write('        <constant>\n')
            self.igsFile.write('          <rgb>\n')
            self.igsFile.write('            <rgb>\n')
            self.igsFile.write('              0.8 0.8 0.8\n')
            self.igsFile.write('            </rgb>\n')
            self.igsFile.write('            <gamma>1</gamma>\n')
            self.igsFile.write('          </rgb>\n')
            self.igsFile.write('        </constant>\n')
            self.igsFile.write('      </albedo>\n')
            self.igsFile.write('      <random_triangle_colours>false' +
                               '</random_triangle_colours>\n')
            self.igsFile.write('      <layer>0</layer>\n')
            self.igsFile.write('    </diffuse>\n')
            self.igsFile.write('  </material>\n')
        else:
            # name
            self.igsFile.write("<!-- %s -->\n" % name)
            # camera
            camData = bpy.data.cameras[name]
            # Blendigo properties/camera.py lens_sensor_dist(...)
            aspect = resolution[0] / float(resolution[1])
            film = 0.001 * camData.sensor_width
            FOV = camData.angle
            if aspect < 1.0:
                FOV = 2 * math.degrees(math.atan(16.0 / (aspect * lens)))
                sensor_width = film * aspect
                lens_sensor_dist = 0.001 * lens
            else:
                sensor_width = 0.032
                lens_sensor_dist = film / (2.0 * math.tan(FOV / 2.0))
            fstop = 8 # default in camera.py
            aperture_radius = lens_sensor_dist / (2.0 * fstop)
            self.igsFile.write('<!--  <camera>-->\n')
            self.uids += 1 # increment before usage
            self.igsFile.write('<!--    <uid>%s</uid>-->\n' % self.uids)
            self.igsFile.write('<!--    <name>uid%s_Camera</name>-->\n' %
                               self.uids)
            self.igsFile.write('<!--    <aperture_radius>' +
                               '%s</aperture_radius>-->\n' % aperture_radius)
            self.igsFile.write('<!--    <focus_distance>10')
            self.igsFile.write('</focus_distance>-->\n')
            self.igsFile.write('<!--    <sensor_width>%s' % sensor_width)
            self.igsFile.write('</sensor_width>-->\n')
            self.igsFile.write('<!--    <lens_sensor_dist>%s' %
                               lens_sensor_dist +
                               '</lens_sensor_dist>-->\n')
            self.igsFile.write('<!--    <exposure_duration>0.008')
            self.igsFile.write('</exposure_duration>-->\n')
            self.igsFile.write('<!--    <lens_shift_up_distance>0' +
                               '</lens_shift_up_distance>-->\n')
            self.igsFile.write('<!--    <lens_shift_right_distance>0' +
                               '</lens_shift_right_distance>-->\n')
            self.igsFile.write('<!--    <autofocus/>-->\n')
            self.igsFile.write('<!--    <aperture_shape>-->\n')
            self.igsFile.write('<!--      <circular/>-->\n')
            self.igsFile.write('<!--    </aperture_shape>-->\n')
            self.igsFile.write('<!--    <keyframe>-->\n')
            self.igsFile.write('<!--      <time>0</time>-->\n')
            ws = 1.0 # TODO: get world scale from Blender
            cam_mat = transform.transposed()
            pos = [ i*ws for i in cam_mat[3][0:3]]
            self.igsFile.write('<!--      <pos>%s %s %s</pos>-->\n' %
                               (self.scale_length * pos[0],
                                self.scale_length * pos[1],
                                self.scale_length * pos[2]))
            self.igsFile.write('<!--      <rotation_quaternion>-->\n')
            self.igsFile.write('<!--        <axis>0 0 1</axis>-->\n')
            self.igsFile.write('<!--        <angle>0</angle>-->\n')
            self.igsFile.write('<!--      </rotation_quaternion>-->\n')
            self.igsFile.write('<!--    </keyframe>-->\n')
            up = [ i*ws for i in cam_mat[1][0:3]]
            self.igsFile.write('<!--    <up>%s %s %s</up>-->\n' %
                               (up[0], up[1], up[2]))
            forwards = [-i*ws for i in cam_mat[2][0:3]]
            self.igsFile.write('<!--    <forwards>%s %s %s</forwards>-->\n' %
                               (forwards[0], forwards[1], forwards[2]))
            self.igsFile.write('<!--    <camera_type>thin-lens-perspective' +
                               '</camera_type>-->\n')
            self.igsFile.write('<!--  </camera>-->\n')

    def writeCone(self, name, transform, info, mat):
        self.writeMesh(name, transform, []) # ignore info

    def writeCylinder(self, name, transform, info, mat):
        self.writeMesh(name, transform, []) # ignore info

    def writeMesh(self, name, transform, info, user_matrix = None):
        try:
            smoothPolygons = info[5]
        except IndexError:
            # primitive
            normal_smoothing = True
        else:
            # if there's a single smooth polygon, turn normal_smoothing on
            normal_smoothing = False
            for smoothPolygon in smoothPolygons:
                if smoothPolygon:
                    normal_smoothing = True
                    break
        worked = self.writeIgMesh(name, transform)
        if worked:
            self.igsFile.write('  <mesh>\n')
            self.uids += 1 # increment before usage
            self.igsFile.write('    <uid>%s</uid>\n' % self.uids)
            self.igsFile.write('    <name>uid%s_%s</name>\n' % (self.uids,
                                                                name))
            self.igsFile.write('    <max_num_subdivisions>0' +
                               '</max_num_subdivisions>\n')
            self.igsFile.write('    <subdivide_pixel_threshold>0.0' +
                               '</subdivide_pixel_threshold>\n')
            self.igsFile.write('    <subdivide_curvature_threshold>0.0' +
                               '</subdivide_curvature_threshold>\n')
            self.igsFile.write('    <displacement_error_threshold>0.0' +
                               '</displacement_error_threshold>\n')
            self.igsFile.write('    <view_dependent_subdivision>false' +
                               '</view_dependent_subdivision>\n')
            self.igsFile.write('    <subdivision_smoothing>true' +
                               '</subdivision_smoothing>\n')
            if normal_smoothing:
                self.igsFile.write('    <normal_smoothing>true' +
                                   '</normal_smoothing>\n')
            else:
                self.igsFile.write('    <normal_smoothing>false' +
                                   '</normal_smoothing>\n')
            self.igsFile.write('    <merge_vertices_with_same_pos_and_normal>' +
                               'true' +
                               '</merge_vertices_with_same_pos_and_normal>\n')
            self.igsFile.write('    <wrap_u>false</wrap_u>\n')
            self.igsFile.write('    <wrap_v>false</wrap_v>\n')
            self.igsFile.write('    <view_dependent_subdivision>false' +
                               '</view_dependent_subdivision>\n')
            self.igsFile.write('    <scale>1.0</scale>\n')
            self.igsFile.write('    <external>\n')
            self.igsFile.write('      <path>igmesh/%s.igmesh</path>\n' % name)
            self.igsFile.write('    </external>\n')
            self.igsFile.write('  </mesh>\n')

    def writeRing(self, name, transform, info, mat):
        self.writeMesh(name, transform, []) # ignore info

    def writeSphere(self, name, transform, info, mat):
        self.writeMesh(name, transform, []) # ignore info

    def writeSunLight(self, name, transform, info):
        if name == 'sun' or name == 'Sun':
            # lamp data
            lamp = bpy.data.lamps[name]
            # see export_arnold_ass.py for sundir calculation
            m = transform
            sunPos, rot, scale = m.decompose()
            obj = bpy.data.objects[name]
            constraint = obj.constraints[0]
            if constraint.type == 'TRACK_TO':
                compass = constraint.target
                m = compass.matrix_world.copy()
                compassPos, rot, scale = m.decompose()
                sundir = [sunPos[0] - compassPos[0],
                          sunPos[1] - compassPos[1],
                          sunPos[2] - compassPos[2]]
                sundirLen = math.sqrt((sundir[0] * sundir[0] +
                                       sundir[1] * sundir[1] +
                                       sundir[2] * sundir[2]))
                sundir[0] = sundir[0] / sundirLen
                sundir[1] = sundir[1] / sundirLen
                sundir[2] = sundir[2] / sundirLen
            self.igsFile.write('  <background_settings>\n')
            self.uids += 1 # increment before usage
            self.igsFile.write('    <uid>%s</uid>\n' % self.uids)
            self.igsFile.write('    <name>Background Settings</name>\n')
            self.igsFile.write('    <background_material>\n')
            self.igsFile.write('      <material>\n')
            self.igsFile.write('        <name>sun</name>\n')
            self.igsFile.write('        <sunsky>\n')
            self.igsFile.write('          <sky_layer>2</sky_layer>\n')
            self.igsFile.write('          <sun_layer>1</sun_layer>\n')
            self.igsFile.write('          <sundir>%s %s %s</sundir>\n' %
                               (sundir[0], sundir[1], sundir[2]))
            turbidity = lamp.sky.atmosphere_turbidity
            self.igsFile.write('          <turbidity>%s</turbidity>\n' %
                               turbidity)
            self.igsFile.write('          <extra_atmospheric>false' +
                               '</extra_atmospheric>\n')
            if lamp.sky.use_sky or self.options.cycles:
                self.igsFile.write('          <enable_sky>true</enable_sky>\n')
            else:
                self.igsFile.write('          <enable_sky>false</enable_sky>\n')
            # TODO: self.igsFile.write('          <sun_layer>1</sun_layer>\n')
            # TODO: self.igsFile.write('          <sky_layer>2</sky_layer>\n')
            self.igsFile.write('          <model>original</model>\n')
            self.igsFile.write('        </sunsky>\n')
            self.igsFile.write('      </material>\n')
            self.igsFile.write('    </background_material>\n')
            self.igsFile.write('  </background_settings>\n')

    def finishExport(self, sun = None):
        # include all exported (mesh) objects
        self.igsFile.write('  <include>\n')
        self.igsFile.write('    <pathname>igmesh/objects.igs</pathname>\n')
        self.igsFile.write('  </include>\n')
        # end scene
        self.igsFile.write('</scene>\n')
        # close
        self.igsFile.close()
        # end scenedata
        self.objFile.write('</scenedata>\n')
        # close igmesh/objects.igs
        self.objFile.close()
