bl_info = {
    "name": "Arnold ASS format",
    "author": "Jan Walter",
    "blender": (2, 5, 8),
    "api": 35622,
    "location": "File > Import-Export",
    "description": "Export a scene in Arnold's ASS file format",
    "warning": "",
    "wiki_url": "",
    "tracker_url": "",
    "category": "Import-Export"}

if "bpy" in locals():
    import imp
    if "export_arnold_ass" in locals():
        imp.reload(export_arnold_ass)

import bpy
from bpy.props import BoolProperty, StringProperty, EnumProperty
from bpy_extras.io_utils import ExportHelper

class ExportArnoldAss(bpy.types.Operator, ExportHelper):
    bl_idname = "export_scene.ass"
    bl_label = "Export Arnold ASS"
    bl_options = {'PRESET'}
    filename_ext = ".ass"
    filter_glob = StringProperty(default = "*.ass", options = {'HIDDEN'})

    # operator properties
    opt_selection = BoolProperty(name = "Selection Only", 
                                 description = "Export selected objects only", 
                                 default = False)
    opt_global_surface_shader = EnumProperty(items = [('none', 
                                                       'none', 
                                                       'none'),
                                                      ('show_ndoteye', 
                                                       'show_ndoteye', 
                                                       'show_ndoteye'),
                                                      ('show_uv', 
                                                       'show_uv', 
                                                       'show_uv'),
                                                      ('show_u', 
                                                       'show_v', 
                                                       'show_u'),
                                                      ('show_v', 
                                                       'show_v', 
                                                       'show_v'),
                                                      ('show_bad_uvs', 
                                                       'show_bad_uvs', 
                                                       'show_bad_uvs'),
                                                      ('show_Ng', 
                                                       'show_Ng',
                                                       'show_Ng'),
                                                      ('show_Ns', 
                                                       'show_Ns',
                                                       'show_Ns'),
                                                      ('show_wire', 
                                                       'show_wire',
                                                       'show_wire'),
                                                      ('show_polywire', 
                                                       'show_polywire',
                                                       'show_polywire')],
                                             name = "Global surface",
                                             description = ("Replace surface "
                                                            + "shaders " +
                                                            "globally"),
                                             default = 'none')
    opt_use_global_illum = BoolProperty(name = "Use GI", 
                                        description = "Use global illumination",
                                        default = False)
    opt_use_instancer = BoolProperty(name = "Use instancer", 
                                     description = "Use an instancer shader",
                                     default = True)
    opt_render = BoolProperty(name = "Start renderer", 
                              description = "Start renderer after export",
                              default = True)

    def execute(self, context):
        keywords = self.as_keywords(ignore = (
                "check_existing", 
                "filter_glob"))
        from . import export_arnold_ass
        return export_arnold_ass.save(self, context, **keywords)

def menu_func(self, context):
    self.layout.operator(ExportArnoldAss.bl_idname, 
                         text = "Arnold ASS (.ass)")

def register():
    bpy.utils.register_module(__name__)
    bpy.types.INFO_MT_file_export.append(menu_func)

def unregister():
    bpy.utils.unregister_module(__name__)
    bpy.types.INFO_MT_file_export.remove(menu_func)

if __name__ == "__main__":
    register()
