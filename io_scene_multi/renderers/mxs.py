import os
import math
import platform
# common exporter interface
from ..common.interface import CommonExporterInterface
from ..common.unique import fix_name
# Blender
import bpy

class MxsExporter(CommonExporterInterface):
    def __init__(self, options):
        CommonExporterInterface.__init__(self, "MxsExporter", options)
        self.mxsFile = None
        self.mxsFilename = ""
        self.usedMaterials = {}

    def prepareExport(self, scene, bbox, directory, name, mblur, light_counter,
                      sun = None):
        self.mxsFilename = os.path.join(directory, "%s.mxs" % name)
        # scene related
        self.scene = scene
        self.bbox = bbox
        if scene.unit_settings.system != 'METRIC':
            print("WARNING: unit_settings == '%s'" % scene.unit_settings.system)
        # from the SDK docs: default units in Maxwell Render are:
        # meters, seconds, radians
        self.scale_length = scene.unit_settings.scale_length
        # light related
        self.light_counter = light_counter
        # primitives related
        self.sphere = None
        self.cylinder = None
        # open
        self.pyFilename = os.path.join(directory, name + "_mxs.py")
        print('open("%s", "w")' % self.pyFilename)
        self.mxsFile = open(self.pyFilename, "w")
        # use Python 2.7 and the mymaxwell module
        self.mxsFile.write('#!/usr/bin/python2.7\n')
        self.mxsFile.write('\n')
        self.mxsFile.write('__author__ = "jan"\n')
        self.mxsFile.write('\n')
        self.mxsFile.write('import sys\n')
        self.mxsFile.write('try:\n')
        self.mxsFile.write('    import pymaxwell\n')
        self.mxsFile.write('except ImportError:\n')
        path = "/Applications/Maxwell 3/Libs/pymaxwell/python2.7-system"
        self.mxsFile.write('    sys.path.append("%s")\n' % path)
        self.mxsFile.write('    # try again\n')
        self.mxsFile.write('    import pymaxwell\n')
        self.mxsFile.write('# scene\n')
        self.mxsFile.write('scene = pymaxwell.Cmaxwell(pymaxwell.mwcallback)\n')

    def writeBlenderMaterial(self, mat):
        matType = None
        fixedMatName = fix_name(mat.name)
        if mat.use_transparency:
            # glass
            matType = "glass"
            self.mxsFile.write('# %s\n' % fixedMatName)
            self.mxsFile.write("%s = scene.createMaterial('%s', True)\n" %
                               (fixedMatName, fixedMatName))
            self.mxsFile.write("layer = %s.addLayer()\n" % fixedMatName)
            self.mxsFile.write("layer.setEnabled(True)\n")
            self.mxsFile.write("bsdf = layer.addBSDF()\n")
            self.mxsFile.write("reflectance = bsdf.getReflectance()\n")
            self.mxsFile.write("reflectance.setActiveIorMode(0)\n")
            ior = mat.raytrace_transparency.ior
            self.mxsFile.write("reflectance.setIOR(%s, 0.0)\n" %
                               ior) # nd, abbe
            self.mxsFile.write("reflectance.setAbsorptionDistance(" +
                               "pymaxwell.DISTANCE_CENTIMETERS, 30.0)\n") # TODO
            self.mxsFile.write("color = pymaxwell.Crgb()\n")
            self.mxsFile.write("color.assign(0.0, 0.0, 0.0)\n")
            self.mxsFile.write("attr = pymaxwell.Cattribute()\n")
            self.mxsFile.write("attr.type = pymaxwell.MAP_TYPE_RGB\n")
            self.mxsFile.write("attr.activeType = pymaxwell.MAP_TYPE_RGB\n")
            self.mxsFile.write("attr.rgb.assign(color)\n")
            self.mxsFile.write("reflectance.setAttribute('color', attr)\n")
            self.mxsFile.write("reflectance.setAttribute('color.tangential', " +
                               "attr)\n")
            self.mxsFile.write("reflectance.enableForceFresnel(True)\n")
            self.mxsFile.write("color.assign(0.96, 0.96, 0.96)\n")
            self.mxsFile.write("attr.rgb.assign(color)\n")
            self.mxsFile.write("reflectance.setAttribute(" +
                               "'transmittance.color'," +
                               " attr)\n")
            self.mxsFile.write("attr.type = pymaxwell.MAP_TYPE_VALUE\n")
            self.mxsFile.write("attr.activeType = pymaxwell.MAP_TYPE_VALUE\n")
            self.mxsFile.write("attr.value = 0.0\n")
            self.mxsFile.write("bsdf.setAttribute('roughness', attr)\n")
            self.mxsFile.write("bsdf.setState(True)\n")
        elif mat.emit != 0.0:
            # light emitter
            matType = "emitter"
            self.mxsFile.write('# %s\n' % fixedMatName)
            self.mxsFile.write("%s = scene.createMaterial('%s', True)\n" %
                               (fixedMatName, fixedMatName))
            self.mxsFile.write("layer = %s.addLayer()\n" % fixedMatName)
            self.mxsFile.write("layer.setEnabled(True)\n")
            self.mxsFile.write("emitter = layer.createEmitter()\n")
            self.mxsFile.write("emitter.setLobeType(" +
                               "pymaxwell.EMISSION_LOBE_DEFAULT)\n")
            self.mxsFile.write("emitter.setActiveEmissionType(" +
                               "pymaxwell.EMISSION_TYPE_PAIR)\n")
            self.mxsFile.write("emitterPair = pymaxwell.CemitterPair()\n")
            self.mxsFile.write("emitterPair.watts = %s\n" % mat.emit)
            self.mxsFile.write("emitterPair.luminousEfficacy = 100.0\n")
            self.mxsFile.write("color = pymaxwell.Crgb()\n")
            rgb = mat.diffuse_color
            self.mxsFile.write("color.assign(%s, %s, %s)\n" %
                               (rgb[0], rgb[1], rgb[2]))
            self.mxsFile.write("emitterPair.rgb.assign(color)\n")
            self.mxsFile.write("emitter.setPair(emitterPair)\n")
            self.mxsFile.write("emitter.setState(True)\n")
        elif mat.raytrace_mirror.use:
            # reflective
            matType = "reflective"
            self.mxsFile.write('# %s\n' % fixedMatName)
            self.mxsFile.write("%s = scene.createMaterial('%s', True)\n" %
                               (fixedMatName, fixedMatName))
            self.mxsFile.write("layer = %s.addLayer()\n" % fixedMatName)
            self.mxsFile.write("layer.setEnabled(True)\n")
            self.mxsFile.write("bsdf = layer.addBSDF()\n")
            self.mxsFile.write("reflectance = bsdf.getReflectance()\n")
            self.mxsFile.write("reflectance.setActiveIorMode(0)\n")
            ior = 20.0 # TODO
            self.mxsFile.write("reflectance.setIOR(%s, 0.0)\n" %
                               ior) # nd, abbe
            self.mxsFile.write("color = pymaxwell.Crgb()\n")
            rgb = mat.diffuse_color
            self.mxsFile.write("color.assign(%s, %s, %s)\n" %
                               (rgb[0], rgb[1], rgb[2]))
            self.mxsFile.write("attr = pymaxwell.Cattribute()\n")
            self.mxsFile.write("attr.type = pymaxwell.MAP_TYPE_RGB\n")
            self.mxsFile.write("attr.activeType = pymaxwell.MAP_TYPE_RGB\n")
            self.mxsFile.write("attr.rgb.assign(color)\n")
            self.mxsFile.write("reflectance.setAttribute('color', attr)\n")
            self.mxsFile.write("reflectance.setAttribute(" +
                               "'color.tangential', attr)\n")
            self.mxsFile.write("color.assign(0.5, 0.5, 0.5)\n")
            self.mxsFile.write("attr.rgb.assign(color)\n")
            self.mxsFile.write("reflectance.setAttribute('scattering', attr)\n")
            self.mxsFile.write("attr.type = pymaxwell.MAP_TYPE_VALUE\n")
            self.mxsFile.write("attr.activeType = pymaxwell.MAP_TYPE_VALUE\n")
            roughness = 0.0
            self.mxsFile.write("attr.value = %s\n" % (100.0 * roughness))
            self.mxsFile.write("bsdf.setAttribute('roughness', attr)\n")
            self.mxsFile.write("bsdf.setState(True)\n")
        elif mat.specular_intensity > 0.0:
            # phong
            matType = "phong"
            self.mxsFile.write('# %s\n' % fixedMatName)
            self.mxsFile.write("%s = scene.createMaterial('%s', True)\n" %
                               (fixedMatName, fixedMatName))
            # layer 1
            self.mxsFile.write("layer = %s.addLayer()\n" % fixedMatName)
            self.mxsFile.write("layer.setEnabled(True)\n")
            weight = 1.0 - mat.roughness
            self.mxsFile.write("attr = pymaxwell.Cattribute()\n")
            self.mxsFile.write("attr.type = pymaxwell.MAP_TYPE_VALUE\n")
            self.mxsFile.write("attr.activeType = pymaxwell.MAP_TYPE_VALUE\n")
            self.mxsFile.write("attr.value = %s\n" % (100.0 * weight))
            self.mxsFile.write("layer.setWeight(attr)\n")
            self.mxsFile.write("bsdf = layer.addBSDF()\n")
            self.mxsFile.write("reflectance = bsdf.getReflectance()\n")
            self.mxsFile.write("reflectance.setActiveIorMode(0)\n")
            self.mxsFile.write("color = pymaxwell.Crgb()\n")
            rgb = mat.diffuse_color
            self.mxsFile.write("color.assign(%s, %s, %s)\n" %
                               (rgb[0], rgb[1], rgb[2]))
            self.mxsFile.write("attr.type = pymaxwell.MAP_TYPE_RGB\n")
            self.mxsFile.write("attr.activeType = pymaxwell.MAP_TYPE_RGB\n")
            self.mxsFile.write("attr.rgb.assign(color)\n")
            self.mxsFile.write("reflectance.setAttribute('color', attr)\n")
            self.mxsFile.write("reflectance.setAttribute(" +
                               "'color.tangential', attr)\n")
            self.mxsFile.write("color.assign(0.5, 0.5, 0.5)\n")
            self.mxsFile.write("attr.rgb.assign(color)\n")
            self.mxsFile.write("reflectance.setAttribute('scattering', attr)\n")
            self.mxsFile.write("attr.type = pymaxwell.MAP_TYPE_VALUE\n")
            self.mxsFile.write("attr.activeType = pymaxwell.MAP_TYPE_VALUE\n")
            roughness = 1.0
            self.mxsFile.write("attr.value = %s\n" % (100.0 * roughness))
            self.mxsFile.write("bsdf.setAttribute('roughness', attr)\n")
            self.mxsFile.write("bsdf.setState(True)\n")
            # layer 2
            self.mxsFile.write("layer2 = %s.addLayer()\n" % fixedMatName)
            self.mxsFile.write("layer2.setEnabled(True)\n")
            self.mxsFile.write("bsdf2 = layer2.addBSDF()\n")
            self.mxsFile.write("reflectance2 = bsdf2.getReflectance()\n")
            self.mxsFile.write("reflectance2.setActiveIorMode(0)\n")
            self.mxsFile.write("color = pymaxwell.Crgb()\n")
            rgb = [1.0, 1.0, 1.0]
            self.mxsFile.write("color.assign(%s, %s, %s)\n" %
                               (rgb[0], rgb[1], rgb[2]))
            self.mxsFile.write("attr = pymaxwell.Cattribute()\n")
            self.mxsFile.write("attr.type = pymaxwell.MAP_TYPE_RGB\n")
            self.mxsFile.write("attr.activeType = pymaxwell.MAP_TYPE_RGB\n")
            self.mxsFile.write("attr.rgb.assign(color)\n")
            self.mxsFile.write("reflectance2.setAttribute('color', attr)\n")
            self.mxsFile.write("reflectance2.setAttribute(" +
                               "'color.tangential', attr)\n")
            self.mxsFile.write("color.assign(0.5, 0.5, 0.5)\n")
            self.mxsFile.write("attr.rgb.assign(color)\n")
            self.mxsFile.write("reflectance2.setAttribute(" +
                               "'scattering', attr)\n")
            self.mxsFile.write("attr.type = pymaxwell.MAP_TYPE_VALUE\n")
            self.mxsFile.write("attr.activeType = pymaxwell.MAP_TYPE_VALUE\n")
            roughness = mat.roughness
            self.mxsFile.write("attr.value = %s\n" % (100.0 * roughness))
            self.mxsFile.write("bsdf2.setAttribute('roughness', attr)\n")
            self.mxsFile.write("bsdf2.setState(True)\n")
        else:
            # diffuse
            matType = "diffuse"
            self.mxsFile.write('# %s\n' % fixedMatName)
            self.mxsFile.write("%s = scene.createMaterial('%s', True)\n" %
                               (fixedMatName, fixedMatName))
            self.mxsFile.write("layer = %s.addLayer()\n" % fixedMatName)
            self.mxsFile.write("layer.setEnabled(True)\n")
            self.mxsFile.write("bsdf = layer.addBSDF()\n")
            self.mxsFile.write("reflectance = bsdf.getReflectance()\n")
            self.mxsFile.write("reflectance.setActiveIorMode(0)\n")
            self.mxsFile.write("color = pymaxwell.Crgb()\n")
            rgb = mat.diffuse_color
            self.mxsFile.write("color.assign(%s, %s, %s)\n" %
                               (rgb[0], rgb[1], rgb[2]))
            self.mxsFile.write("attr = pymaxwell.Cattribute()\n")
            self.mxsFile.write("attr.type = pymaxwell.MAP_TYPE_RGB\n")
            self.mxsFile.write("attr.activeType = pymaxwell.MAP_TYPE_RGB\n")
            self.mxsFile.write("attr.rgb.assign(color)\n")
            self.mxsFile.write("reflectance.setAttribute('color', attr)\n")
            self.mxsFile.write("reflectance.setAttribute(" +
                               "'color.tangential', attr)\n")
            self.mxsFile.write("color.assign(0.0, 0.0, 0.0)\n")
            self.mxsFile.write("attr.rgb.assign(color)\n")
            self.mxsFile.write("reflectance.setAttribute('scattering', attr)\n")
            self.mxsFile.write("bsdf.setState(True)\n")
        return matType

    def writeCamera(self, name, lens, angle, resolution, border, AA_samples,
                    tile_size, transform, isRenderCamera, sun = None):
        self.mxsFile.write('# camera\n')
        self.mxsFile.write('pName = "%s"\n' % name)
        self.mxsFile.write('nSteps = 1\n')
        self.mxsFile.write('shutter = 1/250.0\n')
        aspect = resolution[0] / float(resolution[1])
        if aspect >= 1.0:
            focal = lens
        else:
            focal = lens / aspect
        aperture = 2 * 16.0
        self.mxsFile.write('filmWidth = %s\n' % (aperture / 1000.0))
        self.mxsFile.write('filmHeight = %s\n' % (aperture / (1000.0 * aspect)))
        self.mxsFile.write('iso = 400\n')
        self.mxsFile.write('pDiaphragmType = "CIRCULAR"\n')
        self.mxsFile.write('angle = 0.0\n')
        self.mxsFile.write('nBlades = 0\n')
        self.mxsFile.write('fps = 25\n')
        self.mxsFile.write('xRes = %s\n' % resolution[0])
        self.mxsFile.write('yRes = %s\n' % resolution[1])
        self.mxsFile.write('pixelAspect = %s\n' % 1.0)
        self.mxsFile.write('camera = scene.addCamera(pName,\n')
        self.mxsFile.write('                         nSteps,\n')
        self.mxsFile.write('                         shutter,\n')
        self.mxsFile.write('                         filmWidth,\n')
        self.mxsFile.write('                         filmHeight,\n')
        self.mxsFile.write('                         iso,\n')
        self.mxsFile.write('                         pDiaphragmType,\n')
        self.mxsFile.write('                         angle,\n')
        self.mxsFile.write('                         nBlades,\n')
        self.mxsFile.write('                         fps,\n')
        self.mxsFile.write('                         xRes,\n')
        self.mxsFile.write('                         yRes,\n')
        self.mxsFile.write('                         pixelAspect)\n')
        self.mxsFile.write('iStep = 0\n')
        # LookAt (see luxblend/properties/camera.py)
        m = transform
        matrix = m.transposed()
        pos = matrix[3]
        forwards = -matrix[2]
        target = (pos + forwards)
        up = matrix[1]
        self.mxsFile.write('origin = pymaxwell.Cvector(%s, %s, %s)\n' %
                           (self.scale_length * pos[0],
                            self.scale_length * pos[2],
                            self.scale_length * (-pos[1])))
        self.mxsFile.write('focalPoint = pymaxwell.Cvector(%s, %s, %s)\n' %
                           (self.scale_length * target[0],
                            self.scale_length * target[2],
                            self.scale_length * (-target[1])))
        self.mxsFile.write('up = pymaxwell.Cvector(%s, %s, %s)\n' %
                           (self.scale_length * up[0],
                            self.scale_length * up[2],
                            self.scale_length * (-up[1])))
        self.mxsFile.write('focalLength = %s\n' % (lens / 1000.0)) # 35mm
        self.mxsFile.write('fStop = 8.0\n')
        self.mxsFile.write('focalLengthNeedCorrection = True\n')
        self.mxsFile.write('camera.setStep(iStep,\n')
        self.mxsFile.write('               origin,\n')
        self.mxsFile.write('               focalPoint,\n')
        self.mxsFile.write('               up,\n')
        self.mxsFile.write('               focalLength,\n')
        self.mxsFile.write('               fStop,\n')
        self.mxsFile.write('               %s)\n' %
                           "focalLengthNeedCorrection")
        self.mxsFile.write('camera.setActive()\n')

    def writeCone(self, name, transform, info, mat):
        self.writeMesh(name, transform, info)

    def writeCylinder(self, name, transform, info, mat):
        self.writeMesh(name, transform, info)

    def writeMesh(self, name, transform, info, user_matrix = None):
        # info
        materials = info[0]
        vertices = info[1]
        vertexNormals = info[2]
        polygons = info[3]
        polygonNormals = info[4]
        smoothPolygons = info[5]
        polygonMaterialIndices = info[6]
        hasUVs, uvs = info[7]
        # sort by materials
        infoMaterial = {}
        for matIndex in range(len(materials)):
            matName = materials[matIndex]
            # store for each material a list with len(vertices) 0 entries
            matIndixes = [0] * len(vertices)
            infoMaterial[matIndex] = matIndixes
        for pi in range(len(polygonMaterialIndices)):
            pmi = polygonMaterialIndices[pi]
            polygon = polygons[pi]
            for vi in polygon:
                # count how often a vertex is used per material
                infoMaterial[pmi][vi] += 1
        for matIndex in range(len(materials)):
            counter = 0
            for index in range(len(infoMaterial[matIndex])):
                vi = infoMaterial[matIndex][index]
                # replace the counter by ...
                if vi == 0:
                    # ... -1 if the material doesn't use this vertex
                    infoMaterial[matIndex][index] = -1
                else:
                    # ... or by an increasing index
                    infoMaterial[matIndex][index] = counter
                    counter += 1
        # for each material ...
        for matIndex in range(len(materials)):
            matName = materials[matIndex]
            fixedMatName = fix_name(matName)
            mat = bpy.data.materials[matName]
            # name
            if len(materials) > 1:
                self.mxsFile.write("# %s [%s]\n" % (name, fixedMatName))
                self.mxsFile.write("pName = '%s_%s'\n" % (name, fixedMatName))
            else:
                self.mxsFile.write("# %s\n" % name)
                self.mxsFile.write("pName = '%s'\n" % name)
            # if there's a single smooth polygon, we export normals
            useVertexNormals = False
            for pi in range(len(polygonMaterialIndices)):
                pmi = polygonMaterialIndices[pi]
                if pmi == matIndex:
                    smoothPolygon = smoothPolygons[pi]
                    if smoothPolygon:
                        useVertexNormals = True
                        break
            # number of vertices per face
            nVertexes = 0
            nTriangles = 0
            nNormals = 0
            for pi in range(len(polygonMaterialIndices)):
                pmi = polygonMaterialIndices[pi]
                if pmi == matIndex:
                    polygon = polygons[pi]
                    numVerts = len(polygon)
                    if numVerts == 3:
                        nVertexes += numVerts
                        nTriangles += 1
                        nNormals += 1
                    elif numVerts == 4:
                        nVertexes += numVerts
                        nTriangles += 2
                        nNormals += 1
                    else:
                        print("ERROR: [MxsExporter] triangles/quads expected")
            # nVertexes
            self.mxsFile.write("nVertexes = %s\n" % nVertexes)
            # nTriangles
            self.mxsFile.write("nTriangles = %s\n" % nTriangles)
            # nNormals
            if useVertexNormals:
                nNormals = nVertexes
            self.mxsFile.write("nNormals = %s\n" % nNormals)
            # nNositionsPerVertexes
            self.mxsFile.write("nPositionsPerVertexes = 1\n") # no motion blur
            # mesh
            self.mxsFile.write("mesh = scene.createMesh(pName,\n")
            self.mxsFile.write("                        nVertexes,\n")
            self.mxsFile.write("                        nNormals,\n")
            self.mxsFile.write("                        nTriangles,\n")
            self.mxsFile.write("                        " +
                               "nPositionsPerVertexes)\n")
            # transformation
            pos, rot, scale = transform.decompose()
            euler = rot.to_euler('XZY') # rotation order !!!
            self.mxsFile.write("base, pivot, ok = mesh.getBaseAndPivot()\n")
            self.mxsFile.write("mesh.setBaseAndPivot(base, pivot)\n")
            self.mxsFile.write("mesh.setPosition(pymaxwell.Cvector(" +
                               "%s, %s, %s))\n" %
                               (self.scale_length * pos[0],
                                self.scale_length * pos[2],
                                self.scale_length * (-pos[1])))
            self.mxsFile.write("mesh.setPivotPosition(pymaxwell.Cvector(" +
                               "%s, %s, %s))\n" %
                               (self.scale_length * pos[0],
                                self.scale_length * pos[2],
                                self.scale_length * (-pos[1])))
            self.mxsFile.write("mesh.setRotation(pymaxwell.Cvector(" +
                               "%s, %s, %s))\n" %
                               (math.degrees(euler.x),
                                math.degrees(euler.z),
                                -math.degrees(euler.y)))
            self.mxsFile.write("mesh.setPivotRotation(pymaxwell.Cvector(" +
                               "%s, %s, %s))\n" %
                               (math.degrees(euler.x),
                                math.degrees(euler.z),
                                -math.degrees(euler.y)))
            self.mxsFile.write("mesh.setScale(pymaxwell.Cvector(" +
                               "%s, %s, %s))\n" %
                               (scale[0],
                                scale[2],
                                scale[1]))
            # vertices
            vertexCounter = 0
            for pi in range(len(polygonMaterialIndices)):
                pmi = polygonMaterialIndices[pi]
                if pmi == matIndex:
                    polygon = polygons[pi]
                    numVerts = len(polygon)
                    if numVerts in [3, 4]:
                        for vi in polygon:
                            v = vertices[vi] # use original index
                            x = self.scale_length * v[0]
                            y = self.scale_length * v[2]
                            z = self.scale_length * -v[1]
                            self.mxsFile.write('mesh.setVertex(' +
                                               '%s, 0, ' % vertexCounter +
                                               'pymaxwell.Cvector' +
                                               '(%s, %s, %s))\n' %
                                               (x, y, z))
                            vertexCounter += 1
            # normals (based on smoothPolygons)
            normalCounter = 0
            for pi in range(len(polygonMaterialIndices)):
                pmi = polygonMaterialIndices[pi]
                smoothPolygon = smoothPolygons[pi]
                if pmi == matIndex:
                    if not smoothPolygon:
                        # use face normal
                        normal = polygonNormals[pi]
                    polygon = polygons[pi]
                    if not smoothPolygon:
                        n = normal # face normal
                        self.mxsFile.write("mesh.setNormal(" +
                                           "%s, 0, pymaxwell.Cvector(" %
                                           normalCounter +
                                           "%s, %s, %s))\n" %
                                           (n[0], n[2], -n[1]))
                        normalCounter += 1
                    else:
                        for vi in polygon:
                            # use vertex normal
                            n = vertexNormals[vi]
                            self.mxsFile.write("mesh.setNormal(" +
                                               "%s, 0, pymaxwell.Cvector(" %
                                               normalCounter +
                                               "%s, %s, %s))\n" %
                                               (n[0], n[2], -n[1]))
                            normalCounter += 1
            # triangle
            triangleCounter = 0
            normalCounter = 0
            vertexCounter = 0
            # channel ID for later (uv-coordinates)
            if hasUVs:
                self.mxsFile.write('iChannelID, ok = mesh.addChannelUVW()\n')
            for pi in range(len(polygonMaterialIndices)):
                pmi = polygonMaterialIndices[pi]
                if pmi == matIndex:
                    polygon = polygons[pi]
                    if len(polygon) == 3:
                        if useVertexNormals:
                            ni1 = vertexCounter + 0
                            ni2 = vertexCounter + 1
                            ni3 = vertexCounter + 2
                        else:
                            ni1 = normalCounter
                            ni2 = normalCounter
                            ni3 = normalCounter
                        self.mxsFile.write('mesh.setTriangle(%s,\n' %
                                           triangleCounter +
                                           '                 %s, %s, %s,\n' %
                                           (vertexCounter + 0,
                                            vertexCounter + 1,
                                            vertexCounter + 2) +
                                           '                 %s, %s, %s)\n' %
                                           (ni1, ni2, ni3))
                        triangleCounter += 1
                        normalCounter += 1
                        vertexCounter += 3
                        if hasUVs:
                            uvPoly = uvs[pi]
                            self.mxsFile.write('mesh.setTriangleUVW(%s,\n' %
                                               (triangleCounter-1) +
                                               '                    ' +
                                               'iChannelID,\n' +
                                               '                    ' +
                                               '%s, %s, %s,\n' %
                                               (uvPoly[0][0], 1.0 -
                                                uvPoly[0][1], 0.0) +
                                               '                    ' +
                                               '%s, %s, %s,\n' %
                                               (uvPoly[1][0], 1.0 -
                                                uvPoly[1][1], 0.0) +
                                               '                    ' +
                                               '%s, %s, %s)\n' %
                                               (uvPoly[2][0], 1.0 -
                                                uvPoly[2][1], 0.0))
                    elif len(polygon) == 4:
                        if useVertexNormals:
                            ni1 = vertexCounter + 0
                            ni2 = vertexCounter + 1
                            ni3 = vertexCounter + 2
                        else:
                            ni1 = normalCounter
                            ni2 = normalCounter
                            ni3 = normalCounter
                        self.mxsFile.write('mesh.setTriangle(%s,\n' %
                        triangleCounter +
                                           '                 %s, %s, %s,\n' %
                                           (vertexCounter + 0,
                                            vertexCounter + 1,
                                            vertexCounter + 2) +
                                           '                 %s, %s, %s)\n' %
                                           (ni1, ni2, ni3))
                        triangleCounter += 1
                        if useVertexNormals:
                            ni1 = vertexCounter + 0
                            ni2 = vertexCounter + 2
                            ni3 = vertexCounter + 3
                        self.mxsFile.write('mesh.setTriangle(%s,\n' %
                                           triangleCounter +
                                           '                 %s, %s, %s,\n' %
                                           (vertexCounter + 0,
                                            vertexCounter + 2,
                                            vertexCounter + 3) +
                                           '                 %s, %s, %s)\n' %
                                           (ni1, ni2, ni3))
                        triangleCounter += 1
                        normalCounter += 1
                        vertexCounter += 4
                        if hasUVs:
                            uvPoly = uvs[pi]
                            self.mxsFile.write('mesh.setTriangleUVW(%s,\n' %
                                               (triangleCounter-2) +
                                               '                    ' +
                                               'iChannelID,\n' +
                                               '                    ' +
                                               '%s, %s, %s,\n' %
                                               (uvPoly[0][0], 1.0 -
                                                uvPoly[0][1], 0.0) +
                                               '                    ' +
                                               '%s, %s, %s,\n' %
                                               (uvPoly[1][0], 1.0 -
                                                uvPoly[1][1], 0.0) +
                                               '                    ' +
                                               '%s, %s, %s)\n' %
                                               (uvPoly[2][0], 1.0 -
                                                uvPoly[2][1], 0.0))
                            self.mxsFile.write('mesh.setTriangleUVW(%s,\n' %
                                               (triangleCounter-1) +
                                               '                    ' +
                                               'iChannelID,\n' +
                                               '                    ' +
                                               '%s, %s, %s,\n' %
                                               (uvPoly[0][0], 1.0 -
                                                uvPoly[0][1], 0.0) +
                                               '                    ' +
                                               '%s, %s, %s,\n' %
                                               (uvPoly[2][0], 1.0 -
                                                uvPoly[2][1], 0.0) +
                                               '                    ' +
                                               '%s, %s, %s)\n' %
                                               (uvPoly[3][0], 1.0 -
                                                uvPoly[3][1], 0.0))
                    else:
                        print("ERROR: [MxsExporter] triangles/quads expected")
            # material
            try:
                matType = self.usedMaterials[matName]
            except KeyError:
                matType = self.writeBlenderMaterial(mat)
                self.usedMaterials[matName] = matType
            if matType != None:
                self.mxsFile.write("mesh.setMaterial(%s)\n" % fixedMatName)

    def writeRing(self, name, transform, info, mat):
        self.writeMesh(name, transform, info)

    def writeSphere(self, name, transform, info, mat):
        fixedMatName = fix_name(mat.name)
        # name
        self.mxsFile.write("# %s\n" % name)
        if self.sphere == None:
            path = ("/Applications/Maxwell 3/library/objects/primitives/" +
                    "Sphere.mxs")
            self.mxsFile.write('sphereScene = ' +
                               'pymaxwell.Cmaxwell(pymaxwell.mwcallback)\n')
            self.mxsFile.write('sphereMXS = sphereScene.readMXS("%s")\n' %
                               path)
            self.mxsFile.write("sphere = sphereScene.getObject('Sphere')\n")
            self.sphere = 1
        else:
            self.sphere += 1
        self.mxsFile.write("instance = scene.addObject(sphere)\n")
        self.mxsFile.write('instance.setName("%s")\n' % name)
        # transformation
        pos, rot, scale = transform.decompose()
        euler = rot.to_euler()
        self.mxsFile.write("base, pivot, ok = instance.getBaseAndPivot()\n")
        self.mxsFile.write("instance.setBaseAndPivot(base, pivot)\n")
        self.mxsFile.write("instance.setPosition(pymaxwell.Cvector(" +
                           "%s, %s, %s))\n" %
                           (self.scale_length * pos[0],
                            self.scale_length * pos[2],
                            self.scale_length * (-pos[1])))
        self.mxsFile.write("instance.setPivotPosition(pymaxwell.Cvector(" +
                           "%s, %s, %s))\n" %
                           (self.scale_length * pos[0],
                            self.scale_length * pos[2],
                            self.scale_length * (-pos[1])))
        self.mxsFile.write("instance.setRotation(pymaxwell.Cvector(" +
                           "%s, %s, %s))\n" %
                           (math.degrees(euler.x),
                            math.degrees(euler.z),
                            -math.degrees(euler.y)))
        self.mxsFile.write("instance.setPivotRotation(pymaxwell.Cvector(" +
                           "%s, %s, %s))\n" %
                           (math.degrees(euler.x),
                            math.degrees(euler.z),
                            -math.degrees(euler.y)))
        self.mxsFile.write("instance.setScale(pymaxwell.Cvector(" +
                           "%s, %s, %s))\n" %
                           (2*self.scale_length*scale[0],
                            2*self.scale_length*scale[2],
                            2*self.scale_length*scale[1]))
        # material
        try:
            matType = self.usedMaterials[fixedMatName]
        except KeyError:
            matType = self.writeBlenderMaterial(mat)
            self.usedMaterials[fixedMatName] = matType
        if matType != None:
            self.mxsFile.write("instance.setMaterial(%s)\n" % fixedMatName)

    def writeSunLight(self, name, transform, info):
        # light_counter
        self.light_counter += 1
        # info
        color = info[0]
        energy = info[1]
        falloff = info[2]
        use_diffuse = info[3][0]
        use_specular = info[3][1]
        # lamp data
        lamp = bpy.data.lamps[name]
        # see export_arnold_ass.py for sundir calculation
        m = transform
        sunPos, rot, scale = m.decompose()
        obj = bpy.data.objects[name]
        constraint = obj.constraints[0]
        if constraint.type == 'TRACK_TO':
            compass = constraint.target
            m = compass.matrix_world.copy()
            compassPos, rot, scale = m.decompose()
            sundir = [sunPos[0] - compassPos[0],
                      sunPos[1] - compassPos[1],
                      sunPos[2] - compassPos[2]]
            sundirLen = math.sqrt((sundir[0] * sundir[0] +
                                   sundir[1] * sundir[1] +
                                   sundir[2] * sundir[2]))
            sundir[0] = sundir[0] / sundirLen
            sundir[1] = sundir[1] / sundirLen
            sundir[2] = sundir[2] / sundirLen
        self.mxsFile.write('# sun\n')
        self.mxsFile.write("environment = scene.getEnvironment()\n")
        self.mxsFile.write("ok = environment.setActiveSky('PHYSICAL')\n")
        self.mxsFile.write("ok = environment.setSunDirection(" +
                           "pymaxwell.Cvector(%s, %s, %s))\n" %
                           (sundir[0], sundir[2], -sundir[1]))
        self.mxsFile.write("ok = environment.setSunPositionType(2)\n")
        self.mxsFile.write("sunColor = pymaxwell.Crgb()\n")
        self.mxsFile.write("sunColor.r = 1.0\n")
        self.mxsFile.write("sunColor.g = 1.0\n")
        self.mxsFile.write("sunColor.b = 1.0\n")
        self.mxsFile.write("ok = environment.setSunProperties(" +
                           "pymaxwell.SUN_PHYSICAL,\n")
        self.mxsFile.write("                                  5777,\n")
        self.mxsFile.write("                                  1.0,\n")
        self.mxsFile.write("                                  1.0,\n")
        self.mxsFile.write("                                  sunColor)\n")

    def finishExport(self, sun = None):
        self.mxsFile.write('scene.writeMXS("%s")\n' % self.mxsFilename)
        # close
        self.mxsFile.close()
        # run Python to create MXS file
        if platform.system() == "Darwin":
            # TODO: currently only for OSX
            pythonVer = "/usr/bin/python2.7"
            command = "%s %s" % (pythonVer, self.pyFilename)
            print(command)
            os.system(command)
