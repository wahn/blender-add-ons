/* about __category, __nondiffuse, and __nonspecular see link below */
/* file:///usr/people/jan/bin/3delight-10.0.50/Linux-x86_64/doc/html/3delight_34.html */

light
rib_blender_sunlight(float intensity = 1;
                     color lightcolor = 1; 
                     point from = point "shader" (0,0,0);
                     point to = point "shader" (0,0,1);
                     float __nondiffuse = 0;
                     float __nonspecular = 0;
                     string __category = "")
{
	solar(to - from, 0.0) {
		Cl = intensity * lightcolor;
  }
}
